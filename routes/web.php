<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/activity', function () {
    return \Spatie\Activitylog\Models\Activity::all()->last();
});


Route::get('/', function (Request $request) {
    if ($request->get('id')) {
        $key = $request->get('id');
        /*print_r($request->get('u_id'));exit;*/

        return view('welcome')->withRefKey($key);
    }
    $key = '';
    return view('welcome')->withRefKey($key);
});

Route::get('/run_migrations', function () {
    return Artisan::call('migrate', ["--force" => true]);
});
Route::get('/permission_cache', function () {
    return Artisan::call('cache:forget spatie.permission.cache');
});

//Clear route cache:
Route::get('/route-cache', function () {
    $exitCode = Artisan::call('route:cache');
    return 'Routes cache cleared';
});

//Clear config cache:
Route::get('/config-cache', function () {
    $exitCode = Artisan::call('config:cache');
    return 'Config cache cleared';
});

// Clear application cache:
Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('cache:clear');
    return 'Application cache cleared';
});
Auth::routes();

//Route::get('/hedge' , '');

Route::get('/user/hedge', 'HedgeController@userIndex')->name('user.hedge_index');
Route::get('/user/hedge/profit', 'HedgeController@profitIndex')->name('user.hedge.profit');
Route::get('/user/hedge/deposit', 'HedgeController@depositList')->name('user.hedge.deposit');
Route::get('/user/hedge/withdraw', 'HedgeController@withdrawIndex')->name('user.hedge.withdraw');
Route::get('/user/hedge/myhedge', 'HedgeController@myHedgeIndex')->name('user.hedge.myhegeindex');
Route::post('/user/hedge/bookingAjax', 'HedgeController@bookingAjax')->name('user.hedge.bookingajax');
Route::post('/user/hedge/hedgeBookingBtcAjax', 'HedgeController@hedgeBookingBtcAjax')->name('user.hedge.hedgeBookingBtcAjax');
//Route::post('/user/withdrawAjax', 'UserController@withhDrawAjax')->name('withdraw.request');



// Testing route for Btc Ajax
Route::post('/btcAjax', 'UserController@btcAjax')->name('user.ajax.btc');
Route::post('/cashAjax', 'UserController@btcAjaxCash')->name('user.ajax.cash');

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/home', 'HomeController@index')->name('home');
Route::get('/commands', 'HomeController@commands')->name('commands');

// ROUTES FOR CUSTOMER
Route::get('/crypto', 'UserController@crypto')->name('crypto.crypto');
Route::post('/crypto', 'UserController@crypto')->name('crypto.crypto');
Route::get('/home/callback', 'HomeController@callBack')->name('home.crypto.callBack');


//-----Transaction History---------

Route::get('/user/history', 'UserController@transIndex')->name('trans.index');

//-----Deposit---------
Route::get('/user/deposit', 'UserController@depositeIndex')->name('deposite.index');
Route::get('/user/deposit/list', 'UserController@depositeList')->name('deposite.list');
Route::post('/user/deposit', 'UserController@depositeStore')->name('deposite.store');
Route::post('/user/deposit/cash', 'UserController@depositeCash')->name('deposite.cash');

//-----WithDraw---------
Route::get('/user/withdraw/list', 'UserController@withhDrawIndex')->name('withdraw.index');
Route::post('/user/withdrawAjax', 'UserController@withhDrawAjax')->name('withdraw.request');


//-----Profit---------
Route::get('/user/profit/list', 'UserController@profitIndex')->name('profit.index');
Route::post('/user/profitAjax', 'UserController@userProfitAjax')->name('profit.request');

//--- Contact Us----------
Route::post('/user/contact', 'UserController@contact')->name('contact.request');

//-----Network---------
Route::get('/user/network/', 'UserController@network')->name('user.netowrk.index');
Route::post('/user/network/', 'UserController@networkGet')->name('netowrk.get');


Route::post('/user/notifi/', 'UserController@markRead')->name('notifi.read');

Route::get('/about-us', 'UserController@about')->name('about');
Route::get('/launchnotice', 'UserController@launchnotice')->name('launchnotice');
Route::get('/userlaunchnotice', 'UserController@userlaunchnotice')->name('userlaunchnotice');
Route::get('/affiliate-program', 'UserController@affi')->name('aff');

//-----Gift---------
Route::get('/user/gift/list', 'UserController@giftList')->name('user.gift.list');
Route::get('/user/gift/add', 'UserController@giftAdd')->name('user.gift.add');
Route::post('/user/gift/add', 'UserController@giftAdd')->name('user.gift.add');

Route::post('/user/gift/store', 'UserController@giftStore')->name('user.gift.store');


//-----Gift Approval Ajx---------
Route::post('/user/gift/ajax', 'UserController@giftAjax')->name('user.gift.ajax');

//----- SOS ---------
Route::get('/user/sos', 'UserController@sos')->name('user.sos.add');
Route::post('/user/sos/store', 'UserController@sosStore')->name('user.sos.store');

Route::get('/user/sos/list', 'UserController@soslist')->name('user.sos.list');
Route::get('/user/sos/detail/{id}', 'UserController@sosDetail')->name('user.sos.detail');
Route::post('/user/sos/detailStore/', 'UserController@sosDetailStore')->name('user.sos.detailStore');


Route::get('/email-verify', 'UserController@email')->name('user.email');
/*Route::post('/email-verifyStore', 'EmailController@email')->name('user.email.store');*/
Route::post('/email-verifyStore', 'UserController@emailStore')->name('user.email.store');

//----- User Setting---------
Route::get('/user/setting', 'UserController@setting')->name('user.setting');
Route::post('/user/setting/store', 'UserController@settingStore')->name('user.setting.store');


//----- News Letter ---------
Route::post('/user/newsletter', 'UserController@newsletter')->name('user.newsletter');


//admin

Route::group(['middleware' => 'admin'],
    function () {

//-----Pending---------
        Route::group(['middleware' => 'permission:deposits'], function () {

            Route::get('/admin/pending-request', 'AdminController@pendingIndex')->name('admin.pending.index');
            Route::post('/admin/pending-request', 'AdminController@pendingAjax')->name('admin.pending.ajax');

        });


//-----Approved---------
        Route::get('/admin/approved', 'AdminController@approvedIndex')->name('admin.approved.index');
        Route::post('/admin/approved', 'AdminController@approvedAjax')->name('admin.approved.ajax');


        Route::post('/admin/profit/accept', 'AdminController@profitAjax')->name('admin.profit.approved');


//-----withdraw---------
        Route::Group(['middleware' => 'permission:withdraw'], function () {

            Route::get('/admin/withdraw', 'AdminController@withdrawIndex')->name('admin.withdraw.index');
            Route::post('/admin/withdraw', 'AdminController@withdrawAjax')->name('admin.withdraw.ajax');

        });


//-----profit---------
        Route::get('/admin/profit', 'AdminController@profitReq')->name('admin.profit.index');
        Route::post('/admin/profit', 'AdminController@profitReqAjax')->name('admin.profit.ajax');


//-----Weekly set---------
        Route::Group(['middleware' => 'permission:set daily per'], function () {
            Route::get('/admin/weekly', 'AdminController@weeklyIndex')->name('admin.weekly.index');
            Route::post('/admin/weekly/store', 'AdminController@weeklyStore')->name('admin.weekly.store');

        });


//-----Admin Coin---------
        Route::get('/admin/coin', 'AdminController@coinIndex')->name('admin.coin.index');
        Route::post('/admin/coin/store', 'AdminController@coinStore')->name('admin.coin.store');


//-----Admin notification---------
        Route::Group(['middleware' => 'permission:send notif'], function () {

            Route::get('/admin/notification', 'AdminController@notificationIndex')->name('admin.notification.index');
            Route::post('/admin/notification/store', 'AdminController@notificationStore')->name('admin.notification.store');

        });
//-----Contact Detail---------
        Route::Group(['middleware' => 'permission:contact list'], function () {

            Route::get('/admin/contact', 'AdminController@contactUsers')->name('admin.contact.index');
            Route::get('/admin/contact/detail/{id}', 'AdminController@sosDetail')->name('admin.sos.detail');
            Route::post('/admin/contact/detailStore/', 'AdminController@sosDetailStore')->name('admin.sos.detailStore');
            Route::post('/admin/contact/status/', 'AdminController@contactStatus')->name('admin.sos.status');

        });

//-----Gift---------
        Route::Group(['middleware' => 'permission:gift code'], function () {

            Route::get('/admin/gift/list', 'AdminController@giftList')->name('admin.gift.list');
            Route::get('/admin/gift/add', 'AdminController@giftAdd')->name('admin.gift.add');
            Route::post('/admin/gift/store', 'AdminController@giftStore')->name('admin.gift.store');
            Route::post('/admin/gift/request', 'AdminController@adminGiftAjax')->name('admin.gift.ajax');
            Route::get('/admin/user/list', 'AdminController@allUser')->name('admin.users.list');
        });

//-----User List---------
        Route::group(['middleware' => 'permission:view users'], function () {

            Route::get('/admin/user/list', 'AdminController@allUser')->name('admin.users.list');
            Route::get('/admin/user/{id}', 'AdminController@userDetail')->name('admin.user.detail');

        });

//--- Newsletter----------
        Route::Group(['middleware' => 'permission:send news letter'], function () {

            Route::get('/admin/newsletter', 'AdminController@newsletter')->name('admin.newsletter');
            Route::post('/admin/newsletter/store', 'AdminController@newsletterStore')->name('admin.newsletter.store');

        });


//Roles Management
        Route::Group(['middleware' => 'permission:Users Roles Management'], function () {

            Route::get('/admin/addroles', 'AdminController@addRoles')->name('admin.addroles');
            Route::post('/admin/add_Users', 'AdminController@add_Users')->name('admin.add_Users');
            Route::post('/admin/view_user_info', 'AdminController@view_User_Info')->name('admin.view_user_info');
            Route::get('/admin/view_user_logs', 'AdminController@view_User_Logs')->name('admin.view_user_logs');
            Route::get('/admin/user_log_filer_byId', 'AdminController@user_Log_Filer_ById')->name('admin.user_log_filer_byId');
            Route::post('/admin/add_roles_permissions_ajax', 'AdminController@add_Roles_Permissions_Ajax')->name('admin.add_roles_permissions_ajax');

        });
//        Route::Group(['middleware' => 'permission:Users Roles Management'], function () {

            Route::get('/admin/hedge', 'HedgeController@index')->name('admin.hedge_index');
            Route::post('/admin/add_hedge', 'HedgeController@add_Hedge')->name('admin.add_hedge');
            Route::post('/admin/view_hedge_info', 'HedgeController@view_Hedge_Info')->name('admin.view_hedge_info');
            Route::post('/admin/update_hedge_info', 'HedgeController@update_Hedge_Info')->name('admin.update_hedge_info');
            Route::post('/admin/profitstore', 'HedgeController@profitStore')->name('admin.profitstore');
            Route::get('/admin/hedge_profit_history', 'HedgeController@hedgeProfitList')->name('admin.hedge_profit_history');
            Route::get('/admin/hedge_booking_list', 'HedgeController@hedgeBookingList')->name('admin.hedge_booking_list');
//            Route::get('/admin/hedge_log_filer_byId', 'HedgeController@user_Log_Filer_ById')->name('admin.hedge_log_filer_byId');
//            Route::post('/admin/add_roles_permissions_ajax', 'HedgeController@add_Roles_Permissions_Ajax')->name('admin.add_roles_permissions_ajax');

//        });
    });
