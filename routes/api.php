<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/newfunction', 'UserController@newfunction');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=>'v1'], function () {


    Route::post('/loginApi', 'UserController@loginApi');
    Route::post('/logoutApi', 'UserController@logoutApi');
    Route::post('/regApi', 'UserController@regApi');


    Route::get('/adminCoinApi', 'UserController@adminCoin');
    Route::post('/depositSoreApi', 'UserController@depositSoreApi');
    Route::post('/withdrawApi', 'UserController@withdrawApi');


    Route::get('/depositeListApi', 'UserController@depositeListApi');

    Route::get('/withhDrawListApi', 'UserController@withhDrawListApi');

    Route::get('/profitListApi', 'UserController@profitListApi');


    Route::get('/homeIndexApi', 'UserController@homeIndexApi');




    // ------------ Gift Apis------------------

    Route::get('/giftCodeApi', 'UserController@giftCodeAPi');
    Route::post('/giftAddApi', 'UserController@giftAddAPi');
    Route::post('/giftVerifyApi', 'UserController@giftVerifyApi');
    //depositeStore web side
    Route::post('/giftUseApi', 'UserController@giftUseApi');
    Route::get('/createdGiftListApi', 'UserController@createdGiftListApi');
    Route::get('/getGiftListApi', 'UserController@getGiftListApi');


    // ------------  END ------------------


Route::post('/transactionListApi', 'UserController@transactionListApi');

Route::post('/emailApi', 'UserController@emailApi');

Route::post('/networkApi', 'UserController@networkApi');

/*Route::post('/sosStoreApi', 'UserController@sosStoreApi');*/
Route::post('/btc/profit', 'UserController@btcProfitApi');



Route::post('/settingsApi', 'UserController@settingsApi');
Route::post('/settingsUpdateApi', 'UserController@settingsUpdateApi');

Route::post('/sendPwdCodeApi', 'UserController@sendPwdCodeApi');

Route::post('/verifyPwdCodeApi', 'UserController@verifyPwdCodeApi');

Route::post('/newPwdApi', 'UserController@newPwdApi');

Route::group(['namespace' => 'Auth'], function() {
Route::post('/sendResetLinkEmailAPi', 'ForgotPasswordController@sendResetLinkEmailAPi');
});

//done
Route::post('/issetpincodeApi', 'UserController@isSetPinCodeApi');
Route::post('/pincodeCreateApi', 'UserController@pincodeCreateApi');

Route::post('/pincodeLoginApi', 'UserController@pincodeLoginApi');
//done
Route::post('/btcApi', 'UserController@btcApi');
Route::post('/referralApi', 'UserController@referralApi');


// -----SOS Apis--------------
Route::post('/soslistApi', 'UserController@soslistApi');
Route::post('/sosAddApi', 'UserController@sosAddApi');

Route::post('sosDetailApi', 'UserController@sosDetailApi');

Route::post('contactCommentsApi', 'UserController@contactCommentsApi');


//============ Notification API's===================
Route::post('/markReadNotifyApi', 'UserController@markReadNotifyApi');
//=============== END =============================
});

