<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;
Route::get('/', function (Request $request) {
	 if($request->get('id'))
        {
            $key =$request->get('id');
                  /*print_r($request->get('u_id'));exit;*/

            return view('welcome')->withRefKey($key);
        }
        $key ='';
    return view('welcome')->withRefKey($key);
});


//Clear route cache:
 Route::get('/route-cache', function() {
     $exitCode = Artisan::call('route:cache');
     return 'Routes cache cleared';
 });

 //Clear config cache:
 Route::get('/config-cache', function() {
     $exitCode = Artisan::call('config:cache');
     return 'Config cache cleared';
 }); 

// Clear application cache:
 Route::get('/clear-cache', function() {
     $exitCode = Artisan::call('cache:clear');
     return 'Application cache cleared';
 });
Auth::routes();



// Testing route for Btc Ajax
Route::post('/btcAjax', 'UserController@btcAjax')->name('user.ajax.btc');

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/home', 'HomeController@index')->name('home');

// ROUTES FOR CUSTOMER
Route::get('/crypto', 'UserController@crypto')->name('crypto.crypto');
Route::post('/crypto', 'UserController@crypto')->name('crypto.crypto');
Route::get('/home/callback', 'HomeController@callBack')->name('home.crypto.callBack');


//-----Transaction History---------

Route::get('/user/history', 'UserController@transIndex')->name('trans.index'); 

//-----Deposit---------
Route::get('/user/deposit', 'UserController@depositeIndex')->name('deposite.index');
Route::get('/user/deposit/list', 'UserController@depositeList')->name('deposite.list');
Route::post('/user/deposit', 'UserController@depositeStore')->name('deposite.store');

//-----WithDraw---------
Route::get('/user/withdraw/list', 'UserController@withhDrawIndex')->name('withdraw.index');
Route::post('/user/withdrawAjax', 'UserController@withhDrawAjax')->name('withdraw.request');


//-----Profit---------
Route::get('/user/profit/list', 'UserController@profitIndex')->name('profit.index');
Route::post('/user/profitAjax', 'UserController@userProfitAjax')->name('profit.request');

//--- Contact Us----------
Route::post('/user/contact', 'UserController@contact')->name('contact.request');

//-----Network---------
Route::get('/user/network/', 'UserController@network')->name('user.netowrk.index');
Route::post('/user/network/', 'UserController@networkGet')->name('netowrk.get');


Route::post('/user/notifi/', 'UserController@markRead')->name('notifi.read');

Route::get('/about-us', 'UserController@about')->name('about');
Route::get('/affiliate-program', 'UserController@affi')->name('aff');

//-----Gift---------
Route::get('/user/gift/list', 'UserController@giftList')->name('user.gift.list');
Route::get('/user/gift/add', 'UserController@giftAdd')->name('user.gift.add');
Route::post('/user/gift/add', 'UserController@giftAdd')->name('user.gift.add');

Route::post('/user/gift/store', 'UserController@giftStore')->name('user.gift.store');


//-----Gift Approval Ajx---------
Route::post('/user/gift/ajax', 'UserController@giftAjax')->name('user.gift.ajax');

//----- SOS ---------
Route::get('/user/sos', 'UserController@sos')->name('user.sos.add');
Route::post('/user/sos/store', 'UserController@sosStore')->name('user.sos.store');

Route::get('/user/sos/list', 'UserController@soslist')->name('user.sos.list');
Route::get('/user/sos/detail/{id}', 'UserController@sosDetail')->name('user.sos.detail');
Route::post('/user/sos/detailStore/', 'UserController@sosDetailStore')->name('user.sos.detailStore');


Route::get('/email-verify', 'UserController@email')->name('user.email');
/*Route::post('/email-verifyStore', 'EmailController@email')->name('user.email.store');*/
Route::post('/email-verifyStore', 'UserController@emailStore')->name('user.email.store');

//----- User Setting---------
Route::get('/user/setting', 'UserController@setting')->name('user.setting');
Route::post('/user/setting/store', 'UserController@settingStore')->name('user.setting.store');


//----- News Letter ---------
Route::post('/user/newsletter', 'UserController@newsletter')->name('user.newsletter');

Route::group(['middleware' => 'admin'], function () {
    //-----Pending---------
	Route::get('/admin/pending-request', 'AdminController@pendingIndex')->name('admin.pending.index');
	Route::post('/admin/pending-request', 'AdminController@pendingAjax')->name('admin.pending.ajax'); 


//-----Approved---------
Route::get('/admin/approved', 'AdminController@approvedIndex')->name('admin.approved.index');
Route::post('/admin/approved', 'AdminController@approvedAjax')->name('admin.approved.ajax');


Route::post('/admin/profit/accept', 'AdminController@profitAjax')->name('admin.profit.approved');


//-----withdraw---------
Route::get('/admin/withdraw', 'AdminController@withdrawIndex')->name('admin.withdraw.index');
Route::post('/admin/withdraw', 'AdminController@withdrawAjax')->name('admin.withdraw.ajax');



//-----profit---------
Route::get('/admin/profit', 'AdminController@profitReq')->name('admin.profit.index');
Route::post('/admin/profit', 'AdminController@profitReqAjax')->name('admin.profit.ajax');


//-----Weekly set---------
Route::get('/admin/weekly', 'AdminController@weeklyIndex')->name('admin.weekly.index');
Route::post('/admin/weekly/store', 'AdminController@weeklyStore')->name('admin.weekly.store');


//-----Admin Coin---------
Route::get('/admin/coin', 'AdminController@coinIndex')->name('admin.coin.index');
Route::post('/admin/coin/store', 'AdminController@coinStore')->name('admin.coin.store');


//-----Admin notification---------
Route::get('/admin/notification', 'AdminController@notificationIndex')->name('admin.notification.index');
Route::post('/admin/notification/store', 'AdminController@notificationStore')->name('admin.notification.store');



//-----Contact Detail---------
Route::get('/admin/contact', 'AdminController@contactUsers')->name('admin.contact.index');

Route::get('/admin/contact/detail/{id}', 'AdminController@sosDetail')->name('admin.sos.detail');
Route::post('/admin/contact/detailStore/', 'AdminController@sosDetailStore')->name('admin.sos.detailStore');
Route::post('/admin/contact/status/', 'AdminController@contactStatus')->name('admin.sos.status');

//-----Gift---------
Route::get('/admin/gift/list', 'AdminController@giftList')->name('admin.gift.list');
Route::get('/admin/gift/add', 'AdminController@giftAdd')->name('admin.gift.add');

Route::post('/admin/gift/store', 'AdminController@giftStore')->name('admin.gift.store');

Route::post('/admin/gift/request', 'AdminController@adminGiftAjax')->name('admin.gift.ajax');
Route::get('/admin/user/list', 'AdminController@allUser')->name('admin.users.list');

//-----User List---------
 Route::get('/admin/user/list', 'AdminController@allUser')->name('admin.users.list');
Route::get('/admin/user/{id}', 'AdminController@userDetail')->name('admin.user.detail');


 //--- Newsletter----------
 Route::get('/admin/newsletter', 'AdminController@newsletter')->name('admin.newsletter');

Route::post('/admin/newsletter/store', 'AdminController@newsletterStore')->name('admin.newsletter.store');



	});
