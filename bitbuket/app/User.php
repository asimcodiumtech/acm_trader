<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Profit;
use App\Models\WithDraw;
use App\Models\FundDetail;
use Illuminate\Support\Facades\Hash;

use Cache;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table='users';
    protected $fillable = [
        'name', 
        'email',
        'password',
        'pwd_code',
        'pin_code',
        'remain_balance',
        'withdraw_amount',
        'reff_key',
        'reff_by',
        'level',
        'system_position',
        'financeleft',
        'financeright',
        'btc_address' ,
        'acc_name' ,
        'acc_no',
        'branch_code',
        'acc_type',
        'swift_code',
        'remember_token',
        'app_token',
        'email_status',
        'email_token'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function isOnline($id=null)
    {   
        /*if(!empty($id))
        {  
            
            $token=bin2hex(random_bytes(16));
            $user=self::where('id',$id)->update(['remember_token'=>$token]);
        }
        else
        { 
            $user=self::where('id',$id)->update(['remember_token'=>null]);
             
        }*/
        return Cache::has('active'.$this->id);
    }
    public static function balance($id=null)
    {   
        if(!empty($id))
        {
            $userdata = array(
            
            'deposit' => FundDetail::where('u_id',$id)
                            ->where('deposite_status',1)->get()->count() ? FundDetail::where('deposite_status',1)->sum('amount'): 0,
            'profit' => Profit::where('u_id',$id)->get()->count() ? Profit::where('u_id',$id)->where('withdraw_profit','!=',1)->sum('profit'): 0,
             'withdraw' => WithDraw::where('u_id',$id)->get()->count() ? WithDraw::where('u_id',$id)->where('is_withdraw',1)->sum('withdraw_amount'): 0,
            
                
            );
        
       return $userdata;

        }
        $userdata = array(
            
            'deposit' => FundDetail::where('u_id',auth()->user()->id)
                            ->where('deposite_status',1)->get()->count() ? FundDetail::where('deposite_status',1)->sum('amount'): 0,
            'profit' => Profit::where('u_id',auth()->user()->id)->get()->count() ? Profit::where('u_id',auth()->user()->id)->where('withdraw_profit','!=',1)->sum('profit'): 0,
             'withdraw' => WithDraw::where('u_id',auth()->user()->id)->get()->count() ? WithDraw::where('u_id',auth()->user()->id)->where('is_withdraw',1)->sum('withdraw_amount'): 0,
            
                
            );
        
       return $userdata;
    }
   public static function gainKey($refferby)
   {
    $relativefirst= self::where('reff_key',$refferby)->first();
    $refferby1= $relativefirst->reff_by;
    return  $refferby1;
   }


   public static function TodayProfit()
   { 
    $startDate= date('Y-m-d');
    $modelsub = self::join('fund_details', 'fund_details.u_id', '=', 'users.id')
                                 ->select('fund_details.u_id')
                                 
                            ->where('fund_details.deposit_approved_time','=',$startDate)->distinct()->get();

                            return $modelsub;
   }

}
