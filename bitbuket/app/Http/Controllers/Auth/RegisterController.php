<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\MLM;
use Mail;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            
            
            
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        
        if(!empty($data['ref_key']))
        {   
            $check=User::where('reff_key',$data['ref_key'])->first();
            $ref_key=$check->reff_key;
            $person_id=$check->id;
            $levelcheck=$check->level;
           
            if($ref_key)
            {    
                $reff_f=Hash::make($data['password']);
                $reff_b=$ref_key;
                $system_position=null;
                $level=++$levelcheck;
            }
            else
            {
                $reff_f=Hash::make($data['password']);
                $reff_b='';
                $system_position=null;
                $level=1;
            }
        }
        else
        {
            $reff_f=Hash::make($data['password']);
            $reff_b='';
            $system_position=User::where('admin',2)->where('system_position','!=',null)->get()->count();
            $system_position=++$system_position;
            $level=1;
        }
        $token = openssl_random_pseudo_bytes(16);

        //Convert the binary data into hexadecimal representation.
        $token = bin2hex($token);
        $link = url(config('app.url').'/login'. '?token=' . $token);
        $messagetext = 'You are Welcome to Login';
        $email = 'admin@acmtrader.com';
        $email_to = $data['email'];
        Mail::send('user.mail.registration-mail', ['messagetext' => $messagetext, 'link' => $link], function ($message) use ($email, $email_to) {
            $message->from($email);
            $message->to($email_to);

            $message->subject('Welcome');
        });
       
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'reff_key' => $reff_f,
            'reff_by'  => $reff_b,
            'system_position'=>$system_position,
            'level'=> $level,
            'btc_address' => $data['btc_address'],
            'acc_name' => '',
            'acc_no' => '',
            'branch_code' => '',
            'acc_type' => '',
            'swift_code' => '',
            'email_status' => 2,
            'email_token' => $token

        ]);
    }
}
