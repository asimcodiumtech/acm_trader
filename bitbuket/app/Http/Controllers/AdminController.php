<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\FundDetail;
use App\Models\Profit;
use App\Models\WithDraw;
use App\Models\Weekly;
use App\Models\Gift;
use App\Models\AdminCoin;
use App\Models\Contact;
use App\Models\Comments;
use App\User;
use Carbon\Carbon;
use App\Notifications\MessageFromAdmin;
use App\Models\NewsLetter;
use Mail;
class AdminController extends Controller
{
    public function newsletter(Request $request)
    {
        return view('admin.newsletter.index');
    }
    public function newsletterStore(Request $request)
    {   
        if ($request->ajax())
        {
            if (!empty($request->get('msg')))
            { 
               $messagetext = $request->get('msg');
                $email = 'admin@acmtrader.com';

                $NewsLetter= NewsLetter::all();
                foreach ($NewsLetter as $user) {
                   $email_to=$user->email;
                    Mail::send('user.mail.newsletter', ['messagetext' => $messagetext], function ($message) use ($email, $email_to) {
                    $message->from($email);
                    $message->to($email_to);

                    $message->subject('ACM Newsletter');
                    });
                }
                return ["error" => null, 'success' => '201'];

            }
        }
    }
    public function pendingIndex()
    {
        $model=FundDetail::where('deposite_status' ,"!=",0)->get();
      return view('admin.pending.index')->withUsers($model);
    }
    public function pendingAjax(Request $request)
    {
    	if($request->ajax())
        {
           if(!empty($request->get('id')) && $request->get('deposite')=='doApproved')
            {
                $id=$request->get('id');
                $time_deposited= date('Y-m-d');
                $model=FundDetail::where('id',$id)->update(['deposite_status'=> 1,'deposit_approved_time'=> $time_deposited,'deposite_initial_status'=>0]);
                $u_id=FundDetail::where('id',$id)->first();
                $balance=User::select('remain_balance')->where('id',$u_id->u_id)->first();
                $total=$balance->remain_balance + $u_id->amount;


                $refferby=User::where('id',$u_id->u_id)->first();
                $refferby=$refferby->reff_by;
                $amount=$u_id->amount ;
               
                for($i=1;$i<=3;$i++)
                {
                    $relativefirst=User::where('reff_key',$refferby)->first();
                    if(!empty($refferby))
                    {  
                        if($i==3)
                        {
                            $percentage=0.05;
                        }
                        else
                        {
                            $percentage=0.1;
                        }
                        $withdraw=$relativefirst->withdraw_amount;
                        $balance=$relativefirst->remain_balance;
                        $financeleft=$relativefirst->financeleft;

                        $finalWithamount=$withdraw +  $amount*$percentage;
                        $finalamount=$balance +  $amount*$percentage;
                        $finalfinanceleft=$financeleft +  $amount*$percentage;

                        $update=User::where('reff_key',$refferby)->update(['remain_balance'=>$finalamount,'withdraw_amount'=>$finalWithamount,'financeleft'=>$finalfinanceleft]);
                        $refferby= $relativefirst->reff_by;
                        /*echo "<pre>";
                        print_r($relativefirst);
                        print_r($i); */  
                    }
                }
                /////   systems  person gave profit
                $system=User::where('id',$u_id->u_id)->first();
                if(!empty($system->system_position))
                {    $endingLimit=$system->system_position-3;
                    $positionwise=User::where('system_position','<',$system->system_position)->where('system_position','>=',$endingLimit)->orderBy('system_position', 'DESC')->get();

                    if(!empty($check=$positionwise->toArray()))
                    {   $cc=1;
                        foreach($positionwise as $users)
                        {
                            switch ($cc) 
                            {
                                case 1:
                                   $percentage=0.05;$cc++;
                                    break;
                                case 2:
                                    $percentage=0.03;$cc++;
                                    break;
                                case 3:
                                    $percentage=0.02;$cc++;
                                    break;
                            }
                            $withdraw=$users->withdraw_amount;
                            $balance=$users->remain_balance;
                            $financeright=$users->financeright;
                               
                            $finalWithamount=$withdraw +  $amount*$percentage;
                            $finalamount=$balance +  $amount*$percentage;
                            $finalfinanceright=$financeright +  $amount*$percentage;
                            $update=User::where('id',$users->id)->update(['remain_balance'=>$finalamount,'withdraw_amount'=>$finalWithamount,'financeright'=>$finalfinanceright]);
                        }                            
                    }
                }    
                
                
                $update_balance=User::where('id',$u_id->u_id)->update(['remain_balance'=> $total]);
                return ["error" => null, 'success' => '200'];
            }
            elseif(!empty($request->get('id')) && $request->get('deposite')=='doPending')
            {
                 $id=$request->get('id');
                  
              $model=FundDetail::where('id',$id)->update(['deposite_status'=> 2]);
                return ["error" => null, 'success' => '200'];
            }
            else
            {
                 return ['error' => 409];
            }
        }
    }
    public function profitReq()
    {
        $model=Profit::all();
      return view('admin.profitpending.index')->withUsers($model);
    }
    public function profitReqAjax(Request $request)
    {
      if($request->ajax())
        {
           if(!empty($request->get('id')))
            {
                 $id=$request->get('id');
                  
              $model=Profit::where('id',$id)->update(['withdraw_profit'=> 1]);
              $model=WithDraw::where('profit_id',$id)->update(['is_withdraw'=> 1]);
                return ["error" => null, 'success' => '200'];
            }
           
            else
            {
                 return ['error' => 409];
            }
        }
    }
    public function profitAjax(Request $request)
    {
        if($request->ajax())
        {
           if(!empty($request->get('id')) && $request->get('profit'))
            {
                 $id=$request->get('id');
                  
              $model=FundDetail::where('id',$id)->update(['profit'=> $request->get('profit')]);
              $model1=FundDetail::select('u_id')->where('id',$id)->first();
              $uid=$model1->u_id;

              $model1=Profit::create(['u_id'=> $uid,'profit'=>$request->get('profit')]);
                    
                return ["error" => null, 'success' => '200'];
            }
            
            else
            {
                 return ['error' => 409];
            }
        }
    }
    public function approvedIndex()
    {   
         $model=FundDetail::where('deposite_status',1)->get();
    	return view('admin.approved.index')->withUsers($model);
    }
    public function approvedAjax()
    {
        
    	
    }
    public function withdrawIndex()
    {   
        $model=WithDraw::all();
    	return view('admin.withdraw.index')->withUsers($model);
    }
    public function withdrawAjax(Request $request)
    {
        if($request->ajax())
        {
           
            if(!empty($request->get('id')) && $request->get('withdrawStatus')&& $request->get('u_id'))
            {
                $id=$request->get('id');
                $u_id=$request->get('u_id');
                $model=WithDraw::where('id',$id)->update(['is_withdraw'=> $request->get('withdrawStatus')]);

                $withid=WithDraw::where('id',$id)->first();
                $balance=User::where('id',$u_id)->first();

                $total=$balance->remain_balance - $withid->withdraw_amount;
                $totalWithdraw=$balance->withdraw_amount - $withid->withdraw_amount;
                $update_balance=User::where('id',$u_id)->update(['remain_balance'=> $total,'withdraw_amount'=>$totalWithdraw]);
                  
              /*$model=FundDetail::where('id',$id)
               ->update(['withdraw_status'=> $request->get('withdrawStatus')]);*/
                return ["error" => null, 'success' => '200'];
            }
            else
            {
                 return ['error' => 409];
            }
        }
    	
    }

    public function weeklyIndex()
    {   
        $model=Weekly::where('is_active',1)->first();
        if(empty((array) $model))
    {  
        $model=['weekly_age'=>''];

    }
        
        return view('admin.weekly.index')->withUsers((object)$model);
    }
    public function weeklyStore(Request $request)
    {   
             
        /*echo "<pre>";
        print_r($request->all());exit;*/
        $dateDaily= Weekly::where('is_active',1)->first();
        $checkDate= date('Y-m-d');
        if($checkDate != $dateDaily->date)
        {
                        $model=Weekly::all();
                        $data=Weekly::where('is_active',1)->update(['is_active'=>0]);
                        
                        if($checkDate != $dateDaily->date)
                        
                        $data=Weekly::create($request->all());
                        $startDate= date('Y-m-d');
                        $date=Weekly::where('is_active',1)->update(['date'=>$startDate]);
                        $active_weekly=Weekly::where('is_active',1)->first();
                        ///$mytime = Carbon::now();
                /*$ldate = date('Y-m-d ');
                $start = '2019-11-28';
                $end = '2019-11-28';
                
                $date1 = "2019-12-02"; 
                $date2 = "2019-12-01";*/
                
                        
                        
                    if(!empty((array) $model))
                    {    
                        
                        /*$users=User::TodayProfit();*/
         $users=User::where('admin',2)->get();
                        
                        
                        foreach($users as $user)
                        {    
                            /*$startDate= date('Y-m-d',strtotime((string)$active_weekly->created_at));
                            $endDate= date('Y-m-d',strtotime("-7 day",strtotime((string)$active_weekly->created_at)));*/
                
                             $startDate= date('Y-m-d');
                            $endDate= date('Y-m-d');
                            $percentage=(float)$request->get('weekly_age');
                            $deposite_approved_weekly_profit= FundDetail::weeklyProfit($user->id,$startDate,$endDate,$percentage);
                        }
                    }
                    else
                    { 
                        $data=Weekly::create($request->all());
                
                    }
                        
                        $get=Weekly::where('is_active',1)->first();
                
                        
                        $notification = array(
                    'message' => 'Profit Send Successfully',
                    'alert-type' => 'success',
                    'heading' => 'Succeed',
                );
                        /*Session::flash('alert-type','success');        view('admin.weekly.index')*/
                        
                       return redirect()->route('admin.weekly.index')->withUsers($get)->with($notification);
        }
        
       $get=Weekly::where('is_active',1)->first();
                
                        
                        $notification = array(
                    'message' => 'You are already Set Today Profit',
                    'alert-type' => 'error',
                    'heading' => 'Failed',
                );
       
       return redirect()->route('admin.weekly.index')->withUsers($get)->with($notification);
    }

    public function coinIndex()
    {   
        $model=AdminCoin::where('is_active',1)->first();
        if(empty((array) $model))
    {  
        $model= (object)$model=['btc_id'=>'','qr_code'=>'','usd_id'=>'','acc_name'=>'','branch_code'=>'','swift_code'=>'','bank_type'=>''];

    }
        
        return view('admin.coin.index')->withUsers($model);
    }
    public function coinStore(Request $request)
    {   
        $posterName = "";
        if (request()->hasFile('poster')) {

            /* Remove previous poster */
            $file_path = 'images/qrcodes/'. "/" . $request->get('oldPoster');
            if (file_exists($file_path) && !is_dir($file_path)) {
                @unlink($file_path);
            }

            /* Add new poster */
            $posterName = time() . '.' . request()->poster->getClientOriginalExtension();
            request()->poster->move('images/qrcodes/', $posterName);
        } 
        else 
        {
            $posterName = $request->get('oldPoster');
        }

        


        $model=AdminCoin::all();

        

        if(!empty((array) $model))
       {  
        $data=AdminCoin::where('is_active',1)->update(['is_active'=>0]);

        
        $admincoin = AdminCoin::create([
            'btc_id' => $request->get('btc_id'),
            'qr_code' => $posterName,
            'usd_id' => $request->get('usd_id'),
            'acc_name' => $request->get('acc_name'),
            'branch_code' => $request->get('branch_code'),
            'bank_type' => $request->get('bank_type'),
            'swift_code' => $request->get('swift_code'),
            'is_active' => 1,
        ]);
          
        }
        else
        {
            $admincoin = AdminCoin::create([
            'btc_id' => $request->get('btc_id'),
            'qr_code' => $posterName,
            'usd_id' => $request->get('usd_id'),
            'acc_name' => $request->get('acc_name'),
            'branch_code' => $request->get('branch_code'),
            'bank_type' => $request->get('bank_type'),
            'swift_code' => $request->get('swift_code'),
            'is_active' => 1,
        ]);
        }
        $get=AdminCoin::where('is_active',1)->first();
        return view('admin.coin.index')->withUsers($get);
    }

    public function notificationIndex(Request $request)
    {
        return view('admin.notification.index');
    }
    public function notificationStore(Request $request)
    {    
        $message =$request->get('message');
         $model= User::where('admin','=',2)->get();
         foreach($model as $user)
         {
            $user->notify(new MessageFromAdmin($message));
         }
        $notification = array(
    'message' => 'Notification Send Successfully',
    'alert-type' => 'success',
    'heading' => 'Succeeded',
);
        return redirect()->route('admin.notification.index')->with($notification);;
    }
    public function contactUsers()
    {
        $model=Contact::all();
        Contact::markAsRead();
        return view('admin.contact.index')->withUsers($model);
    }
    public function contactStatus(Request $request)
    {
        if($request->ajax())
        {

            if(!empty($request->get('status')) && !empty($request->get('id')) )
            {

                 Contact::where('id',$request->get('id'))->update(['status'=>$request->get('status')]);
                return ["error" => null, 'success' => '201'];
            }
        }
    }
    public function giftList()
    {
        $model=Gift::all();
        return view('admin.gift.list')->withGifts($model);
    }
    public function giftAdd()
    {
       $hash = bin2hex(random_bytes(16));
/*$bytes = openssl_random_pseudo_bytes(32);
$hash = bin2hex($bytes);*/

         /* echo serialize($hash);
          exit;*/
        return view('admin.gift.add')->withHash($hash);
    }


    public function giftStore(Request $request)
    {
      
      $check= Gift::where('gift_code',$request->get('gift_code'))->first();

      
      if(!empty($check))
      {
        return redirect()->route('admin.gift.add');
      }
      else
      { 
        
        


        $request->merge(['created_by' => auth()->user()->id]);

        $post=$request->all();

        /*echo "<pre>";
        print_r($post);exit;*/
        $create=Gift::create($post);
         $notification = array(
    'message' => 'Gift Created Successfully',
    'alert-type' => 'success',
    'heading' => 'Succeeded',
);
        return redirect()->route('admin.gift.list')->with($notification);
    
      }
    }
    public function adminGiftAjax(Request $request)
    {
       if($request->ajax())
        {

            if(!empty($request->get('id')))
            {
                $checkData=Gift::where('id',$request->get('id'))->first();

                if($checkData->created_by==auth()->user()->id)
                {
                    $update=Gift::where('id',$request->get('id'))->update(['status'=>1]);
                    $getUser=User::where('id',$checkData->user_id)->first();
                    
                     $time_deposited= date('Y-m-d');
                     $request->merge(['amount' => $checkData->amount,'coin_type' => 'gift','deposit_approved_time' => $time_deposited,'deposite_status'=> 1,'u_id'=>$checkData->user_id]);

                     
                    $FundDetail = FundDetail::create($request->all());



                                    $id=User::where('id',$checkData->user_id)->first();
                $balance=User::select('remain_balance')->where('id',$id->id)->first();
                $total=$balance->remain_balance + $checkData->amount;


                $refferby=User::where('id',$id->id)->first();
                $refferby=$refferby->reff_by;
                $amount=$checkData->amount ;
               
                for($i=1;$i<=3;$i++)
                {
                    $relativefirst=User::where('reff_key',$refferby)->first();
                    if(!empty($refferby))
                    {  
                        if($i==3)
                        {
                            $percentage=0.05;
                        }
                        else
                        {
                            $percentage=0.1;
                        }
                        $withdraw=$relativefirst->withdraw_amount;
                        $balance=$relativefirst->remain_balance;
                        $financeleft=$relativefirst->financeleft;

                        $finalWithamount=$withdraw +  $amount*$percentage;
                        $finalamount=$balance +  $amount*$percentage;
                        $finalfinanceleft=$financeleft +  $amount*$percentage;

                        $update=User::where('reff_key',$refferby)->update(['remain_balance'=>$finalamount,'withdraw_amount'=>$finalWithamount,'financeleft'=>$finalfinanceleft]);
                        $refferby= $relativefirst->reff_by;
                        /*echo "<pre>";
                        print_r($relativefirst);
                        print_r($i); */  
                    }
                }
                /////   systems  person gave profit
                $system=User::where('id',$id->id)->first();
                if(!empty($system->system_position))
                {    $endingLimit=$system->system_position-3;
                    $positionwise=User::where('system_position','<',$system->system_position)->where('system_position','>=',$endingLimit)->orderBy('system_position', 'DESC')->get();

                    if(!empty($check=$positionwise->toArray()))
                    {   $cc=1;
                        foreach($positionwise as $users)
                        {
                            switch ($cc) 
                            {
                                case 1:
                                   $percentage=0.05;$cc++;
                                    break;
                                case 2:
                                    $percentage=0.03;$cc++;
                                    break;
                                case 3:
                                    $percentage=0.02;$cc++;
                                    break;
                            }
                            $withdraw=$users->withdraw_amount;
                            $balance=$users->remain_balance;
                            $financeright=$users->financeright;
                               
                            $finalWithamount=$withdraw +  $amount*$percentage;
                            $finalamount=$balance +  $amount*$percentage;
                            $finalfinanceright=$financeright +  $amount*$percentage;
                            $update=User::where('id',$users->id)->update(['remain_balance'=>$finalamount,'withdraw_amount'=>$finalWithamount,'financeright'=>$finalfinanceright]);
                        }                            
                    }
                }    
                
                
                $update_balance=User::where('id',$id->id)->update(['remain_balance'=> $total]);


                            /*$remainig=$getUser->remain_balance + $checkData->amount;
                            $withdrawable= $getUser->withdraw_amount + $checkData->amount;
                            $userUpate=User::where('id',$checkData->user_id)->update(['remain_balance'=>$remainig,'withdraw_amount'=>$withdrawable]);*/
                    return ["error" => null, 'success' => '200']; 
                }
            }
        }
    }
    public function allUser(Request $request)
    {
        if($request->get('status'))
        {
            $user=User::where('admin','!=',1)->where('remember_token','!=',null)->get();
            return view('admin.users.list')->withUsers($user);
        }
        $user=User::where('admin','!=',1)->get();
        return view('admin.users.list')->withUsers($user);
    }
    public function userDetail(Request $request,$id)
    { 
        
        
           $request->session()->put('user',$id);
            
           
                 $user=User::where('id',$id)->first();
             $userData= User::where('id',$user->id)->first();
                 $id=$user->id;


             $model=User::where('id',$id)->first();
            $exchangeInvested=FundDetail::where('u_id',$id)->where('deposite_status',1)->sum('amount');
            $exchangeProfit=Profit::where('u_id',$id)->sum('profit');
            $exchangeShow =  $exchangeInvested  ;
            $balance=$model->remain_balance;
            
            
            $withdraw2=Withdraw::where('u_id',$id)->where('is_withdraw',1)->sum('withdraw_amount');
            
            $minus= $withdraw2 - ($model->financeleft +     $model->financeright);
            if($minus<=0)
            {
                $minus=0;
            }
            
            $exchange2ndtime= $exchangeShow + $exchangeProfit -$minus;
            /*echo "<pre>";
            print_r($balance);exit;*/
                 
                    $startDate= date('Y-m-d');
                       $endDate= date('Y-m-d',strtotime("-60 day",strtotime($startDate))) ;
                 $validWithdraw= FundDetail::where('u_id',$id)->where('deposite_status',1)->where('deposit_approved_time','<=',$endDate)->where('deposite_initial_status',0)->get();
                 $array=FundDetail::where('u_id',$id)->where('deposite_status',1)->where('deposit_approved_time','<=',$endDate)->where('deposite_initial_status',0)->get()->toArray();

                 $withdrawUpdate=User::where('id',$id)->first();
                  /*echo "<pre>";
                  print_r($withdrawUpdate->withdraw_amount);exit;*/
                    if(sizeof($array) == 0)
                    {             
                                    
                    }
                    else
                    {
                        foreach($validWithdraw as $user)
                                    {
                                         
                                          $finalamount= $withdrawUpdate->withdraw_amount + $user->amount;
                                          $withdrawUpdate=User::where('id',$id)->update(['withdraw_amount'=>$finalamount]);

                                          $updateDeposit=FundDetail::where('id',$user->id)->update(['deposite_initial_status'=>1]);
                                    }
                                    

                    }

                    
                    $startDate= date('Y-m-d');
                    $tprofit=Profit::where('u_id',$id)->sum('profit');
                    $todayProfit=Profit::where('u_id',$id)->first();
                    $todayProfit=Profit::where('u_id',$id)->where('profit_time',$startDate)->sum('profit');
                    $todayProfit=(int)$todayProfit;


                     $withdraw=Withdraw::where('u_id',$id)->first();
                    $withdraw=Withdraw::where('u_id',$id)->where('is_withdraw',1)->sum('withdraw_amount');
                    $withdraw=(int)$withdraw;

                      $fund=FundDetail::where('u_id',$id)->where('deposite_status',1)->sum('amount');
                      if($balance>0)
                      {
                        $total_percent=$tprofit/$balance *100;
                      }
                      else{
                        $total_percent=0;
                      }
                      
                    

                    //print_r($total_percent);exit;
                    
                   

            $weekly=Weekly::where('is_active',1)->first();

            if(!empty($weekly))
            {    
               /* $model=date('Y-m-d');
                 $weekly=date('Y-m-d',strtotime("-1 day",strtotime((string)$model)));
                $endDate= date('Y-m-d',strtotime("-7 day",strtotime((string)$model)));
                 //print_r($endDate);exit;
                $profitLastWeekAvgPercent=Profit::where('u_id',$id)->whereBetween('profit_time',[$endDate,$weekly])->sum('percentage');
                $profitLastWeekCount=Profit::where('u_id',$id)->whereBetween('profit_time',[$endDate,$weekly])->count();
               
                if($profitLastWeekCount<=0)
                {
                  $profitLastWeekCount=1;
                }
                $totalAvgAmount=$profitLastWeekAvgPercent /$profitLastWeekCount;*/
                $model=date('Y-m-d');
                 $daily=date('Y-m-d',strtotime("0 day",strtotime((string)$model)));
                 $profitLastDayPercent=Profit::where('u_id',$id)->where('profit_time','=',$daily)->first();
                 
                 if(empty($profitLastDayPercent))
                 {
                  $totalAvgAmount=0;
                 }
                 else
                 {
                  $totalAvgAmount=$profitLastDayPercent->percentage;
                 }
                 
                
            }
            else
            {
                $totalAvgAmount=0;
            }

           

            $coin= AdminCoin::where('is_active',1)->first();
            /*echo "<pre>";
            print_r($coin);exit;*/
            $usercoin= User::where('id',$id)->first();

        
          /*auth()->user()->unReadNotifications->markAsRead();*/


             /*for($i=1;$i<=3;$i++)
             {  
                if($i==3)
                {
                    $percentage=0.05;
                }
                else
                {
                     $percentage=0.1;
                }
            }
            
            echo "<pre>";
            print_r($refferby->level);exit;*/
            $currentId = $id;
        $data = User::where('id', $currentId)->first();
            // --------right Team Finance Code -----------
                    $lteambalance= $data->financeleft;
                    if(empty($lteambalance))
                    {
                        $lteambalance=0;
                    }


            // --------right Team Finance  Code End -----------



               // --------left Team Finance Code -----------
                       $rteambalance= $data->financeright;
                       if(empty($rteambalance))
                    {
                        $rteambalance=0;
                    }

            // --------left Team Finance  Code End -----------



            // --------left Team count Code ------------

        
        $reffkey = $data->reff_key;
        
       
        $datainner = User::where('reff_by', $reffkey)->get();
        $leftCountTeam=0;
        
        
        if (!empty($check1=$datainner->toArray()))
        {
            foreach ($datainner as $value)
            {
                $datainner1 = User::where('reff_by', $value->reff_key)
                    ->get();
                
                $leftCountTeam++;
                  
                if (!empty($check2=$datainner1->toArray()))
                {     
                    foreach ($datainner1 as  $value1)
                    {
                        
                        

                        $datainner2 = User::where('reff_by', $value1->reff_key)
                            ->get();

                        $leftCountTeam++;
                        if (!empty($datainner2->toArray()))
                        {
                            foreach ($datainner2 as  $value2)
                            { $leftCountTeam++;
                                
                            }
                        }
                    }
                }
            }
        }


          /*echo "<pre>";
            print_r($leftCountTeam);exit;*/

            // --------left Team count Code End ------------


            // --------right Team count Code ------------
           
              $rightTeam = 0;
            if(!empty($data->system_position))
            {
                $afterSystem = User::where('system_position','>', $data->system_position)
                    ->get();
                   
             
                
                

                if (!empty($check2=$afterSystem->toArray()))
                {
                    foreach ($afterSystem as  $value7)
                    {
                        
                        $rightTeam++;
                    }
                }
            }
            // --------right Team count Code End ------------
           

           $reffral_key=$data->reff_key;

         
             $allUnapproveddeposite=FundDetail::where('u_id',$id)->where('deposite_status',0)->get();
            foreach ($allUnapproveddeposite as $key => $value) 
            {
              $doApproved=FundDetail::paymentVerify($value->id);
            }



         

        

        return view('admin.users.detail')->withModel($model)->withCoin($coin)
           ->withPercent($total_percent)->withBalance($balance)->withTodayP($todayProfit)->withWithdraw($withdraw)->withFund($fund)->withAvg($totalAvgAmount)->withRightTeam($rightTeam)
                ->withLeftTeam($leftCountTeam)->withLbalance($lteambalance)->withRbalance($rteambalance)->withReffKey($reffral_key)->withExchange($exchangeShow)->withUsercoin($usercoin)
                ->withExchange2ndtime($exchange2ndtime);;
    }
    public function sosDetail($id)
   {   
        $contact=Contact::where('id',$id)->first();
        $comments=Comments::where('contact_id',$id)->get();
        return view('admin.contact.detail')->withContact($contact)->withComments($comments);
   }

   public function sosDetailStore(Request $request)
   {   
         $posterName = "";
            

        if (request()->hasFile('contact_pic1')) {
            
             /*echo "<pre>";
        print_r(request()->contact_pic->getClientOriginalExtension());exit; getClientOriginalName  getClientOriginalExtension*/

            $posterName = time() . '.' . request()->contact_pic1->getClientOriginalName();
            request()->contact_pic1->move(public_path('images/comment_pic'), $posterName);
        }

        $request->merge(['comment_pic' => $posterName]);

        $model=Comments::create($request->all());

        $notification = array(
        'message' => 'Message Send to Admin Successfully',
        'alert-type' => 'success',
        'heading' => 'Succeeded',
            );
             
        return redirect()->route('admin.sos.detail',$request->get('contact_id'))->with($notification);
   }
}
