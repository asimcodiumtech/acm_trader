
application/x-httpd-php UserController.php ( PHP script text )
<?php

namespace App\Http\Controllers;

use App\Models\AdminCoin;
use App\Models\Comments;
use App\Models\Contact;
use App\Models\FundDetail;
use App\Models\Gift;
use App\Models\NewsLetter;
use App\Models\Profit;
use App\Models\Weekly;
use App\Models\WithDraw;
use App\User;
use Cache;
use Carbon\Carbon;
use Cryptobox;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Mail;
use Validator;


class UserController extends Controller
{
    use SendsPasswordResetEmails;
    use RegistersUsers;

    public function markReadNotifyApi(request $request)
    {
        $validator = Validator::make($request->all(), [
            'app_token' => ['required', 'string'],
            'email' => ['required', 'string'],
            'notifi' => ['required', 'numeric', "in:1"]

        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $user1 = User::where('app_token', $request->get('app_token'))->where('email', $request->get('email'))->first();
        if (!empty($user1)) {
            $chekaa = $this->guard()->login($user1);
            $user = Auth::user();
            if ($request->get('notifi')) {
                auth()->user()->unReadNotifications->markAsRead();
                return response()->json(['success' => 'ok', 'message' => 'Admin Notifications Mark As Read']);
            }
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function transIndex()
    {
        $model = FundDetail::where('u_id', auth()->user()
            ->id)
            ->get();

        return view('user.history.index')
            ->withUsers($model);
    }

    public function depositeIndex()
    {
        $model = AdminCoin::where('is_active', 1)->first();
        if (empty((array)$model)) {
            $model = (object)$model = ['btc_id' => 'Not Set', 'usd_id' => 'Not Set'];
        }
        /*echo "<pre>";
        print_r($model->btc_id);exit;
        */
        return view('user.deposit.index')->withCoin($model);
    }

    public function depositeCash(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->get('amount'))) {
                if ($request->get('coin_type') == 'cash') {
                    $btc = file_get_contents('https://blockchain.info/ticker');

//                    $convert_amount= json_decode($btc)->USD->sell * $request->get('amount');
                    $convert_amount = $request->get('amount');
                    /*echo "<pre>";
                    print_r(json_decode($btc)->USD->sell);exit;*/
//                    $time_deposited= date('Y-m-d');
                    $request->merge(['amount' => $convert_amount, 'coin_type' => 'cash']);
//                    $request->merge(['amount' => $convert_amount,'coin_type' => 'cash','btc_admin' => $request->get('amount'),'btc_usd' => json_decode($btc)->USD->sell, 'deposit_approved_time' => $time_deposited]);
                }

                $time_deposited = date('Y-m-d');
//                $request->merge(['amount' => $checkData->amount,'coin_type' => 'gift','deposit_approved_time' => $time_deposited,

                /*echo "<pre>";
                print_r($request->all());exit;*/

                $FundDetail = FundDetail::create($request->all());

                return ["error" => null, 'success' => '200'];
                exit();
            } else {
                if ($request->get('coin_type') == 'gift') {
                    $check = Gift::where('gift_code', $request->get('gift_code'))->where('status', 0)->first();

                    if (!empty($check)) {
                        if ($check->created_by != auth()->user()->id) {
                            $update = Gift::where('gift_code', $request->get('gift_code'))->where('status', 0)->update(['user_id' => auth()->user()->id, 'status' => 2]);

                            /*$getUser=User::where('id',auth()->user()->id)->first();

                            $remainig=$getUser->remain_balance + $check->amount;
                            $withdrawable= $getUser->withdraw_amount + $check->amount;
                            $userUpate=User::where('id',auth()->user()->id)->update(['remain_balance'=>$remainig,'withdraw_amount'=>$withdrawable]);*/
                            return ["error" => null, 'success' => '203'];
                        }
                        if ($check->created_by == auth()->user()->id) {
                            $update = Gift::where('gift_code', $request->get('gift_code'))->where('status', 0)->update(['user_id' => auth()->user()->id, 'status' => 2]);

                            $checkData = Gift::where('gift_code', $request->get('gift_code'))->first();
                            if ($checkData->created_by == auth()->user()->id) {
                                $update = Gift::where('gift_code', $request->get('gift_code'))->update(['status' => 1]);
                                $getUser = User::where('id', $checkData->user_id)->first();

                                $time_deposited = date('Y-m-d');
                                $request->merge(['amount' => $checkData->amount, 'coin_type' => 'gift', 'deposit_approved_time' => $time_deposited, 'deposite_status' => 1, 'u_id' => $checkData->user_id]);
                                $FundDetail = FundDetail::create($request->all());
                                $id = User::where('id', $checkData->user_id)->first();
                                $balance = User::select('remain_balance')->where('id', $id->id)->first();
                                $total = $balance->remain_balance + $checkData->amount;
                                $refferby = User::where('id', $id->id)->first();
                                $refferby = $refferby->reff_by;
                                $amount = $checkData->amount;

                                for ($i = 1; $i <= 3; $i++) {
                                    $relativefirst = User::where('reff_key', $refferby)->first();
                                    if (!empty($refferby)) {
                                        if ($i == 3) {
                                            $percentage = 0.05;
                                        } else {
                                            $percentage = 0.1;
                                        }
                                        $withdraw = $relativefirst->withdraw_amount;
                                        $balance = $relativefirst->remain_balance;
                                        $financeleft = $relativefirst->financeleft;

                                        $finalWithamount = $withdraw + $amount * $percentage;
                                        $finalamount = $balance + $amount * $percentage;
                                        $finalfinanceleft = $financeleft + $amount * $percentage;

                                        $update = User::where('reff_key', $refferby)->update(['remain_balance' => $finalamount, 'withdraw_amount' => $finalWithamount, 'financeleft' => $finalfinanceleft]);
                                        $refferby = $relativefirst->reff_by;

                                    }
                                }
                                $system = User::where('id', $id->id)->first();
                                if (!empty($system->system_position)) {
                                    $endingLimit = $system->system_position - 3;
                                    $positionwise = User::where('system_position', '<', $system->system_position)->where('system_position', '>=', $endingLimit)->orderBy('system_position', 'DESC')->get();

                                    if (!empty($check = $positionwise->toArray())) {
                                        $cc = 1;
                                        foreach ($positionwise as $users) {
                                            switch ($cc) {
                                                case 1:
                                                    $percentage = 0.05;
                                                    $cc++;
                                                    break;
                                                case 2:
                                                    $percentage = 0.03;
                                                    $cc++;
                                                    break;
                                                case 3:
                                                    $percentage = 0.02;
                                                    $cc++;
                                                    break;
                                            }
                                            $withdraw = $users->withdraw_amount;
                                            $balance = $users->remain_balance;
                                            $financeright = $users->financeright;

                                            $finalWithamount = $withdraw + $amount * $percentage;
                                            $finalamount = $balance + $amount * $percentage;
                                            $finalfinanceright = $financeright + $amount * $percentage;
                                            $update = User::where('id', $users->id)->update(['remain_balance' => $finalamount, 'withdraw_amount' => $finalWithamount, 'financeright' => $finalfinanceright]);
                                        }
                                    }
                                }
                                $update_balance = User::where('id', $id->id)->update(['remain_balance' => $total]);
                                return ["error" => null, 'success' => '203'];
                            }
                            return ["error" => null, 'success' => '203'];
                        }
                        return ['error' => '303'];
                    }
                    return ['error' => '301'];
                }
                return ['error' => '409'];
            }
        }
        /*$FundDetail=FundDetail::create($request->all());

        return redirect()->route('deposite.index');*/
    }

//update
    public function depositeStore(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->get('amount'))) {
                if ($request->get('coin_type') == 'BTC') {
                    $btc = file_get_contents('https://blockchain.info/ticker');

                    $convert_amount = json_decode($btc)->USD->sell * $request->get('amount');
                    /*echo "<pre>";
                    print_r(json_decode($btc)->USD->sell);exit;*/
                    $request->merge(['amount' => $convert_amount, 'btc_admin' => $request->get('amount'), 'btc_usd' => json_decode($btc)->USD->sell]);
                }

                /*echo "<pre>";
                print_r($request->all());exit;*/
                $FundDetail = FundDetail::create($request->all());

                return ["error" => null, 'success' => '200'];
            } else {
                if ($request->get('coin_type') == 'gift') {
                    $check = Gift::where('gift_code', $request->get('gift_code'))->where('status', 0)->first();

                    if (!empty($check)) {
                        if ($check->created_by != auth()->user()->id) {
                            //assinged to user but need to approved by crater approvel 2 panding or need aprovel
                            $update = Gift::where('gift_code', $request->get('gift_code'))->where('status', 0)->update(['user_id' => auth()->user()->id, 'status' => 2]);

                            /*$getUser=User::where('id',auth()->user()->id)->first();

                            $remainig=$getUser->remain_balance + $check->amount;
                            $withdrawable= $getUser->withdraw_amount + $check->amount;
                            $userUpate=User::where('id',auth()->user()->id)->update(['remain_balance'=>$remainig,'withdraw_amount'=>$withdrawable]);*/
                            return ["error" => null, 'success' => '203'];
                        }
                        if ($check->created_by == auth()->user()->id) {
                            $update = Gift::where('gift_code', $request->get('gift_code'))->where('status', 0)->update(['user_id' => auth()->user()->id, 'status' => 2]);

                            $checkData = Gift::where('gift_code', $request->get('gift_code'))->first();
                            if ($checkData->created_by == auth()->user()->id) {
                                $update = Gift::where('gift_code', $request->get('gift_code'))->update(['status' => 1]);
                                $getUser = User::where('id', $checkData->user_id)->first();

                                $time_deposited = date('Y-m-d');
                                $request->merge(['amount' => $checkData->amount, 'coin_type' => 'gift', 'deposit_approved_time' => $time_deposited, 'deposite_status' => 1, 'u_id' => $checkData->user_id]);
                                $FundDetail = FundDetail::create($request->all());
                                $id = User::where('id', $checkData->user_id)->first();
//frozen update
                                $frozen = User::select('frozen')->where('id', $id->id)->first();
                                $newfrozen = $frozen->frozen + $checkData->amount;
                                User::where('id', $id->id)->update(['frozen' => $newfrozen]);

//                                $balance = User::select('remain_balance')->where('id', $id->id)->first();
//                                $total = $balance->remain_balance + $checkData->amount;
                                $refferby = User::where('id', $id->id)->first();
                                $refferby = $refferby->reff_by;
                                $amount = $checkData->amount;

                                for ($i = 1; $i <= 3; $i++) {
                                    $relativefirst = User::where('reff_key', $refferby)->first();
                                    if (!empty($refferby)) {
                                        if ($i == 3) {
                                            $percentage = 0.05;
                                        } else {
                                            $percentage = 0.1;
                                        }
//                                        $withdraw = $relativefirst->withdraw_amount;
//                                        $balance = $relativefirst->remain_balance;
                                        $financeleft = $relativefirst->financeleft;

//                                        $finalWithamount = $withdraw + $amount * $percentage;
//                                        $finalamount = $balance + $amount * $percentage;
                                        $finalfinanceleft = $financeleft + $amount * $percentage;

                                        $update = User::where('reff_key', $refferby)->update(['financeleft' => $finalfinanceleft]);
                                        $refferby = $relativefirst->reff_by;

                                    }
                                }
                                $system = User::where('id', $id->id)->first();
                                if (!empty($system->system_position)) {
                                    $endingLimit = $system->system_position - 3;
                                    $positionwise = User::where('system_position', '<', $system->system_position)->where('system_position', '>=', $endingLimit)->orderBy('system_position', 'DESC')->get();

                                    if (!empty($check = $positionwise->toArray())) {
                                        $cc = 1;
                                        foreach ($positionwise as $users) {
                                            switch ($cc) {
                                                case 1:
                                                    $percentage = 0.05;
                                                    $cc++;
                                                    break;
                                                case 2:
                                                    $percentage = 0.03;
                                                    $cc++;
                                                    break;
                                                case 3:
                                                    $percentage = 0.02;
                                                    $cc++;
                                                    break;
                                            }
//                                            $withdraw = $users->withdraw_amount;
//                                            $balance = $users->remain_balance;
                                            $financeright = $users->financeright;

//                                            $finalWithamount = $withdraw + $amount * $percentage;
//                                            $finalamount = $balance + $amount * $percentage;
                                            $finalfinanceright = $financeright + $amount * $percentage;
                                            $update = User::where('id', $users->id)->update(['financeright' => $finalfinanceright]);
                                        }
                                    }
                                }
//                                $update_balance = User::where('id', $id->id)->update(['remain_balance' => $total]);
                                return ["error" => null, 'success' => '203'];
                            }
                            return ["error" => null, 'success' => '203'];
                        }
                        return ['error' => '303'];
                    }
                    return ['error' => '301'];
                }
                return ['error' => '409'];
            }
        }
        /*$FundDetail=FundDetail::create($request->all());

        return redirect()->route('deposite.index');*/
    }

//need update
    public function depositeList(Request $request)
    {
        if ($request->session()->has('user')) {
            $id = (int)$request->session()->get('user');
        } else {
            $id = auth()->user()->id;
        }
        $model = FundDetail::where('u_id', $id)->orderBy('created_at', 'desc')
            ->get();
        $balancemodel = User::Balance($id);
        $allDeposit = FundDetail::deposit($id);

        $Balance = $balancemodel['deposit'] + $balancemodel['profit'] - $balancemodel['withdraw'];
        $alldeposit = $allDeposit['allDeposit'];
        $totalDeposit = $Balance + $alldeposit;

        return view('user.deposit.list')->withUsers($model)->withTotal($totalDeposit);
    }

//update
    public function withhDrawIndex(Request $request)
    {
        if ($request->session()->has('user')) {
            $id = (int)$request->session()->get('user');
        } else {
            $id = auth()->user()->id;
        }
        $model = WithDraw::where('is_withdraw', '!=', 0)->where('u_id', $id)->get();

        return view('user.withdraw.index')
            ->withUsers($model);
    }

//    public function withhDrawAjax(Request $request)
//    {
//        if ($request->ajax()) {
//
//            if (!empty($request->get('u_id')) && $request->get('withdraw_amount') && $request->get('is_withdraw')) {
//                $checkamount = User::where('id', $request->get('u_id')
//                    ->first();)
//                $checkamount1 = $checkamount->remain_balance;
//                $withdraw_amount = $checkamount->withdraw_amount;
//                /*echo "<pre>";
//                print_r($request->all());exit;*/
//                $commingAmount = $request->get('withdraw_amount');
//                if ($request->get('coin_type') == 'BTC') {
//                    $btc = file_get_contents('https://blockchain.info/ticker');
//
//                    /* $convert_amount= json_decode($btc)->USD->sell * $commingAmount;*/
//                    $convert_amount = $commingAmount;
//                } else {
//                    $convert_amount = $commingAmount;
//                }
//
//                if ($convert_amount <= $checkamount1) {
//
//                    if ($convert_amount <= $withdraw_amount) {
//                        if (!empty($request->get('acc_name')) && $request->get('usd_id') && $request->get('branch_code')) {
//
//                            $updateModel = $model = WithDraw::create($request->all());
//                            $update = [
//                                'acc_name' => $request->get('acc_name'),
//                                'acc_no' => $request->get('usd_id'),
//                                'branch_code' => $request->get('branch_code'),
//                                'acc_type' => $request->get('bank_type'),
//                                'swift_code' => $request->get('swift_code')
//                            ];
//                            $user = User::where('id', auth()->user()->id)->update($update);
//                            return ["error" => null, 'success' => '200'];
//                        }
//
//                        if (!empty($request->get('btc_id'))) {
//                            $update = [
//
//                                'btc_address' => $request->get('btc_id')
//                            ];
//                            $user = User::where('id', auth()->user()->id)->update($update);
//                            $model = WithDraw::create($request->all());
//                            return ["error" => null, 'success' => '200'];
//                        }
//                    } else {
//                        return ['error' => 405];
//
//                    }
//                } else {
//                    return ['error' => 408];
//
//                }
//
//                /*$model=FundDetail::where('id',$id)
//                ->update(['withdraw_status'=> $request->get('withdrawStatus'),
//                        'withdraw_id'=> $request->get('withdrawId')]);*/
//
//            } elseif (!empty($request->get('id')) && $request->get('withdrawStatus')) {
//                $id = $request->get('id');
//
//                $model = FundDetail::where('id', $id)->update(['withdraw_status' => $request->get('withdrawStatus')]);
//                return ["error" => null, 'success' => '200'];
//            } else {
//                return ['error' => 409];
//            }
//        }
//
//    }


    public function userProfitAjax(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->get('id')) && $request->get('coin_type') && $request->get('trans_id')) {
                $id = $request->get('id');

                $model = Profit::where('id', $id)->update(['withdraw_profit' => 2, 'profit_trans_id' => $request->get('trans_id'), 'coin_type' => $request->get('coin_type')]);
                $model0 = Profit::where('id', $id)->first();
                $amount = $model0->profit;
                $model1 = WithDraw::create(['is_withdraw' => 2, 'u_id' => $model0->u_id, 'profit_id' => $id,

                    'withdraw_trans_id' => $request->get('trans_id'), 'coin_type' => $request->get('coin_type'), 'withdraw_amount' => $amount]);
                return ["error" => null, 'success' => '200'];
            } else {
                return ['error' => 409];
            }
        }

    }

    public function profitIndex(Request $request)
    {
        if ($request->session()->has('user')) {
            $id = (int)$request->session()->get('user');
        } else {
            $id = auth()->user()->id;
        }

        $model = Profit::where('u_id', $id)->orderBy('created_at', 'desc')->get();

        $balancemodel = User::balance($id);

        $profit = $balancemodel['deposit'];
        /* echo "<pre>";
        print_r($id);exit;*/

        return view('user.profit.index')->withUsers($model)->withtProfit($profit);
    }

    public function newsletter(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'email' => ['required', 'string', 'email', 'max:255', 'unique:newsletter']
        ],
            [


                'email.unique' => 'You have Already been subscribed to our Newsletter',

            ]);

        if ($validator->fails()) {

            return redirect('/#newsletter')->withErrors($validator);
        }
        $update = NewsLetter::create(['email' => $request->get('email')]);
        $email = 'admin@acmtrader.com';
        $messagetext = 'Thank you for subscribing to our newsletter. We are looking forward to sharing great opportunities and products with you. Our newsletters will provide you with essential information relating to trades, affiliate marketing and keep you up to date with products that will come your way.';
        $email_to = $request->get('email');
        Mail::send('user.mail.newsletter', ['messagetext' => $messagetext], function ($message) use ($email, $email_to) {
            $message->from($email);
            $message->to($email_to);

            $message->subject('Subscribed ACM Newsletter');
        });

        return redirect('/#newsletter')->withSuccess('You Subscribed Successfully');

    }

    /*public function profitAjax(Request $request)
    {
      if($request->ajax())
        {
           if(!empty($request->get('id')) && $request->get('withdrawId') && $request->get('withdrawStatus'))
            {
                 $id=$request->get('id');

              $model=FundDetail::where('id',$id)
              ->update(['withdraw_status'=> $request->get('withdrawStatus'),
                        'withdraw_id'=> $request->get('withdrawId')]);
                return ["error" => null, 'success' => '200'];
            }
            elseif(!empty($request->get('id')) && $request->get('withdrawStatus'))
            {
                 $id=$request->get('id');

              $model=FundDetail::where('id',$id)
               ->update(['withdraw_status'=> $request->get('withdrawStatus')]);
                return ["error" => null, 'success' => '200'];
            }
            else
            {
                 return ['error' => 409];
            }
        }

    }*/

    public function contact(Request $request)
    {
        $post = $request->all();
        $email = 'admin@acmtrader.com';

        $messagetext = $post['name'] . ' with ' . $post['email'] . ' says you : ' . $post['message'];
        $subject = $post['name'] . ' Contact you';
        $email_to = "admin@acmtrader.com";
        Mail::send('user.mail.newsletter', ['messagetext' => $messagetext], function ($message) use ($email, $email_to, $subject) {
            $message->from($email);
            $message->to($email_to);

            $message->subject($subject);
        });

        /*$model = Contact::create($post);*/
        return redirect('/');
    }

    public function setting()
    {

        $model = User::where('id', auth()->user()->id)->first();
        return view('user.setting.index')->withUser($model);
    }

    public function settingStore(Request $request)
    {


        if ($request->ajax()) {
            if ($request->get('ecode') == 'send') {
                $hash = bin2hex(random_bytes(4));
                $expiresAt = Carbon::now()->addMinutes(5);
                $user = User::where('id', auth()->user()->id)->update(['pwd_code' => $hash]);
                $model = User::where('id', auth()->user()->id)->first();

                $email = 'admin@acmtrader.com';
                $messagetext = 'You are Welcome to Login';
                $email_to = $model->email;
                Mail::send('user.mail.new-password', ['messagetext' => $messagetext, 'link' => $hash], function ($message) use ($email, $email_to) {
                    $message->from($email);
                    $message->to($email_to);

                    $message->subject('Password Reset Code');
                });

                Cache::put('active' . $hash, true, $expiresAt);
                return ["error" => null, 'success' => '200'];

            }
            if ($request->get('ecode') == 'verify') {

                $val = Cache::has('active' . $request->get('ecodev'));
                $model = User::where('pwd_code', $request->get('ecodev'))->first();
                if (!empty($model->id) && $val) {

                    return ["error" => null, 'success' => '201'];
                } else {
                    return ["error" => 404, 'success' => false];
                }
                //$user=User::where('id',auth()->user()->id)->update(['pwd_code'=>$hash]);

                //return ["error" => null, 'success' => $val];
            }
            if ($request->get('ecode') == 'password') {

                $hashpass = Hash::make($request->get('ecodev'));
                $user = User::where('id', auth()->user()->id)->update(['password' => $hashpass]);

                return ["error" => null, 'success' => '201'];
            }

        }

        $notification = array(
            'message' => 'Your BTC Address Update Successfully',
            'alert-type' => 'success',
            'heading' => 'Successed',
        );

        $post = $request->except(['_token']);;
        $user = User::where('id', auth()->user()->id)->update($post);


        return redirect()->route('user.setting')->with($notification);


    }

    public function email(Request $request)
    {

        return view('user.email');
    }

    public function emailStore(Request $request)
    {
        $post = $request->all();
        //print_r($post['email']);exit;
        $model = User::where('email', $post['email'])->first();
        if (!empty($model->email)) {
            $model = User::where('email', $post['email'])->update(['email_status' => 2]);

            //Generate a random string.
            //Generate a random string.
            $token = openssl_random_pseudo_bytes(16);

            //Convert the binary data into hexadecimal representation.
            $token = bin2hex($token);
            $link = url(config('app.url') . '/login' . '?token=' . $token);
            $messagetext = 'You are Welcome to Login';
            $email = 'admin@acmtrader.com';
            $email_to = $post['email'];
            Mail::send('user.mail.registration-mail', ['messagetext' => $messagetext, 'link' => $link], function ($message) use ($email, $email_to) {
                $message->from($email);
                $message->to($email_to);

                $message->subject('Welcome');
            });
            $model = User::where('email', $post['email'])->update(['email_token' => $token]);

            $notification = array(
                'message' => 'Email verification Link Send to your Email',
                'alert-type' => 'success',
                'heading' => 'Successed',
            );

            return redirect()->route('user.email')->with($notification);


        } else {

            $notification = array(
                'message' => 'This Email is not Valid',
                'alert-type' => 'error',
                'heading' => 'Failed',
            );

            return redirect()->route('user.email')->with($notification);
            /*Session::flash('failed', 'This Email is not Valid');
              return redirect()->route('user.email')->with('failed', 'This Email is not Valid Try Again!');*/

        }


    }

    public function network(Request $request)
    {
        if ($request->session()->has('user')) {
            $currentId = (int)$request->session()->get('user');
        } else {
            $currentId = auth()->user()->id;
        }

        $data = User::where('id', $currentId)->first();
        $reffkey = $data->reff_key;
        $jsonarray = ['id' => 1, 'name' => $data->name, 'parent' => 0];
        $final[] = $jsonarray = ['id' => 1, 'name' => $data->name . " (Me)", 'parent' => 0];

        $datainner = User::where('reff_by', $reffkey)->get();

        $count = 2;
        $count1 = 2;

        if (!empty($check1 = $datainner->toArray())) {
            foreach ($datainner as $value) {

                $jsonarray = ['id' => $count, 'name' => $value->name, 'parent' => 1];

                $final[] = $jsonarray;

                $datainner1 = User::where('reff_by', $value->reff_key)
                    ->get();
                $parent = $count;
                $count++;

                if (!empty($check2 = $datainner1->toArray())) {
                    foreach ($datainner1 as $value1) {

                        $jsonarray = ['id' => $count, 'name' => $value1->name, 'parent' => $parent];
                        $final[] = $jsonarray;

                        $datainner2 = User::where('reff_by', $value1->reff_key)
                            ->get();

                        $parent2 = $count;
                        $count++;
                        if (!empty($datainner2->toArray())) {
                            foreach ($datainner2 as $value2) {
                                if ($count != $parent2) {
                                    $jsonarray = ['id' => $count, 'name' => $value2->name, 'parent' => $parent2];
                                    $final[] = $jsonarray;
                                    $count++;
                                }
                            }
                        }
                    }
                }
            }
        }
        if (!empty($data->system_position)) {
            $afterSystem = User::where('system_position', '>', $data->system_position)
                ->get();


            $aftercount = $count++;
            $parent = 1;

            if (!empty($check2 = $afterSystem->toArray())) {
                foreach ($afterSystem as $value7) {
                    $jsonarray = ['id' => $aftercount, 'name' => ucfirst($value7->name), 'level' => 1, 'parent' => $parent, 'parenta' => 1];
                    $final[] = $jsonarray;
                    $parent = $aftercount;
                    $aftercount++;
                }
            }
        }
        /*echo "<pre>";
        print_r($final);exit;
        print_r(json_encode($final,true));exit;*/

        return view('user.network.index')->withFinal($final);
    }

    public function markRead(request $request)
    {
        if ($request->ajax()) {

            if ($request->get('notifi')) {
                auth()->user()->unReadNotifications->markAsRead();
                return ["error" => null, 'success' => '200'];
            } else {
                return ['error' => 409];
            }
        }
    }

    public function about()
    {
        return view('about');
    }

    public function affi()
    {
        return view('aff');
    }


    public function giftList(Request $request)
    {
        if ($request->session()->has('user')) {
            $id = (int)$request->session()->get('user');

        } else {
            $id = auth()->user()->id;

        }
        $model = Gift::where('created_by', $id)->get();
        return view('user.gift.list')->withGifts($model)->withId($id);
    }

    public function soslist()
    {
        $model = Contact::where('user_id', auth()->user()->id)->get();
        return view('user.sos.index')->withUsers($model);
    }

    public function sosDetail($id)
    {
        $contact = Contact::where('id', $id)->first();
        $comments = Comments::where('contact_id', $id)->get();

        $latest = Comments::where('contact_id', $id)->where('u_id', 2)->where("admin_reply", 1)->latest('created_at')->update(['admin_reply' => 0]);
        return view('user.sos.detail')->withContact($contact)->withComments($comments);
    }

    public function sosDetailStore(Request $request)
    {
        $posterName = "";


        if (request()->hasFile('contact_pic1')) {

            /*echo "<pre>";
        print_r(request()->contact_pic->getClientOriginalExtension());exit; getClientOriginalName  getClientOriginalExtension*/

            $posterName = time() . '.' . request()->contact_pic1->getClientOriginalName();
            request()->contact_pic1->move(public_path('images/comment_pic'), $posterName);
        }

        $request->merge(['comment_pic' => $posterName]);

        $model = Comments::create($request->all());

        $notification = array(
            'message' => 'Message Send to Admin Successfully',
            'alert-type' => 'success',
            'heading' => 'Succeeded',
        );

        return redirect()->route('user.sos.detail', $request->get('contact_id'))->with($notification);
    }

    public function sos()
    {

        return view('user.sos.add');
    }

    public function sosStore(Request $request)
    {

        $validator = Validator::make($request->all(), [

            'contact_pic1' => ['mimes:jpeg,png,zip,pdf', 'max:2048'],

        ]);

        if ($validator->fails()) {
            $notification = array(
                'message' => 'File Must be jpeg,png,zip,pdf with in Size of 2MB',
                'alert-type' => 'error',
                'heading' => 'Failed',
            );
            return redirect()->route('user.sos.add')->with($notification);
        }
        $check = Contact::where('user_id', auth()->user()->id)->where('status', '!=', 1)->first();

        if (!empty($check)) {
            $notification = array(
                'message' => '1 Ticket at time for Resolve',
                'alert-type' => 'error',
                'heading' => 'Failed',
            );
            return redirect()->route('user.sos.list')->with($notification);
        }

        $post = $request->get('message');
        $request->merge(['name' => auth()->user()->name]);
        $request->merge(['user_id' => auth()->user()->id]);
        $request->merge(['email' => auth()->user()->email]);
        $request->merge(['phone_number' => '']);


        $posterName = "";


        if (request()->hasFile('contact_pic1')) {

            /*echo "<pre>";
        print_r(request()->contact_pic->getClientOriginalExtension());exit; getClientOriginalName  getClientOriginalExtension*/

            $posterName = time() . '.' . request()->contact_pic1->getClientOriginalName();
            request()->contact_pic1->move(public_path('images/contact_pic'), $posterName);
        }

        $request->merge(['contact_pic' => $posterName]);

        $model = Contact::create($request->all());

        $notification = array(
            'message' => 'Message Send to Admin Successfully',
            'alert-type' => 'success',
            'heading' => 'Succeeded',
        );
        return redirect()->route('user.sos.add')->with($notification);

    }

//update
    public function giftAdd()
    {
        $hash = bin2hex(random_bytes(16));
        /*$bytes = openssl_random_pseudo_bytes(32);
$hash = bin2hex($bytes);*/

        /* echo serialize($hash);
          exit;*/

//        $profit = User::where('id', auth()->user()->id)->first();
//        $amount = $profit->withdraw_amount;

        $user_vale = User::where('id', auth()->user()->id)->first();
        $exchangeProfit = Profit::where('u_id', auth()->user()->id)->sum('profit');
        $profit_withdrawed = WithDraw::where('u_id', auth()->user()->id)->where('coin_type', 'profit')->where('is_withdraw', 1)->sum('withdraw_amount');
        $usd_withdrawed = WithDraw::where('u_id', auth()->user()->id)->where('coin_type', 'usd')->where('is_withdraw', 1)->sum('withdraw_amount');

        $left_withdrawed = WithDraw::where('u_id', auth()->user()->id)->where('coin_type', 'left')->where('is_withdraw', 1)->sum('withdraw_amount');
        $right_withdrawed = WithDraw::where('u_id', auth()->user()->id)->where('coin_type', 'right')->where('is_withdraw', 1)->sum('withdraw_amount');

        $profit_withdraw_able = $exchangeProfit - $profit_withdrawed;
        $usd_withdraw_able = $user_vale->unfrozen - $usd_withdrawed;
        $left_withdraw_able = $user_vale->financeleft - $left_withdrawed;
        $right_withdraw_able = $user_vale->financeright - $right_withdrawed;


        /**
         * @category    Main Example - Custom Payment Box ((json, bootstrap4, mobile friendly, white label product, your own logo)
         * @package     GoUrl Cryptocurrency Payment API
         * copyright    (c) 2014-2020 Delta Consultants
         * @desc        GoUrl Crypto Payment Box Example (json, bootstrap4, mobile friendly, optional - free White Label Product - Bitcoin/altcoin Payments with your own logo and all payment requests through your server, open source)
         * @crypto      Supported Cryptocoins - Bitcoin, BitcoinCash, BitcoinSV, Litecoin, Dash, Dogecoin, Speedcoin, Reddcoin, Potcoin, Feathercoin, Vertcoin, Peercoin, MonetaryUnit, UniversalCurrency
         * @website     https://gourl.io/bitcoin-payment-gateway-api.html#p8
         * @live_demo   https://gourl.io/lib/examples/example_customize_box.php
         * @note    You can delete folders - 'Examples', 'Screenshots' from this archive
         */


        /********************** NOTE - 2018-2020 YEARS *************************************************************************/
        /*****                                                                                                             *****/
        /*****     This is NEW 2018-2020 latest Bitcoin Payment Box Example  (mobile friendly JSON payment box)            *****/
        /*****                                                                                                             *****/
        /*****     You can generate php payment box code online - https://gourl.io/lib/examples/example_customize_box.php  *****/
        /*****         White Label Product - https://gourl.io/lib/test/example_customize_box.php?method=curl&logo=custom   *****/
        /*****         Light Theme - https://gourl.io/lib/examples/example_customize_box.php?theme=black                   *****/
        /*****         Black Theme - https://gourl.io/lib/examples/example_customize_box.php?theme=default             *****/
        /*****         Your Own Logo - https://gourl.io/lib/examples/example_customize_box.php?theme=default&logo=custom   *****/
        /*****                                                                                                             *****/
        /***********************************************************************************************************************/


        DEFINE("CRYPTOBOX_PHP_FILES_PATH", app_path() . '/lib');

        // path to directory with files: cryptobox.class.php / cryptobox.callback.php / cryptobox.newpayment.php;
        // cryptobox.newpayment.php will be automatically call through ajax/php two times - payment received/confirmed
        DEFINE("CRYPTOBOX_IMG_FILES_PATH", asset("/images") . '/');      // path to directory with coin image files (directory 'images' by default)
        DEFINE("CRYPTOBOX_JS_FILES_PATH", asset("/cryptojs") . '/');            // path to directory with files: ajax.min.js/support.min.js

        // Change values below
        // --------------------------------------
        DEFINE("CRYPTOBOX_LANGUAGE_HTMLID", "alang");    // any value; customize - language selection list html id; change it to any other - for example 'aa';  default 'alang'
        DEFINE("CRYPTOBOX_COINS_HTMLID", "acoin");        // any value;  customize - coins selection list html id; change it to any other - for example 'bb';   default 'acoin'
        DEFINE("CRYPTOBOX_PREFIX_HTMLID", "acrypto_");    // any value; prefix for all html elements; change it to any other - for example 'cc';    default 'acrypto_'

        // Open Source Bitcoin Payment Library
        // ---------------------------------------------------------------
        //dd(CRYPTOBOX_PHP_FILES_PATH . "/cryptobox.class.php");

        require_once(CRYPTOBOX_PHP_FILES_PATH . "/cryptobox.class.php");


        /*********************************************************/
        /****  PAYMENT BOX CONFIGURATION VARIABLES  ****/
        /*********************************************************/

        // IMPORTANT: Please read description of options here - https://gourl.io/api-php.html#options

        $userID = auth()->user()->id;            // place your registered userID or md5(userID) here (user1, user7, uo43DC, etc).
        // You can use php $_SESSION["userABC"] for store userID, amount, etc
        // You don't need to use userID for unregistered website visitors - $userID = "";
        // if userID is empty, system will autogenerate userID and save it in cookies
        $userFormat = "COOKIE";     // save userID in cookies (or you can use IPADDRESS, SESSION, MANUAL)
        $orderID = "gift";           // invoice number - 000383
        $amountUSD = 0.01;         // invoice amount - 2.21 USD; or you can use - $amountUSD = convert_currency_live("EUR", "USD", 22.37); // convert 22.37EUR to USD

        $period = "1 MINUTE";   // one time payment, not expiry
        $def_language = "en";         // default Language in payment box
        $def_coin = "bitcoin";      // default Coin in payment box


        // List of coins that you accept for payments
        //$coins = array('bitcoin', 'bitcoincash', 'bitcoinsv', 'litecoin', 'dogecoin', 'dash', 'speedcoin', 'reddcoin', 'potcoin', 'feathercoin', 'vertcoin', 'peercoin', 'monetaryunit', 'universalcurrency');
        $coins = array('bitcoin');  // for example, accept payments in bitcoin, bitcoincash, litecoin, dash, speedcoin

        // Create record for each your coin - https://gourl.io/editrecord/coin_boxes/0 ; and get free gourl keys
        // It is not bitcoin wallet private keys! Place GoUrl Public/Private keys below for all coins which you accept

        $all_keys = array("bitcoin" => array("public_key" => "-your public key for Bitcoin box-", "private_key" => "-your private key for Bitcoin box-"),
            "bitcoincash" => array("public_key" => "-your public key for BitcoinCash box-", "private_key" => "-your private key for BitcoinCash box-"),
            "litecoin" => array("public_key" => "-your public key for Litecoin box-", "private_key" => "-your private key for Litecoin box-")); // etc.

        // Demo Keys; for tests (example - 5 coins)
        /* $all_keys = array(
                      "speedcoin" => array( "public_key" => "49401AACb3hFSpeedcoin77SPDPUB1hYbx8qxI8ORiyCheDccG",
                                        "private_key" => "49401AACb3hFSpeedcoin77SPDPRVl3NeOVA1ofQlTp5pozYB6")); */// Demo keys!
        $all_keys = array(
            "bitcoin" => array("public_key" => "50135AAiWAInBitcoin77BTCPUBuIS97PyiG1pptLIw5N3ekj6",
                "private_key" => "50135AAiWAInBitcoin77BTCPRVwNygkqzetvtaDlV1oNU5bYt"));

        //  IMPORTANT: Add in file /lib/cryptobox.config.php your database settings and your gourl.io coin private keys (need for Instant Payment Notifications) -
        /* if you use demo keys above, please add to /lib/cryptobox.config.php -
        $cryptobox_private_keys = array("25654AAo79c3Bitcoin77BTCPRV0JG7w3jg0Tc5Pfi34U8o5JE", "25678AACxnGODogecoin77DOGEPRVFvl6IDdisuWHVJLo5m4eq",
                    "25656AAeOGaPBitcoincash77BCHPRV8quZcxPwfEc93ArGB6D", "25657AAOwwzoLitecoin77LTCPRV7hmp8s3ew6pwgOMgxMq81F",
                    "25678AACxnGODogecoin77DOGEPRVFvl6IDdisuWHVJLo5m4eq", "25658AAo79c3Dash77DASHPRVG7w3jg0Tc5Pfi34U8o5JEiTss",
                    "20116AA36hi8Speedcoin77SPDPRVNOwjzYNqVn4Sn5XOwMI2c");
        Also create table "crypto_payments" in your database, sql code - https://github.com/cryptoapi/Payment-Gateway#mysql-table
        Instruction - https://gourl.io/api-php.html
    */

        // Re-test - all gourl public/private keys
        $def_coin = strtolower($def_coin);
        if (!in_array($def_coin, $coins)) $coins[] = $def_coin;
        foreach ($coins as $v) {
            if (!isset($all_keys[$v]["public_key"]) || !isset($all_keys[$v]["private_key"])) die("Please add your public/private keys for '$v' in \$all_keys variable");
            elseif (!strpos($all_keys[$v]["public_key"], "PUB")) die("Invalid public key for '$v' in \$all_keys variable");
            elseif (!strpos($all_keys[$v]["private_key"], "PRV")) die("Invalid private key for '$v' in \$all_keys variable");
            elseif (strpos(CRYPTOBOX_PRIVATE_KEYS, $all_keys[$v]["private_key"]) === false)
                die("Please add your private key for '$v' in variable \$cryptobox_private_keys, file /lib/cryptobox.config.php.");
        }


        // Current selected coin by user
        $coinName = cryptobox_selcoin($coins, $def_coin);


        // Current Coin public/private keys
        $public_key = $all_keys[$coinName]["public_key"];
        $private_key = $all_keys[$coinName]["private_key"];


        /** PAYMENT BOX **/
        $options = array(
            "public_key" => $public_key, // your public key from gourl.io
            "private_key" => $private_key,    // your private key from gourl.io
            "webdev_key" => "",          // optional, gourl affiliate key
            "orderID" => $orderID,        // order id or product name
            "userID" => $userID,         // unique identifier for every user
            "userFormat" => $userFormat,     // save userID in COOKIE, IPADDRESS, SESSION  or MANUAL
            "amount" => 0,           // product price in btc/bch/bsv/ltc/doge/etc OR setup price in USD below
            "amountUSD" => $amountUSD,  // we use product price in USD
            "period" => $period,         // payment valid period
            "language" => $def_language  // text on EN - english, FR - french, etc
        );

        // Initialise Payment Class
        $box = new Cryptobox ($options);

        // coin name
        $coinName = $box->coin_name();

        // php code end :)
        // ---------------------

        // NOW PLACE IN FILE "lib/cryptobox.newpayment.php", function cryptobox_new_payment(..) YOUR ACTIONS -
        // WHEN PAYMENT RECEIVED (update database, send confirmation email, update user membership, etc)
        // IPN function cryptobox_new_payment(..) will automatically appear for each new payment two times - payment received and payment confirmed
        // Read more - https://gourl.io/api-php.html#ipn
        /*echo "<pre>";
       print_r($box);exit;*/
        return view('user.gift.add', [
            'box' => $box,
            'coins' => $coins,
            'def_coin' => $def_coin,
            'def_language' => $def_language
        ])->withHash($hash)->withprofitwithdrawable($profit_withdraw_able)->withusdwithdrawable($usd_withdraw_able)->withleftwithdrawable($left_withdraw_able)->withrightwithdrawable($right_withdraw_able);
    }

//update
//withdraw
    public function withhDrawAjax(Request $request)
    {
        if ($request->ajax()) {
dd($request->ajax());
            $is_allowed_withdraw = WithDraw::where('u_id', auth()->user()->id)->where('is_withdraw','!=',1)->get()->toArray();
            if(sizeof($is_allowed_withdraw) != 0)
            {
                return ['error' => 1010];
            }

            if (!empty($request->get('u_id')) && $request->get('withdraw_amount') && $request->get('is_withdraw')) {

                $commingAmount = $request->get('withdraw_amount');
//                $checkamount = User::where('id', $request->get('u_id'))->first();
                $user_vale = User::where('id', $request->get('u_id'))->first();
                $exchangeProfit = Profit::where('u_id', $request->get('u_id'))->sum('profit');
                $profit_withdrawed = WithDraw::where('u_id', $request->get('u_id'))->where('coin_type', 'profit')->where('is_withdraw', 1)->sum('withdraw_amount');
                $usd_withdrawed = WithDraw::where('u_id', $request->get('u_id'))->where('coin_type', 'usd')->where('is_withdraw', 1)->sum('withdraw_amount');

                $left_withdrawed = WithDraw::where('u_id', $request->get('u_id'))->where('coin_type', 'left')->where('is_withdraw', 1)->sum('withdraw_amount');
                $right_withdrawed = WithDraw::where('u_id', $request->get('u_id'))->where('coin_type', 'right')->where('is_withdraw', 1)->sum('withdraw_amount');

                $profit_withdraw_able = $exchangeProfit - $profit_withdrawed;
                $usd_withdraw_able = $user_vale->unfrozen - $usd_withdrawed;
                $left_withdraw_able = $user_vale->financeleft - $left_withdrawed;
                $right_withdraw_able = $user_vale->financeright - $right_withdrawed;


                if ($request->get('coin_type') == 'right') {

                    if ($right_withdraw_able >= $commingAmount) {
                        $model = WithDraw::create($request->all());
                        return ["error" => null, 'success' => '200'];
                    } else {
                        return ['error' => 405];
                    }

                } elseIf ($request->get('coin_type') == 'left') {
                    if ($left_withdraw_able >= $commingAmount) {

                        $model = WithDraw::create($request->all());
                        return ["error" => null, 'success' => '200'];
                    } else {
                        return ['error' => 405];
                    }
                } elseif ($request->get('coin_type') == 'profit') {
                    if ($profit_withdraw_able >= $commingAmount) {

                        $model = WithDraw::create($request->all());

                        return ["error" => null, 'success' => '200'];
                    } else {
                        return ['error' => 405];
                    }
                } elseif ($request->get('coin_type') == 'usd') {
                    if ($usd_withdraw_able >= $commingAmount) {
                        $model = WithDraw::create($request->all());
                        return ["error" => null, 'success' => '200'];
                    } else {
                        return ['error' => 405];
                    }
                }
//                if (!empty($request->get('btc_id'))) {
//                    $update = [
//
//                        'btc_address' => $request->get('btc_id')
//                    ];
//                    $user = User::where('id', auth()->user()->id)->update($update);
//
//                    return ["error" => null, 'success' => '200'];
//                }

//                $checkamount1 = $checkamount->remain_balance;
//                $withdraw_amount = $checkamount->withdraw_amount;
                /*echo "<pre>";
                print_r($request->all());exit;*/
//                $commingAmount = $request->get('withdraw_amount');


//                if ($convert_amount <= $checkamount1) {
//                    if ($convert_amount <= $withdraw_amount) {
//
////                        if (!empty($request->get('acc_name')) && $request->get('usd_id') && $request->get('branch_code')) {
////
////                            $updateModel = $model = WithDraw::create($request->all());
////                            $update = [
////                                'acc_name' => $request->get('acc_name'),
////                                'acc_no' => $request->get('usd_id'),
////                                'branch_code' => $request->get('branch_code'),
////                                'acc_type' => $request->get('bank_type'),
////                                'swift_code' => $request->get('swift_code')
////                            ];
////                            $user = User::where('id', auth()->user()->id)->update($update);
////                            return ["error" => null, 'success' => '200'];
////                        }
//
//
//                    } else {
//                        return ['error' => 405];
//
//                    }
            } else {
                return ['error' => 408];

            }

            /*$model=FundDetail::where('id',$id)
            ->update(['withdraw_status'=> $request->get('withdrawStatus'),
                    'withdraw_id'=> $request->get('withdrawId')]);*/

//            } elseif (!empty($request->get('id')) && $request->get('withdrawStatus')) {
//                $id = $request->get('id');
//
//                $model = FundDetail::where('id', $id)->update(['withdraw_status' => $request->get('withdrawStatus')]);
//                return ["error" => null, 'success' => '200'];
//            } else {
//                return ['error' => 409];
        }
    }

    public function giftStore(Request $request)
    {

        $check = Gift::where('gift_code', $request->get('gift_code'))->first();


        if (!empty($check)) {

            return redirect()->route('user.gift.add');
        } else {

            if (!empty($request->get('u_id')) && $request->get('withdraw_amount') && $request->get('is_withdraw')) {
                $user_vale = User::where('id', auth()->user()->id)->first();

                $commingAmount = $request->get('withdraw_amount');
                $request->merge(['btc_id' => $user_vale->btc_address, 'amount' => $commingAmount, 'created_by' => auth()->user()->id]);


                $exchangeProfit = Profit::where('u_id', auth()->user()->id)->sum('profit');
                $profit_withdrawed = WithDraw::where('u_id', auth()->user()->id)->where('coin_type', 'profit')->where('is_withdraw', 1)->sum('withdraw_amount');
                $usd_withdrawed = WithDraw::where('u_id', auth()->user()->id)->where('coin_type', 'usd')->where('is_withdraw', 1)->sum('withdraw_amount');

                $left_withdrawed = WithDraw::where('u_id', auth()->user()->id)->where('coin_type', 'left')->where('is_withdraw', 1)->sum('withdraw_amount');
                $right_withdrawed = WithDraw::where('u_id', auth()->user()->id)->where('coin_type', 'right')->where('is_withdraw', 1)->sum('withdraw_amount');

                $profit_withdraw_able = $exchangeProfit - $profit_withdrawed;
                $usd_withdraw_able = $user_vale->unfrozen - $usd_withdrawed;
                $left_withdraw_able = $user_vale->financeleft - $left_withdrawed;
                $right_withdraw_able = $user_vale->financeright - $right_withdrawed;
                if ($request->get('coin_type') == 'right') {

                    if ($right_withdraw_able >= $commingAmount) {
                        WithDraw::create($request->all());
                        Gift::create($request->all());
                        return ["error" => null, 'success' => '200'];
                    } else {
                        return ['error' => 405];
                    }

                } elseIf ($request->get('coin_type') == 'left') {
                    if ($left_withdraw_able >= $commingAmount) {

                        WithDraw::create($request->all());
                        Gift::create($request->all());
                        return ["error" => null, 'success' => '200'];
                    } else {
                        return ['error' => 405];
                    }
                } elseif ($request->get('coin_type') == 'profit') {
                    if ($profit_withdraw_able >= $commingAmount) {

                        WithDraw::create($request->all());
                        Gift::create($request->all());

                        return ["error" => null, 'success' => '200'];
                    } else {
                        return ['error' => 405];
                    }
                } elseif ($request->get('coin_type') == 'usd') {
                    if ($usd_withdraw_able >= $commingAmount) {
                        WithDraw::create($request->all());
                        Gift::create($request->all());
                        return ["error" => null, 'success' => '200'];
                    } else {
                        return ['error' => 405];
                    }
                }

            } else {
                return ['error' => 408];

            }
//            var_dump($request->all());
//            exit();
////add btc id into table from users table
//            $post = $request->all();
//            $user = User::where('id', auth()->user()->id)->first();
//
////add to withdraw  stat table with one stats will auto withdraw form blance
//            $amount = $request->get('withdraw_amount');
//            $see = $user->withdraw_amount - $amount;
//            $remain = $user->remain_balance - $amount;
//
//            /*echo "<pre>";
//        print_r($amount);
//        echo "<pre>";
//        print_r($see);
//        echo "<pre>";
//        print_r($remain);*/
//
//            if ($see >= 0) {
//
//                $update = User::where('id', auth()->user()->id)->update(['withdraw_amount' => $see, 'remain_balance' => $remain]);
//                $create = Gift::create($post);
//                /*echo "<pre>";
//         print_r($post['amount']);exit;*/
//                $notification = array(
//                    'message' => 'Gift Created Successfully',
//                    'alert-type' => 'success',
//                    'heading' => 'Succeeded',
//                    'amount' => $post['amount'],
//                    'code' => $post['gift_code']
//                );
//                return redirect()->route('user.gift.list')->with($notification)->with($post['amount'])->withCode($post['gift_code']);
//            } else {
//                $notification = array(
//                    'message' => 'Amount is greater than Withdrawable',
//                    'alert-type' => 'error',
//                    'heading' => 'Failed',
//
//                );
//                return redirect()->route('user.gift.list')->with($notification);
//            }
//
//
//            return redirect()->route('user.gift.list');

        }
    }

    public function btcAjax(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->get('amount'))) {

                /**
                 * @category    Main Example - Custom Payment Box ((json, bootstrap4, mobile friendly, white label product, your own logo)
                 * @package     GoUrl Cryptocurrency Payment API
                 * copyright    (c) 2014-2020 Delta Consultants
                 * @desc        GoUrl Crypto Payment Box Example (json, bootstrap4, mobile friendly, optional - free White Label Product - Bitcoin/altcoin Payments with your own logo and all payment requests through your server, open source)
                 * @crypto      Supported Cryptocoins - Bitcoin, BitcoinCash, BitcoinSV, Litecoin, Dash, Dogecoin, Speedcoin, Reddcoin, Potcoin, Feathercoin, Vertcoin, Peercoin, MonetaryUnit, UniversalCurrency
                 * @website     https://gourl.io/bitcoin-payment-gateway-api.html#p8
                 * @live_demo   https://gourl.io/lib/examples/example_customize_box.php
                 * @note    You can delete folders - 'Examples', 'Screenshots' from this archive
                 */


                /********************** NOTE - 2018-2020 YEARS *************************************************************************/
                /*****                                                                                                             *****/
                /*****     This is NEW 2018-2020 latest Bitcoin Payment Box Example  (mobile friendly JSON payment box)            *****/
                /*****                                                                                                             *****/
                /*****     You can generate php payment box code online - https://gourl.io/lib/examples/example_customize_box.php  *****/
                /*****         White Label Product - https://gourl.io/lib/test/example_customize_box.php?method=curl&logo=custom   *****/
                /*****         Light Theme - https://gourl.io/lib/examples/example_customize_box.php?theme=black                   *****/
                /*****         Black Theme - https://gourl.io/lib/examples/example_customize_box.php?theme=default             *****/
                /*****         Your Own Logo - https://gourl.io/lib/examples/example_customize_box.php?theme=default&logo=custom   *****/
                /*****                                                                                                             *****/
                /***********************************************************************************************************************/


                // Change path to your files
                // --------------------------------------
                DEFINE("CRYPTOBOX_PHP_FILES_PATH", app_path() . '/lib');

                // path to directory with files: cryptobox.class.php / cryptobox.callback.php / cryptobox.newpayment.php;
                // cryptobox.newpayment.php will be automatically call through ajax/php two times - payment received/confirmed
                DEFINE("CRYPTOBOX_IMG_FILES_PATH", asset("/images") . '/');      // path to directory with coin image files (directory 'images' by default)
                DEFINE("CRYPTOBOX_JS_FILES_PATH", asset("/cryptojs") . '/');            // path to directory with files: ajax.min.js/support.min.js

                // Change values below
                // --------------------------------------
                DEFINE("CRYPTOBOX_LANGUAGE_HTMLID", "alang");    // any value; customize - language selection list html id; change it to any other - for example 'aa';  default 'alang'
                DEFINE("CRYPTOBOX_COINS_HTMLID", "acoin");        // any value;  customize - coins selection list html id; change it to any other - for example 'bb';   default 'acoin'
                DEFINE("CRYPTOBOX_PREFIX_HTMLID", "acrypto_");    // any value; prefix for all html elements; change it to any other - for example 'cc';    default 'acrypto_'

                // Open Source Bitcoin Payment Library
                // ---------------------------------------------------------------
                //dd(CRYPTOBOX_PHP_FILES_PATH . "/cryptobox.class.php");

                require_once(CRYPTOBOX_PHP_FILES_PATH . "/cryptobox.class.php");


                /*********************************************************/
                /****  PAYMENT BOX CONFIGURATION VARIABLES  ****/
                /*********************************************************/
                if ($request->get('type') == "gift") {
                    $amountusdbtc = $request->get('amount');
                } else {
                    $amountusdbtc = $request->get('amount') + $request->get('amount') * 0.1;
                    /*print_r($amountusdbtc);exit;*/

                }
                // IMPORTANT: Please read description of options here - https://gourl.io/api-php.html#options

                $userID = auth()->user()->id;            // place your registered userID or md5(userID) here (user1, user7, uo43DC, etc).
                // You can use php $_SESSION["userABC"] for store userID, amount, etc
                // You don't need to use userID for unregistered website visitors - $userID = "";
                // if userID is empty, system will autogenerate userID and save it in cookies
                $userFormat = "COOKIE";     // save userID in cookies (or you can use IPADDRESS, SESSION, MANUAL)
                $orderID = $request->get('type');           // invoice number - 000383
                $amountUSD = $amountusdbtc;         // invoice amount - 2.21 USD; or you can use - $amountUSD = convert_currency_live("EUR", "USD", 22.37); // convert 22.37EUR to USD

                $period = "1 MINUTE";   // one time payment, not expiry
                $def_language = "en";         // default Language in payment box
                $def_coin = "bitcoin";      // default Coin in payment box


                // List of coins that you accept for payments
                //$coins = array('bitcoin', 'bitcoincash', 'bitcoinsv', 'litecoin', 'dogecoin', 'dash', 'speedcoin', 'reddcoin', 'potcoin', 'feathercoin', 'vertcoin', 'peercoin', 'monetaryunit', 'universalcurrency');
                $coins = array('bitcoin');  // for example, accept payments in bitcoin, bitcoincash, litecoin, dash, speedcoin

                // Create record for each your coin - https://gourl.io/editrecord/coin_boxes/0 ; and get free gourl keys
                // It is not bitcoin wallet private keys! Place GoUrl Public/Private keys below for all coins which you accept

                $all_keys = array("bitcoin" => array("public_key" => "-your public key for Bitcoin box-", "private_key" => "-your private key for Bitcoin box-"),
                    "bitcoincash" => array("public_key" => "-your public key for BitcoinCash box-", "private_key" => "-your private key for BitcoinCash box-"),
                    "litecoin" => array("public_key" => "-your public key for Litecoin box-", "private_key" => "-your private key for Litecoin box-")); // etc.

                // Demo Keys; for tests (example - 5 coins)
                /*$all_keys = array(
                      "speedcoin" => array( "public_key" => "49401AACb3hFSpeedcoin77SPDPUB1hYbx8qxI8ORiyCheDccG",
                                        "private_key" => "49401AACb3hFSpeedcoin77SPDPRVl3NeOVA1ofQlTp5pozYB6"));*/ // Demo keys!


                $all_keys = array("bitcoin" => array("public_key" => "50135AAiWAInBitcoin77BTCPUBuIS97PyiG1pptLIw5N3ekj6",
                    "private_key" => "50135AAiWAInBitcoin77BTCPRVwNygkqzetvtaDlV1oNU5bYt"));

                //  IMPORTANT: Add in file /lib/cryptobox.config.php your database settings and your gourl.io coin private keys (need for Instant Payment Notifications) -
                /* if you use demo keys above, please add to /lib/cryptobox.config.php -
        $cryptobox_private_keys = array("25654AAo79c3Bitcoin77BTCPRV0JG7w3jg0Tc5Pfi34U8o5JE", "25678AACxnGODogecoin77DOGEPRVFvl6IDdisuWHVJLo5m4eq",
                    "25656AAeOGaPBitcoincash77BCHPRV8quZcxPwfEc93ArGB6D", "25657AAOwwzoLitecoin77LTCPRV7hmp8s3ew6pwgOMgxMq81F",
                    "25678AACxnGODogecoin77DOGEPRVFvl6IDdisuWHVJLo5m4eq", "25658AAo79c3Dash77DASHPRVG7w3jg0Tc5Pfi34U8o5JEiTss",
                    "20116AA36hi8Speedcoin77SPDPRVNOwjzYNqVn4Sn5XOwMI2c");
        Also create table "crypto_payments" in your database, sql code - https://github.com/cryptoapi/Payment-Gateway#mysql-table
        Instruction - https://gourl.io/api-php.html
    */

                // Re-test - all gourl public/private keys
                $def_coin = strtolower($def_coin);
                if (!in_array($def_coin, $coins)) $coins[] = $def_coin;
                foreach ($coins as $v) {
                    if (!isset($all_keys[$v]["public_key"]) || !isset($all_keys[$v]["private_key"])) die("Please add your public/private keys for '$v' in \$all_keys variable");
                    elseif (!strpos($all_keys[$v]["public_key"], "PUB")) die("Invalid public key for '$v' in \$all_keys variable");
                    elseif (!strpos($all_keys[$v]["private_key"], "PRV")) die("Invalid private key for '$v' in \$all_keys variable");
                    elseif (strpos(CRYPTOBOX_PRIVATE_KEYS, $all_keys[$v]["private_key"]) === false)
                        die("Please add your private key for '$v' in variable \$cryptobox_private_keys, file /lib/cryptobox.config.php.");
                }


                // Current selected coin by user
                $coinName = cryptobox_selcoin($coins, $def_coin);


                // Current Coin public/private keys
                $public_key = $all_keys[$coinName]["public_key"];
                $private_key = $all_keys[$coinName]["private_key"];


                /** PAYMENT BOX **/
                $options = array(
                    "public_key" => $public_key, // your public key from gourl.io
                    "private_key" => $private_key,    // your private key from gourl.io
                    "webdev_key" => "",          // optional, gourl affiliate key
                    "orderID" => $orderID,        // order id or product name
                    "userID" => $userID,         // unique identifier for every user
                    "userFormat" => $userFormat,     // save userID in COOKIE, IPADDRESS, SESSION  or MANUAL
                    "amount" => 0,           // product price in btc/bch/bsv/ltc/doge/etc OR setup price in USD below
                    "amountUSD" => $amountUSD,  // we use product price in USD
                    "period" => $period,         // payment valid period
                    "language" => $def_language  // text on EN - english, FR - french, etc
                );

                // Initialise Payment Class
                $box = new Cryptobox ($options);

                // coin name
                $coinName = $box->coin_name();

                // php code end :)
                // ---------------------

                // NOW PLACE IN FILE "lib/cryptobox.newpayment.php", function cryptobox_new_payment(..) YOUR ACTIONS -
                // WHEN PAYMENT RECEIVED (update database, send confirmation email, update user membership, etc)
                // IPN function cryptobox_new_payment(..) will automatically appear for each new payment two times - payment received and payment confirmed
                // Read more - https://gourl.io/api-php.html#ipn
                /*$data=print_r($box->display_cryptobox_bootstrap($coins, $def_coin, $def_language, null, 200, 200, false, "default", "default", 250, "", "ajax", false));*/
                $html = $box->display_cryptobox_bootstrap($coins, $def_coin, $def_language, null, 200, 200, false, "default", "default", 250, "", "ajax", false);

                $myVar = htmlspecialchars($html, ENT_QUOTES);
                $check = htmlentities($html);
                return ["error" => null, 'success' => '200', 'box' => $box,
                    'coins' => $coins,
                    'def_coin' => $def_coin,
                    'def_language' => $def_language, 'data' => $box->display_cryptobox_bootstrap($coins, $def_coin, $def_language, null, 200, 200, false, "default", "default", 250, "", "ajax", false)];
                /*return response()->json(['success' => 'ok','box' => $box,
            'coins' => $coins,
            'def_coin' => $def_coin,
            'def_language' => $def_language]);*/

            }
        }
    }

    public function btcAjaxCash(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->get('amount'))) {

//    if($request->get('type')=="cash")
//    {
//        $amountusdbtc= $request->get('amount


                $check = Gift::where('gift_code', $request->get('gift_code'))->first();


                if (!empty($check)) {
                    //                  code is already exist
                    return redirect()->route('user.gift.add');
                } else {
                    $request->merge(['created_by' => auth()->user()->id, "status" => '0']);

                    $post = $request->all();
                    $create = Gift::create($post);

                    $notification = array(
                        'message' => 'Gift Created Successfully',
                        'alert-type' => 'success',
                        'heading' => 'Succeeded',
                        'amount' => $post['amount'],
                        'code' => $post['gift_code']
                    );
                    return $notification;
//                    $user = User::where('id', auth()->user()->id)->first();

//
//                    $amount = $request->get('amount');
//                    $see = $user->withdraw_amount - $amount;
//                    $remain = $user->remain_balance - $amount;

                    /*echo "<pre>";
                    print_r($amount);
                    echo "<pre>";
                    print_r($see);
                    echo "<pre>";
                    print_r($remain);*/

//                    if ($see >= 0) {
//
//                        $update = User::where('id', auth()->user()->id)->update(['withdraw_amount' => $see, 'remain_balance' => $remain]);
//                        $create = Gift::create($post);
//                        /*echo "<pre>";
//                        print_r($post['amount']);exit;*/
//                        $notification = array(
//                            'message' => 'Gift Created Successfully',
//                            'alert-type' => 'success',
//                            'heading' => 'Succeeded',
//                            'amount' => $post['amount'],
//                            'code' => $post['gift_code']
//                        );
//                        return redirect()->route('user.gift.list')->with($notification)->with($post['amount'])->withCode($post['gift_code']);
//                    } else {
//                        $notification = array(
//                            'message' => 'Amount is greater than Withdrawable',
//                            'alert-type' => 'error',
//                            'heading' => 'Failed',
//
//                        );
//                        return redirect()->route('user.gift.list')->with($notification);
//                    }


                    return redirect()->route('user.gift.list');

                }


//    return ["error" => null, 'success' => '200','box' => $box,
//            'coins' => $coins,
//            'def_coin' => $def_coin,
//            'def_language' => $def_language,'data'=>$box->display_cryptobox_bootstrap($coins, $def_coin, $def_language, null, 200, 200, false, "default", "default", 250, "", "ajax", false)];
                /*return response()->json(['success' => 'ok','box' => $box,
            'coins' => $coins,
            'def_coin' => $def_coin,
            'def_language' => $def_language]);*/

            } else {
                alert('please file the required filed');
            }

        }
    }

    public function btcApi(Request $request)
    {
        //app_token pin_code  email
        $validator = Validator::make($request->all(), ['type' => ['required', 'string'], 'amount' => ['required', "numeric"], 'app_token' => ['required', 'string']

        ]);

        if ($validator->fails()) {
            return response()
                ->json(['error' => $validator->errors()], 401);
        }

        $user1 = User::where('app_token', $request->get('app_token'))
            ->first();
        if (!empty($user1)) {
            if ($request->get('type') == "gift" || $request->get('type') == "deposit") {

                $chekaa = $this->guard()
                    ->login($user1);
                $user = Auth::user();
                if (!empty($request->get('amount'))) {

                    /**
                     * @category    Main Example - Custom Payment Box ((json, bootstrap4, mobile friendly, white label product, your own logo)
                     * @package     GoUrl Cryptocurrency Payment API
                     * copyright    (c) 2014-2020 Delta Consultants
                     * @desc        GoUrl Crypto Payment Box Example (json, bootstrap4, mobile friendly, optional - free White Label Product - Bitcoin/altcoin Payments with your own logo and all payment requests through your server, open source)
                     * @crypto      Supported Cryptocoins - Bitcoin, BitcoinCash, BitcoinSV, Litecoin, Dash, Dogecoin, Speedcoin, Reddcoin, Potcoin, Feathercoin, Vertcoin, Peercoin, MonetaryUnit, UniversalCurrency
                     * @website     https://gourl.io/bitcoin-payment-gateway-api.html#p8
                     * @live_demo   https://gourl.io/lib/examples/example_customize_box.php
                     * @note    You can delete folders - 'Examples', 'Screenshots' from this archive
                     */

                    /********************** NOTE - 2018-2020 YEARS *************************************************************************/
                    /*****                                                                                                             *****/
                    /*****     This is NEW 2018-2020 latest Bitcoin Payment Box Example  (mobile friendly JSON payment box)            *****/
                    /*****                                                                                                             *****/
                    /*****     You can generate php payment box code online - https://gourl.io/lib/examples/example_customize_box.php  *****/
                    /*****         White Label Product - https://gourl.io/lib/test/example_customize_box.php?method=curl&logo=custom   *****/
                    /*****         Light Theme - https://gourl.io/lib/examples/example_customize_box.php?theme=black                   *****/
                    /*****         Black Theme - https://gourl.io/lib/examples/example_customize_box.php?theme=default             *****/
                    /*****         Your Own Logo - https://gourl.io/lib/examples/example_customize_box.php?theme=default&logo=custom   *****/
                    /*****                                                                                                             *****/
                    /***********************************************************************************************************************/

                    // Change path to your files
                    // --------------------------------------
                    DEFINE("CRYPTOBOX_PHP_FILES_PATH", app_path() . '/lib');

                    // path to directory with files: cryptobox.class.php / cryptobox.callback.php / cryptobox.newpayment.php;
                    // cryptobox.newpayment.php will be automatically call through ajax/php two times - payment received/confirmed
                    DEFINE("CRYPTOBOX_IMG_FILES_PATH", asset("/images") . '/'); // path to directory with coin image files (directory 'images' by default)
                    DEFINE("CRYPTOBOX_JS_FILES_PATH", asset("/cryptojs") . '/'); // path to directory with files: ajax.min.js/support.min.js
                    // Change values below
                    // --------------------------------------
                    DEFINE("CRYPTOBOX_LANGUAGE_HTMLID", "alang"); // any value; customize - language selection list html id; change it to any other - for example 'aa';  default 'alang'
                    DEFINE("CRYPTOBOX_COINS_HTMLID", "acoin"); // any value;  customize - coins selection list html id; change it to any other - for example 'bb';   default 'acoin'
                    DEFINE("CRYPTOBOX_PREFIX_HTMLID", "acrypto_"); // any value; prefix for all html elements; change it to any other - for example 'cc';    default 'acrypto_'
                    // Open Source Bitcoin Payment Library
                    // ---------------------------------------------------------------
                    //dd(CRYPTOBOX_PHP_FILES_PATH . "/cryptobox.class.php");
                    require_once(CRYPTOBOX_PHP_FILES_PATH . "/cryptobox.class.php");

                    /*********************************************************/
                    /****  PAYMENT BOX CONFIGURATION VARIABLES  ****/
                    /*********************************************************/
                    if ($request->get('type') == "gift") {
                        $amountusdbtc = $request->get('amount');
                    } else {
                        $amountusdbtc = $request->get('amount') + $request->get('amount') * 0.1;
                        /*print_r($amountusdbtc);exit;*/

                    }
                    // IMPORTANT: Please read description of options here - https://gourl.io/api-php.html#options
                    $userID = auth()->user()->id; // place your registered userID or md5(userID) here (user1, user7, uo43DC, etc).
                    // You can use php $_SESSION["userABC"] for store userID, amount, etc
                    // You don't need to use userID for unregistered website visitors - $userID = "";
                    // if userID is empty, system will autogenerate userID and save it in cookies
                    $userFormat = "COOKIE"; // save userID in cookies (or you can use IPADDRESS, SESSION, MANUAL)
                    $orderID = $request->get('type'); // invoice number - 000383
                    $amountUSD = $amountusdbtc; // invoice amount - 2.21 USD; or you can use - $amountUSD = convert_currency_live("EUR", "USD", 22.37); // convert 22.37EUR to USD
                    $period = "1 MINUTE"; // one time payment, not expiry
                    $def_language = "en"; // default Language in payment box
                    $def_coin = "bitcoin"; // default Coin in payment box


                    // List of coins that you accept for payments
                    //$coins = array('bitcoin', 'bitcoincash', 'bitcoinsv', 'litecoin', 'dogecoin', 'dash', 'speedcoin', 'reddcoin', 'potcoin', 'feathercoin', 'vertcoin', 'peercoin', 'monetaryunit', 'universalcurrency');
                    $coins = array(
                        'bitcoin'
                    ); // for example, accept payments in bitcoin, bitcoincash, litecoin, dash, speedcoin
                    // Create record for each your coin - https://gourl.io/editrecord/coin_boxes/0 ; and get free gourl keys
                    // It is not bitcoin wallet private keys! Place GoUrl Public/Private keys below for all coins which you accept
                    $all_keys = array(
                        "bitcoin" => array(
                            "public_key" => "-your public key for Bitcoin box-",
                            "private_key" => "-your private key for Bitcoin box-"
                        ),
                        "bitcoincash" => array(
                            "public_key" => "-your public key for BitcoinCash box-",
                            "private_key" => "-your private key for BitcoinCash box-"
                        ),
                        "litecoin" => array(
                            "public_key" => "-your public key for Litecoin box-",
                            "private_key" => "-your private key for Litecoin box-"
                        )
                    ); // etc.
                    // Demo Keys; for tests (example - 5 coins)
                    /*$all_keys = array(
                                  "speedcoin" => array( "public_key" => "49401AACb3hFSpeedcoin77SPDPUB1hYbx8qxI8ORiyCheDccG",
                                                    "private_key" => "49401AACb3hFSpeedcoin77SPDPRVl3NeOVA1ofQlTp5pozYB6"));*/
                    // Demo keys!


                    $all_keys = array(
                        "bitcoin" => array(
                            "public_key" => "50135AAiWAInBitcoin77BTCPUBuIS97PyiG1pptLIw5N3ekj6",
                            "private_key" => "50135AAiWAInBitcoin77BTCPRVwNygkqzetvtaDlV1oNU5bYt"
                        )
                    );

                    //  IMPORTANT: Add in file /lib/cryptobox.config.php your database settings and your gourl.io coin private keys (need for Instant Payment Notifications) -
                    /* if you use demo keys above, please add to /lib/cryptobox.config.php -
                    $cryptobox_private_keys = array("25654AAo79c3Bitcoin77BTCPRV0JG7w3jg0Tc5Pfi34U8o5JE", "25678AACxnGODogecoin77DOGEPRVFvl6IDdisuWHVJLo5m4eq",
                                "25656AAeOGaPBitcoincash77BCHPRV8quZcxPwfEc93ArGB6D", "25657AAOwwzoLitecoin77LTCPRV7hmp8s3ew6pwgOMgxMq81F",
                                "25678AACxnGODogecoin77DOGEPRVFvl6IDdisuWHVJLo5m4eq", "25658AAo79c3Dash77DASHPRVG7w3jg0Tc5Pfi34U8o5JEiTss",
                                "20116AA36hi8Speedcoin77SPDPRVNOwjzYNqVn4Sn5XOwMI2c");
                    Also create table "crypto_payments" in your database, sql code - https://github.com/cryptoapi/Payment-Gateway#mysql-table
                    Instruction - https://gourl.io/api-php.html
                    */

                    // Re-test - all gourl public/private keys
                    $def_coin = strtolower($def_coin);
                    if (!in_array($def_coin, $coins)) $coins[] = $def_coin;
                    foreach ($coins as $v) {
                        if (!isset($all_keys[$v]["public_key"]) || !isset($all_keys[$v]["private_key"])) die("Please add your public/private keys for '$v' in \$all_keys variable");
                        elseif (!strpos($all_keys[$v]["public_key"], "PUB")) die("Invalid public key for '$v' in \$all_keys variable");
                        elseif (!strpos($all_keys[$v]["private_key"], "PRV")) die("Invalid private key for '$v' in \$all_keys variable");
                        elseif (strpos(CRYPTOBOX_PRIVATE_KEYS, $all_keys[$v]["private_key"]) === false) die("Please add your private key for '$v' in variable \$cryptobox_private_keys, file /lib/cryptobox.config.php.");
                    }

                    // Current selected coin by user
                    $coinName = cryptobox_selcoin($coins, $def_coin);

                    // Current Coin public/private keys
                    $public_key = $all_keys[$coinName]["public_key"];
                    $private_key = $all_keys[$coinName]["private_key"];

                    /** PAYMENT BOX **/
                    $options = array(
                        "public_key" => $public_key, // your public key from gourl.io
                        "private_key" => $private_key, // your private key from gourl.io
                        "webdev_key" => "", // optional, gourl affiliate key
                        "orderID" => $orderID, // order id or product name
                        "userID" => $userID, // unique identifier for every user
                        "userFormat" => $userFormat, // save userID in COOKIE, IPADDRESS, SESSION  or MANUAL
                        "amount" => 0, // product price in btc/bch/bsv/ltc/doge/etc OR setup price in USD below
                        "amountUSD" => $amountUSD, // we use product price in USD
                        "period" => $period, // payment valid period
                        "language" => $def_language
                        // text on EN - english, FR - french, etc

                    );

                    // Initialise Payment Class
                    $box = new Cryptobox($options);

                    // coin name
                    $coinName = $box->coin_name();

                    // php code end :)
                    // ---------------------
                    // NOW PLACE IN FILE "lib/cryptobox.newpayment.php", function cryptobox_new_payment(..) YOUR ACTIONS -
                    // WHEN PAYMENT RECEIVED (update database, send confirmation email, update user membership, etc)
                    // IPN function cryptobox_new_payment(..) will automatically appear for each new payment two times - payment received and payment confirmed
                    // Read more - https://gourl.io/api-php.html#ipn
                    /*$data=print_r($box->display_cryptobox_bootstrap($coins, $def_coin, $def_language, null, 200, 200, false, "default", "default", 250, "", "ajax", false));



                    $box->display_cryptobox_bootstrap($coins, $def_coin, $def_language, null, 200, 200, false, "default", "default", 250, "", "ajax", false)



                    */
                    $html = $box->display_cryptobox_bootstrap($coins, $def_coin, $def_language, null, 200, 200, false, "default", "default", 250, "", "ajax", false);

                    $myVar = htmlspecialchars($html, ENT_QUOTES);
                    $check = htmlentities($html);


                    /*$DD = json_decode(file_get_contents($box->cryptobox_json_url()) , true);*/
                    $arrContextOptions = array(
                        "ssl" => array(
                            "verify_peer" => false,
                            "verify_peer_name" => false,
                        ),
                    );
                    $DD = json_decode(file_get_contents($box->cryptobox_json_url(), false, stream_context_create($arrContextOptions)), true);
                    $link = $box->cryptobox_json_url();

                    return ["error" => null, 'success' => '200', 'box' => $box, 'coins' => $coins, 'def_coin' => $def_coin, 'def_language' => $def_language, 'data' => $DD, "link" => $link];
                    /*return response()->json(['success' => 'ok','box' => $box,
                        'coins' => $coins,
                        'def_coin' => $def_coin,
                        'def_language' => $def_language]);*/
                } else {
                    return response()->json(['error' => 'Amount Not Provided'], 401);

                }
            } else {
                return response()
                    ->json(['error' => 'Type Must be gift or deposit'], 401);

            }
        } else {
            return response()
                ->json(['error' => 'Unauthorised'], 401);

        }
    }

    public function giftCodeAPi(Request $request)
    {
        $user1 = User::where('app_token', $request->get('app_token'))->first();


        if (!empty($user1)) {

            $hash = bin2hex(random_bytes(16));
            return response()->json(['success' => 'ok', 'code' => $hash]);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }

    }

    public function giftAddAPi(Request $request)
    {
        $user1 = User::where('app_token', $request->get('app_token'))->first();


        if (!empty($user1)) {
            $chekaa = $this->guard()->login($user1);

            $userData = User::where('id', auth()->user()->id)->first();
            $id = auth()->user()->id;

            $user = Auth::user();
            $check = Gift::where('gift_code', $request->get('gift_code'))->first();


            if (!empty($check)) {
                return response()->json(['error' => 'Gif Code Already Used'], 401);
            } else {


                $request->merge(['created_by' => $user->id]);

                $post = $request->all();
                $user = User::select('withdraw_amount')->where('id', $user->id)->first();


                $amount = $request->get('amount');

                if (!empty($amount)) {
                    $see = $user->withdraw_amount - $amount;
                    $remain = $user->remain_balance - $amount;

                    if ($see >= 0) {

                        $update = User::where('id', $user->id)->update(['withdraw_amount' => $see, 'remain_balance' => $remain]);
                        $create = Gift::create($post);
                        return response()->json(['success' => 'ok']);
                    }
                    return response()->json(['error' => 'Not sufficicent Profit'], 401);
                } else {
                    return response()->json(['error' => 'Amount Not Provided'], 401);
                }


            }
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);

        }
    }
//update
    public function giftAjax(Request $request)
    {
        if ($request->ajax()) {

            if (!empty($request->get('id'))) {
                $checkData = Gift::where('id', $request->get('id'))->first();

                if ($checkData->created_by == auth()->user()->id) {
                    $update = Gift::where('id', $request->get('id'))->update(['status' => 1]);
                    $getUser = User::where('id', $checkData->user_id)->first();

                    $time_deposited = date('Y-m-d');
                    $request->merge(['amount' => $checkData->amount, 'coin_type' => 'gift', 'deposit_approved_time' => $time_deposited, 'deposite_status' => 1, 'u_id' => $checkData->user_id,'deposite_initial_status'=>0]);


                    $FundDetail = FundDetail::create($request->all());

//
//                    $id = User::where('id', $checkData->user_id)->first();
//                    $balance = User::select('remain_balance')->where('id', $id->id)->first();
//                    $total = $balance->remain_balance + $checkData->amount;
                    //using person
                    $id = User::where('id', $checkData->user_id)->first();
                    $frozen = User::select('frozen')->where('id', $id->id)->first();
                    $newfrozen = $frozen->frozen + $checkData->amount;
                    User::where('id', $id->id)->update([ 'frozen' => $newfrozen]);

                    $refferby = User::where('id', $id->id)->first();
                    $refferby = $refferby->reff_by;
                    $amount = $checkData->amount;

                    for ($i = 1; $i <= 3; $i++) {
                        $relativefirst = User::where('reff_key', $refferby)->first();
                        if (!empty($refferby)) {
                            if ($i == 3) {
                                $percentage = 0.05;
                            } else {
                                $percentage = 0.1;
                            }
//                            $withdraw = $relativefirst->withdraw_amount;
//                            $balance = $relativefirst->remain_balance;
                            $financeleft = $relativefirst->financeleft;

//                            $finalWithamount = $withdraw + $amount * $percentage;
//                            $finalamount = $balance + $amount * $percentage;
                            $finalfinanceleft = $financeleft + $amount * $percentage;

                            $update = User::where('reff_key', $refferby)->update(['financeleft' => $finalfinanceleft]);
                            $refferby = $relativefirst->reff_by;
                            /*echo "<pre>";
                        print_r($relativefirst);
                        print_r($i); */
                        }
                    }
                    /////   systems  person gave profit
                    $system = User::where('id', $id->id)->first();
                    if (!empty($system->system_position)) {
                        $endingLimit = $system->system_position - 3;
                        $positionwise = User::where('system_position', '<', $system->system_position)->where('system_position', '>=', $endingLimit)->orderBy('system_position', 'DESC')->get();

                        if (!empty($check = $positionwise->toArray())) {
                            $cc = 1;
                            foreach ($positionwise as $users) {
                                switch ($cc) {
                                    case 1:
                                        $percentage = 0.05;
                                        $cc++;
                                        break;
                                    case 2:
                                        $percentage = 0.03;
                                        $cc++;
                                        break;
                                    case 3:
                                        $percentage = 0.02;
                                        $cc++;
                                        break;
                                }
//                                $withdraw = $users->withdraw_amount;
//                                $balance = $users->remain_balance;
                                $financeright = $users->financeright;

//                                $finalWithamount = $withdraw + $amount * $percentage;
//                                $finalamount = $balance + $amount * $percentage;
                                $finalfinanceright = $financeright + $amount * $percentage;
                                $update = User::where('id', $users->id)->update(['financeright' => $finalfinanceright]);
                            }
                        }
                    }


//                    $update_balance = User::where('id', $id->id)->update(['remain_balance' => $total]);


                    /*$remainig=$getUser->remain_balance + $checkData->amount;
                            $withdrawable= $getUser->withdraw_amount + $checkData->amount;
                            $userUpate=User::where('id',$checkData->user_id)->update(['remain_balance'=>$remainig,'withdraw_amount'=>$withdrawable]);*/
                    return ["error" => null, 'success' => '200'];
                }
            }
        }
    }


    public function giftVerifyApi(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'gift_id' => 'exists:gifts,id'


        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $user1 = User::where('app_token', $request->get('app_token'))->first();


        if (!empty($user1)) {
            $chekaa = $this->guard()->login($user1);

            $userData = User::where('id', auth()->user()->id)->first();
            $id = auth()->user()->id;

            if (!empty($request->get('gift_id'))) {
                $user = Auth::user();
                $checkData = Gift::where('id', $request->get('gift_id'))->first();
                if ($checkData->created_by == $user->id) {
                    $update = Gift::where('id', $request->get('gift_id'))->update(['status' => 1]);
                    $getUser = User::where('id', $checkData->user_id)->first();
                    $time_deposited = date('Y-m-d');
                    $request->merge(['amount' => $checkData->amount, 'coin_type' => 'gift', 'deposit_approved_time' => $time_deposited, 'deposite_status' => 1, 'u_id' => $checkData->user_id]);


                    $FundDetail = FundDetail::create($request->all());


                    $id = User::where('id', $checkData->user_id)->first();
                    $balance = User::select('remain_balance')->where('id', $id->id)->first();
                    $total = $balance->remain_balance + $checkData->amount;


                    $refferby = User::where('id', $id->id)->first();
                    $refferby = $refferby->reff_by;
                    $amount = $checkData->amount;

                    for ($i = 1; $i <= 3; $i++) {
                        $relativefirst = User::where('reff_key', $refferby)->first();
                        if (!empty($refferby)) {
                            if ($i == 3) {
                                $percentage = 0.05;
                            } else {
                                $percentage = 0.1;
                            }
                            $withdraw = $relativefirst->withdraw_amount;
                            $balance = $relativefirst->remain_balance;
                            $financeleft = $relativefirst->financeleft;

                            $finalWithamount = $withdraw + $amount * $percentage;
                            $finalamount = $balance + $amount * $percentage;
                            $finalfinanceleft = $financeleft + $amount * $percentage;

                            $update = User::where('reff_key', $refferby)->update(['remain_balance' => $finalamount, 'withdraw_amount' => $finalWithamount, 'financeleft' => $finalfinanceleft]);
                            $refferby = $relativefirst->reff_by;
                            /*echo "<pre>";
                        print_r($relativefirst);
                        print_r($i); */
                        }
                    }
                    /////   systems  person gave profit
                    $system = User::where('id', $id->id)->first();
                    if (!empty($system->system_position)) {
                        $endingLimit = $system->system_position - 3;
                        $positionwise = User::where('system_position', '<', $system->system_position)->where('system_position', '>=', $endingLimit)->orderBy('system_position', 'DESC')->get();

                        if (!empty($check = $positionwise->toArray())) {
                            $cc = 1;
                            foreach ($positionwise as $users) {
                                switch ($cc) {
                                    case 1:
                                        $percentage = 0.05;
                                        $cc++;
                                        break;
                                    case 2:
                                        $percentage = 0.03;
                                        $cc++;
                                        break;
                                    case 3:
                                        $percentage = 0.02;
                                        $cc++;
                                        break;
                                }
                                $withdraw = $users->withdraw_amount;
                                $balance = $users->remain_balance;
                                $financeright = $users->financeright;

                                $finalWithamount = $withdraw + $amount * $percentage;
                                $finalamount = $balance + $amount * $percentage;
                                $finalfinanceright = $financeright + $amount * $percentage;
                                $update = User::where('id', $users->id)->update(['remain_balance' => $finalamount, 'withdraw_amount' => $finalWithamount, 'financeright' => $finalfinanceright]);
                            }
                        }
                    }

                    $update_balance = User::where('id', $id->id)->update(['remain_balance' => $total]);

                    /*  $remainig=$getUser->remain_balance + $checkData->amount;
                            $withdrawable= $getUser->withdraw_amount + $checkData->amount;
                            $userUpate=User::where('id',$checkData->user_id)->update(['remain_balance'=>$remainig,'withdraw_amount'=>$withdrawable]);*/
                    return ["error" => null, 'success' => '200'];
                }
                return ['error' => 'You are not that person who created Gift code'];
            }
        }
        return ['error' => "Unauthorised"];
    }

    public function giftUseApi(Request $request)
    {
        $user1 = User::where('app_token', $request->get('app_token'))->first();


        if (!empty($user1)) {
            $chekaa = $this->guard()->login($user1);

            $userData = User::where('id', auth()->user()->id)->first();
            $id = auth()->user()->id;
            if ($request->get('coin_type') == 'gift') {

                $gift_code = $request->get("gift_code");
                $check = Gift::where("gift_code", $gift_code)->where('status', 0)->first();


                if (!empty($check)) {
                    $user = Auth::user();
                    if ($check->created_by != $user->id) {
                        $update = Gift::where('gift_code', $request->get('gift_code'))->where('status', 0)->update(['user_id' => $user->id, 'status' => 2]);

                        /*$getUser=User::where('id',auth()->user()->id)->first();

                        $remainig=$getUser->remain_balance + $check->amount;
                        $withdrawable= $getUser->withdraw_amount + $check->amount;
                        $userUpate=User::where('id',auth()->user()->id)->update(['remain_balance'=>$remainig,'withdraw_amount'=>$withdrawable]);*/
                        return ["error" => null, 'success' => '200'];
                    }
                    if ($check->created_by == auth()->user()->id) {
                        $update = Gift::where('gift_code', $request->get('gift_code'))->where('status', 0)->update(['user_id' => auth()->user()->id, 'status' => 2]);

                        $checkData = Gift::where('gift_code', $request->get('gift_code'))->first();
                        if ($checkData->created_by == auth()->user()->id) {
                            $update = Gift::where('gift_code', $request->get('gift_code'))->update(['status' => 1]);
                            $getUser = User::where('id', $checkData->user_id)->first();

                            $time_deposited = date('Y-m-d');
                            $request->merge(['amount' => $checkData->amount, 'coin_type' => 'gift', 'deposit_approved_time' => $time_deposited, 'deposite_status' => 1, 'u_id' => $checkData->user_id]);
                            $FundDetail = FundDetail::create($request->all());
                            $id = User::where('id', $checkData->user_id)->first();
                            $balance = User::select('remain_balance')->where('id', $id->id)->first();
                            $total = $balance->remain_balance + $checkData->amount;
                            $refferby = User::where('id', $id->id)->first();
                            $refferby = $refferby->reff_by;
                            $amount = $checkData->amount;

                            for ($i = 1; $i <= 3; $i++) {
                                $relativefirst = User::where('reff_key', $refferby)->first();
                                if (!empty($refferby)) {
                                    if ($i == 3) {
                                        $percentage = 0.05;
                                    } else {
                                        $percentage = 0.1;
                                    }
                                    $withdraw = $relativefirst->withdraw_amount;
                                    $balance = $relativefirst->remain_balance;
                                    $financeleft = $relativefirst->financeleft;

                                    $finalWithamount = $withdraw + $amount * $percentage;
                                    $finalamount = $balance + $amount * $percentage;
                                    $finalfinanceleft = $financeleft + $amount * $percentage;

                                    $update = User::where('reff_key', $refferby)->update(['remain_balance' => $finalamount, 'withdraw_amount' => $finalWithamount, 'financeleft' => $finalfinanceleft]);
                                    $refferby = $relativefirst->reff_by;

                                }
                            }
                            $system = User::where('id', $id->id)->first();
                            if (!empty($system->system_position)) {
                                $endingLimit = $system->system_position - 3;
                                $positionwise = User::where('system_position', '<', $system->system_position)->where('system_position', '>=', $endingLimit)->orderBy('system_position', 'DESC')->get();

                                if (!empty($check = $positionwise->toArray())) {
                                    $cc = 1;
                                    foreach ($positionwise as $users) {
                                        switch ($cc) {
                                            case 1:
                                                $percentage = 0.05;
                                                $cc++;
                                                break;
                                            case 2:
                                                $percentage = 0.03;
                                                $cc++;
                                                break;
                                            case 3:
                                                $percentage = 0.02;
                                                $cc++;
                                                break;
                                        }
                                        $withdraw = $users->withdraw_amount;
                                        $balance = $users->remain_balance;
                                        $financeright = $users->financeright;

                                        $finalWithamount = $withdraw + $amount * $percentage;
                                        $finalamount = $balance + $amount * $percentage;
                                        $finalfinanceright = $financeright + $amount * $percentage;
                                        $update = User::where('id', $users->id)->update(['remain_balance' => $finalamount, 'withdraw_amount' => $finalWithamount, 'financeright' => $finalfinanceright]);
                                    }
                                }
                            }
                            $update_balance = User::where('id', $id->id)->update(['remain_balance' => $total]);
                            return ["error" => null, 'success' => '203'];
                        }
                        return ["error" => null, 'success' => '203'];
                    }
                    return ['error' => 'same person not used Gift Code'];
                }
                return ['error' => "Code Not Verified From System"];
            }
            return ['error' => "coin type not provided or mismatch"];
        }
        return ['error' => "Unauthorised"];
    }


    public function createdGiftListApi(Request $request)
    {
        $user1 = User::where('app_token', $request->get('app_token'))->first();


        if (!empty($user1)) {
            $chekaa = $this->guard()->login($user1);

            $userData = User::where('id', auth()->user()->id)->first();
            $id = auth()->user()->id;
            $user = Auth::user();

            $model = Gift::where('created_by', $user->id)->orWhere('user_id', $user->id)->get();
            return response()->json(['success' => 'ok', 'data' => $model]);
        }
        return ['error' => "Unauthorised"];

    }

    public function getGiftListApi(Request $request)
    {
        $user1 = User::where('app_token', $request->get('app_token'))->first();


        if (!empty($user1)) {
            $chekaa = $this->guard()->login($user1);

            $userData = User::where('id', auth()->user()->id)->first();
            $id = auth()->user()->id;
            $user = Auth::user();

            $model = Gift::where('user_id', $user->id)->get();

            if (!empty($model)) {
                return response()->json(['success' => 'ok', 'data' => $model]);
            }
            return response()->json(['success' => 'ok', 'data' => 'no list']);

        }
        return ['error' => "Unauthorised"];

    }







//////////////---------------- All Mobile Api -----------/////////////////////


//////////////---------------- LoginApi -----------/////////////////////
    public function loginApi(Request $request)
    {

        /*echo "<pre>";
    print_r($request->all());exit;*/


        if (Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')])) {

            if ($request->get('app_token')) {

                $user = Auth::user();
                if ($user->email_status == 1) {
                    $update = User::where('id', $user->id)->update(['app_token' => $request->get('app_token')]);
                    $user = User::where('id', $user->id)->get();
                    return response()->json(['success' => 'ok', 'data' => $user]);
                } elseif ($user->email_status == 2) {
                    return response()->json(['error' => 'Check Your Email to verify'], 401);
                } else {
                    return response()->json(['error' => 'Email not verified'], 401);

                }

            } else {
                return response()->json(['error' => 'App Token Not Provided'], 401);
            }

            /* $success['token'] =  $user->createToken('MyApp')-> accessToken; */

        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }

    }

    public function logoutApi(Request $request)
    {


        $user = User::where('app_token', $request->get('app_token'))->first();
        if (!empty($user)) {
            $chekaa = Auth::guard()->login($user);


            $user = Auth::user();
            $user->app_token = null;
            $user->save();
            return response()->json(['success' => 'ok', 'data' => $user]);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function referralApi(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'app_token' => ['required'],

        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $user1 = User::where('app_token', $request->get('app_token'))->first();
        if (!empty($user1)) {
            $chekaa = Auth::guard()->login($user1);
            $user = Auth::user();
            $link = url('/') . 'register/?id=' . $user->reff_key;
            return response()->json(['success' => 'ok', 'referralkey' => $link]);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }

    }


//////////////---------------- Register  Api -----------/////////////////////
    public function regApi(Request $request)
    {
        // name email password , password_confirmation  btc_address
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'btc_address' => ['required', 'string']

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }


        if (!empty($request->get('ref_key'))) {
            $check = User::where('reff_key', $request->get('ref_key'))->first();
            $ref_key = $check->reff_key;
            $person_id = $check->id;
            $levelcheck = $check->level;

            if ($ref_key) {
                $reff_f = Hash::make($request->get('password'));
                $reff_b = $ref_key;
                $system_position = null;
                $level = ++$levelcheck;
            } else {
                $reff_f = Hash::make($request->get('password'));
                $reff_b = '';
                $system_position = null;
                $level = 1;
            }
        } else {
            $reff_f = Hash::make($request->get('password'));
            $reff_b = '';
            $system_position = User::where('admin', 2)->where('system_position', '!=', null)->get()->count();
            $system_position = ++$system_position;
            $level = 1;
        }

        $token = openssl_random_pseudo_bytes(16);

        //Convert the binary data into hexadecimal representation.
        $token = bin2hex($token);
        $link = url(config('app.url') . '/login' . '?token=' . $token);
        $messagetext = 'You are Welcome to Login';
        $email = 'admin@acmtrader.com';
        $email_to = $request->get('email');
        Mail::send('user.mail.registration-mail', ['messagetext' => $messagetext, 'link' => $link], function ($message) use ($email, $email_to) {
            $message->from($email);
            $message->to($email_to);

            $message->subject('Welcome');
        });

        User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'reff_key' => $reff_f,
            'reff_by' => $reff_b,
            'system_position' => $system_position,
            'level' => $level,
            'btc_address' => $request->get('btc_address'),
            'acc_name' => '',
            'acc_no' => '',
            'branch_code' => '',
            'acc_type' => '',
            'swift_code' => '',
            'email_status' => 2,
            'email_token' => $token

        ]);

        return response()->json(['success' => 'ok', 'info' => 'Email Verification Link send to Your Provided Email']);


    }


//////////////---------------- Get Admin Coin Data  -----------/////////////////////


    public function adminCoin(Request $request)
    {
        $user = User::where('app_token', $request->get('app_token'))->first();


        if (!empty($user)) {
            $chekaa = $this->guard()->login($user);

            $userData = User::where('id', auth()->user()->id)->first();
            $id = auth()->user()->id;
            $coin = AdminCoin::where('is_active', 1)->first();

            if (empty($coin)) {
                $coin = 'Coin detail not provided by Admin';
                return response()->json(['error' => 401, 'data' => $coin]);
            }

            return response()->json(['success' => 'ok', 'data' => $coin]);

        } else {

            return response()->json(['error' => 'Not autehnticate User ']);
        }
    }


//////////////----------------deposit Sore Api  -----------/////////////////////
    public function depositSoreApi(Request $request)
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password'), 'app_token' => request('app_token')])) {
            if ($request->get('amount')) {
                if ($request->get('coin_type') == 'BTC') {
                    $btc = file_get_contents('https://blockchain.info/ticker');

                    $convert_amount = json_decode($btc)->USD->sell * $request->get('amount');
                    /*echo "<pre>";
                        print_r(json_decode($btc)->USD->sell);exit;*/
                    $request->merge(['amount' => $convert_amount, 'btc_admin' => $request->get('amount'), 'btc_usd' => json_decode($btc)->USD->sell]);
                }

                /*echo "<pre>";
                    print_r($request->all());exit;*/
                $FundDetail = FundDetail::create($request->all());

                return ["error" => null, 'success' => '200'];
            } else {

                return response()->json(['error' => 404]);

            }
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);

        }

    }


//////////////----------------withdraw Api-----------/////////////////////
    public function withdrawApi(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'app_token' => ['required', 'string'],
            'is_withdraw' => ['required', 'numeric', "in:2"],
            'u_id' => ['required', 'numeric'],
            'withdraw_amount' => ['required', 'numeric'],
            'btc_id' => ['required', 'string'],
            'coin_type' => ['required', "in:BTC"],

        ]);


        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $user1 = User::where('app_token', $request->get('app_token'))->first();
        if (!empty($user1)) {
            $chekaa = $this->guard()->login($user1);
            $user = Auth::user();

            if (!empty($request->get('u_id')) && $request->get('withdraw_amount') && $request->get('is_withdraw')) {
                $checkamount = User::where('id', $request->get('u_id'))
                    ->first();
                $checkamount1 = $checkamount->remain_balance;
                $withdraw_amount = $checkamount->withdraw_amount;
                /*echo "<pre>";
                print_r($request->all());exit;*/
                $commingAmount = $request->get('withdraw_amount');
                if ($request->get('coin_type') == 'BTC') {
                    $btc = file_get_contents('https://blockchain.info/ticker');

                    $convert_amount = json_decode($btc)->USD->sell * $commingAmount;
                } else {
                    $convert_amount = $commingAmount;
                }

                if ($convert_amount <= $checkamount1) {

                    if ($convert_amount <= $withdraw_amount) {
                        /*if (!empty($request->get('acc_name')) && $request->get('usd_id') && $request->get('branch_code'))
                        {

                            $updateModel = $model = WithDraw::create($request->all());
                            $update=[
                                'acc_name'=>$request->get('acc_name'),
                                'acc_no'=>$request->get('usd_id'),
                                'branch_code'=>$request->get('branch_code'),
                                'acc_type'=>$request->get('bank_type'),
                                'swift_code'=>$request->get('swift_code')
                            ];
                            $user=User::where('id',auth()-> user()->id)->update($update);
                            return ["error" => null, 'success' => '200'];
                        }*/

                        if (!empty($request->get('btc_id'))) {
                            $update = [

                                'btc_address' => $request->get('btc_id')
                            ];
                            $user = User::where('id', auth()->user()->id)->update($update);
                            $model = WithDraw::create($request->all());
                            return ["error" => null, 'success' => '200'];
                        }
                    } else {
                        return ['error' => 405];

                    }
                } else {
                    return ['error' => 408];

                }

                /*$model=FundDetail::where('id',$id)
                ->update(['withdraw_status'=> $request->get('withdrawStatus'),
                        'withdraw_id'=> $request->get('withdrawId')]);*/

            } elseif (!empty($request->get('id')) && $request->get('withdrawStatus')) {
                $id = $request->get('id');

                $model = FundDetail::where('id', $id)->update(['withdraw_status' => $request->get('withdrawStatus')]);
                return ["error" => null, 'success' => '200'];
            } else {
                return ['error' => 409];
            }
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function emailApi(Request $request)
    {
        $post = $request->all();
        /*print_r($post['email']);exit;*/
        $model = User::where('email', $post['email'])->first();
        if (!empty($model->email)) {
            $model = User::where('email', $post['email'])->update(['email_status' => 2]);

            //Generate a random string.
            //Generate a random string.
            $token = openssl_random_pseudo_bytes(16);

            //Convert the binary data into hexadecimal representation.
            $token = bin2hex($token);
            $link = url(config('app.url') . '/login' . '?token=' . $token);
            $messagetext = 'You are Welcome to Login';
            $email = 'admin@acmtrader.com';
            $email_to = $post['email'];
            Mail::send('user.mail.registration-mail', ['messagetext' => $messagetext, 'link' => $link], function ($message) use ($email, $email_to) {
                $message->from($email);
                $message->to($email_to);

                $message->subject('Welcome');
            });
            $model = User::where('email', $post['email'])->update(['email_token' => $token]);


            return response()->json(['success' => 'ok', 'email' => 'email send']);
        } else {
            return ['error' => 'Not A system User'];
        }


    }

    public function transactionListApi(Request $request)
    {
        $user = User::where('app_token', $request->get('app_token'))->first();

        if (!empty($user)) {
            $chekaa = $this->guard()->login($user);
            if (!empty($request->get('app_token'))) {
                $userData = User::where('id', auth()->user()->id)->first();
                $id = auth()->user()->id;

                $model = FundDetail::where('u_id', $id)->where('coin_type', 'BTC')->get()->toArray();
                $balancemodel = User::Balance();
                $allDeposit = FundDetail::deposit();

                $Balance = $balancemodel['deposit'] + $balancemodel['profit'] - $balancemodel['withdraw'];
                $alldeposit = $allDeposit['allDeposit'];
                $totalDeposit = $Balance + $alldeposit;

                $is_withdraw = WithDraw::where('is_withdraw', '!=', 0)->where('u_id', $id)->get()->toArray();
                $check = [];
                foreach ($is_withdraw as $key => $value) {

                    $check[] = array_merge($value, ['type' => 'withdraw']);


                }

                $model1 = [];
                foreach ($model as $key => $value) {

                    $model1[] = array_merge($value, ['type' => 'deposit']);


                }

                $profit = Profit::where('u_id', $id)->get()->toArray();

                $data = array_merge($check, $model1);

                $sortedArr = collect($data)->sortByDesc('created_at')->values();
                /*echo "<pre>";
          print_r($sortedArr);exit;*/


                return response()->json(['success' => 'ok', 'allTransaction' => $sortedArr]);
            } else {
                return ['error' => 'app_token not ptovided'];
            }
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function depositeListApi(Request $request)
    {
        if (Auth::attempt(['app_token' => request('app_token')])) {
            if (!empty($request->get('app_token'))) {
                $userData = User::where('app_token', $request->get('app_token'))->first();
                $id = $userData->id;
                $model = FundDetail::where('u_id', $id)->get();
                $balancemodel = User::Balance();
                $allDeposit = FundDetail::deposit();

                $Balance = $balancemodel['deposit'] + $balancemodel['profit'] - $balancemodel['withdraw'];
                $alldeposit = $allDeposit['allDeposit'];
                $totalDeposit = $Balance + $alldeposit;

                return response()->json(['success' => 'ok', 'totalDeposit' => $totalDeposit, 'depositelist' => $model]);
            } else {
                return ['error' => 'app_token not ptovided'];
            }
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function withhDrawListApi(Request $request)
    {
        $user = User::where('app_token', $request->get('app_token'))->first();
        if (!empty($user)) {
            $chekaa = $this->guard()->login($user);
            if (!empty($request->get('app_token'))) {
                $userData = User::where('id', auth()->user()->id)->first();
                $id = auth()->user()->id;
                $model = WithDraw::where('is_withdraw', '!=', 0)->where('u_id', $id)->get();
                return response()->json(['success' => 'ok', 'withhDrawList' => $model]);
            } else {
                return ['error' => 'app_token not ptovided'];
            }
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }


    public function profitListApi(Request $request)
    {
        $user = User::where('app_token', $request->get('app_token'))->first();

        if (!empty($user)) {
            $chekaa = $this->guard()->login($user);


            if (!empty($request->get('app_token'))) {

                $userData = User::where('id', auth()->user()->id)->first();
                $id = auth()->user()->id;

                $model = Profit::where('u_id', $id)->get();
                $balancemodel = User::balance();
                //$profit=$balancemodel['deposit'] +$balancemodel['profit'];
                $profit = $balancemodel['deposit'];

                return response()->json(['success' => 'ok', 'profitList' => $model, 'balanceDeposite' => $profit]);


            } else {
                return ['error' => 'app_token not ptovided'];
            }
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function homeIndexApi(Request $request)
    {
        $user1 = User::where('app_token', $request->get('app_token'))->first();

        if (!empty($user1)) {
            $chekaa = $this->guard()->login($user1);


            if (!empty($request->get('app_token'))) {
                $user = Auth::user();
                $userData = User::where('id', $user->id)->first();
                $id = $user->id;


                $model = User::where('id', $id)->first();
                $exchangeInvested = FundDetail::where('u_id', $id)->where('deposite_status', 1)->sum('amount');
                $exchangeProfit = Profit::where('u_id', $id)->sum('profit');
                $exchangeShow = $exchangeInvested;
                $balance = $model->remain_balance;


                $withdraw2 = Withdraw::where('u_id', $id)->where('is_withdraw', 1)->sum('withdraw_amount');

                $minus = $withdraw2 - ($model->financeleft + $model->financeright);
                if ($minus <= 0) {
                    $minus = 0;
                }

                $exchange2ndtime = $exchangeShow + $exchangeProfit - $minus;
                /*echo "<pre>";
            print_r($balance);exit;*/

                $startDate = date('Y-m-d');
                $endDate = date('Y-m-d', strtotime("-60 day", strtotime($startDate)));
                $validWithdraw = FundDetail::where('u_id', $id)->where('deposite_status', 1)->where('deposit_approved_time', '<=', $endDate)->where('deposite_initial_status', 0)->get();
                $array = FundDetail::where('u_id', $id)->where('deposite_status', 1)->where('deposit_approved_time', '<=', $endDate)->where('deposite_initial_status', 0)->get()->toArray();

                $withdrawUpdate = User::where('id', $id)->first();

                if (sizeof($array) == 0) {

                } else {
                    foreach ($validWithdraw as $user) {

                        $finalamount = $withdrawUpdate->withdraw_amount + $user->amount;
                        $withdrawUpdate = User::where('id', $id)->update(['withdraw_amount' => $finalamount]);

                        $updateDeposit = FundDetail::where('id', $user->id)->update(['deposite_initial_status' => 1]);
                    }


                }


                $startDate = date('Y-m-d');
                $tprofit = Profit::where('u_id', $id)->sum('profit');
                $todayProfit = Profit::where('u_id', $id)->first();
                $todayProfit = Profit::where('u_id', $id)->where('profit_time', $startDate)->sum('profit');
                $todayProfit = (int)$todayProfit;


                $withdraw = Withdraw::where('u_id', $id)->first();
                $withdraw = Withdraw::where('u_id', $id)->where('is_withdraw', 1)->sum('withdraw_amount');
                $withdraw = (int)$withdraw;

                $fund = FundDetail::where('u_id', $id)->where('deposite_status', 1)->sum('amount');
                if ($balance > 0) {
                    $total_percent = $tprofit / $balance * 100;
                } else {
                    $total_percent = 0;
                }


                //print_r($total_percent);exit;


                $weekly = Weekly::where('is_active', 1)->first();

                if (!empty($weekly)) {
                    /* $model=date('Y-m-d');
                 $weekly=date('Y-m-d',strtotime("-1 day",strtotime((string)$model)));
                $endDate= date('Y-m-d',strtotime("-7 day",strtotime((string)$model)));
                 //print_r($endDate);exit;
                $profitLastWeekAvgPercent=Profit::where('u_id',$id)->whereBetween('profit_time',[$endDate,$weekly])->sum('percentage');
                $profitLastWeekCount=Profit::where('u_id',$id)->whereBetween('profit_time',[$endDate,$weekly])->count();

                if($profitLastWeekCount<=0)
                {
                  $profitLastWeekCount=1;
                }
                $totalAvgAmount=$profitLastWeekAvgPercent /$profitLastWeekCount;*/
                    $model = date('Y-m-d');
                    $daily = date('Y-m-d', strtotime("0 day", strtotime((string)$model)));
                    $profitLastDayPercent = Profit::where('u_id', $id)->where('profit_time', '=', $daily)->first();

                    if (empty($profitLastDayPercent)) {
                        $totalAvgAmount = 0;
                    } else {
                        $totalAvgAmount = $profitLastDayPercent->percentage;
                    }


                } else {
                    $totalAvgAmount = 0;
                }


                $coin = AdminCoin::where('is_active', 1)->first();
                /*echo "<pre>";
            print_r($coin);exit;*/
                $usercoin = User::where('id', $id)->first();

                $notification = auth()->user()->unReadNotifications;
                /*auth()->user()->unReadNotifications->markAsRead();*/


                /*for($i=1;$i<=3;$i++)
             {
                if($i==3)
                {
                    $percentage=0.05;
                }
                else
                {
                     $percentage=0.1;
                }
            }

            echo "<pre>";
            print_r($refferby->level);exit;*/
                $currentId = $id;
                $data = User::where('id', $currentId)->first();
                // --------right Team Finance Code -----------
                $lteambalance = $data->financeleft;
                if (empty($lteambalance)) {
                    $lteambalance = 0;
                }


                // --------right Team Finance  Code End -----------


                // --------left Team Finance Code -----------
                $rteambalance = $data->financeright;
                if (empty($rteambalance)) {
                    $rteambalance = 0;
                }

                // --------left Team Finance  Code End -----------


                // --------left Team count Code ------------


                $reffkey = $data->reff_key;


                $datainner = User::where('reff_by', $reffkey)->get();
                $leftCountTeam = 0;


                if (!empty($check1 = $datainner->toArray())) {
                    foreach ($datainner as $value) {
                        $datainner1 = User::where('reff_by', $value->reff_key)
                            ->get();

                        $leftCountTeam++;

                        if (!empty($check2 = $datainner1->toArray())) {
                            foreach ($datainner1 as $value1) {


                                $datainner2 = User::where('reff_by', $value1->reff_key)
                                    ->get();

                                $leftCountTeam++;
                                if (!empty($datainner2->toArray())) {
                                    foreach ($datainner2 as $value2) {
                                        $leftCountTeam++;

                                    }
                                }
                            }
                        }
                    }
                }


                /*echo "<pre>";
            print_r($leftCountTeam);exit;*/

                // --------left Team count Code End ------------


                // --------right Team count Code ------------

                $rightTeam = 0;
                if (!empty($data->system_position)) {
                    $afterSystem = User::where('system_position', '>', $data->system_position)
                        ->get();


                    if (!empty($check2 = $afterSystem->toArray())) {
                        foreach ($afterSystem as $value7) {

                            $rightTeam++;
                        }
                    }
                }
                // --------right Team count Code End ------------


                $reffral_key = $data->reff_key;

                /* return view('user.dashboard')->withModel($model)->withCoin($coin)
           ->withPercent($total_percent)->withBalance($balance)->withTodayP($todayProfit)->withWithdraw($withdraw)->withFund($fund)->withAvg($totalAvgAmount)->withNotify($notification)->withRightTeam($rightTeam)
                ->withLeftTeam($leftCountTeam)->withLbalance($lteambalance)->withRbalance($rteambalance)->withReffKey($reffral_key)->withExchange($exchangeShow)->withUsercoin($usercoin)
                ->withExchange2ndtime($exchange2ndtime);
*/
                $allUnapproveddeposite = FundDetail::where('u_id', auth()->user()->id)->where('deposite_status', 0)->get();
                foreach ($allUnapproveddeposite as $key => $value) {
                    $doApproved = FundDetail::paymentVerify($value->id);
                }


                return response()->json(['success' => 'ok',

                    'withModel' => $model,
                    'withCoin' => $coin,
                    'withPercent' => $total_percent,
                    'withBalance' => $balance,
                    'withTodayP' => $todayProfit,
                    'withWithdraw' => $withdraw,
                    'withFund' => $fund,

                    'withAvg' => $totalAvgAmount,
                    'withNotify' => $notification,
                    'withRightTeam' => $rightTeam,
                    'withLeftTeam' => $leftCountTeam,
                    'withLbalance' => $lteambalance,
                    'withRbalance' => $rteambalance,

                    'withReffKey' => $reffral_key,

                    'withExchange' => $exchangeShow,

                    'withUsercoin' => $usercoin,

                    'withExchange2ndtime' => $exchange2ndtime,


                ]);
            } else {
                return ['error' => 'app_token not ptovided'];
            }
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }


    public function networkApi(Request $request)
    {
        $user = User::where('app_token', $request->get('app_token'))->first();

        if (!empty($user)) {
            $chekaa = $this->guard()->login($user);

            $userData = User::where('id', auth()->user()->id)->first();
            $id = auth()->user()->id;
            $currentId = auth()->user()->id;

            $data = User::where('id', $currentId)->first();
            $reffkey = $data->reff_key;
            $jsonarray = ['id' => 1, 'name' => $data->name, 'parent' => 0];
            $final[] = $jsonarray = ['id' => 1, 'name' => $data->name . " (Me)", 'parent' => 0];

            $datainner = User::where('reff_by', $reffkey)->get();

            $count = 2;
            $count1 = 2;

            if (!empty($check1 = $datainner->toArray())) {
                foreach ($datainner as $value) {

                    $jsonarray = ['id' => $count, 'name' => $value->name, 'parent' => 1];

                    $final[] = $jsonarray;

                    $datainner1 = User::where('reff_by', $value->reff_key)
                        ->get();
                    $parent = $count;
                    $count++;

                    if (!empty($check2 = $datainner1->toArray())) {
                        foreach ($datainner1 as $value1) {

                            $jsonarray = ['id' => $count, 'name' => $value1->name, 'parent' => $parent];
                            $final[] = $jsonarray;

                            $datainner2 = User::where('reff_by', $value1->reff_key)
                                ->get();

                            $parent2 = $count;
                            $count++;
                            if (!empty($datainner2->toArray())) {
                                foreach ($datainner2 as $value2) {
                                    if ($count != $parent2) {
                                        $jsonarray = ['id' => $count, 'name' => $value2->name, 'parent' => $parent2];
                                        $final[] = $jsonarray;
                                        $count++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (!empty($data->system_position)) {
                $afterSystem = User::where('system_position', '>', $data->system_position)
                    ->get();


                $aftercount = $count++;
                $parent = 1;

                if (!empty($check2 = $afterSystem->toArray())) {
                    foreach ($afterSystem as $value7) {
                        $jsonarray = ['id' => $aftercount, 'name' => ucfirst($value7->name), 'level' => 1, 'parent' => $parent, 'parenta' => 1];
                        $final[] = $jsonarray;
                        $parent = $aftercount;
                        $aftercount++;
                    }
                }
            }
            /*echo "<pre>";
            print_r($final);exit;
            print_r(json_encode($final,true));exit;*/
            return response()->json(['success' => 'ok', 'data' => $final]);
        } else {
            return response()->json(['error' => 'Unauthorised']);
        }


    }

    public function sosStoreApi(Request $request)
    {

        $user1 = User::where('app_token', $request->get('app_token'))->first();
        if (!empty($user1)) {
            $chekaa = $this->guard()->login($user1);
            $post = $request->get('message');
            if (!empty($request->get('message'))) {
                $request->merge(['name' => auth()->user()->name]);
                $request->merge(['email' => auth()->user()->email]);
                $request->merge(['phone_number' => '']);

                $model = Contact::create($request->all());
                return response()->json(['success' => 'ok', 'message' => 'Message Send Successfully as User']);
            } else {
                return response()->json(['error' => 404, 'Message' => 'message field not provided']);
            }
        } else {
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255'],
                'phone_number' => ['required', 'integer', 'min:8'],
                'message' => ['required', 'string'],

            ]);

            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 401);
            }


            $model = Contact::create($request->all());
            return response()->json(['success' => 'ok', 'message' => 'Message Send Successfully As non user']);


        }

    }

    public function btcProfitApi(Request $request)
    {
        $user1 = User::where('app_token', $request->get('app_token'))->first();
        if (!empty($user1)) {
            $chekaa = $this->guard()->login($user1);

            $id = auth()->user()->id;

            $userdata = array(

                'deposit' => FundDetail::where('u_id', $id)
                    ->where('deposite_status', 1)->get()->count() ? FundDetail::where('deposite_status', 1)->sum('amount') : 0,
                'profit' => Profit::where('u_id', $id)->get()->count() ? Profit::where('u_id', $id)->where('withdraw_profit', '!=', 1)->sum('profit') : 0,
                'withdraw' => WithDraw::where('u_id', $id)->get()->count() ? WithDraw::where('u_id', $id)->where('is_withdraw', 1)->sum('withdraw_amount') : 0,


            );

            $sum = FundDetail::where('u_id', $id)->where('deposite_status', 1)->sum('amount');
            $deposit = $userdata['deposit'];


            $profit = Profit::where('u_id', $id)->get()->toArray();
            $data = [];
            foreach ($profit as $key => $value) {

                $sum = $sum + $value['profit'];
                $value += ["afterProfit" => $sum];
                $data[] = $value;


            }
            $sortedArr = collect($data)->sortByDesc('created_at')->values();
            /* echo "<pre>";
                 print_r($data);
             exit;*/

            /*$data = array_merge($is_withdraw, $model);
*/


            $userdata = array(

                'deposit' => FundDetail::where('u_id', $id)
                    ->where('deposite_status', 1)->get()->count() ? FundDetail::where('deposite_status', 1)->sum('amount') : 0,
                'profit' => Profit::where('u_id', $id)->get()->count() ? Profit::where('u_id', $id)->where('withdraw_profit', '!=', 1)->sum('profit') : 0
            );
            $balance = $userdata['deposit'] + $userdata['profit'];


            $model = date('Y-m-d');
            $daily = date('Y-m-d', strtotime("0 day", strtotime((string)$model)));
            $profitLastDayPercent = Profit::where('u_id', auth()->user()->id)->where('profit_time', '=', $daily)->first();

            if (empty($profitLastDayPercent)) {
                $totalAvgAmount = 0;
            } else {
                $totalAvgAmount = $profitLastDayPercent->percentage;
            }

            return response()->json(['success' => 'ok', 'data' => $sortedArr]);
        } else {
            return response()->json(['error' => 404, 'Message' => 'Unauthorised']);
        }
    }

    public function settingsApi(Request $request)
    {
        // app_token
        $user1 = User::where('app_token', $request->get('app_token'))->first();
        if (!empty($user1)) {
            $chekaa = $this->guard()->login($user1);

            $userData = User::where('id', auth()->user()->id)->first();
            $id = auth()->user()->id;
            $user = Auth::user();
            return response()->json(['success' => 'ok', 'name' => $user->name, 'email' => $user->email, 'btcAddress' => $user->btc_address]);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);

        }
    }

    public function settingsUpdateApi(Request $request)
    {
        // app_token and btc_address
        $user1 = User::where('app_token', $request->get('app_token'))->first();
        if (!empty($user1)) {
            if (!empty($request->get('btc_address'))) {
                $chekaa = $this->guard()->login($user1);
                $user = Auth::user();
                $post = $request->except(['app_token']);
                $user = User::where('id', auth()->user()->id)->update($post);
            } else {
                return response()->json(['error' => 'btc_address not provided'], 401);
            }
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function sendPwdCodeApi(Request $request)
    {
        // app_token
        $user1 = User::where('app_token', $request->get('app_token'))->first();
        if (!empty($user1)) {

            $chekaa = $this->guard()->login($user1);
            $user = Auth::user();
            $hash = bin2hex(random_bytes(4));
            $expiresAt = Carbon::now()->addMinutes(5);
            $user = User::where('id', auth()->user()->id)->update(['pwd_code' => $hash]);
            $model = User::where('id', auth()->user()->id)->first();

            $email = 'admin@acmtrader.com';
            $messagetext = 'You are Welcome to Login';
            $email_to = $model->email;
            Mail::send('user.mail.new-password', ['messagetext' => $messagetext, 'link' => $hash], function ($message) use ($email, $email_to) {
                $message->from($email);
                $message->to($email_to);

                $message->subject('Password Reset Code');
            });

            Cache::put('active' . $hash, true, $expiresAt);
            return ["error" => null, 'success' => '200', 'info' => 'code expire in 5 minutes'];
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function verifyPwdCodeApi(Request $request)
    {
        $user1 = User::where('app_token', $request->get('app_token'))->first();
        if (!empty($user1)) {
            //  app_token and ecode_value
            $chekaa = $this->guard()->login($user1);
            $user = Auth::user();
            $val = Cache::has('active' . $request->get('ecode_value'));
            $model = User::where('pwd_code', $request->get('ecode_value'))->first();
            if (!empty($model->id) && $val) {

                return ["error" => null, 'success' => '200'];
            } else {
                return ["error" => 404, 'success' => false];
            }
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function newPwdApi(Request $request)
    {   //app_token password  password_confirmation
        $validator = Validator::make($request->all(), [
            'app_token' => ['required', 'string'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $user1 = User::where('app_token', $request->get('app_token'))->first();
        if (!empty($user1)) {

            $chekaa = $this->guard()->login($user1);
            $user = Auth::user();
            $hashpass = Hash::make($request->get('password'));
            $user = User::where('id', auth()->user()->id)->update(['password' => $hashpass]);

            return ["error" => null, 'success' => '201'];
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function pincodeCreateApi(Request $request)
    {
        //app_token pin_code
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email'],
            'app_token' => ['required', 'string'],
            'pin_code' => ['required', "numeric", "digits:4"]

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $user1 = User::where('app_token', $request->get('app_token'))->where('email', $request->get('email'))->first();
        if (!empty($user1)) {

            $chekaa = $this->guard()->login($user1);
            $user = Auth::user();


            $user = User::where('id', auth()->user()->id)->update(['pin_code' => $request->get('pin_code')]);

            return ["error" => null, 'success' => '200', 'info' => 'Pin Code Created Successfully'];
        } else {

            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function pincodeLoginApi(Request $request)
    {
        //app_token pin_code  email
        $validator = Validator::make($request->all(), [

            'pin_code' => ['required', "numeric", "digits:4"],
            'app_token' => ['required', 'string']

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $user1 = User::where('pin_code', $request->get('pin_code'))->first();
        if (!empty($user1)) {

            $chekaa = $this->guard()->login($user1);
            $user = Auth::user();
            if ($user->email_status == 1) {
                $update = User::where('id', $user->id)->update(['app_token' => $request->get('app_token')]);
                $user = User::where('id', $user->id)->get();
                return response()->json(['success' => 'ok', 'data' => $user]);
            } elseif ($user->email_status == 2) {
                return response()->json(['error' => 'Check Your Email to verify'], 401);
            } else {
                return response()->json(['error' => 'Email not verified'], 401);

            }
        } else {

            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function soslistApi(Request $request)
    {
        //app_token
        $validator = Validator::make($request->all(), [
            'app_token' => ['required', 'string']

        ]);


        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }


        $user1 = User::where('app_token', $request->get('app_token'))->first();
        if (!empty($user1)) {
            $chekaa = $this->guard()->login($user1);
            $user = Auth::user();
            $model = Contact::where('user_id', auth()->user()->id)->get();

            $data = [];
            $final = [];
            foreach ($model as $user) {

                /*$data= array_merge($is_withdraw, $model);*/
                if ($user->adminReply($user->id)) {
                    $data['id'] = $user->id;
                    $data['subject'] = $user->subject;
                    $data['message'] = $user->message;
                    $data['created_at'] = $user->created_at;
                    $data['status'] = 4;
                    $final[] = $data;
                } else {
                    $data['id'] = $user->id;
                    $data['subject'] = $user->subject;
                    $data['message'] = $user->message;
                    $data['created_at'] = $user->created_at;
                    $data['status'] = $user->status;

                    $final[] = $data;
                }
            }

            return response()->json(['success' => 'ok', 'data' => $final]);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function sosAddApi(Request $request)
    {


        //app_token
        $validator = Validator::make($request->all(), [
            'app_token' => ['required', 'string'],
            'message' => ['required', 'string'],
            'subject' => ['required', 'string'],
            'contact_pic1' => ['mimes:jpeg,png,zip,pdf', 'max:2048'],

        ]);


        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $user1 = User::where('app_token', $request->get('app_token'))->first();
        if (!empty($user1)) {
            $chekaa = $this->guard()->login($user1);
            $user = Auth::user();
            $check = Contact::where('user_id', auth()->user()->id)->where('status', '!=', 1)->first();

            if (!empty($check)) {

                return response()->json(['error' => '1 Ticket at time for Resolve'], 401);
            }
            $post = $request->get('message');
            $request->merge(['name' => auth()->user()->name]);
            $request->merge(['user_id' => auth()->user()->id]);
            $request->merge(['email' => auth()->user()->email]);
            $request->merge(['phone_number' => '']);


            $posterName = "";


            if (request()->hasFile('contact_pic1')) {

                /*echo "<pre>";
            print_r(request()->contact_pic->getClientOriginalExtension());exit; getClientOriginalName  getClientOriginalExtension*/

                $posterName = time() . '.' . request()->contact_pic1->getClientOriginalName();
                request()->contact_pic1->move(public_path('images/contact_pic'), $posterName);
            }

            $request->merge(['contact_pic' => $posterName]);

            $model = Contact::create($request->all());

            return response()->json(['success' => 'ok', 'message' => 'Message Send to Admin Successfully']);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function sosDetailApi(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'app_token' => ['required', 'string'],
            'id' => ['required', 'numeric'],
        ]);


        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $user1 = User::where('app_token', $request->get('app_token'))->first();
        if (!empty($user1)) {
            $chekaa = $this->guard()->login($user1);
            $user = Auth::user();

            $id = $request->get('id');
            $contact = Contact::where('id', $id)->first();
            $comments = Comments::where('contact_id', $id)->get();

            $latest = Comments::where('contact_id', $id)->where('u_id', 2)->where("admin_reply", 1)->latest('created_at')->update(['admin_reply' => 0]);

            return response()->json(['success' => 'ok', 'contact' => $contact, 'comments' => $comments]);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function contactCommentsApi(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'app_token' => ['required', 'string'],

            'contact_id' => ['required', 'numeric'],
            'u_id' => ['required', 'numeric'],

            'text' => ['required', 'string'],
            'contact_pic1' => ['mimes:jpeg,png,zip,pdf', 'max:2048'],
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $user1 = User::where('app_token', $request->get('app_token'))->first();
        $check = Contact::where('id', $request->get('contact_id'))->where('status', 0)->first();
        if (!empty($user1)) {
            if (empty($check)) {
                return response()->json(['error' => 'This Issue is already Resolved'], 401);
            }


            $posterName = "";
            if (request()->hasFile('contact_pic1')) {

                /*echo "<pre>";
            print_r(request()->contact_pic->getClientOriginalExtension());exit; getClientOriginalName  getClientOriginalExtension*/

                $posterName = time() . '.' . request()->contact_pic1->getClientOriginalName();
                request()->contact_pic1->move(public_path('images/comment_pic'), $posterName);
            }

            $request->merge(['comment_pic' => $posterName]);
            $model = Comments::create($request->all());
            return response()->json(['success' => 'ok', 'message' => 'comment add Successfully']);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }


}
