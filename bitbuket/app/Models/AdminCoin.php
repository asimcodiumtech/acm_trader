<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminCoin extends Model
{
	protected $table="admin_coins";
    protected $fillable=['btc_id','qr_code','usd_id','is_active','acc_name','branch_code','bank_type','swift_code'];
}
