<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Weekly extends Model
{
    protected $table="weekly";
    protected $fillable=['weekly_age','is_active','date'];
}
