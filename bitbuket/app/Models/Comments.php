<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Comments extends Model
{
     protected $table="contact_comments";
	protected $fillable = [
        'contact_id', 'text','u_id','comment_pic','admin_reply'
    ];

    public function name($id)
    {   
        if(!empty($id))
        {
            $model=User::select('name')->where('id',$id)->first();

            return $model->name;

        }
        else
        {
            return '';
        }
    }
}
