<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Profit;
use App\Models\WithDraw;
//use App\Models\FundDetail;
use App\User;

class FundDetail extends Model
{
    protected $table="fund_details";
    protected $fillable=['u_id','trans_id','coin_type','amount','btc_admin','btc_usd','acc_name','branch_code','bank_type','swift_code','usd_id','profit','withdraw_status','withdraw_id','deposite_status','deposit_approved_time','deposite_initial_status','type'];

    public static function deposit()
    {
        if (!empty($id)) {
            $userdata = array(

            'allDeposit' => FundDetail::where('u_id',$id)
                            ->count() ? FundDetail::sum('amount'): 0,




            );
        }
        else
        {
           $userdata = array(

            'allDeposit' => FundDetail::where('u_id',auth()->user()->id)
                            ->count() ? FundDetail::sum('amount'): 0,




            );

        }

       return $userdata;
    }
    public  function totalDeposit($profit,$count,$status,$sum)
    {

     if($status == 1)
      { $sum=$profit;
            return $sum;

       }
       else
       {
        return 0;
       }

    }
    public  static function paymentVerify($fundID)
    {




                 $time_deposited= date('Y-m-d');

                 $id=$fundID;
                $model=FundDetail::where('id',$id)->update(['deposite_status'=> 1,'deposit_approved_time'=> $time_deposited,'deposite_initial_status'=>0]);
                $u_id=FundDetail::where('id',$id)->first();
                $balance=User::select('remain_balance')->where('id',$u_id->u_id)->first();
                $total=$balance->remain_balance + $u_id->amount;


                $refferby=User::where('id',$u_id->u_id)->first();
                $refferby=$refferby->reff_by;
                $amount=$u_id->amount ;

                for($i=1;$i<=3;$i++)
                {
                    $relativefirst=User::where('reff_key',$refferby)->first();
                    if(!empty($refferby))
                    {
                        if($i==3)
                        {
                            $percentage=0.05;
                        }
                        else
                        {
                            $percentage=0.1;
                        }
                        $withdraw=$relativefirst->withdraw_amount;
                        $balance=$relativefirst->remain_balance;
                        $financeleft=$relativefirst->financeleft;

                        $finalWithamount=$withdraw +  $amount*$percentage;
                        $finalamount=$balance +  $amount*$percentage;
                        $finalfinanceleft=$financeleft +  $amount*$percentage;

                        $update=User::where('reff_key',$refferby)->update(['remain_balance'=>$finalamount,'withdraw_amount'=>$finalWithamount,'financeleft'=>$finalfinanceleft]);
                        $refferby= $relativefirst->reff_by;

                    }
                }
                /////   systems  person gave profit
                $system=User::where('id',$u_id->u_id)->first();
                if(!empty($system->system_position))
                {    $endingLimit=$system->system_position-3;
                    $positionwise=User::where('system_position','<',$system->system_position)->where('system_position','>=',$endingLimit)->orderBy('system_position', 'DESC')->get();

                    if(!empty($check=$positionwise->toArray()))
                    {   $cc=1;
                        foreach($positionwise as $users)
                        {
                            switch ($cc)
                            {
                                case 1:
                                   $percentage=0.05;$cc++;
                                    break;
                                case 2:
                                    $percentage=0.03;$cc++;
                                    break;
                                case 3:
                                    $percentage=0.02;$cc++;
                                    break;
                            }
                            $withdraw=$users->withdraw_amount;
                            $balance=$users->remain_balance;
                            $financeright=$users->financeright;

                            $finalWithamount=$withdraw +  $amount*$percentage;
                            $finalamount=$balance +  $amount*$percentage;
                            $finalfinanceright=$financeright +  $amount*$percentage;
                            $update=User::where('id',$users->id)->update(['remain_balance'=>$finalamount,'withdraw_amount'=>$finalWithamount,'financeright'=>$finalfinanceright]);
                        }
                    }
                }
            $update_balance=User::where('id',$u_id->u_id)->update(['remain_balance'=> $total]);
    }
    public static function weeklyProfit($userId,$active_weekly_start,$active_weekly_end,$percentage)
    {
       if($percentage!=0)
        {
            $per_day=$percentage/7;
            $approved_deposit=self::where('u_id',$userId)->where('deposite_status',1)->whereBetween('deposit_approved_time',[$active_weekly_end,$active_weekly_start])->sum('amount');
            $average_profit=self::where('u_id',$userId)->where('deposite_status',1)->whereBetween('deposit_approved_time',[$active_weekly_end,$active_weekly_start])->get()->toArray();
            /*$sum=0;
            $divide=1;*/
            $sum= $percentage;

           /* foreach($average_profit as $value)
            {
                $date1=date_create($value['deposit_approved_time']);
                $date2=date_create($active_weekly_start);
                $diff=date_diff($date1,$date2);

               if($diff->days >0)
               {

                $sum = ($sum +$diff->days*$per_day)/$divide;

                $divide++;
               }

            }*/
               $balancemodel=User::where('id',$userId)->first();
                $check_balance=$balancemodel->remain_balance;
                $minus= $balancemodel->remain_balance -($balancemodel->financeleft + $balancemodel->financeright);
                $withdraw_balance=$balancemodel->withdraw_amount;
                if($minus>0)
                {
                    if($check_balance>0)
                    {   (float) $percentage_amount=(float) $minus * (float)$sum/100;
                        $final_amount= $percentage_amount + $check_balance;
                        $final_withdraw_amount= $percentage_amount + $withdraw_balance;
                        if($final_withdraw_amount<=0)
                        {
                          $final_withdraw_amount=0;
                        }
                        if($final_amount<=0)
                        {
                          $final_amount=0;
                        }
                        $balancemodel=User::where('id',$userId)->update(['remain_balance'=>$final_amount,'withdraw_amount'=>$final_withdraw_amount]);
                        $startDate= date('Y-m-d');
                        $pro=Profit::create(['u_id'=>$userId,'profit'=>(float) $percentage_amount,'percentage'=>(float) $sum,'profit_time'=>$startDate]);
                    }
                }

        }
        else
        {


        }
    }
    public  function refNo($id)
    {
        $model=User::where('id',$id)->first();

        return $model->name;
    }
}
