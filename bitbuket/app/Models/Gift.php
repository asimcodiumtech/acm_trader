<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Gift extends Model
{
    protected $table="gifts";
    protected $fillable=['user_id','gift_code','amount','status','created_by'];
    
    public function name($id)
    {   
    	if(!empty($id))
    	{
    		$model=User::select('name')->where('id',$id)->first();

    		return $model->name;

    	}
    	else
    	{
    		return '';
    	}
       

    }
}
