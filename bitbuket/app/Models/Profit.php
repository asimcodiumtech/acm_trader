<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Profit;
use App\Models\WithDraw;
use App\Models\FundDetail;
use App\User;

class Profit extends Model
{
    protected $table="profits";
    protected $fillable=['u_id','profit_trans_id','coin_type','profit','withdraw_profit','percentage','profit_time','type'];


    public static function profit()
    {
        $userdata = array(
            
            'deposit' => FundDetail::where('u_id',auth()->user()->id)
                            ->where('deposite_status',1)->get()->count() ? FundDetail::where('deposite_status',1)->sum('amount'): 0,
            'profit' => Profit::where('u_id',auth()->user()->id)->get()->count() ? Profit::where('u_id',auth()->user()->id)->where('withdraw_profit','!=',1)->sum('profit'): 0,
             
            
                
            );
        
       return $userdata;
    }
    public  function totalDeposit($profit,$count,$status,$sum)
    {
          
     if($status == 1)
      { $sum=$profit;
            return $sum;
           
       }
       else
       {
        return 0;
       }    
    }
     public  function tprofit($profit,$count,$status,$sum)
    {    

      if($status == 1)
      { $sum=$profit;
            return $sum;
      }
	   else
	   {
	    return 0;
	   } 
    }
}


