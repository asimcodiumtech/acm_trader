<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class WithDraw extends Model
{
    protected $table="with_draws";
    protected $fillable=['u_id','profit_id','deposite_id','withdraw_trans_id','coin_type','withdraw_amount','is_withdraw' ,'acc_name','branch_code','bank_type','swift_code','usd_id','btc_id','type'];
    
    
    
    public static function refNo($id)
    {
        $model=User::where('id',$id)->first();

        return $model->name;
    }
}
