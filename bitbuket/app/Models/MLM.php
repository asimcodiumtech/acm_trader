<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MLM extends Model
{
    protected $table="MLM";
	protected $fillable = [
        'person_id', 'reffer_id', 'system_ref','reffer_key'
    ];
}
