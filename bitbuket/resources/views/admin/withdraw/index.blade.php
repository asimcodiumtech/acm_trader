@extends('layouts.app')

@section('dashboardcontent')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 style="color: white;">Withdraw List</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Withdraw List</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<section class="content">
      <div class="row">
        <div class="col-12">

			<div class="card">
			            <div class="card-header" style="background-color: #35354B;color: white;">
			              <h3 class="card-title"></h3>
			            </div>
			            <!-- /.card-header -->
			            <div class="card-body" style="background-color: #35354B;color: white;">
                    <div class="table-responsive">
			              <table  class="table table-bordered table-striped example1">
			                <thead style="background-color:#131311; color:white;">
			                <tr>
			                  
			                  <th>Coin Type</th>
			                  <th>Amount</th>
			                  <th>Reference</th>
                                <th>BTC Address</th>
                               
        			             <th>Created At</th>
                                 <th>Withdraw Status</th>
			                  
			                </tr>
			                </thead>
			                <tbody style="background-color: #35354B;color: white;">
			                @foreach($users as $user)
                             <tr>
                             
						        <td>
						          {{$user->coin_type}}
						        </td>
						        <td>
                                  {{$user->withdraw_amount}}
                                </td>
                                
                                
                                <td>
                      {{$user->refNo($user->u_id)}}
                    </td>
                                
                                <td>
                                   
						          {{$user->btc_id}}
                                 
                                 
                              </td>
                              
						        <td>
                                   
						          {{$user->created_at}}
                                 
                                 
                              </td>
						        
                                
						         
						        <td>
						          @if($user->is_withdraw==1)
						            <button  class="btn btn-success"><b> Processed </b></button>
						            @elseif($user->is_withdraw==2)
						            <button  class="btn btn-danger draw" data-id="{{$user->id}}" data-u-id="{{$user->u_id}}"><b> pending </b></button>
						            
						            @else
						           <button  class="btn btn-danger"><b> Not Approved </b></button>
						           @endif
						        </td>
						        
                             </tr>
			                @endforeach
			                </tbody>
			                <tfoot style="background-color:#131311; color:white;">
			                <tr>
			                  
			                  <th>Coin Type</th>
			                  <th>Amount</th>
			                  <th>Reference</th>
                                <th>BTC Address</th>
                                <th>Created At</th>
        			            <th>Withdraw Status</th>
			                  
			                </tr>
			                </tfoot>
			              </table>
                  </div>
			            </div>
			            <!-- /.card-body -->
			</div>
	   </div>
	  </div>
</section>	


<script>
  $(function () {
    $(".example1").DataTable({
        responsive: true
    } );
    $('.example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
 $(document).ready(function() {
$('.example1').on('click', '.draw', function () {
	var id = $(this).data('id');
  var u_id = $(this).data('u-id');
	



                 swal({
                            title: "Are you sure you want to Approved ?",
                            text: "",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes, Processed !!",
                            cancelButtonText: "No, cancel it !!",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },

	
                        function(isConfirm){
                            if (isConfirm) {
                              
                                var url = '<?= route('admin.withdraw.ajax')?>';
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {"_token": "{{ csrf_token() }}", id: id,withdrawStatus: 1,u_id:u_id},
                    success: function (response) {
                        if (response.success) {
                            swal("Hey !!", "You Gave Withdraw amount Successfully " , "success");
                            $('.confirm').on('click', function () {
                                window.location.reload();
                            });
                            
                        } else {
                            if (response.error == 409) {
                               
                                swal("Cancelled !!", "Hey, your Approval is denied !!", "error");

                            }
                            if (response.error == 401) {
                               
                                swal("Cancelled !!", "This Amount is alreayd Withdraw", "error");

                            }
                             else {
                                alert("Status can't be change.")
                            }
                        }
                    },
                    error: function () {
                        alert('Error occured');
                    }
                });

                                
                            }
                            else {
                                swal("Cancelled !!", "Hey, Cancelled the Transaction !!", "error");
                            }
                        });


});
});
  
</script>	
@endsection