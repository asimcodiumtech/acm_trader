@extends('layouts.app')

@section('dashboardcontent')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 style="color: white;">Newsletter</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">News Letter</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


<section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8" >
    				<div class="card card-primary">
              <div class="card-header" style="background-color: #42426a;color: white;">
                <h3 class="card-title">Newsletter Send</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="" enctype="multipart/form-data">
                @csrf
                <div class="card-body" style="background-color:#35354B; color:white;">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Send Message</label>
                    <textarea class="form-control message"  name="message"  style="background-color:#131311; color:white;"> </textarea>
                  </div>
                  
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="button" class="btn btn-primary news" style="background-color: #35354B;color: white;">Submit</button>
                </div>
              </form>
            		</div>
           </div>
       </div>
   </div>
</section>

<script>
  function topRight() {

var values= [{"positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false}];
        return values[0];

}
  /*toastr.success("{{ Session::get('success') }}", topRight());*/
    var type = "{{ Session::get('alert-type') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}","{{ Session::get('heading') }}",  topRight() );
            break;

        case 'warning':
            toastr.warning("{{ Session::get('message') }}" ,"{{ Session::get('heading') }}", topRight());
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}" ,"{{ Session::get('heading') }}", topRight());
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}" ,"{{ Session::get('heading') }}", topRight());
            break;
    }



      $(document).ready(function() {
     
     
        $('.news').on('click', function () 
          {

              var msg= $('.message').val();
              console.log(msg);
              
              if(msg=='' || msg==false)
              {
                sweetAlert("Oops...", "Newsletter is empty !!", "error");
              }
              else
              {
                var url = '<?= route('admin.newsletter.store')?>';
                var data={"_token": "{{ csrf_token() }}",msg:msg};
                  $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (response) {
                      if (response.success=='201') {
                             
                               swal("Newsletter Send", "Successfully !!", "success");
                                $('.confirm').on('click', function () {
                                window.location.reload();
                            });
                             
                               
                        }

                        
                    },
                    error: function () {
                         
                    }
                });
              }


          });

});
    
 </script>

@endsection