@extends('layouts.app')

@section('dashboardcontent')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 style="color: white;">Customer Support</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">SOS</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


<section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8 mx-auto" >
            <div class="card card-primary">
              <div class="card-header" style="background-color: #42426a;color: white;">
                <h3 class="card-title">  {{$contact->name($contact->user_id)}}  </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="" enctype="multipart/form-data">
                @csrf
                <div class="card-body" style="background-color:#35354B; color:white;">
                  <div class="form-group">
                            <label for="exampleInputEmail1">Subject</label>
                            <input type="text" class="form-control amountaa" id="exampleInputEmail2" placeholder="Enter Subject  Here" name="subject" value="{{$contact->subject}}"  style="background-color:#131311; color:white;" readonly>
                          </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Send Message</label>
                    <textarea class="form-control"  name="message" value="" style="background-color:#131311; color:white;" readonly> {{$contact->message}}</textarea>
                  </div>
                   @if(!empty($contact->contact_pic))
                  <div class="form-group">
                     <label for="exampleInputEmail1">File</label>
                    <a href="{{asset('public/images/contact_pic/')}}/{{$contact->contact_pic}}" download="{{$contact->contact_pic}}" class="form-control" style="background-color:#131311; color:white;">{{$contact->contact_pic}}</a>
                  </div>
                  @endif
                  
                <!-- /.card-body -->

                
              </form>
                </div>
           </div>
       </div>
   </div>

@foreach($comments as $comment)
   
        <div class="row">
          <!-- left column -->
          <div class="col-md-8 mx-auto" >
            <div class="card card-primary">
              <div class="card-header" style="background-color: #42426a;color: white;">
                @if($comment->u_id==auth()->user()->id)
                <h3 class="card-title">Admin</h3>
                @else
                <h3 class="card-title">{{$comment->name($comment->u_id)}}</h3>
                @endif
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
                <div class="card-body" style="background-color:#35354B; color:white;">
                  
                  <div class="form-group">
                    <label for="exampleInputEmail1">Send Message</label>
                    <textarea class="form-control"  name="message" value="" style="background-color:#131311; color:white;" readonly>{{$comment->text}} </textarea>
                  </div>
                   @if(!empty($comment->comment_pic))
                  <div class="form-group">
                     <label for="exampleInputEmail1">File</label>
                    <a href="{{asset('public/images/comment_pic/')}}/{{$comment->comment_pic}}" download="{{$comment->comment_pic}}" class="form-control" style="background-color:#131311; color:white;">{{$comment->comment_pic}}</a>
                  </div>
                  @endif
                  
                
              
                </div>
           </div>
       </div>
   </div>
 
 @endforeach
  @if($contact->status==1)
                         
                         
                          @else
        <div class="row">
          <!-- left column -->
          <div class="col-md-8 mx-auto" >
            <div class="card card-primary">
              <div class="card-header" style="background-color: #42426a;color: white;">
                <h3 class="card-title">Admin</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="{{ route('admin.sos.detailStore') }}" enctype="multipart/form-data">
                @csrf
                <div class="card-body" style="background-color:#35354B; color:white;">
                  
                  <div class="form-group">
                    <label for="exampleInputEmail1">Send Message</label>
                    <textarea class="form-control"  name="text" value="" style="background-color:#131311; color:white;" required> </textarea>
                    <input type="hidden" name="contact_id" value="{{$contact->id}}">
                     <input type="hidden" name="u_id" value="{{auth()->user()->id}}">
                  </div>
                  <div class="col-md-8">
                    <input id="logo" type="file" class="custom-file-input" style="background-color:#131311 !important; color:white !important;"  name="contact_pic1">
                   <label for="logo" class="custom-file-label text-truncate" style="background-color:#131311; color:white;">Choose file...</label>
                   </div>
                  
                <!-- /.card-body -->

                <div class="card-footer">
                  <a href="{{ route('admin.contact.index') }}" class="btn btn-warning" style="background-color: #35354B;color: white;">Back</a>
                  <button type="submit" class="btn btn-primary float-right" style="background-color: #35354B;color: white;">Submit</button>
                </div>
              </form>
                </div>
           </div>
       </div>
    @endif

</section>

<script>
$('.custom-file-input').on('change', function() { 
   let fileName = $(this).val().split('\\').pop(); 
   $(this).next('.custom-file-label').addClass("selected").html(fileName); 
});
  function topRight() {

var values= [{"positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false}];
        return values[0];

}
  /*toastr.success("{{ Session::get('success') }}", topRight());*/
    var type = "{{ Session::get('alert-type') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}","{{ Session::get('heading') }}",  topRight() );
            break;

        case 'warning':
            toastr.warning("{{ Session::get('message') }}" ,"{{ Session::get('heading') }}", topRight());
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}" ,"{{ Session::get('heading') }}", topRight());
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}" ,"{{ Session::get('heading') }}", topRight());
            break;
    }
    
 </script>

@endsection