@extends('layouts.app')

@section('dashboardcontent')
<style>
.bg {
  
  background-repeat: no-repeat;
  
  background-position: center;
  height: 20vh;
  width: 20vh;
}
</style>
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 style="color: white;">Coin Info</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Coin Set Form</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


<section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8" >
    				<div class="card card-primary">
              <div class="card-header" style="background-color: #42426a;color: white;">
                <h3 class="card-title">Set Coin Trasaction Id</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="{{ route('admin.coin.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="card-body" style="background-color:#35354B; color:white;">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Set  Acount Name</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter acount Name " name="acc_name" value="{{$users->acc_name}}" required style="background-color:#131311; color:white;">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Set  Account Number</label>
                    <input type="number" class="form-control" id="exampleInputEmail1" placeholder="Enter acount number " name="usd_id" value="{{$users->usd_id}}" required style="background-color:#131311; color:white;">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Set  Branch Code</label>
                    <input type="number" class="form-control" id="exampleInputEmail1" placeholder="Enter Branch Code" name="branch_code" value="{{$users->branch_code}}" required style="background-color:#131311; color:white;">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Set  Bank Type</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Bank type" name="bank_type" value="{{$users->bank_type}}" required style="background-color:#131311; color:white;">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Set  Swift Code</label>
                    <input type="number" class="form-control" id="exampleInputEmail1" placeholder="Enter Swift Code" name="swift_code" value="{{$users->swift_code}}" required style="background-color:#131311; color:white;">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Set Bit Coin Transaction Id</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter bitcoin Transaction id" name="btc_id" value="{{$users->btc_id}}" required style="background-color:#131311; color:white;">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Qr Code</label>
                    <div class="input-group">
                      <div class="custom-file" >
                        <input type="file" class="custom-file-input" id="exampleInputFile" name="poster" style="background-color:#131311; color:white;">
                        <label class="custom-file-label" for="exampleInputFile" style="background-color:#131311; color:white;">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                     <div class="size_img_contianer">
                <img src="{{ asset('/images/qrcodes/')}}/{{$users->qr_code}}" class="size_img" style ="width: 26%;"/>
            </div>
                  </div>

                          


                  <input type="hidden"  name="oldPoster" value="{{$users->qr_code}}">

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" style="background-color: #35354B;color: white;">Submit</button>
                </div>
              </form>
            		</div>
           </div>
       </div>
   </div>
</section>

<script type="text/javascript">
  $(function () {
  $('.currencydiv').hide();
  $('.coin_type').change(function()
{   
   if($('.coin_type').val()=='BTC')
   { 
       $('.currencydiv').show();
        $('.currency').attr('required','required');
    }
   else if($('.coin_type').val()=='USD')
   { 
       $('.currencydiv').hide();
        $('.currency').removeAttr('required');
    }
    else
    {

    }

});
});
  $(document).ready(function () {
  bsCustomFileInput.init();
});

</script>


@endsection