@extends('layouts.app')

@section('dashboardcontent')
<style>
.bg {
  
  background-repeat: no-repeat;
  
  background-position: center;
  height: 20vh;
  width: 20vh;
}
</style>
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 style="color: white;">Gift Add</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Gift Add</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


<section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8" >
    				<div class="card card-primary">
              <div class="card-header" style="background-color: #42426a;color: white;">
                <h3 class="card-title">Set Gift Detail</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="{{ route('admin.gift.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="card-body" style="background-color:#35354B; color:white;">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Gift Code</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter acount Name " name="gift_code" value="{{$hash}}" required style="background-color:#131311; color:white;" readonly>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Set  Amount</label>
                    <input type="number" class="form-control" id="exampleInputEmail1" placeholder="Enter acount number " name="amount" value="" required style="background-color:#131311; color:white;">
                  </div>
                 

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" style="background-color: #35354B;color: white;">Submit</button>
                </div>
              </form>
            		</div>
           </div>
       </div>
   </div>
</section>

<script type="text/javascript">

</script>


@endsection