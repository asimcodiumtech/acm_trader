@extends('layouts.app')

@section('dashboardcontent')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 style="color: white;">User List</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">User List</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<section class="content">
      <div class="row">
        <div class="col-12">

			<div class="card">
			            <div class="card-header" style="background-color: #35354B;color: white;">
			              <h3 class="card-title"></h3>
			            </div>
			            <!-- /.card-header -->
			            <div class="card-body" style="background-color: #35354B;color: white;">
                    <div class="table-responsive">
			              <table  class="table table-bordered table-striped example1">
			                <thead style="background-color:#131311; color:white;">
			                <tr>
			                  
			                  <th>Name</th>
			                  <th>Email</th>
			                  <th>BTC address </th>
                               <th>Balance</th>
                                <th>Date and Time</th>
                                
        			             
                                 <th>Active Status</th>
                                 <th>Action</th>
			                  
			                </tr>
			                </thead>
			                <tbody style="background-color: #35354B;color: white;">
			                @foreach($users as $user)
                             <tr>
                             
						        <td>
						          {{$user->name}}
						        </td>
						        <td>
                                  {{$user->email}}
                                </td>
                                <td>
				                     {{$user->btc_address}}
				                </td>
                                
                                <td>
				                     {{$user->remain_balance}}
				                </td>
				                <td>
				                     {{$user->created_at}}
				                </td>
                                
                                <td>
						          @if($user->isOnline())
						            <button  class="btn btn-success"><b> Active{{$user->isOnline()}} </b></button>
						            @else

						            @if($user->isOnline($user->id))
						            @endif
						           
						           @endif
						        </td>
						        <td>
				                     <a class="btn btn-info" href="{{route('admin.user.detail',$user->id)}}">View Detail</a> 
				                </td>

						        
                             </tr>
			                @endforeach
			                </tbody>
			                <tfoot style="background-color:#131311; color:white;">
			                <tr>
			                  
			                  <th>Name</th>
			                  <th>Email</th>
			                  <th>BTC address </th>
                               <th>Balance</th>
                                <th>Date and Time</th>
                                
        			             
                                  <th>Active Status</th>
                                 <th>Action</th>
			                  
			                </tr>
			                </tfoot>
			              </table>
                  </div>
			            </div>
			            <!-- /.card-body -->
			</div>
	   </div>
	  </div>
</section>	


<script>
  $(function () {
    $(".example1").DataTable({
        responsive: true
    } );
    $('.example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
 
  
</script>	
@endsection