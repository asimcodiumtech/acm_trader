@extends('layouts.app')

@section('dashboardcontent')
<style>
body {
  margin: 0;
}
.content {
  padding: 10px 20px;
}
.content p {
  font-family: sans-serif;
}

/******/

.ticker-container {
  width: 100%;
  overflow: hidden;
}

.ticker-canvas {
  width: calc((200px * 6) + 2px);
  /* 
  200px = minimum width of ticker item before widget script starts removing ticker codes
  15 = number of ticker codes
  2px = accounts for 1px external border
  */
  -webkit-animation-iteration-count: infinite;
  animation-iteration-count: infinite;
  -webkit-animation-timing-function: linear;
  animation-timing-function: linear;
  -webkit-animation-name: ticker-canvas;
  animation-name: ticker-canvas;
  -webkit-animation-duration: 20s;
  animation-duration: 20s;
}
.ticker-canvas:hover {
  animation-play-state: paused
}

@-webkit-keyframes ticker-canvas {
  0% {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    visibility: visible;
  }
  100% {
    -webkit-transform: translate3d(-100%, 0, 0);
    transform: translate3d(-100%, 0, 0);
  }
}

@keyframes ticker-canvas {
  0% {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    visibility: visible;
  }
  100% {
    -webkit-transform: translate3d(-100%, 0, 0);
    transform: translate3d(-100%, 0, 0);
  }
}
.tradingview-widget-container {
  position: relative;
}
.tradingview-widget-container iframe {
    position: absolute;
    top: 0;
}
.tradingview-widget-container iframe:nth-of-type(2) {
  left: 100%;
}

.sweet-alert select {
    width: 100%;
    box-sizing: border-box;
    border-radius: 3px;
    border: 1px solid #d7d7d7;
    height: 43px;
    margin-top: 10px;
    margin-bottom: 17px;
    font-size: 18px;
    background-color:#131311; color:white;
</style>
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 " style="color: white;">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success" style="background-color: #35354b!important;">
              <div class="inner">
                <div class="row">
                    <div class="col-lg-12 col-6">
                            <p>Left Team</p>
                    </div>
                 </div>
                 <div class="row">
                    <div class="col-lg-6 col-4">
                        <span  class="btn btn-sm btn-info " style="background-color: #00c245;border-color: #00c245;">100% </span>
                          <i class="fa fa-bar-chart" aria-hidden="true"></i>
                    </div>
                    <div class="col-lg-6 col-4">
                            <p>{{$leftTeam}}<br><span style="font-size: 13px;">User</span></p>
                    </div>

                 </div>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success" style="background-color: #35354b!important;">
              <div class="inner">
                <div class="row">
                    <div class="col-lg-12 col-6">
                            <p>Right Team</p>
                    </div>
                 </div>
                 <div class="row">
                    <div class="col-lg-6 col-4">
                            <span  class="btn btn-sm btn-info " style="background-color: red;border-color: red;">100% </span>
                            
                    </div>
                    <div class="col-lg-6 col-4">
                            <p>{{$rightTeam}}<br><span style="font-size: 13px;">User</span></p>
                    </div>

                 </div>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info" style="background-color: #35354b!important;">
              <div class="inner">
                <div class="row">
                    <div class="col-lg-3 col-6">
                            <p>Balance</p>
                    </div>
                 </div>
                 <div class="row">
                    <div class="col-lg-6 col-4">
                            <p>{{$todayP}} USD<br><span style="font-size: 13px;">Today Profit</span></p>
                            
                    </div>
                    <div class="col-lg-6 col-4">
                            <p>{{$balance}}<br><span style="font-size: 13px;">USD</span></p>
                    </div>

                 </div>
                 <!-- <div class="row">
                    <div class="col-lg-6 col-4">
                            <p>Today Profit<br>USD</p>
                    </div>
                    <div class="col-lg-6 col-4">
                            <p>USD</p>
                    </div>
                 </div> -->
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
             
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger" style="background-color: #35354b!important;">
              <div class="inner">
               <div class="row">
                    <div class="col-lg-12 col-12">
                            <p>Team Finance</p>
                    </div>
                 </div>
                 <div class="row">
                    <div class="col-lg-6 col-4">
                            <p>{{$lbalance}} USD <br><span style="font-size: 13px;">Left</span></p>
                            
                    </div>
                    <div class="col-lg-6 col-4">
                            <p>{{$rbalance}} USD<br><span style="font-size: 13px;">Right</span></p>
                    </div>

                 </div>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
             
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
           <div class="col-md-8 col-lg-8 col-xl-8 col-xxl-8">
                
                
               <div class="card card-primary card-outline card-outline-tabs" style="background-color: #35354b!important;">
              <div class="card-header p-0 border-bottom-0">
                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="true">Chart</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill" href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false">Market</a>
                  </li>
                  
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-three-tabContent">
                  <div class="tab-pane fade active show" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                     <div class="ttradingview-widget-container">
  <div id="tradingview_ca3fa"></div>
  <div class="tradingview-widget-copyright"><a href="" rel="noopener" target="_blank"><span class="blue-text">ETHBTC Chart</span></a></div>
  <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
  <script type="text/javascript">
  new TradingView.widget(
  {
  
  "symbol": "BINANCE:ETHBTC",
  "interval": "30",
  "height": 510,
  "width": "100%",
  "timezone": "Etc/UTC",
  "theme": "Dark",
  "style": "1",
  "locale": "en",
  "toolbar_bg": "#f1f3f6",
  "enable_publishing": false,
  "allow_symbol_change": true,
  "container_id": "tradingview_ca3fa"
}
  );
  </script>
</div>
<!-- TradingView Widget END -->
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                     <!-- TradingView Widget BEGIN -->
<!-- TradingView Widget BEGIN -->
<div class="ttradingview-widget-container">
  <div class="tradingview-widget-container__widget"></div>
  <div class="tradingview-widget-copyright"><a href="" rel="noopener" target="_blank"><span class="blue-text">Crypto Screener</span></a></div>
  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-screener.js" async>
  {
  "width": "101%",
  "height": 512,
  "defaultColumn": "overview",
  "defaultScreen": "general",
  "market": "crypto",
  "showToolbar": false,
  "colorTheme": "dark",
  "locale": "en"
}
  </script>
</div>
<!-- TradingView Widget END -->
<!-- TradingView Widget END -->
                  </div>
                  
                 
                </div>
              </div>
              <!-- /.card -->
            </div>
            @if(round($avg, 2) >= 0 )
                <div class="card" style="    background-color: #00800059;border: #09950b 1px solid; text-align: center;margin-left: 0%;margin-top: 4%;">
                  <h style="font-size: 42px;
    color: green;"> Total Average Profit</h><br>
    <h style="font-size: 42px;
    color: green;"> {{round($avg, 2)}} %</h>

              </div>
              @else
              <div class="card" style="background-color: #80000059;
    border: #fa0a0a 1px solid; text-align: center;margin-left: 0%;margin-top: 4%;">
                  <h style="font-size: 42px;
        color: #d90909;"> Total Average Profit</h><br>
    <h style="font-size: 42px;
        color: #d90909;"> {{round($avg, 2)}} %</h>

              </div>
              @endif

            </div> 
            <div class="col-md-4 col-lg-4 col-xl-4 col-xxl-4" style="">
                    <div class="card">
                      <div class="card-header" style="background-color: #35354b;color:white">
                        <h3 class="card-title">STATS</h3>

                        <div class="card-tools">
                          <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                          </button>
                          
                        </div>
                      </div>
                      <!-- /.card-header -->
                      
                      <!-- /.card-body -->
                      <div class="card-footer bg-white p-0">
                        <ul class="nav nav-pills flex-column" style="background-color: #35354b;">
                          <li class="nav-item" >
                            <h class="nav-link">
                              USD INVESTED
                              
                            </h>
                            <p href="#" class="nav-link" style="font-weight: bold;">
                                {{round($exchange, 2)}}
                              
                            </p>
                            <h class="nav-link">
                              Total USD return
                              
                            </h>
                            <p href="#" class="nav-link" style="font-weight: bold;">
                                {{$todayP}}
                              
                            </p>
                          </li>
                          <li class="nav-item">
                            <h8 class="nav-link">
                              Total <span style="color:#07c1ca">USD</span> IN ASSETS ON EXCHANGE :
                              
                            </h8>
                            <h3 class="nav-link" >
                              @if(round($avg, 2)< 0)
                                      <i class="fas fa-wallet" style="color:red"></i> <span style="color:red;text-align: center;">{{round($exchange2ndtime,2)}}({{round($avg, 2)}} %)
                              </span>
                              @else
                                  <i class="fas fa-wallet" style="color:#07c887"></i> <span style="color:#07c887;text-align: center;">{{round($exchange2ndtime,2)}}({{round($avg, 2)}} %)
                              </span>

                              @endif
                              
                              
                            </h3>
                            
                                    
                              
                            
                          </li>
                          <li class="nav-item">
                            
                            <h4 class="nav-link">
                                <i class="fa fa-receipt" style="color:red"></i>
                              Trading API Exchange
                              
                            </h4>
                            <h5 class="nav-link">
                                <i class="fa fa-receipt" style="color:#07c1ca;display:none;"></i>
                              Account : <span  style="font-weight: bold;">Default</span>
                              
                            </h5>
                            <h5 class="nav-link">
                                <i class="fa fa-receipt" style="color:#07c1ca;display:none;"></i>
                              Exchange : <span  style="font-weight: bold;">Binance</span>
                              
                            </h5>
                            <p href="#" class="nav-link" style="text-align: center;margin-top: 30%;">
                              <!--  <button   class="btn btn-primary withdraw" data-id="{{auth()->user()->id}}" style="color: black;
    background: #e7efef;
    border: #d4dad9 1px solid;">Withdraw</button>
                               <button  class="btn btn-primary deposit" data-id="{{auth()->user()->id}}" style="color: black;
    background: #e7efef;
    border: #d4dad9 1px solid;">Fund Account</button> -->
                              
                            </p>
                          </li>





                          
                        </ul>
                      </div>
                      <!-- /.footer -->
                    </div>
                    <div class="card" style="margin-top: 8%; height:19%;background-color: #35354b;">
                      <!-- <div class="card-header" style="background-color: #35354b;color:white">
                       
                     
                        
                      </div> -->
                      <ul class="nav nav-pills flex-column" style="background-color: #35354b;">
                      <li class="nav-item" style="background-color: #35354b;color:white">
                            
                            <h4 class="nav-link">
                                
                              Referral Link
                              
                            </h4>
                            
                            
                            <p href="#" class="nav-link" style="text-align: center;">
                               
    <div class="input-group">
  <!-- <p type="text" class="form-control" >Helo </p> -->
  <input type="text" class="form-control"   value="{{route('register')}}/?id={{$reffKey}}" id="myInput" readonly >
  <span class="input-group-btn">
    <button class="btn btn-success" type="button" onclick="myFunction()">Copy</button>
  </span>
</div>

                              
                            </p>
                          </li>
                        </ul>
                 

          </div>
                </div>

        <!--   <div class="row">
          
           <div class="col-md-6 col-lg-6 col-xl-6 col-xxl-6">
          <div class="card-body" style="    background-color: #00800059;
    height: 192px;
    width: 715px;
">
               
              </div>
          </div>
      </div> -->
           <div class="row">
          <!-- Left col -->
           <div class="col-md-12 col-lg-12 col-xl-12 col-xxl-12">
          <!-- TradingView Widget BEGIN -->
<div class="ticker-container">
<div class="ticker-canvas">
<div class="tradingview-widget-container">
  <div class="tradingview-widget-container__widget"></div>
  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-tickers.js">
  {
  "symbols": [
    {
      "proName": "OANDA:SPX500USD",
      "title": "S&P 500"
    },
    {
      "proName": "OANDA:NAS100USD",
      "title": "Nasdaq 100"
    },
    {
      "proName": "FX_IDC:EURUSD",
      "title": "EUR/USD"
    },
    {
      "proName": "BITSTAMP:BTCUSD",
      "title": "BTC/USD"
    },
    {
      "proName": "BITSTAMP:ETHUSD",
      "title": "ETH/USD"
    }
  ],
  "colorTheme": "dark",
  "isTransparent": false,
  "locale": "en"
}
  </script>
</div>
</div>
</div>
<!-- TradingView Widget END -->
                
            </div> 
          </div>



        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->

   
      




    </section>
    <script src="{{ asset('js/chart/tv.js')}}"></script>
    <script src="{{ asset('js/chart/bundle.js')}}"></script>
<script type="text/javascript">
   $( document ).ready(function() {
  $( ".tradingview-widget-container iframe" ).clone().appendTo( ".tradingview-widget-container" );


});

 function myFunction() {
  var copyText = document.getElementById("myInput");
  copyText.select();
  copyText.setSelectionRange(0, 99999)
  document.execCommand("copy");
  alert("Link is copied ");
}

  




</script>


@endsection