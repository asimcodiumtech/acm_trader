<!DOCTYPE html>
<html lang="en">


<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<meta name="description" content="Cryptocash is Professional Creative Template" />-->
    <!-- SITE TITLE -->
    <title>ACM-Traders</title>
    <!-- Favicon Icon -->
    <link rel="shortcut icon" type="image/x-icon" href="images/acm_trader.png" style="height:200px;width:100px;">
    <!-- Animation CSS -->
    <link rel="stylesheet" href="whitelanding/assets/css/animate.css">
    <!-- Latest Bootstrap min CSS -->
    <link rel="stylesheet" href="whitelanding/assets/bootstrap/css/bootstrap.min.css">
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="whitelanding/assets/css/font-awesome.min.css">
    <!-- ionicons CSS -->
    <link rel="stylesheet" href="whitelanding/assets/css/ionicons.min.css">
    <!-- cryptocoins CSS -->
    <link rel="stylesheet" href="whitelanding/assets/css/cryptocoins.css">
    <!--- owl carousel CSS-->
    <link rel="stylesheet" href="whitelanding/assets/owlcarousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="whitelanding/assets/owlcarousel/css/owl.theme.default.min.css">
    <!-- Magnific Popup CSS -->
    <link rel="stylesheet" href="whitelanding/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="whitelanding/assets/css/spop.min.css">
    <!-- Style CSS -->
    <link rel="stylesheet" href="whitelanding/assets/css/style.css">
    <link rel="stylesheet" href="whitelanding/assets/css/responsive.css">
    <link rel="stylesheet" href="whitelanding/assets/css/style1.css">
    <link rel="stylesheet" href="whitelanding/assets/css/style3.css">
    <!-- Color CSS -->
    <link id="layoutstyle" rel="stylesheet" href="whitelanding/assets/color/theme.css">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106310707-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-106310707-1', {
            'anonymize_ip': true
        });

    </script>

    <!-- Start of StatCounter Code -->
    <script>
        <!--
        var sc_project = 11921154;
        var sc_security = "6c07f98b";
        var scJsHost = (("https:" == document.location.protocol) ?
            "https://secure." : "http://www.");
        //

        -->
        document.write("


    <sc"+"ript src='" +scJsHost +"statcounter.com/counter/counter.js'>
        </"+"script>");
    </script>
    <noscript>
        <div class="statcounter"><a title="web analytics" href="https://statcounter.com/"><img class="statcounter" src="https://c.statcounter.com/11921154/0/6c07f98b/0/" alt="web analytics" /></a></div>
    </noscript>
    <!-- End of StatCounter Code -->
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/5dfca1f143be710e1d22f56b/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();

    </script>
    <!--End of Tawk.to Script-->
</head>

<body class="v_dark" data-spy="scroll" data-offset="110">

    <!-- END LOADER -->

    <!-- START HEADER -->
    <style type="text/css">
        .contact_detail {
            padding-left: 17px !important;
        }

        .btn.btn-radius {
            border-radius: 5px !important;
        }



        .banner_vector2 {
            width: 65%;
            height: 60%;
            top: 28px;
            z-index: 10;
            position: relative;
        }

        #bgc {
            background-color: rgb(255, 255, 255) !important;
            height: 80px;
        }

        .newsletter_form button {
            padding: 7px 6px;
            position: absolute;
            right: 5px;
            top: 5px;
        }

    </style>
    <header class="header_wrap fixed-top pt-0">
        <div class="container-fluid p-0">
            <nav class="navbar navbar-expand-lg" id="nav-bar-color">
                <a class="navbar-brand page-scroll animation" href="#home_section" data-animation="fadeInDown" data-animation-delay="1s">
                    <img src="images/acm_trader.png" alt="logoimage" / style="height:200px !important; width:200px !important; margin-top:13px !important;">

                </a>
                <button class="navbar-toggler animation" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" data-animation="fadeInDown" data-animation-delay="1.1s">
                    <span class="ion-android-menu"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto mr-5">
                        <li class="dropdown animation" data-animation="fadeInDown" data-animation-delay="1.1s">
                            <a class="nav-link dropdown-toggle active" href="{{url('/')}}">Home</a>

                        </li>
                        <li class="animation" data-animation="fadeInDown" data-animation-delay="1.2s"><a class="nav-link page-scroll nav_item" href="#service">Services</a></li>
                        <li class="animation" data-animation="fadeInDown" data-animation-delay="1.3s"><a class="nav-link page-scroll nav_item" href="#about">About</a></li>
                        <li class="animation" data-animation="fadeInDown" data-animation-delay="1.8s"><a class="nav-link page-scroll nav_item" href="#aff">Affiliated Program</a></li>
                        <li class="animation" data-animation="fadeInDown" data-animation-delay="1.8s"><a class="nav-link page-scroll nav_item" href="#faq">FAQs</a></li>

                        <li class="animation" data-animation="fadeInDown" data-animation-delay="1.8s"><a class="nav-link page-scroll nav_item" href="#contact">Contact</a></li>
                    </ul>
                    <ul class="navbar-nav nav_btn align-items-center navbar-buttons">

                        <li class="animation" data-animation="fadeInDown" data-animation-delay="2s"><a class="btn btn-default btn-radius nav_item" href="{{route('login')}}" style="color: white;">Login</a></li>
                        <li class="animation" data-animation="fadeInDown" data-animation-delay="2s"><a class="btn btn-default btn-radius nav_item" href="{{route('register')}}" style="color: white;">Register</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <!-- END HEADER -->

    <!-- START SECTION BANNER -->
    <section id="home_section" class="section_banner bg_black_dark" data-z-index="1" data-parallax="scroll" style="background-image: url('whitelanding/assets/images/Section1-bg.png'); background-repeat: no-repeat; background-size: 100% 100%;">
        <!-- <div id="banner_bg_effect" class="banner_effect"></div> -->
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                    <h1>World’s #1</h1>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 text-center mt-1">
                    <h2 class="Auto">automated crypto trading bot</h2>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 text-center mt-1">
                    <h6 class="Auto">Take your emotion out of the equation</h6>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 text-center mt-1">
                    <p class="AutoP">A trading bot that automagically buy and sell various cryptocurrencies at the right time with the goal<br> of generating a profit, computer algorithm that automatically execute trades on your behalf
                    </p>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 text-center mt-1">

                    <img class="banner_vector2" alt="banner_vector2" src="whitelanding/assets/images/Untitled-3.png">
                </div>

            </div>
        </div>
    </section>

    <section id="service" class="small_pb" data-z-index="1" data-parallax="scroll" style="background-image: url('whitelanding/assets/images/WhyAcmbg.png'); background-repeat: no-repeat; width: 100%;background-size: cover;position: relative;
Padding: 200px 0;">
        <div class="container service-div">
            <div class="row align-items-center">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="box_wrap text-center animation  why_image_text_div" data-animation="fadeInUp" data-animation-delay="0.6s">
                            <h4 class="why_image_text"><img class="why_images" src="whitelanding/assets/images/brain.png"> Using artificial intelligence (AI)</h4>
                            <p>
                                Bot trading
                                is using software to
                                automate
                                trading. Bots
                                talk to an
                                exchange via an “API” and can
                                place buy and sell orders for you </p>
                        </div>

                        <div class="box_wrap text-center animation why_image_text_div" data-animation="fadeInUp" data-animation-delay="0.8s">
                            <h4 class="why_image_text"><img class="why_images" src="whitelanding/assets/images/focus.png"> Focus on more Meaningful things</h4>
                            <p>You Don't need to be with your eyes glued to the screen all day long. Our team has spent years learning, watching, adapting and trading different charting patterns. And we build an automated crypto robot based on this research.</p>
                        </div>
                        <div class="box_wrap text-center animation why_image_text_div" data-animation="fadeInUp" data-animation-delay="1s">
                            <h4 class="why_image_text"><img class="why_images" src="whitelanding/assets/images/process.png"> Efficient Execution </h4>
                            <p>efficient execution of your trades and better
                                entry prices. If you’re quicker in terms of trade
                                execution, you have a time advantage.
                                A trading bot can also detect chart pattens
                                that are more difficult to spot by a human eye </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12" id="boxAllign">
                        <div class="box_wrap text-center animation why_image_text_div" data-animation="fadeInUp" data-animation-delay="0.6s">
                            <h4 class="why_image_text"><img class="why_images" src="whitelanding/assets/images/brai.png"> Eliminates Emotions</h4>
                            <p>Eliminates the emotions out of the decision-making process. That's kind of a big deal. Many crypto traders fail because they don't have the discipline required to be a professional trader.</p>
                        </div>

                        <div class="box_wrap text-center animation why_image_text_div" data-animation="fadeInUp" data-animation-delay="0.8s">
                            <h4 class="why_image_text"><img class="why_images" src="whitelanding/assets/images/return.png"> Maximize Returns</h4>
                            <p>If you want a unique opportunity to maximize your crypto returns ACM TRADER automatically bus cheap crypto and looks to sell it for a higher price on a crypto exchange.</p>
                        </div>
                    </div>

                    <!-- whyyyyyyyyyyyyyyyyyyyyy -->

                    <div class="col-lg-3 col-md-6 col-sm-12 text-left" id="boxAllign1">
                        <h2 class="ml-0 h-margin" id="H2">WHY</h2>
                        <h2 id="H3"> ACM TRADER</h2>
                        <p class="ml-0 mb-3 p-margin" id="P1">ACM TRADER was founded on the basis of delivering financial emancipation and to allow people to enjoy life by doing more meaningful things and to ease the stress of making</p>
                        <a href="{{route('about')}}" class="btn btn-default btn-radius" data-animation="fadeInUp" data-animation-delay="1s">Learn More</a>
                    </div>

                </div>

            </div>
        </div>
    </section>

    <section id="about" class="small_pt" data-z-index="1" data-parallax="scroll" style=" background-color: #f6f8ff;">
        <div class="container about-div">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-12 col-sm-12 pl-0">
                    <div class="title_default_light title_border">
                        <h2 id="about" class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">ABOUT</h2>
                        <h3 id="about1" class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Background History </h3>
                        <p id="about2" class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">ACM TRADER (Automated Crypto Market Trader ) was founded
                            under the notion of trading cryptocurrency, in real time without
                            spending long hours in the market. However before the concept
                            came into motion, the founder Dave had been trading the
                            market favorably as he spent long hours doing market analysis
                            and making steady profits. Characteristically Dave is a pioneer, </p>
                    </div>
                    <a href="{{route('about')}}" class="btn btn-default btn-radius" data-animation="fadeInUp" data-animation-delay="1s">Learn More</a>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="text_md_center">
                        <img class="animation " data-animation="zoomIn" data-animation-delay="0.2s" src="whitelanding/assets/images/Aboutus-panel.png" alt="aboutimg2" />
                    </div>
                </div>
            </div>

        </div>
    </section>



    <section id="aff" class="small_pt" data-z-index="1" data-parallax="scroll" style="background-image: url('whitelanding/assets/images/Affiliate-Bg.png'); background-repeat: no-repeat; background-size: 100% 100%;">
        <div class="container aff-div">
            <div class="row align-items-center">
                <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                    <h1 class="Affi">Affiliated Program</h1>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                    <p class="Affi1">Earn passive income by join our affiliate program and sharing this opportunity with the world.<br>Click below for more information</p>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                    <img class="Affi2" class="animation" data-animation="zoomIn" data-animation-delay="0.2s" src="whitelanding/assets/images/Image-1.png" alt="aboutimg2" />
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 text-center mt-5 ml-2">
                    <a href="{{route('aff')}}" class="btn btn-default btn-radius" data-animation="fadeInUp" data-animation-delay="1s">Learn More</a>
                </div>
            </div>

        </div>
    </section>


    <section class="small_pt" id="faq" data-z-index="1" data-parallax="scroll" style="background-image: url('whitelanding/assets/images/Faq-bg.png'); background-repeat: no-repeat; background-size: 100% 100%;">
        <div class="container questions-div">
            <div class="row">
                <div class="col-lg-8 col-md-12 offset-lg-2 ">
                    <div class="title_default_light title_border text-center">
                        <h2 id="H2" class="animation h-margin" data-animation="fadeInUp" data-animation-delay="0.2s">Have Any Questions?</h2>
                        <p id="P1" class="animation p-margin" data-animation="fadeInUp" data-animation-delay="0.4s">Frequently asked questions (FAQ) or Questions and Answers (Q&A), are listed questions and answers, all supposed to be commonly asked in some context</p>
                    </div>
                </div>
            </div>
            <div class="row small_space">
                <div class="col-lg-8 col-md-12 offset-lg-2 rounded" style="background-color: white;">
                    <div id="accordion" class="faq_question">
                        <div class="card animation  my-3" data-animation="fadeInUp" data-animation-delay="0.6s" expanded="false" style="background-color:#f8f8f8 !important;">
                            <div class="card-header rounded" id="headingOne">
                                <h6 class="mb-0"> <a data-toggle="collapse" href="#collapseOne" aria-controls="collapseOne">
                                        <img src="whitelanding/assets/images/FAQicons/1.png" alt=""> &nbsp;&nbsp;What is a trading bot ?</a> </h6>
                            </div>
                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">Cryptocurrency trading bots are computer programs that automagically buy and sell various cryptocurrencies at the right time with the goal of generating a profit, using computer algorithm that automatically
                                    execute trades on your behalf</div>
                            </div>
                        </div>
                        <div class="card animation  my-3" data-animation="fadeInUp" data-animation-delay="0.6s" style="background-color:#f8f8f8 !important;">
                            <div class="card-header" id="headingTwo">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><img src="whitelanding/assets/images/FAQicons/2.png" alt=""> &nbsp;&nbsp;How do trading bots
                                        trade?</a> </h6>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">Bot trading is using software to automate trading. Bots talk to an exchange via an “API” and can place buy and sell orders for you. </div>
                            </div>
                        </div>
                        <div class="card animation  my-3" data-animation="fadeInUp" data-animation-delay="0.8s" style="background-color:#f8f8f8 !important;">
                            <div class="card-header" id="headingThree">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><img src="whitelanding/assets/images/FAQicons/3.png" alt=""> &nbsp;&nbsp;Are trading bots
                                        legal?</a> </h6>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">It is fully legal and welcome on most cryptocurrency exchanges</div>
                            </div>
                        </div>
                        <div class="card animation my-3" data-animation="fadeInUp" data-animation-delay="1s" style="background-color:#f8f8f8 !important;">
                            <div class="card-header" id="headingFour">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour"><img src="whitelanding/assets/images/FAQicons/4.png" alt="">&nbsp;&nbsp; What is a crypto trading
                                        bot?</a> </h6>
                            </div>
                            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                <div class="card-body">A trading bot is a piece of software that's designed to analyze cryptocurrency trading data and to place sell or buy orders on a trader's behalf. </div>
                            </div>
                        </div>





                        <div class="card animation my-3" data-animation="fadeInUp" data-animation-delay="1s" style="background-color:#f8f8f8 !important;">
                            <div class="card-header" id="four">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#cola" aria-expanded="false" aria-controls="cola"><img src="whitelanding/assets/images/FAQicons/5.png" alt=""> &nbsp;&nbsp; How Do Bots Work?</a> </h6>
                            </div>
                            <div id="cola" class="collapse" aria-labelledby="four" data-parent="#accordion">
                                <div class="card-body">Investors can subscribe to free bot programs to aid in their cryptocurrency trading. On the other hand, many bots have user fees, some of which can be quite steep. Typically, investors seek out the bot or bots
                                    that will be most useful for them </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- START SECTION CONTACT   #f6f8ff-->
    <section id="contact" class="contact_section small_pt" data-z-index="1" data-parallax="scroll" style="background-image: url('whitelanding/assets/images/Contactus-bg.png'); background-repeat: no-repeat;background-size: 100% 100%;">
        <div class="container contact-div">

            <div class="row">
                <div class="col-lg-12 offset-lg-1 col-md-12 col-sm-12 contact-title">
                    <h1 data-animation="fadeInLeft" data-animation-delay="0.1s" id="H2">Contact With Us</h1>
                    <p data-animation="fadeInLeft" data-animation-delay="0.1s" id="P4">Our office is located in a beautiful building and garden</p>
                </div>
            </div>

            <div class="row align-items-center small_space">
                <div class="col-lg-3 offset-lg-1 col-md-4 col-sm-12">
                    <div class="bg_light_dark  contact_box_s2 animation" data-animation="fadeInLeft" data-animation-delay="0.1s" id="bgc">

                        <div class="contact_detail">
                            <p>
                                <ul class="list_none social_icon">
                                    <li class="animation" data-animation="fadeInUp" data-animation-delay="0.4s"><a href="#"><i class=" fa fa-phone" style="background-color: #ff0004 !important;color: white !important;"></i></a></li>
                                </ul>+23 0123 4567
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3  col-md-4 col-sm-12">
                    <div class="bg_light_dark  contact_box_s2 animation" data-animation="fadeInLeft" data-animation-delay="0.1s" id="bgc">

                        <div class="contact_detail">
                            <p>
                                <ul class="list_none social_icon">
                                    <li class="animation" data-animation="fadeInUp" data-animation-delay="0.4s"><a href="#"><i class="fa fa-envelope-o" style="background-color: #ff0004 !important;color: white !important;"></i></a></li>
                                </ul>Admin@acmtrader.com
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3  col-md-3 col-sm-12">
                    <div class="bg_light_dark  contact_box_s2  animation" data-animation="fadeInLeft" data-animation-delay="0.1s" id="bgc">

                        <div class="contact_detail">
                            <p>
                                <ul class="list_none social_icon">
                                    <li class="animation" data-animation="fadeInUp" data-animation-delay="0.4s"><a href="#"><i class="fa fa-map-marker" style="background-color: #ff0004 !important;color: white !important;"></i>
                                        </a></li>
                                </ul>20 Guild Rd, Charlton London
                            </p>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row align-items-center small_space form-div ">
                <div class="col-lg-9 offset-lg-1 col-md-9 col-sm-12" style="background-color: white;padding: 1%;margin:0 10%;">
                    <form id="{{route('contact.request')}}">
                        <div class="row">
                            <div class="form-group col-lg-4  animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                                <input type="text" required="required" placeholder="Enter Name *" id="first-name" class="form-control form-control-wid" name="name" placeholder="">
                            </div>
                            <div class="form-group col-lg-4 animation" data-animation="fadeInUp" data-animation-delay="0.3s">
                                <input type="email" required="required" placeholder="Enter Email *" id="email" class="form-control form-control-wid" name="email">
                            </div>
                            <div class="form-group col-lg-4 animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                                <input type="number" required="required" placeholder="Enter Phone Number" id="subject" class="form-control form-control-wid" name="phone_number">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-12 animation" data-animation="fadeInUp" data-animation-delay="1s">
                                <textarea required="required" placeholder="Message *" id="description" class="form-control" name="message" rows="2"></textarea>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-lg-2 col-md-2 text-center animation" data-animation="fadeInUp" data-animation-delay="1.2s">
                                <button type="submit" title="Submit Your Message!" class="btn btn-default btn-radius btn-block" id="" name="submit" value="Submit" style="width:100px;">Submit</button>
                            </div>
                            <div class="contct_follow col-lg-5 offset-lg-5 ">
                                <span class="text-uppercase animation" data-animation="fadeInUp" data-animation-delay="0.2s">Follow Us</span>
                                <ul class="list_none social_icon">
                                    <li class="animation" data-animation="fadeInUp" data-animation-delay="0.4s"><a target="_blank" href="https://www.facebook.com"><i class=" fa fa-facebook"></i></a></li>
                                    <li class="animation" data-animation="fadeInUp" data-animation-delay="0.5s"><a target="_blank" href="https://www.twitter.com"><i class=" fa fa-twitter"></i></a></li>
                                    <li class="animation" data-animation="fadeInUp" data-animation-delay="0.6s"><a href="#"><i class=" fa fa-google-plus"></i></a></li>
                                    <li class="animation" data-animation="fadeInUp" data-animation-delay="0.7s"><a target="_blank" href="https://www.pinterest.com"><i class=" fa fa-pinterest"></i></a></li>
                                    <li class="animation" data-animation="fadeInUp" data-animation-delay="0.8s"><a target="_blank" href="https://www.linkedin.com"><i class=" fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <footer data-z-index="1" data-parallax="scroll" style="background-image: url('whitelanding/assets/images/Footer-bg.png'); background-repeat: no-repeat; ">

        <div class="container" id="newsletter">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                    <h4 id="H2" class="footer_title border_title animation pl-0 footer-desc" data-animation="fadeInUp" data-animation-delay="0.2s">Newsletter</h4>
                    <p id="P4" class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">By subscribing to our mailing list you will always be update with the latest<br> news from us.</p>

                </div>
               <div class="col-lg-6 offet-lg-4 col-md-12 col-sm-12 text-center mx-auto">
                   @foreach ($errors->all() as $message) 
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{$message}}</strong> 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
    
               @endforeach
               @if(session('success'))
    
    <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{session('success')}}</strong> 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
@endif
                    <div class="input-group mb-3 p-1 shadow-box rounded  ">
                      <form class="form-inline animation" data-animation="fadeInUp" data-animation-delay="0.4s" action="{{route('user.newsletter')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="text" class="form-control" placeholder="Enter Email Address"  style="border:none;" name="email" required>
                        <div class="input-group-append">
                            <button class="btn btn-success btn-info" value="Submit" name="submit" title="Subscribe" type="submit">Submit</button>
                        </div>
                          </form>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                    <p id="P1"> Cryptocurrency
                        is a volatile market and not so easy to trade. If you’re
                        an inexperienced trader, you <br>should consider the benefits of using a
                        crypto trading bot to increase your profits.</p>
                </div>
                <div class="col-lg-8 offset-lg-2 col-md-7 col-sm-12 text-center mx-auto">
                    <a id="line" href="#service">Services</a>
                    <a id="line" href="#about">About</a>
                    <a id="line" href="#aff">Affiliated Program</a>
                    <a id="line" href="#faq">FAQs</a>

                    <a id="line" href="#contact">Contact</a>


                </div>

            </div>
        </div>
        <div class="col-md-8 offset-md-2">
            <hr  style="width: 88% !important;">

        </div>
        <div class="container">
            <div class="row footer-div">

                <div class="col-md-4 offset-md-2">
                    <p id="P4" class="copyright">Copyright &copy; 2018 All Rights Reserved.</p>
                </div>
                <div class="col-md-4">
                    <ul class="list_none footer_menu">
                        <li><a>Privacy Policy</a></li>
                        <li><a>Terms & Conditions</a></li>
                    </ul>
                </div>
                <div class="col-md-8 offset-md-2">
  <div class="row">
    <div class="col-md-12">
      <div id="P2">
        <p   class="text-justify"  style="margin-bottom: 0px !important;"><u><strong>Risk Warning: </strong></u>ACM Trader uses operating systems and information to provide potentially lucrative rewards, however these rewards are tied to high risks. The high magnitude of leverage can work advantageously for you as well as disadvantageously against you. As a user of the ACM Trader, trading software you must be mindful of the risk factors involved in trading the market and be prepared to accept them in order to understand the behavioral patterns of various markets traded. Trading at a high degree involves substantial risk of loss and may not be fitting for all investors. Be warned not to trade with loaned money or with money that you cannot afford to lose. ACM Trader's platform does not pressure or persuade anyone to fund an account and does not promise successive rewards. ACM Trader only creates the potential of realistically moderate to high returns on a calculated time frame. There are no guarantees of consistent returns however there is an increased likelihood of lucrative gains</p>
      </div>
    </div>
    <div class="col-md-12">
      <div id="">
        <p  class="text-justify" style="margin-bottom: 0px !important;">ACM Trader does not act as an investment advisory, however the information contained on this website is based on open research and general market commentary. Website owners and affiliates will therefore, not be liable for any loss or damage, including without limitation to, any loss of profit, which may occur directly or indirectly from the use of or reliance on the systems ability to trade. Please note that the past performance of any trading system or techniques is not necessarily indicative of future results.</p>
      </div>
    </div>
  </div>
</div>
            </div>

        </div>
    </footer>

    <script src="whitelanding/assets/js/jquery-1.12.4.min.js"></script>
    <!-- Latest compiled and minified Bootstrap -->
    <script src="whitelanding/assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- owl-carousel min js  -->
    <script src="whitelanding/assets/owlcarousel/js/owl.carousel.min.js"></script>
    <!-- magnific-popup min js  -->
    <script src="whitelanding/assets/js/magnific-popup.min.js"></script>
    <!-- waypoints min js  -->
    <script src="whitelanding/assets/js/waypoints.min.js"></script>
    <!-- parallax js  -->
    <script src="whitelanding/assets/js/parallax.js"></script>
    <!-- countdown js  -->
    <script src="whitelanding/assets/js/jquery.countdown.min.js"></script>
    <!-- particles min js  -->
    <script src="whitelanding/assets/js/particles.min.js"></script>
    <!-- scripts js -->
    <script src="whitelanding/assets/js/jquery.dd.min.js"></script>
    <!-- jquery.counterup.min js -->
    <script src="whitelanding/assets/js/jquery.counterup.min.js"></script>

    <script src="whitelanding/assets/js/notification.js"></script>
    <!-- scripts js -->
    <script src="whitelanding/assets/js/scripts.js"></script>
</body>

</html>
