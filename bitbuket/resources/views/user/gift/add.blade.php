@extends('layouts.app')

@section('dashboardcontent')
<style>
.bg {
  
  background-repeat: no-repeat;
  
  background-position: center;
  height: 20vh;
  width: 20vh;
}
</style>
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 style="color: white;">Gift Add</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Gift Add</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


<section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8" >
    				<div class="card card-primary">
              <div class="card-header" style="background-color: #42426a;color: white;">
                <h3 class="card-title">Set Gift Detail</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="{{ route('user.gift.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="card-body" style="background-color:#35354B; color:white;">
                  <div class="form-group">
                    <select  name="coin_type" class="form-control coin_type"  tabindex="3" required style="background-color:#131311; color:white;"><option value="">Please Select</option><option value="BTC">Purchase gift code</option><option value="gift">Purchase gift code with available balance</option></select>                 
                     </div>
                     <div class="gift" style="display:none;">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Profit Available</label>
                          <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="" value="$ {{$amount}} " required style="background-color:#131311; color:white;" readonly>
                        </div>


                          <div class="form-group">
                             <input type="hidden" class="form-control" id="exampleInputEmail1" placeholder="Enter acount Name " name="gift_code" value="{{$hash}}" required style="background-color:#131311; color:white;" readonly>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Set  Amount</label>
                            <input type="number" class="form-control amountaa" id="exampleInputEmail2" placeholder="Enter amount in USD " name="amount" value="" required style="background-color:#131311; color:white;">
                          </div>
                          
                          <div class="card-footer" >
                  <button type="submit" class="btn btn-primary" style="background-color: #35354B;color: white;">Submit</button>
                </div>
                          
                  </div>
                   <link rel="stylesheet" href="https://bootswatch.com/4/superhero/bootstrap.css" crossorigin="anonymous">   
      
    <!-- Note - If your website not use Bootstrap4 CSS as main style, please use custom css style below and delete css line above. 
    It isolate Bootstrap CSS to a particular class 'bootstrapiso' to avoid css conflicts with your site main css style -->
    <!-- <link rel="stylesheet" href="css/superhero.min.css" crossorigin="anonymous"> -->


    <!-- JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.12.0/js/all.js" crossorigin="anonymous"></script>
    <script src="<?php echo CRYPTOBOX_JS_FILES_PATH; ?>support.min.js" crossorigin="anonymous"></script>

    <!-- CSS for Payment Box -->
    <style>
            html { font-size: 14px; }
            @media (min-width: 768px) { html { font-size: 16px; } .tooltip-inner { max-width: 350px; } }
            .mncrpt .container { max-width: 980px; }
            .mncrpt .box-shadow { box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .05); }
            img.radioimage-select { padding: 7px; border: solid 2px #ffffff; margin: 7px 1px; cursor: pointer; box-shadow: none; }
            img.radioimage-select:hover { border: solid 2px #a5c1e5; }
            img.radioimage-select.radioimage-checked { border: solid 2px #7db8d9; background-color: #f4f8fb; }
    </style>
                    <div class="btc" style="display:none;">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Set  Amount</label>
                          <input type="number" class="form-control amountInput" id="amountInput" placeholder="Enter amount in USD " name="as" value=""  style="background-color:#131311; color:white;">
                        </div>
                        <div class="form-group">
                          
                          <button type="button" class="btn btn-primary float-right payment" style="background-color: #35354B;color: white;">Make Payment</button>
                        </div>
                        
                    <div class="form-group btcData">
                          <!-- Bootstrap4 CSS - -->
                       
                    </div>
                </div>
                 

                <!-- /.card-body -->

                
              </form>
              
            		</div>
           </div>
       </div>
   </div>
</section>

<script type="text/javascript">
    $('.coin_type').on('change', function() {
           
               var vcoin_type = $("select.coin_type").children("option:selected").val();
              if(vcoin_type=="BTC")
              {
                 
                 $('.btc').show();
                 $('.gift').hide();
                 $('.card-footer').hide();
                 
              }
              
              if(vcoin_type=="gift")
              {  
                console.log(vcoin_type);
                $('.gift').show();
                $('.card-footer').show();
                $('.btc').hide();
                
              }

              if(vcoin_type=='')
              {  
                console.log(vcoin_type);
                $('.gift').hide();
                $('.btc').hide();
                $('.card-footer').hide();
              }
        });
$(".payment").click( function () {
   
       var amountInput=$('.amountInput').val();
                if(amountInput)
                 {
                   var amount=parseFloat(amountInput);
                   var afterFee=Number(parseFloat(amountInput) + parseFloat(amountInput)*0.1).toFixed(3);
                   
                   
                        $('.amountInputfee').val(afterFee);
                        
                        var url = '<?= route('user.ajax.btc')?>';
                        data={"_token": "{{ csrf_token() }}",amount:amount,type:'gift'};
                              $.ajax({
                                type: "POST",
                                url: url,
                                data: data,
                                success: function (response) {
                                    console.log(response);
                                      $('.bootstrapiso').html('');
                                          $('.bootstrapiso').html('');$('.bootstrapiso').html('');
                                    $('.btcData').fadeOut().html(response.data).fadeIn();
                                    
                                },
                                error: function () {
                                    
                                }
                            });
                    
                }
                else
                {
                   
                    $('.btcData').fadeOut().html('').fadeIn();
                    $('.amountInputfee').val(0);
                    
                }
            });
            
            $(".amount").on('change keyup', function () {
                
                if($(this).val() )
                {    var checkamount=parseInt($(this).val());
                     var fee=Number(parseFloat($(this).val()) + parseFloat($(this).val())*0.1).toFixed(3);
                     $('.amountfee').val(fee);
                
                }
                else
                {
                    $('.amountfee').val(0);
                }
            });

</script>


@endsection