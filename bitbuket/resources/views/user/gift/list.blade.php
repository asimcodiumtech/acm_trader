@extends('layouts.app')

@section('dashboardcontent')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 style="color: white;">Gift List</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Gift List</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<section class="content">
      <div class="row">
        <div class="col-12">

			<div class="card">
			            <div class="card-header" style="background-color: #35354B;color: white;">
			              <h3 class="card-title">All Gift List</h3>

			            </div>
                      @if(auth()->user()->admin==2)
                        <div class="card-header" style="background-color: #35354B;color: white;">
                          <a  class="btn btn-info" href="{{route('user.gift.add')}}">Add Gift</a> 

                        </div>
                        @endif
                        

			            <!-- /.card-header -->
                         			            <div class="card-body" style="background-color: #35354B;color: white;">

                    <div class="table-responsive">
			              <table  class="table table-bordered table-striped example1">
			                <thead style="background-color:#131311; color:white;">
			                <tr>
			                  <th>Gift Code</th>
			                  <th>Assign To</th>
			                  <th>Gift Amount</th>
			                  <th>Status</th>
                              <th>Action</th>
                              <th>Created At</th>
                              
			                </tr>
			                </thead>
			                <tbody style="background-color: #35354B;color: white;">
			                @foreach($gifts as $gift)
                            @if($gift->created_by==auth()->user()->id ||$gift->user_id==auth()->user()->id

                             || $gift->created_by==$id ||$gift->user_id==$id

                             )
                            <tr>
                             	<td>
          						    <div class="input-group">
  <!-- <p type="text" class="form-control" >Helo </p> -->
  <input type="text" class="form-control"   value="{{$gift->gift_code}}" id="myInput{{$gift->id}}" readonly style="    background-color: #1a242e !important;    color: #ffffff !important;">
  @if($gift->status==0)
  <span class="input-group-btn">
   <button class="btn btn-success" type="button" onclick="myFunction2('{{$gift->id}}')">Copy</button>
  </span>
  @endif
</div>
                         
                          
          						</td>
          						<td>
                                    @if($gift->user_id)
          						   ({{$gift->user_id}})
                                   {{$gift->name($gift->user_id)}}
                                   @endif
          						</td>
                                <td>
          						    {{$gift->amount}}
          						</td>
          						
                                <td>
                                   @if($gift->status==1)
                                    <button  class="btn btn-success"><b> Used </b></button>
                                    @elseif($gift->status==2)
                                    <button  class="btn btn-warning"><b> Pending</b></button>
                                    
                                    @else
                                   <button  class="btn btn-danger"><b> Not Assigned </b></button>
                                   @endif
                                </td>
                                <td>
                                    @if($gift->created_by== auth()->user()->id ||$gift->created_by==$id)
                                    @if($gift->status==1)
                                    <button  class="btn btn-success"><b> Verified </b></button>
                                    @elseif($gift->status==2 && auth()->user()->admin==2)
                                    <button  class="btn btn-info pending"  data-id="{{$gift->id}}"><b> Need Approval</b></button>
                                    
                                    @else
                                   <button  class="btn btn-danger"><b> Not Assigned </b></button>
                                   @endif
                                   @endif
                                </td>
                                <td>
          						    {{$gift->created_at}}
          						</td>
						    </tr>
                            @endif
			                @endforeach
			                </tbody>
			                <tfoot style="background-color:#131311; color:white;">
			                <tr>
    			             <th>Gift Code</th>
                        <th>Assign To</th>
                        <th>Gift Amount</th>
                        <th>Status</th>
                        <th>Action</th>
                        <th>Created At</th>
			                </tr>
			                </tfoot>
			              </table>
                  </div>
			            </div>
			            
			</div>
	   </div>
	  </div>
</section>	


<script>
$(function () {
    $(".example1").DataTable({
        responsive: true
    });
    $('.example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
  function myFunction2(data) {
      if(data==null)
    {
      data='';
    }
    var id= "myInput" + data;
  var copyText = document.getElementById(id);
  copyText.select();
  copyText.setSelectionRange(0, 99999)
  document.execCommand("copy");
  alert("Link is copied ");
}

 $(document).ready(function() {
     
     var amount =  "{{ Session::get('amount') }}";


  var code = "{{ Session::get('code') }}";

  

  if(code!='')
  {
    console.log(code);

  var modi ="modal";

   html='<div class="input-group">' ;
   html+='<input type="text" class="form-control"   value="'+code+'" id="myInput" readonly style="    background-color: #1a242e !important;    color: #ffffff !important;">';
 
  
  html+='<span class="input-group-btn">';
   html+= '<button class="btn btn-success" type="button" onclick="myFunction2()" style="margin: 8px 5px 0 !important;">Copy</button></span></div>';
  swal({
            title: "Created Link",
            text: "Note : All Created link yoy get 10% Discount",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Ok",
            animation: "slide-from-top",
            inputPlaceholder: "Write something",
            closeOnCancel: false
        });
                 $('.showSweetAlert').css("margin-top","-350px");
    $('.showSweetAlert fieldset').html(html);

  }
$('.example1').on('click', '.pending', function () {
  var id = $(this).data('id');
  



                 swal({
                            title: "Are you sure you want to Approved ?",
                            text: "",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes, Processed !!",
                            cancelButtonText: "No, cancel it !!",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },

  
                        function(isConfirm){
                            if (isConfirm) {
                              console.log(id);
                                var url = '<?= route('user.gift.ajax')?>';
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {"_token": "{{ csrf_token() }}", id: id,deposite: "doApproved"},
                    success: function (response) {
                        if (response.success) {
                            swal("Hey !!", "Gift Approved Successfully " , "success");
                            $('.confirm').on('click', function () {
                                window.location.reload();
                            });
                            
                        } else {
                            if (response.error == 409) {
                               
                                swal("Cancelled !!", "Hey, your Approval is denied !!", "error");

                            }
                            if (response.error == 401) {
                               
                                swal("Cancelled !!", "This Amount is alreayd Withdraw", "error");

                            }
                             else {
                                alert("Status can't be change.")
                            }
                        }
                    },
                    error: function () {
                        alert('Error occured');
                    }
                });

                                
                            }
                            else {
                                swal("Cancelled !!", "Hey, Cancelled the process !!", "error");
                            }
                        });


});

});

</script>	
<script>
  function topRight() {

var values= [{"positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false}];
        return values[0];

}
  /*toastr.success("{{ Session::get('success') }}", topRight());*/
    var type = "{{ Session::get('alert-type') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}","{{ Session::get('heading') }}",  topRight() );
            break;

        case 'warning':
            toastr.warning("{{ Session::get('message') }}" ,"{{ Session::get('heading') }}", topRight());
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}" ,"{{ Session::get('heading') }}", topRight());
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}" ,"{{ Session::get('heading') }}", topRight());
            break;
    }
    
 </script>
@endsection