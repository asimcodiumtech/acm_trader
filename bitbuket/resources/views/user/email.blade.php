<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from bestwebcreator.com/cryptocash/demo/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 21 Feb 2020 10:29:22 GMT -->
<head>
<!-- Meta -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Cryptocash is Professional Creative Template" />
<!-- SITE TITLE -->
<title>ACM-Traders</title>
<!-- Favicon Icon -->
<link rel="shortcut icon" type="image/x-icon" href="{{asset('images/acm_trader.png')}}">
<!-- Animation CSS -->
<link rel="stylesheet" href="{{ asset('landing/assets/css/animate.css')}}" >
<!-- Latest Bootstrap min CSS -->
<link rel="stylesheet" href="{{ asset('landing/assets/bootstrap/css/bootstrap.min.css')}}">
<!-- Google Font -->
<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
<!-- Font Awesome CSS -->
<link rel="stylesheet" href="{{ asset('landing/assets/css/font-awesome.min.css')}}" >
<!-- ionicons CSS -->
<link rel="stylesheet" href="{{ asset('landing/assets/css/ionicons.min.css')}}">
<!--- owl carousel CSS-->
<link rel="stylesheet" href="{{ asset('landing/assets/owlcarousel/css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{ asset('landing/assets/owlcarousel/css/owl.theme.default.min.css')}}">
<!-- Magnific Popup CSS -->
<link rel="stylesheet" href="{{ asset('landing/assets/css/magnific-popup.css')}}">
<!-- Style CSS -->
<link rel="stylesheet" href="{{ asset('landing/assets/css/style.css')}}">
<link rel="stylesheet" href="{{ asset('landing/assets/css/responsive.css')}}">
<!-- Color CSS -->
<link id="layoutstyle" rel="stylesheet" href="{{ asset('landing/assets/color/theme.css')}}">
<link rel="stylesheet" href="{{ asset('css/toastr/toastr.min.css')}}">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106310707-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
 gtag('config', 'UA-106310707-1', { 'anonymize_ip': true });
</script>

<!-- Start of StatCounter Code -->
<script>
    <!--
    var sc_project=11921154;
    var sc_security="6c07f98b";
        var scJsHost = (("https:" == document.location.protocol) ?
        "https://secure." : "http://www.");
            //-->
            
document.write("<sc"+"ript src='" +scJsHost +"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><a title="web analytics" href="https://statcounter.com/"><img class="statcounter" src="https://c.statcounter.com/11921154/0/6c07f98b/0/" alt="web analytics" /></a></div></noscript>
<!-- End of StatCounter Code -->

</head>

<body >
<!--<div class="demo">-->
<!--    <ul class="list_none">-->
<!--        <li><a href="http://bestwebcreator.com/cryptocash/" class="dm" title="See All Demo"><i class="ion-android-apps"></i></a></li>-->
<!--        <li><a href="http://bit.ly/2pzJavi" class="bg-green" title="Buy Now"><i class="ion-android-cart"></i></a></li>-->
<!--        <li><a href="http://bit.ly/2IePjW6" class="txt" title="PSD">PSD</a></li>-->
<!--        <li><a href="javascript:void(0)" class="icon"><i class="fa fa-gear"></i></a>-->
<!--            <div class="color-switch">-->
<!--                <p>Color Switcher</p>-->
<!--                <div class="color_box">-->
<!--                    <button value="theme" class="default active"></button>-->
<!--                    <button value="theme-green" class="green"></button>-->
<!--                    <button value="theme-orange" class="orange"></button>-->
<!--                    <button value="theme-lightgreen" class="lightgreen"></button>-->
<!--                    <button value="theme-redpink" class="redpink"></button>-->
<!--                </div>-->
<!--            </div>-->
<!--        </li>-->
<!--    </ul>-->
<!--</div>-->

<!-- START LOADER -->
<div id="loader-wrapper" class="">
    <div id="loading-center-absolute" class="">
        <div class="object " id="object_four" style="background-color: black;"></div>
        <div class="object " id="object_three" style="background-color: black;"></div>
        <div class="object " id="object_two" style="background-color: black;"></div>
        <div class="object " id="object_one" style="background-color: black;"></div>
    </div>
    <div class="loader-section section-left "  style="background-color: black;"></div>
    <div class="loader-section section-right " style="background-color: black;"></div>

</div>
<!-- END LOADER --> 

<!-- START HEADER -->
<header class="header_wrap fixed-top">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg"> 
            <a class="navbar-brand animation" href="{{url('/')}}" data-animation="fadeInDown" data-animation-delay="1s"> 
                <img src="{{asset('images/acm_trader.png')}}" alt="logoimage" / style="height:152px !important; width:150px !important;"> 
                <img class="logo_dark" src="{{asset('images/acm_trader.png')}}" alt="logoimage" / style="height:24px !important; width:50px !important;"> 
            </a>
            <button class="navbar-toggler animation" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" data-animation="fadeInDown" data-animation-delay="1.1s"> 
                <span class="ion-android-menu"></span> 
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav m-auto">
                    <li class="dropdown animation" data-animation="fadeInDown" data-animation-delay="1.1s">
                        <a  class="nav-link dropdown-toggle" href="{{url('/')}}">Home</a>
                               
                    </li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.2s"><a class="nav-link page-scroll nav_item" href="{{url('/')}}#service">Services</a></li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.3s"><a class="nav-link page-scroll nav_item" href="{{route('about')}}">About</a></li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.8s"><a class="nav-link page-scroll nav_item" href="">Affiliated Program</a></li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.8s"><a class="nav-link page-scroll nav_item" href="{{url('/')}}#faq">FAQs</a></li>
                    
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.8s"><a class="nav-link page-scroll nav_item" href="{{url('/')}}#contact">Contact</a></li>
                    
                    
                </ul>
                <ul class="navbar-nav nav_btn align-items-center">
                    
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="2s"><a class="btn btn-default btn-radius " href="{{route('login')}}">Login</a></li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="2s"><a class="btn btn-default btn-radius" href="{{route('register')}}">Register</a></li>
                </ul>
            </div>
        </nav>
    </div>
</header>
<!-- END HEADER --> 

<!-- START SECTION BANNER -->
<section class="section_breadcrumb bg_black_dark" data-z-index="1" data-parallax="scroll" data-image-src="assets/images/home_banner_bg.png">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="banner_text text-center">
                    <h1 class="animation" data-animation="fadeInUp" data-animation-delay="1.1s">Email Verify</h1>
                    <ul class="breadcrumb bg-transparent justify-content-center animation m-0 p-0" data-animation="fadeInUp" data-animation-delay="1.3s"> 
                        <li><a href="index.html">Home</a> </li>
                        <li><span>Email Verify</span></li> 
                    </ul>
                </div>
            </div>
            
        </div>
    </div>
</section>
<!-- END SECTION BANNER --> 

<!-- START SECTION LOGIN --> 
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="authorize_box">
                   
                    <div class="title_default_dark title_border text-center">
                        <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Verify Your Email Address</h4>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s"></p>
                    </div>
                    
                    <div class="field_form authorize_form">
                        <form action="{{ route('user.email.store') }}" method="POST">
                            @csrf
                            <div class="form-group col-md-12 animation" data-animation="fadeInUp" data-animation-delay="0.6s">
                                <input type="email" class="form-control" required="" placeholder="Email" name="email">
                            </div>
                            <div class="form-group col-md-12 text-center animation" data-animation="fadeInUp" data-animation-delay="0.8s">
                                <button class="btn btn-default btn-radius" type="submit">Submit</button>
                            </div>

                            
                            
                           
                        </form>
                    </div>
                </div>
                <div class="divider small_divider"></div>
                <div class="text-center">
                    <span class="animation" data-animation="fadeInUp" data-animation-delay="1s">Don't have an account?<a href="{{'register'}}"> Register</a></span>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END SECTION LOGIN --> 

<!-- START FOOTER SECTION -->
<footer>
    <div class="top_footer blue_light_bg" data-z-index="1" data-parallax="scroll" data-image-src="assets/images/footer_bg.png">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-12">
                    <div class="newsletter_form">
                        <h4 class="footer_title border_title animation" data-animation="fadeInUp" data-animation-delay="0.2s">Newsletter</h4>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">By subscribing to our mailing list you will always be update with the latest news from us.</p>
                        <form class="subscribe_form animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                            <input class="input-rounded" type="text" required placeholder="Enter Email Address"/>
                          <button type="submit" title="Subscribe" class="btn-info" name="submit" value="Submit"> Subscribe </button>
                        </form>
                    </div>
                </div>
                <div class="col-lg-3 offset-lg-1 col-md-6 col-sm-8 res_md_mt_30 res_sm_mt_20">
                    <h4 class="footer_title border_title animation" data-animation="fadeInUp" data-animation-delay="0.2s">Quick Links</h4>
                    <ul class="footer_link half_link list_none">
                       <li class="animation" data-animation="fadeInUp" data-animation-delay="0.2s"><a href="{{url('/')}}#service">SERVICES</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.3s"><a href="{{route('about')}}">ABOUT</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.4s"><a href="">AFFILIATED PROGRAM</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.5s"><a href="{{url('/')}}#faq">FAQ</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.6s"><a href="{{url('/')}}#contact">Contact</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 offset-lg-1 col-md-6 col-sm-4 res_md_mt_30 res_sm_mt_20">
                    <h4 class="footer_title border_title animation" data-animation="fadeInUp" data-animation-delay="0.2s">Social</h4>
                    <ul class="footer_social list_none">
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.2s"><a href="#service"><i class="ion-social-facebook"></i> Facebook</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.3s"><a href="#service"><i class="ion-social-twitter"></i> Twitter</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.4s"><a href="#service"><i class="ion-social-googleplus"></i> Google Plus</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.5s"><a href="#service"><i class="ion-social-pinterest"></i> Pintrest</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.6s"><a href="#service"><i class="ion-social-instagram-outline"></i> Instagram</a></li>
                    </ul>
                </div>
                
            </div>
        </div>
    </div>
    <div class="bottom_footer">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <p class="copyright">Copyright &copy; 2018 All Rights Reserved.</p>
        </div>
        <div class="col-md-6">
          <ul class="list_none footer_menu">
            <li><a href="#servicos">Privacy Policy</a></li>
            <li><a href="#tem">Terms & Conditions</a></li>
          </ul>
        </div>
      </div>
    </div>
    </div>
</footer>
<!-- END FOOTER SECTION --> 


<a href="#" class="scrollup btn-default"><i class="ion-ios-arrow-up"></i></a> 

<!-- Latest jQuery --> 
<script src="{{ asset('landing/assets/js/jquery-1.12.4.min.js')}}"></script> 
<!-- Latest compiled and minified Bootstrap --> 
<script src="{{ asset('landing/assets/bootstrap/js/bootstrap.min.js')}}"></script> 
<!-- owl-carousel min js  --> 
<script src="{{ asset('landing/assets/owlcarousel/js/owl.carousel.min.js')}}"></script> 
<!-- magnific-popup min js  --> 
<script src="{{ asset('landing/assets/js/magnific-popup.min.js')}}"></script> 
<!-- waypoints min js  --> 
<script src="{{ asset('landing/assets/js/waypoints.min.js')}}"></script> 
<!-- parallax js  --> 
<script src="{{ asset('landing/assets/js/parallax.js')}}"></script>     
<!-- countdown js  --> 
<script src="{{ asset('landing/assets/js/jquery.countdown.min.js')}}"></script> 
<!-- particles min js  --> 
<script src="{{ asset('landing/assets/js/particles.min.js')}}"></script> 
<!-- scripts js --> 
<script src="{{ asset('landing/assets/js/jquery.dd.min.js')}}"></script> 
<!-- jquery.counterup.min js --> 
<script src="{{ asset('landing/assets/js/jquery.counterup.min.js')}}"></script> 
<!-- scripts js --> 
<script src="{{ asset('landing/assets/js/scripts.js')}}"></script>
<script src="{{ asset('js/toastr/toastr.min.js')}}"></script>
<script>
  function topRight() {

var values= [{"positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false}];
        return values[0];

}
  /*toastr.success("{{ Session::get('success') }}", topRight());*/
    var type = "{{ Session::get('alert-type') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}","{{ Session::get('heading') }}",  topRight() );
            break;

        case 'warning':
            toastr.warning("{{ Session::get('message') }}" ,"{{ Session::get('heading') }}", topRight());
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}" ,"{{ Session::get('heading') }}", topRight());
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}" ,"{{ Session::get('heading') }}", topRight());
            break;
    }
    
 </script>
</body>

<!-- Mirrored from bestwebcreator.com/cryptocash/demo/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 21 Feb 2020 10:29:25 GMT -->
</html>