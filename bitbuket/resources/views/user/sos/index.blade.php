@extends('layouts.app')

@section('dashboardcontent')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 style="color: white;">SOS List</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">SOS List</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<section class="content">
      <div class="row">
        <div class="col-12">

			<div class="card">
			            <div class="card-header" style="background-color: #35354B;color: white;">
			              <h3 class="card-title">All SOS</h3>
			            </div>
                  <div class="card-header" style="background-color: #35354B;color: white;">
                          <a  class="btn btn-info float-right" href="{{route('user.sos.add')}}">Create Ticket</a> 

                        </div>

			            <!-- /.card-header -->
			            <div class="card-body" style="background-color: #35354B;color: white;">
                    <div class="table-responsive">
			              <table  class="table table-bordered table-striped example1">
			                <thead style="background-color:#131311; color:white;">
			                <tr>
			                  <th>Ticket NO</th>
			                  <th>Subject</th>
			                  <th>Created At</th>
                        <th>Action</th>
			                </tr>
			                </thead>
			                <tbody style="background-color: #35354B;color: white;">
			                @foreach($users as $user)
                            
                            @if($user->adminReply($user->id))
                            <tr style="background-color: #0000005e;">
                            @else
                            <tr >

                          
                          @endif
                            
                              <td>
                          
                                     {{$user->id}} 
                      </td>
                             	<td>
                                   

                    <a  class="" href="{{route('user.sos.detail',$user->id)}}">{{$user->subject}} </a>

                                  


                                     
          						</td>
          						
                                
          						<td>
          						    {{$user->created_at}}
          						</td>
                      <td>
                        @if($user->status==1)
                          <button class="btn btn-success">Resolved</button> 
                          @elseif($user->status==2 && $user->adminReply($user->id))
                          <button class="btn btn-info">Awaiting Reply</button> 
                          @elseif($user->status==2 )
                          <button class="btn btn-warning">In process</button> 
                          @elseif($user->status==0 && $user->adminReply($user->id))
                          <button class="btn btn-info">Awaiting Reply</button>
                          @else
                          <button class="btn btn-danger">Unresolved</button>
                          @endif
                          
                          
                      </td>
                      
                      
						    </tr>
			                @endforeach
			                </tbody>
			                <tfoot style="background-color:#131311; color:white;">
			                <tr>
                        <th>Ticket NO#</th>
                        <th>Subject</th>
                        <th>Created At</th>
                          <th>Action</th>
			                </tr>
			                </tfoot>
			              </table>
                  </div>
			            </div>
			            <!-- /.card-body -->
			</div>
	   </div>
	  </div>
</section>	


<script>
 
  function topRight() {

var values= [{"positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false}];
        return values[0];

}
  /*toastr.success("{{ Session::get('success') }}", topRight());*/
    var type = "{{ Session::get('alert-type') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}","{{ Session::get('heading') }}",  topRight() );
            break;

        case 'warning':
            toastr.warning("{{ Session::get('message') }}" ,"{{ Session::get('heading') }}", topRight());
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}" ,"{{ Session::get('heading') }}", topRight());
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}" ,"{{ Session::get('heading') }}", topRight());
            break;
    }
    
 </script>
@endsection