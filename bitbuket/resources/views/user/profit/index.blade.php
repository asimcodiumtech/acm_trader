@extends('layouts.app')

@section('dashboardcontent')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 style="color: white;">Profit Information</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Profit List List</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<section class="content">
      <div class="row">
        <div class="col-12">

			<div class="card">
			            <div class="card-header" style="background-color: #35354B;color: white;">
			              <h3 class="card-title">Profit List</h3>
			            </div>
			            <!-- /.card-header -->
			            <div class="card-body" style="background-color: #35354B;color: white;">
                    <div class="table-responsive">
			              <table  class="table table-bordered table-striped example1">
			                <thead style="background-color:#131311; color:white;">
			                <tr>
			                	
			                  <th>Amount</th>
                        <th>% profit</th>
			                  
			                  <th>Time</th>
			                  
			                  <th>Status</th>
                        <th>Total Amount</th>
			                </tr>
			                </thead>
			                <tbody style="background-color: #35354B;color: white;">
                        {{ $sum=0}}
			                @foreach($users as $user)
                             <tr>
                             	
                      <td>
						          {{$user->profit}}
						        </td>
                    <td>
                      {{$user->percentage}}
                    </td>


						        <td>
						          {{$user->created_at}}
						        </td>
                    
						        
						        <td>
						          @if($user->percentage>0)
						            <button  class="btn btn-success"><b> Win </b></button>
						            @elseif($user->percentage<0)
						            <button  class="btn btn-danger"><b> Loss </b></button>
						            
						            @else
						           <button  class="btn btn-danger " data-id="{{$user->id}}" ><b> No Applied </b></button>
						           @endif
						        </td>
                    <td>
                     @if($user->withdraw_profit!=1)
                       {{$sum= $sum + $user->profit}}
                     @else
                     <button  class="btn btn-success"><b> Already Withdraw </b></button>
                     @endif
                    </td>
                             </tr>
			                @endforeach
			                </tbody>
			                <tfoot style="background-color:#131311; color:white;">
			                <tr>
			                  
			                  <th>Amount</th>
                        <th>% profit</th>
			                  <th>Time</th>
                        
			                  <th>Status</th>
                        <th>Total Amount</th>
			                </tr>
			                </tfoot>
			              </table>
                    </div>
			            </div>
			            <!-- /.card-body -->
			</div>
	   </div>
	  </div>
</section>	


<script>
  $(function () {
    $(".example1").DataTable({
        responsive: true
    });
    $('.example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
  
  $(document).ready(function() {
      var val;
      var id;
      var coin_type;



       
$('.example1').on('click', '.getProfit', function () {
   id = $(this).data('id');
   
   html='<input type="number" tabindex="3" placeholder="enter your BTC/account number" name="profit" class="first">';
   html+='<select  name="coin_type" class="coin_type" tabindex="3" required><option value="">Please Select</option><option value="BTC">BTC</option><option value="USD">USD</option>';

                 swal({
            title: "Enter Transaction id where admin send profit",
            text: "Please complete all field !!",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            animation: "slide-from-top",
            inputPlaceholder: "Write something",
            closeOnCancel: false
        });
$('.showSweetAlert fieldset').html(html);


                 $('.confirm').click(function(){


                  
                  
                 
         var vcoin_type = $("select.coin_type").children("option:selected").val();
         var trans_id=$('.first').val();

                  

                  
                      var url = '<?= route('profit.request')?>';
                  $.ajax({
                    type: "POST",
                    url: url,
                    data: {"_token": "{{ csrf_token() }}", id:id,coin_type:vcoin_type,trans_id:trans_id},
                    success: function (response) {
                        console.log(response);
                        if (response.success) {
                             $('body').find('.sweet-overlay').remove();
                                   $('body').find('.hideSweetAlert').remove();
                               swal("Request Submit to Admin", "Successfully !!", "success");
                                $('.confirm').on('click', function () {
                                window.location.reload();
                            });
                               
                        } else {
                            if (response.error == 4091) {
                                
                                alert("User already has an active subscription.");
                            } else {
                                $(this).parent().parent().parent().remove();
                                   
                                   $('body').find('.sweet-overlay').remove();
                                   $('body').find('.hideSweetAlert').remove();

                               swal("Request Rejected", "You can't fill all fields !!", "error");
                             }
                        }
                    },
                    error: function () {
                        alert('Error occured');
                    }
                });

  });
                 $('.cancel').click(function(){
            $('body').find('.sweet-overlay').remove();
           // $(this).parent().parent().closest(".sweet-overlay").remove();
             $(this).parent().parent().remove();

             
          });


                 


});

});

</script>	
@endsection