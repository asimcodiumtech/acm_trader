@extends('layouts.app')

@section('dashboardcontent')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 style="color: white;">Deposit Information</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Deposit List</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<section class="content">
      <div class="row">
        <div class="col-12">

			<div class="card">
			            <div class="card-header" style="background-color: #35354B;color: white;">
			              <h3 class="card-title">All Deposit List</h3>
			            </div>
			            <!-- /.card-header -->
			            <div class="card-body" style="background-color: #35354B;color: white;">
			            	<div class="table-responsive">
			              <table  class="table table-bordered table-striped example1">
			                <thead style="background-color:#131311; color:white;">
			                <tr>
			                	<th>Currency</th>

			                  <th>Amount</th>
			                  <th>Reference</th>
                              <th>BTC Address</th>
			                  
			                  <th>Time</th>
			                  
			                  <th>Deposit Status</th>
			                  <th>Total Amount</th>
			                </tr>
			                </thead>
			                <tbody style="background-color: #35354B;color: white;">
			                	{{ $sum=0}}
			                @foreach($users as $user)
                             <tr>
                             	<td>
						          {{$user->coin_type}}
						        </td>
                             	<td>
						          {{$user->amount}}
						        </td>
						        <td>
						            @if($user->coin_type=="USD")
						         {{$user->refNo($user->u_id)}}
						         @endif
						        </td>
						        <td>
						          {{$user->trans_id}}
						        </td>

						        <td>
						          {{$user->created_at}}
						        </td>
						        
						        <td>
						          @if($user->deposite_status==1)
						            <button  class="btn btn-success"><b> Processed </b></button>
						            @elseif($user->deposite_status==2)
						            <button  class="btn btn-danger"><b> pending </b></button>
						            
						            @else
						           <button  class="btn btn-danger"><b> No Applied </b></button>
						           @endif
						        </td>
						        <td>
						          @if($user->totalDeposit($user->amount,$user->id,$user->deposite_status,$sum))
                       
                       {{$sum= $sum+ $user->totalDeposit($user->amount,$user->id,$user->deposite_status,$sum)}}
                     @else
                     @if($user->deposite_status==1)
						            <button  class="btn btn-success"><b> Processed </b></button>
						            @elseif($user->deposite_status==2)
						            <button  class="btn btn-danger"><b> pending </b></button>
						            
						            @else
						           <button  class="btn btn-danger"><b> Already Withdraw</b></button>
						           @endif
                     @endif
						        </td>
                             </tr>

			                @endforeach
			                </tbody>
			                <tfoot style="background-color:#131311; color:white;">
			                <tr>
			                  <th>Currency</th>
			                  <th>Amount</th>
			                   <th>Reference</th>
                              <th>BTC Address</th>
			                  <th>Time</th>
			                  <th>Deposit Status</th>
			                  <th>Total Amount</th>
			                </tr>
			                </tfoot>
			              </table>
			          </div>
			            </div>
			            <!-- /.card-body -->
			</div>
	   </div>
	  </div>
</section>	


<script>
  $(function () {
    $(".example1").DataTable({
        responsive: true
    });
    $('.example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>	
@endsection