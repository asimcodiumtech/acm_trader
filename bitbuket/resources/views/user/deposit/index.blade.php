@extends('layouts.app')

@section('dashboardcontent')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Deposite</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Deposit Amount</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


<section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8" >
    				<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Deposit</h3>

                
              </div>
              <!-- /.card-header -->
              <!-- form start -->

              <form method="POST" action="{{ route('deposite.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                  
                      
                      <p >Admin Account Number : {{$coin->btc_id}}</p><br>

                   
                  
                  
                    <p >Amount Bitcoin Address : {{$coin->usd_id}}</p>
                    
                  </div>
                  <div class="form-group">

                    <label for="exampleInputEmail1">Amount</label>
                    <input type="number" class="form-control" id="exampleInputEmail1" placeholder="Enter Amount" name="amount">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Coint type</label>
                    <select class="form-control coin_type" name="coin_type" required>
                        <option value="">Please Select</option>
                          <option value="USD">USD</option>
                          <option value="BTC">BTC</option>
                    </select>
                    
                  </div>
                  <div class="form-group currencydiv">
                    <label for="exampleInputEmail1">BTC ID</label>
                    <input type="text" class="form-control currency" id="exampleInputEmail1" placeholder="Enter BTC ID" name="trans_id">
                  </div>
                 
                   <input type="hidden"  name="deposite_status" value="2">
                   <input type="hidden"  name="u_id" value="{{auth()->user()->id}}">
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            		</div>
           </div>
       </div>
   </div>
</section>

<script type="text/javascript">
  $(function () {
  $('.currencydiv').hide();
  $('.coin_type').change(function()
{   
   if($('.coin_type').val()=='BTC')
   { 
       $('.currencydiv').show();
        $('.currency').attr('required','required');
    }
   else if($('.coin_type').val()=='USD')
   { 
       $('.currencydiv').hide();
        $('.currency').removeAttr('required');
    }
    else
    {

    }

});
});

</script>

@endsection