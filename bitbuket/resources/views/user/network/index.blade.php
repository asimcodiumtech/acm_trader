@extends('layouts.app')

@section('dashboardcontent')
<style type="text/css">
#orgChart{
    width: auto;
    height: auto;
}

#orgChartContainer{
    width: 100%;
    height: 100%;
    overflow: auto;
    background: #35354B;
}
.box {
  float: left;
  height: 20px;
  width: 20px;
  margin-bottom: 15px;
  border: 1px solid black;
  clear: both;
}

.red {
  background-color: red;
}

.green {
  background-color: green;
}

.blue {
  background-color: blue;
}
    </style>
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark" style="color: white !important;">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Network </li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
          
    <div id="orgChartContainer">
       <div style="color:white !important; margin-left: 1%;margin-top: 0.5%"><div class='box red' ></div> &nbsp Direct Referral</div>
<br>
<div style="color:white !important; margin-left: 1%;margin-top: 0.5%"><div class='box green'></div>&nbsp System Referral</div>

      <div id="orgChart"></div>
    </div>
    <div id="consoleOutput">
    </div>
            <!-- /.card -->

            <!-- DIRECT CHAT -->
            
            <!--/.direct-chat -->

            <!-- TO DO List -->
            
            <!-- /.card -->
          </section>
          <!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
         
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <script type="text/javascript" src="{{ asset('refral/dist/js/jquery-1.11.1.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('refral/dist/js/jquery.orgchart.js')}}"></script>
 
    <script type="text/javascript">
      var final ={!! json_encode($final) !!};
    var testData = [
        {id: 1, name: 'Me', parent: 0},
        {id: 2, name: 'CEO Office', parent: 1},
        {id: 3, name: 'Division 1', parent: 1},
        {id: 4, name: 'Division 2', parent: 3},
        
        
    ];
    console.log(final);
    console.log(testData);
     $(function(){

  org_chart = $('#orgChart').orgChart({

    data: final,// your data

    showControls:false,// display add or remove node button.

    allowEdit:false,// click the node's title to edit


});

});

   

    // just for example purpose
    function log(text){
        $('#consoleOutput').append('<p>'+text+'</p>')
    }
    </script>
   
@endsection