<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from bestwebcreator.com/cryptocash/demo/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 21 Feb 2020 10:29:26 GMT -->
<head>
<!-- Meta -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Cryptocash is Professional Creative Template" />
<!-- SITE TITLE -->
<title>ACM-Traders</title>
<!-- Favicon Icon -->
<link rel="shortcut icon" type="image/x-icon" href="{{asset('images/acm_trader.png')}}">
<!-- Animation CSS -->
<link rel="stylesheet" href="{{ asset('landing/assets/css/animate.css')}}" >
<!-- Latest Bootstrap min CSS -->
<link rel="stylesheet" href="{{ asset('landing/assets/bootstrap/css/bootstrap.min.css')}}">
<!-- Google Font -->
<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
<!-- Font Awesome CSS -->
<link rel="stylesheet" href="{{ asset('landing/assets/css/font-awesome.min.css')}}" >
<!-- ionicons CSS -->
<link rel="stylesheet" href="{{ asset('landing/assets/css/ionicons.min.css')}}">
<!--- owl carousel CSS-->
<link rel="stylesheet" href="{{ asset('landing/assets/owlcarousel/css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{ asset('landing/assets/owlcarousel/css/owl.theme.default.min.css')}}">
<!-- Magnific Popup CSS -->
<link rel="stylesheet" href="{{ asset('landing/assets/css/magnific-popup.css')}}">
<!-- Style CSS -->
<link rel="stylesheet" href="{{ asset('landing/assets/css/style.css')}}">
<link rel="stylesheet" href="{{ asset('landing/assets/css/responsive.css')}}">
<!-- Color CSS -->
<link id="layoutstyle" rel="stylesheet" href="{{ asset('landing/assets/color/theme.css')}}">


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106310707-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
 gtag('config', 'UA-106310707-1', { 'anonymize_ip': true });
</script>

<!-- Start of StatCounter Code -->
<script>
    <!--
    var sc_project=11921154;
    var sc_security="6c07f98b";
        var scJsHost = (("https:" == document.location.protocol) ?
        "https://secure." : "http://www.");
            //-->
            
document.write("<sc"+"ript src='" +scJsHost +"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><a title="web analytics" href="https://statcounter.com/"><img class="statcounter" src="https://c.statcounter.com/11921154/0/6c07f98b/0/" alt="web analytics" /></a></div></noscript>
<!-- End of StatCounter Code -->

</head>
<style>
.box{ 
width: 30% !important; 
height: 200px !important; 
/*border: 5px dashed #f7a239 !important;*/
} 
img{ 
width: 100% !important; 
height: 100% !important; 
}
</style>

<body>



<!-- START LOADER -->
<div id="loader-wrapper" class="">
    <div id="loading-center-absolute" class="">
        <div class="object " id="object_four" style="background-color: black;"></div>
        <div class="object " id="object_three" style="background-color: black;"></div>
        <div class="object " id="object_two" style="background-color: black;"></div>
        <div class="object " id="object_one" style="background-color: black;"></div>
    </div>
    <div class="loader-section section-left "  style="background-color: black;"></div>
    <div class="loader-section section-right " style="background-color: black;"></div>

</div>
<!-- END LOADER --> 

<!-- START HEADER -->
<header class="header_wrap fixed-top">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg"> 
            <a class="navbar-brand animation" href="{{url('/')}}" data-animation="fadeInDown" data-animation-delay="1s"> 
                <img src="{{asset('images/acm_trader.png')}}" alt="logoimage" / style="height:152px !important; width:150px !important;"> 
                <img class="logo_dark" src="{{asset('images/acm_trader.png')}}" alt="logoimage" / style="height:24px !important; width:50px !important;">  
            </a>
            <button class="navbar-toggler animation" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" data-animation="fadeInDown" data-animation-delay="1.1s"> 
                <span class="ion-android-menu"></span> 
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav m-auto">
                    <li class="dropdown animation" data-animation="fadeInDown" data-animation-delay="1.1s">
                        <a  class="nav-link dropdown-toggle" href="{{url('/')}}">Home</a>
                               
                    </li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.2s"><a class="nav-link page-scroll nav_item" href="{{url('/')}}#service">Services</a></li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.3s"><a class="nav-link page-scroll nav_item" href="{{route('about')}}">About</a></li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.8s"><a class="nav-link page-scroll nav_item" href="#">Affiliated Program</a></li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.8s"><a class="nav-link page-scroll nav_item" href="{{url('/')}}#faq">FAQs</a></li>
                    
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.8s"><a class="nav-link page-scroll nav_item" href="{{url('/')}}#contact">Contact</a></li>
                    
                    
                    
                  
                </ul>
                <ul class="navbar-nav nav_btn align-items-center">
                    
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="2s"><a class="btn btn-default btn-radius " href="{{route('login')}}">Login</a></li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="2s"><a class="btn btn-default btn-radius" href="{{route('register')}}">Register</a></li>
                </ul>
            </div>
        </nav>
    </div>
</header>
<!-- END HEADER --> 

<!-- START SECTION BANNER -->
<section class="section_breadcrumb blue_light_bg" data-z-index="1" data-parallax="scroll" data-image-src="assets/images/home_banner_bg.png">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="banner_text text-center">
                    <h1 class="animation" data-animation="fadeInUp" data-animation-delay="1.1s">About Us</h1>
                    <ul class="breadcrumb bg-transparent justify-content-center animation m-0 p-0" data-animation="fadeInUp" data-animation-delay="1.3s"> 
                        <li><a href="{{url('/')}}">Home</a> </li>
                        <li><span>About us</span></li> 
                    </ul>
                </div>
            </div>
            
        </div>
    </div>
</section>
<!-- END SECTION BANNER --> 
<!-- <div class="text-center"> 
                    
                </div> -->
<!-- <div class="row">
           
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="text-center"> 
                    <img class="animation" data-animation="zoomIn" data-animation-delay="0.2s" src="http://codiumtech.com/referral/public/images/acm_trader.png" alt="aboutimg2"/> 
                </div>
            </div>
        </div> -->
<!-- <section class="small_pb">
    <div class="container">
        <div class="row">
           
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="text-center"> 
                    <img class="animation" data-animation="zoomIn" data-animation-delay="0.2s" src="http://codiumtech.com/referral/public/images/acm_trader.png" alt="aboutimg2"/> 
                </div>
            </div>
        </div>
    </div>
</section> -->
<!-- START SECTION ABOUT -->

<section class="small_pb">
    <div class="container">

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="res_md_mb_30 res_sm_mb_20">
                     <!--<div class="box"> 
                    <img class="" data-animation="" data-animation-delay="0.2s" src="http://codiumtech.com/referral/public/images/acm_trader.png" alt="aboutimg2"
                    style="
    margin-left: 100%;
    "
                    /> </div>-->
                    <div class="title_default_dark title_border">
                        
                      <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">About</h4>
                      <h7 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Background History</h7>
                      <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">CACM TRADER (Automated Crypto Market Trader ) was founded under the notion of trading cryptocurrency, in real time without spending long hours in the market. However before the concept came into motion, the founder Dave had been trading the market favorably as he spent long hours doing market analysis and making steady profits. Characteristically Dave is a pioneer, similarly to the most successful traders from all over the world; he knows the market so well that he knows when to take advantage of artificially high pricing. This made him an influential trader to his peers and grew to being an influential financial figure within the industry itself. Dave had become a digital currency trading genius; he assumed the rare skill of accomplishing unbelievable feats </p>
                      <p class="animation" data-animation="fadeInUp" data-animation-delay="0.8s">His life was dedicated to trading and that made him exceedingly good at reading market trends and analyzing charts more successfully. His return margins were high and hardly made any dismal losses. He acquired an immediate reputation as a successful trader, and those around him trusted his decision making; consequently his capital limit increased significantly as more and more traders realized his skill. This put him in a perfect position to reap even more profits for a considerable period of time. </p>
                      <p class="animation" data-animation="fadeInUp" data-animation-delay="0.8s">Nevertheless, since successful trades required Dave to work tirelessly, he had little time reserved for his family. Dave spent long hours analyzing charts, placing trades and making calculated decisions, making it increasingly difficult for him to focus on family. He had unintentionally neglected his family and hadn’t realized the amount of damage he caused to his relationship with his wife and the amount of hours he should have spent with his children. Trading on a professional level compromised his marriage and resulted in permanent separation with his wife and dull relationships with his children.</p>
                      <p class="animation" data-animation="fadeInUp" data-animation-delay="0.8s">So it was apparent that time was a troubling matter and that being so, he lost his moral compass which blunted his ability to make clever decisions in trading and life in general. Dave found himself drowning in depressive limbo and had nobody to turn to. He couldn’t focus and it seemed as though he couldn’t find his flow, hence Dave was frequently frustrated and started losing money and his contracts with recognized firms he had been working alongside with were terminated. His risk management skills were dismal and hardly made any impressive profits.</p>
                      <p class="animation" data-animation="fadeInUp" data-animation-delay="0.8s">Reality had struck Dave really hard and he realized that he had to get his life back in order. He had to reflect on the important things; he came into terms with the fact that he lost his family and close friends because he spent little time with them and this was because he spent all his time trading cryptocurrency. But trading rewarded him with a soothing lifestyle, where he did not have to worry about money or any problems related to money. It was ironic that the same thing that rewarded him with the life he always dreamed of ended up taking everything away from him.</p>

                          <p class="animation" data-animation="fadeInUp" data-animation-delay="0.8s">Therefore, in the quest to organize his life he sought an innovative way to trade the market. This new approach was to be in par with technological advancements currently dominating the emerging 4th revolution. He recognized that, in order to spend quality time with loved ones, he had to find a way to automate the way he trades, analyses the market and the way he sourced relevant information. So with that concept in mind, he conducted a research about the possibilities of making his idea come to life. He gathered everything he knew about market analysis, market trends and trading the market overall. Then kept in touch with IT specialists in order to confirm whether the concept he came up with was possible or not. He presented the idea to the IT specialists and they were impressed, they wanted to attempt this innovative concept, so they agreed to build an Automated Crypto Market Trading Bot.</p>

                          <p class="animation" data-animation="fadeInUp" data-animation-delay="0.8s">This Automated Crypto Market Trading Bot aimed at being the safest yet most lucrative trading solution. Thus, the IT people had to be precise in their coding, so that the Automated  system trades similarly to Dave however makes safer decisions, has better risk management skills, understands the market better and utilizes useful information more efficiently. This ensures successful trades and minimizes losses significantly.</p>

                          <p class="animation" data-animation="fadeInUp" data-animation-delay="0.8s">As time went on the system was finally completed and was to be put to the test. Dave started using the system immediately and it worked quite effectively. Now he could trade the cryptocurrency market without spending a ridiculous amount of time staring at the computer or staying up to date with the news on the internet. The Automated Robot gathered all the necessary data needed to make safe decisions therefore executes smarter trades. He could now spend more time with family without worrying about charts, trends or even the market itself, because the Automated Robot he built took care of every trade and hardly experienced any shocking losses.</p>


                             <p class="animation" data-animation="fadeInUp" data-animation-delay="0.8s">So with all the time he had, he was able to focus on his family. He could rebuild a healthier relationship with his children and be more attentive to his wife. And in no time Dave won his family back and did not have any further financial or social complications.</p>

                              <p class="animation" data-animation="fadeInUp" data-animation-delay="0.8s">After conducting thorough studies on the cryptocurrency market, Dave learned that there are an increasing number of amateur traders that loses billions of dollars trading various cryptocurrencies. It became apparent to him that the system he had built would work suitably for average traders. Statistically, majority of traders have day jobs therefore have little time to pay attention to trading. Consequently accounts are blown and profits are hardly ever made. Thus Dave decided to go public with his Automated Crypto Market Trading Bot to grant people the opportunity to make steady revenue without worrying about analyzing charts or watching the news in order to trade favorably. This Automated Crypto Market Trading Bot does all the imperative work for traders making trading more convenient and eases financial strains.</p>











                              <p class="animation" data-animation="fadeInUp" data-animation-delay="0.8s">Thereafter the system was updated in order to cater for public use. It was to help users to trade favorably and not to spend tireless hours on the computer screen making no returns and blowing accounts over and over again. Automated Crypto Market Trading Bot makes trading much easier, since the system trades under the guidance of frequently updated features that ensure relevant information is gathered, trades safely and returns profits more successfully. Every trader had to have their own version of the ACM TRADER that trades according to the amount funded on their accounts. </p>
                              <p class="animation" data-animation="fadeInUp" data-animation-delay="0.8s">Now the system has increasing testimonies, happier traders and innovative features that ensure reasonable returns that are updated on a daily basis</p>






                              <p class="animation" data-animation="fadeInUp" data-animation-delay="0.8s">Now the system has increasing testimonies, happier traders and innovative features that ensure reasonable returns that are updated on a daily basis</p>




                              <p class="animation" data-animation="fadeInUp" data-animation-delay="0.8s">Hence the whole purpose of creating an automated trading robot, reduced the many hours Dave spent trading and when the system proved to be successful it could be taken to the public, where average people have the opportunity to trade and make steady returns without having to quit their day jobs or spend long periods on their devices losing money due to unforeseen mistakes. In eloquent summary, ACM TRADER was founded on the basis of delivering financial emancipation and to allow people to enjoy life by doing more meaningful things and to ease the stress of making money, only to find themselves in difficult financial circumstances.</p>
                              <p class="animation" data-animation="fadeInUp" data-animation-delay="0.8s">ACM TRADER is a success, more and more people are interested in the idea and its growing by the minute. Trading is difficult and requires diligent attention, which is why the ACM TRADER is programmed to trade like a true expert, similarly to Dave himself.</p>


                      
                             
                          


                       


                      
                    </div>

                     
                     
                </div>
            </div>
           <!--  <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="text-center"> 
                    <img class="animation" data-animation="zoomIn" data-animation-delay="0.2s" src="assets/images/about_img2.png" alt="aboutimg2"/> 
                </div>
            </div> -->
        </div>
    </div>
</section>
<!-- END SECTION ABOUT --> 

<!-- START SECTION WHY CHOOSE US -->
<!-- <section class="small_pt">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-8 offset-lg-2 col-md-12 col-sm-12">
                <div class="title_default_dark title_border text-center">
                  <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Why Choose Us?</h4>
                  <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Perspiciatis unde omnis iste natus error sit.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="box_wrap radius_box bg-white text-center animation" data-animation="fadeInUp" data-animation-delay="0.6s">
                    <img src="assets/images/wc_icon1.png" alt="wc_icon1"/>
                    <h4>Fully Secured Data</h4>
                    <p>Cryptocash bitcoin ensures every block and transaction it accepts is a valid, increasing not only your security but also helping prevent miners and banks from taking control of Bitcoin.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="box_wrap radius_box bg-white text-center animation" data-animation="fadeInUp" data-animation-delay="0.8s">
                    <img src="assets/images/wc_icon2.png" alt="wc_icon2"/>
                    <h4>A Better User Interface</h4>
                    <p>Bitcoin wallet has features most other wallets don't have. But if you don't need them, you can use several other wallets on top of Bitcoin without losing Bitcoin Core's security and privacy benefits.</p>
                </div>
            </div>
            <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-3 col-sm-12">
                <div class="box_wrap radius_box bg-white text-center animation" data-animation="fadeInUp" data-animation-delay="1s">
                    <img src="assets/images/wc_icon3.png" alt="wc_icon3"/>
                    <h4>Support The Network</h4>
                    <p>Bitcoin helps support other peers.This isn't as useful as helping to keep Bitcoin decentralized, but it's an easy way for broadband users to contribute to less well-connected users.</p>
                </div>
            </div>
        </div>
    </div>
</section> -->
<!-- END SECTION WHY CHOOSE US --> 

<!-- START SECTION COUNTER --> 
<!-- <section class="counter_wrap overlay background_bg counter_bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="box_counter text-center res_sm_mb_20">
                    <i class="ion-ios-pie animation" data-animation="fadeInUp" data-animation-delay="0.2s"></i>
                    <h3 class="counter animation" data-animation="fadeInUp" data-animation-delay="0.3s">500</h3>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Recurring Buys</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="box_counter text-center res_sm_mb_20">
                    <i class="ion-help-buoy animation" data-animation="fadeInUp" data-animation-delay="0.5s"></i>
                    <h3 class="counter animation" data-animation="fadeInUp" data-animation-delay="0.6s">800</h3>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.7s">Support Countries</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="box_counter text-center res_xs_mb_20">
                    <i class="ion-ios-locked animation" data-animation="fadeInUp" data-animation-delay="0.8s"></i>
                    <h3 class="counter animation" data-animation="fadeInUp" data-animation-delay="0.9s">1200</h3>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="1s">Easy &amp; Secure</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="box_counter text-center">
                    <i class="ion-android-contacts animation" data-animation="fadeInUp" data-animation-delay="1.1s"></i>
                    <h3 class="counter animation" data-animation="fadeInUp" data-animation-delay="1.2s">1500</h3>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="1.3s">Producers</p>
                </div>
            </div>
        </div>
    </div>
</section> -->
<!-- END SECTION COUNTER -->

<!-- START SECTION TEAM -->
<!-- <section class="section_team bg_gray">
    <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-12 offset-lg-2">
            <div class="title_default_dark title_border text-center">
              <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Our Team</h4>
              <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">we are proud of our great team. He is one of the most motivated and enthusiastic people we have, and is always ready and willing to help out where needed. </p>
            </div>
          </div>
        </div>
        <div class="row small_space">
            <div class="col-lg-3 col-md-6 col-sm-6 res_md_mb_30 res_sm_mb_20">
                <div class="bg-white radius_box team_box_s3 animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                    <div class="text-center"> 
                        <img src="assets/images/team_img1.png" alt="team1"/>
                        <div class="team_social_s2 list_none">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                        </ul>
                    </div>
                    </div>
                    <div class="team_info text-center">
                        <h4><a href="#team1" class="content-popup">Derek Castro</a></h4>
                        <p>Head Of Marketing</p>
                    </div>
                    <div id="team1" class="team_pop mfp-hide">
                        <div class="row m-0">
                            <div class="col-md-4 text-center"> 
                                <div class="team_img_wrap">
                                    <img class="w-100" src="assets/images/team-lg-1.jpg" alt="user_img-lg"/> 
                                    <div class="team_title">
                                        <h4>Derek Castro</h4>
                                        <span>Head Of Marketing</span>
                                    </div>
                                </div>
                                <div class="social_single_team list_none mt-3">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="pt-3">
                                    <h5>About</h5>
                                    <hr>
                                    <p>Founder of Venus Media Ltd and Owner of leading website for affiliates in the entertainment industry TakeBucks, he is a videographer, photographer and producer with a big number of successful entrepreneurships under his name over the last 18 years.</p>
                                    <p>Founder of Venus Media Ltd and Owner of leading website for affiliates in the entertainment industry TakeBucks, he is a videographer, photographer and producer with a big number of successful entrepreneurships under his name over the last 18 years.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 res_md_mb_30 res_sm_mb_20">
                <div class="bg-white radius_box team_box_s3 animation" data-animation="fadeInUp" data-animation-delay="0.6s">
                    <div class="text-center"> 
                        <img src="assets/images/team_img2.png" alt="team2"/>
                        <div class="team_social_s2 list_none">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                        </ul>
                    </div>
                    </div>
                    <div class="team_info text-center">
                        <h4><a href="#team2" class="content-popup">Jessica Bell</a></h4>
                        <p>Head Of Sale</p>
                    </div>
                    <div id="team2" class="team_pop mfp-hide">
                        <div class="row m-0">
                            <div class="col-md-4 text-center"> 
                                <div class="team_img_wrap">
                                    <img class="w-100" src="assets/images/team-lg-2.jpg" alt="user_img-lg"/> 
                                    <div class="team_title"> 
                                        <h4>Jessica Bell</h4>
                                        <span>Head Of Sale</span>
                                    </div>
                                </div>
                                <div class="social_single_team list_none mt-3">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="pt-3">
                                    <h5>About</h5>
                                    <hr>
                                    <p>Founder of Venus Media Ltd and Owner of leading website for affiliates in the entertainment industry TakeBucks, he is a videographer, photographer and producer with a big number of successful entrepreneurships under his name over the last 18 years.</p>
                                    <p>Founder of Venus Media Ltd and Owner of leading website for affiliates in the entertainment industry TakeBucks, he is a videographer, photographer and producer with a big number of successful entrepreneurships under his name over the last 18 years.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 res_md_mb_30 res_sm_mb_20">
                <div class="bg-white radius_box team_box_s3 animation" data-animation="fadeInUp" data-animation-delay="0.8s">
                    <div class="text-center"> 
                        <img src="assets/images/team_img3.png" alt="team3"/>
                        <div class="team_social_s2 list_none">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                        </ul>
                    </div>
                    </div>
                    <div class="team_info text-center">
                        <h4><a href="#team3" class="content-popup">Alvaro Martin</a></h4>
                        <p>Blockchain App Developer</p>
                    </div>
                    <div id="team3" class="team_pop mfp-hide">
                        <div class="row m-0">
                            <div class="col-md-4 text-center"> 
                                <div class="team_img_wrap">
                                    <img class="w-100" src="assets/images/team-lg-3.jpg" alt="user_img-lg"/> 
                                    <div class="team_title"> 
                                        <h4>Alvaro Martin</h4>
                                        <span>Blockchain App Developer</span>
                                    </div>
                                </div>
                                <div class="social_single_team list_none mt-3">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                        </ul>
                                    </div>
                            </div>
                            <div class="col-md-8">
                                <div class="pt-3">
                                    <h5>About</h5>
                                    <hr>
                                    <p>Founder of Venus Media Ltd and Owner of leading website for affiliates in the entertainment industry TakeBucks, he is a videographer, photographer and producer with a big number of successful entrepreneurships under his name over the last 18 years.</p>
                                    <p>Founder of Venus Media Ltd and Owner of leading website for affiliates in the entertainment industry TakeBucks, he is a videographer, photographer and producer with a big number of successful entrepreneurships under his name over the last 18 years.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="bg-white radius_box team_box_s3 animation" data-animation="fadeInUp" data-animation-delay="1s">
                    <div class="text-center"> 
                        <img src="assets/images/team_img4.png" alt="team4"/>
                        <div class="team_social_s2 list_none">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                        </ul>
                    </div>
                    </div>
                    <div class="team_info text-center">
                        <h4><a href="#team4" class="content-popup">Maria Willium</a></h4>
                        <p>Community Manager</p>
                    </div>
                    <div id="team4" class="team_pop mfp-hide">
                        <div class="row m-0">
                            <div class="col-md-4 text-center">
                                <div class="team_img_wrap">
                                    <img class="w-100" src="assets/images/team-lg-4.jpg" alt="user_img-lg"/>
                                    <div class="team_title"> 
                                        <h4>Maria Willium</h4>
                                        <span>Community Manager</span>
                                    </div>
                                </div>
                                <div class="social_single_team list_none mt-3">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                        </ul>
                                    </div> 
                            </div>
                            <div class="col-md-8">
                                <div class="pt-3">
                                    <h5>About</h5>
                                    <hr>
                                    <p>Founder of Venus Media Ltd and Owner of leading website for affiliates in the entertainment industry TakeBucks, he is a videographer, photographer and producer with a big number of successful entrepreneurships under his name over the last 18 years.</p>
                                    <p>Founder of Venus Media Ltd and Owner of leading website for affiliates in the entertainment industry TakeBucks, he is a videographer, photographer and producer with a big number of successful entrepreneurships under his name over the last 18 years.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="divider large_divider"></div>
        <div class="row">
          <div class="col-md-12">
            <div class="title_default_dark title_border text-center">
              <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Invester Board</h4>
            </div>
          </div>
        </div>
        <div class="row small_space justify-content-center">
          <div class="col-lg-9 col-md-12">
            <div class="row">
              <div class="col-lg-4 col-md-6 col-sm-6 res_md_mb_30 res_sm_mb_20">
                <div class="bg-white radius_box team_box_s3 animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                    <div class="text-center"> 
                        <img src="assets/images/team_img5.png" alt="team5"/>
                        <div class="team_social_s2 list_none">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                        </ul>
                    </div>
                    </div>
                    <div class="team_info text-center">
                        <h4><a href="#team5" class="content-popup">Tricia Diyana</a></h4>
                        <p>Invester</p>
                    </div>
                    <div id="team5" class="team_pop mfp-hide">
                        <div class="row m-0">
                            <div class="col-md-4 text-center"> 
                                <div class="team_img_wrap">
                                    <img class="w-100" src="assets/images/team-lg-5.jpg" alt="user_img-lg"/>
                                    <div class="team_title"> 
                                        <h4>Tricia Diyana</h4>
                                        <span>Invester</span>
                                    </div> 
                                </div>
                                <div class="social_single_team list_none mt-3">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                        </ul>
                                    </div>
                            </div>
                            <div class="col-md-8">
                                <div class="pt-3">
                                    <h5>About</h5>
                                    <hr>
                                    <p>Founder of Venus Media Ltd and Owner of leading website for affiliates in the entertainment industry TakeBucks, he is a videographer, photographer and producer with a big number of successful entrepreneurships under his name over the last 18 years.</p>
                                    <p>Founder of Venus Media Ltd and Owner of leading website for affiliates in the entertainment industry TakeBucks, he is a videographer, photographer and producer with a big number of successful entrepreneurships under his name over the last 18 years.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-6 col-sm-6 res_md_mb_30 res_sm_mb_20">
                <div class="bg-white radius_box team_box_s3 animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                    <div class="text-center"> 
                        <img src="assets/images/team_img6.png" alt="team6"/>
                        <div class="team_social_s2 list_none">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                        </ul>
                    </div>
                    </div>
                    <div class="team_info text-center">
                        <h4><a href="#team6" class="content-popup">Kent Pierce</a></h4>
                        <p>Invester</p>
                    </div>
                    <div id="team6" class="team_pop mfp-hide">
                        <div class="row m-0">
                            <div class="col-md-4 text-center"> 
                                <div class="team_img_wrap">
                                    <img class="w-100" src="assets/images/team-lg-6.jpg" alt="user_img-lg"/>
                                    <div class="team_title"> 
                                        <h4>Kent Pierce</h4>
                                        <span>Invester</span>
                                    </div> 
                                </div>
                                <div class="social_single_team list_none mt-3">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                        </ul>
                                    </div>
                            </div>
                            <div class="col-md-8">
                                <div class="pt-3">
                                    <h5>About</h5>
                                    <hr>
                                    <p>Founder of Venus Media Ltd and Owner of leading website for affiliates in the entertainment industry TakeBucks, he is a videographer, photographer and producer with a big number of successful entrepreneurships under his name over the last 18 years.</p>
                                    <p>Founder of Venus Media Ltd and Owner of leading website for affiliates in the entertainment industry TakeBucks, he is a videographer, photographer and producer with a big number of successful entrepreneurships under his name over the last 18 years.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-3 col-sm-6 offset-sm-3">
                <div class="bg-white radius_box team_box_s3 animation" data-animation="fadeInUp" data-animation-delay="0.6s">
                    <div class="text-center"> 
                        <img src="assets/images/team_img7.png" alt="team7"/>
                        <div class="team_social_s2 list_none">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                        </ul>
                    </div>
                    </div>
                    <div class="team_info text-center">
                        <h4><a href="#team7" class="content-popup">Rose Morgen</a></h4>
                        <p>Invester</p>
                    </div>
                    <div id="team7" class="team_pop mfp-hide">
                        <div class="row m-0">
                            <div class="col-md-4 text-center"> 
                                <div class="team_img_wrap">
                                    <img class="w-100" src="assets/images/team-lg-7.jpg" alt="user_img-lg"/> 
                                    <div class="team_title"> 
                                        <h4>Rose Morgen</h4>
                                        <span>Invester</span>
                                    </div>
                                </div>
                                <div class="social_single_team list_none mt-3">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                        </ul>
                                    </div>
                            </div>
                            <div class="col-md-8">
                                <div class="pt-3">
                                    <h5>About</h5>
                                    <hr>
                                    <p>Founder of Venus Media Ltd and Owner of leading website for affiliates in the entertainment industry TakeBucks, he is a videographer, photographer and producer with a big number of successful entrepreneurships under his name over the last 18 years.</p>
                                    <p>Founder of Venus Media Ltd and Owner of leading website for affiliates in the entertainment industry TakeBucks, he is a videographer, photographer and producer with a big number of successful entrepreneurships under his name over the last 18 years.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="divider small_divider"></div>
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="#" class="btn btn-default btn-radius animation" data-animation="fadeInUp" data-animation-delay="0.2s">View All Team <i class="ion-ios-arrow-thin-right"></i></a>
            </div>
        </div>
    </div>
    <div class="divider large_divider"></div>
    <div class="angle_bottom"></div>
</section> -->
<!-- END SECTION TEAM --> 

<!-- START SECTION TESTIMONIAL -->
<!-- <section>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12 offset-lg-2">
              <div class="title_default_dark title_border text-center">
                <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Testimonial</h4>
              </div>
            </div>
        </div>
        <div class="row small_space">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="testimonial_slider owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial_wrap rounded_img">
                            <img src="assets/images/testmonial_client1.jpg" class="animation" data-animation="zoomIn" data-animation-delay="0.2s" alt="testmonial_client1"/>
                            <h5 class="animation" data-animation="fadeInUp" data-animation-delay="0.3s">Kerri Reece</h5>
                            <span class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">CEO Company</span>
                            <p class="animation" data-animation="fadeInUp" data-animation-delay="0.5s">You should not expect anything more. This is a fantastic program, punctual paying, thanks a lot. I recommend this hyip!</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial_wrap rounded_img">
                            <img src="assets/images/testmonial_client2.jpg" class="animation" data-animation="zoomIn" data-animation-delay="0.2s" alt="testmonial_client2"/>
                            <h5 class="animation" data-animation="fadeInUp" data-animation-delay="0.3s">Alvaro Martin</h5>
                            <span class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">CEO Company</span>
                            <p class="animation" data-animation="fadeInUp" data-animation-delay="0.5s">This is a realistic program for anyone looking for site to invest. Paid to me regularly, keep up good work.This is a realistic program for anyone looking  </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial_wrap rounded_img">
                            <img src="assets/images/testmonial_client3.jpg" class="animation" data-animation="zoomIn" data-animation-delay="0.2s" alt="testmonial_client3"/>
                            <h5 class="animation" data-animation="fadeInUp" data-animation-delay="0.3s">Jessica Bell</h5>
                            <span class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Head Of Sale</span>
                            <p class="animation" data-animation="fadeInUp" data-animation-delay="0.5s">This is a realistic program for anyone looking for site to invest. Paid to me regularly, keep up good work.This is a realistic program for anyone looking  </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->
<!-- END SECTION TESTIMONIAL -->

<!-- START SECTION CALL TO ACTION- -->
<!-- <section class="blue_light_bg">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-9">
                <div class="action-content res_md_mb_20">
                    <h3 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Let Us Help You to Find a Solution That Meets Your Needs</h3>
                    <p class="m-0 animation" data-animation="fadeInUp" data-animation-delay="0.4s">if you think it's just you're looking for. Please contact us!</p>
                </div>
            </div>
            <div class="col-lg-3 text-lg-right">
                <a href="#" class="btn btn-default btn-radius animation" data-animation="fadeInUp" data-animation-delay="0.45">Contact Us <i class="ion-ios-arrow-thin-right"></i></a>
            </div>
        </div>
    </div>
</section> -->
<!-- END SECTION CALL TO ACTION -->

<!-- START CLIENTS SECTION -->
<!-- <section class="client_logo small_pb">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title_default_dark title_border text-center">
                    <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Our Sponsers</h4>
                </div>
            </div>
        </div>
        <div class="row align-items-center text-center overflow_hide small_space">
            <div class="col-lg-3 col-md-4 col-6 logo_border">
                <div class="d-flex flex-wrap align-items-center justify-content-center h-100 animation" data-animation="fadeInUp" data-animation-delay="0.3s"> 
                    <img src="assets/images/client_logo_dark_gray1.png" alt="client_logo_dark_gray1" /> 
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-6 logo_border">
                <div class="d-flex flex-wrap align-items-center justify-content-center h-100 animation" data-animation="fadeInUp" data-animation-delay="0.4s"> 
                    <img src="assets/images/client_logo_dark_gray2.png" alt="client_logo_dark_gray2" /> 
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-6 logo_border">
                <div class="d-flex flex-wrap align-items-center justify-content-center h-100 animation" data-animation="fadeInUp" data-animation-delay="0.5s"> 
                    <img src="assets/images/client_logo_dark_gray3.png" alt="client_logo_dark_gray3" /> 
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-6 logo_border">
                <div class="d-flex flex-wrap align-items-center justify-content-center h-100 animation" data-animation="fadeInUp" data-animation-delay="0.6s"> 
                    <img src="assets/images/client_logo_dark_gray4.png" alt="client_logo_dark_gray4" /> 
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-6 logo_border">
                <div class="d-flex flex-wrap align-items-center justify-content-center h-100 animation" data-animation="fadeInUp" data-animation-delay="0.7s"> 
                    <img src="assets/images/client_logo_dark_gray5.png" alt="client_logo_dark_gray5" /> 
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-6 logo_border">
                <div class="d-flex flex-wrap align-items-center justify-content-center h-100 animation" data-animation="fadeInUp" data-animation-delay="0.8s"> 
                    <img src="assets/images/client_logo_dark_gray6.png" alt="client_logo_dark_gray6" /> 
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-6 logo_border">
                <div class="d-flex flex-wrap align-items-center justify-content-center h-100 animation" data-animation="fadeInUp" data-animation-delay="0.9s"> 
                    <img src="assets/images/client_logo_dark_gray7.png" alt="client_logo_dark_gray7" /> 
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-6 logo_border">
                <div class="d-flex flex-wrap align-items-center justify-content-center h-100 animation" data-animation="fadeInUp" data-animation-delay="1s"> 
                    <img src="assets/images/client_logo_dark_gray8.png" alt="client_logo_dark_gray8" /> 
                </div>
            </div>
        </div>
    </div>
</section> -->
<!-- END CLIENTS SECTION -->

<!-- START SECTION FAQ -->

<!-- END SECTION FAQ -->

<!-- START FOOTER SECTION -->
<footer>
    <div class="top_footer v_dark" data-z-index="1" data-parallax="scroll" data-image-src="assets/images/footer_bg.png">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-12">
                    <div class="newsletter_form">
                        <h4 class="footer_title border_title animation" data-animation="fadeInUp" data-animation-delay="0.2s">Newsletter</h4>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">By subscribing to our mailing list you will always be update with the latest news from us.</p>
                        <form class="subscribe_form animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                            <input class="input-rounded" type="text" required placeholder="Enter Email Address"/>
                          <button type="submit" title="Subscribe" class="btn-info" name="submit" value="Submit"> Subscribe </button>
                        </form>
                    </div>
                </div>
                <div class="col-lg-3 offset-lg-1 col-md-6 col-sm-8 res_md_mt_30 res_sm_mt_20">
                    <h4 class="footer_title border_title animation" data-animation="fadeInUp" data-animation-delay="0.2s">Quick Links</h4>
                    <ul class="footer_link half_link list_none">
                       <li class="animation" data-animation="fadeInUp" data-animation-delay="0.2s"><a href="{{url('/')}}#service">SERVICES</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.3s"><a href="{{route('about')}}">ABOUT</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.4s"><a href="#">AFFILIATED PROGRAM</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.5s"><a href="{{url('/')}}#faq">FAQ</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.6s"><a href="{{url('/')}}#contact">Contact</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 offset-lg-1 col-md-6 col-sm-4 res_md_mt_30 res_sm_mt_20">
                    <h4 class="footer_title border_title animation" data-animation="fadeInUp" data-animation-delay="0.2s">Social</h4>
                    <ul class="footer_social list_none">
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.2s"><a href="#service"><i class="ion-social-facebook"></i> Facebook</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.3s"><a href="#service"><i class="ion-social-twitter"></i> Twitter</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.4s"><a href="#service"><i class="ion-social-googleplus"></i> Google Plus</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.5s"><a href="#service"><i class="ion-social-pinterest"></i> Pintrest</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.6s"><a href="#service"><i class="ion-social-instagram-outline"></i> Instagram</a></li>
                    </ul>
                </div>
                
            </div>
        </div>
    </div>
    <div class="bottom_footer">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <p class="copyright">Copyright &copy; 2018 All Rights Reserved.</p>
        </div>
        <div class="col-md-6">
          <ul class="list_none footer_menu">
            <li><a href="#service">Privacy Policy</a></li>
            <li><a href="#about">Terms & Conditions</a></li>
          </ul>
        </div>
      </div>
    </div>
    </div>
</footer>
<!-- END FOOTER SECTION --> 

<a href="#" class="scrollup btn-default"><i class="ion-ios-arrow-up"></i></a> 

<!-- Latest jQuery --> 
<script src="{{ asset('landing/assets/js/jquery-1.12.4.min.js')}}"></script> 
<!-- Latest compiled and minified Bootstrap --> 
<script src="{{ asset('landing/assets/bootstrap/js/bootstrap.min.js')}}"></script> 
<!-- owl-carousel min js  --> 
<script src="{{ asset('landing/assets/owlcarousel/js/owl.carousel.min.js')}}"></script> 
<!-- magnific-popup min js  --> 
<script src="{{ asset('landing/assets/js/magnific-popup.min.js')}}"></script> 
<!-- waypoints min js  --> 
<script src="{{ asset('landing/assets/js/waypoints.min.js')}}"></script> 
<!-- parallax js  --> 
<script src="{{ asset('landing/assets/js/parallax.js')}}"></script>     
<!-- countdown js  --> 
<script src="{{ asset('landing/assets/js/jquery.countdown.min.js')}}"></script> 
<!-- particles min js  --> 
<script src="{{ asset('landing/assets/js/particles.min.js')}}"></script> 
<!-- scripts js --> 
<script src="{{ asset('landing/assets/js/jquery.dd.min.js')}}"></script> 
<!-- jquery.counterup.min js --> 
<script src="{{ asset('landing/assets/js/jquery.counterup.min.js')}}"></script> 
<!-- scripts js --> 
<script src="{{ asset('landing/assets/js/scripts.js')}}"></script>
</body>

<!-- Mirrored from bestwebcreator.com/cryptocash/demo/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 21 Feb 2020 10:29:30 GMT -->
</html>