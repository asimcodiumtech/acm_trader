<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
   <!--  <a href="index3.html" class="brand-link">
      <img src="{{ asset('/images/acm_trader.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Admin</span>
    </a> -->

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image" style="height: 72px">
          <img src="{{ asset('/images/acm_trader.png')}}" class="img-circle elevation-2" alt="User Image" style="width: 100%;
    height: 167%;">
          
        </div>
        
      </div>
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
         <img src="{{ asset('/images/admin.png')}}" class="img-circle elevation-2" alt="User Image"> 
    <!--     <img src="{{ asset('/images/acm_trader.png')}}" class="img-circle elevation-2" alt="User Image" style="width: 100%;-->
    <!--height: 167%;">-->
          
        </div>
        <div class="info">
            <i class="glyphicon glyphicon-user"></i>
          <a href="#" class="d-block">{{auth()->user()->name}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          
          <li class="nav-item">
            <a href="{{route('home')}}" class="nav-link">
              <i class="nav-icon fas fa-chart-line"></i>
              <p>
                Dashboard
                
              </p>
            </a>
          </li>
          @if(!empty(auth()->user()->admin==1))
            @if (in_array(request()->route()->getName(), ['admin.user.detail', 'deposite.list','profit.index','withdraw.index','user.gift.list','user.netowrk.index']))

         <li class="nav-item">
                <a href="{{route('user.netowrk.index')}}" class="nav-link">
                  <i class="nav-icon fas fa-th"></i>
                  <p>
                    My Network
                    
                  </p>
                </a>
              </li>
          <li class="nav-item">
                <a href="{{route('deposite.list')}}" class="nav-link">
                  <i class="fas fa-arrow-down"></i>
                  <p>
                      Deposit Hisotry
                 </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('profit.index')}}" class="nav-link">
                  <i class="nav-icon fa fa-hand-holding-usd"></i>
                  <p>
                    Profit History 
                    
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('withdraw.index')}}" class="nav-link">
                  <i class="nav-icon fa fa-arrow-up"></i>
                  <p>
                    Withdrawals History
                    
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('user.gift.list')}}" class="nav-link">
                  <i class="nav-icon fas fa-gift"></i>
                  <p>
                     Gift List
                    
                  </p>
                </a>
              </li>


@else
<li class="nav-item">
                <a href="{{route('admin.users.list')}}" class="nav-link">
                  <i class="nav-icon fas fa-th"></i>
                  <p>
                     All User List                    
                  </p>
                </a>
              </li>
          <li class="nav-item">
                <a href="{{route('admin.notification.index')}}" class="nav-link">
                  <i class="nav-icon fas fa-th"></i>
                  <p>
                      Send Notification
                    
                  </p>
                </a>
              </li>
<li class="nav-item">
                <a href="{{route('admin.newsletter')}}" class="nav-link">
                  <i class="nav-icon fas fa-th"></i>
                  <p>
                      Send NewsLetter Message 
                    
                  </p>
                </a>
              </li>

          <li class="nav-item">
                <a href="{{route('admin.weekly.index')}}" class="nav-link">
                  <i class="nav-icon fas fa-th"></i>
                  <p>
                     Set Daily Percentage
                    
                  </p>
                </a>
              </li>
              
           <li class="nav-item">
                <a href="{{route('admin.pending.index')}}" class="nav-link">
                  <i class="nav-icon fas fa-th"></i>
                  <p>
                     Deposit Request
                    
                  </p>
                </a>
              </li>
              
              <li class="nav-item">
                <a href="{{route('admin.withdraw.index')}}" class="nav-link">
                  <i class="nav-icon fas fa-th"></i>
                  <p>
                    Withdraw Lists
                    
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.contact.index')}}" class="nav-link">
                  <i class="nav-icon fas fa-th"></i>
                  <p>
                     Contact List
                    <i class=" right">{{App\Models\Contact::unread()}}</i>
                  </p> 
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.gift.list')}}" class="nav-link">
                  <i class="nav-icon fas fa-th"></i>
                  <p>
                     Gift List
                    
                  </p>
                </a>
              </li>



@endif

          @else

              <li class="nav-item">
                <a href="{{route('user.netowrk.index')}}" class="nav-link">
                  <i class="nav-icon fas fa-network-wired"></i>
                  <p>
                    My Network
                    
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('user.gift.add')}}" class="nav-link">
                  <i class="nav-icon fas fa-gift"></i>
                  <p>
                    Gift Code
                    
                  </p>
                </a>
              </li>
              
                <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-coins"></i>
              <p>
                Transaction History
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <!-- <li class="nav-item">
                <a href="{{route('deposite.index')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Deposit Amount</p>
                </a>
              </li> -->
              <li class="nav-item">
                <a href="{{route('deposite.list')}}" class="nav-link">
                  <i class="fas fa-arrow-down"></i>
                  <p>
                      Deposit Hisotry
                 </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('profit.index')}}" class="nav-link">
                  <i class="nav-icon fa fa-hand-holding-usd"></i>
                  <p>
                    Profit History 
                    
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('withdraw.index')}}" class="nav-link">
                  <i class="nav-icon fa fa-arrow-up"></i>
                  <p>
                    Withdrawals History
                    
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('user.gift.list')}}" class="nav-link">
                  <i class="nav-icon fas fa-gift"></i>
                  <p>
                     Gift List
                    
                  </p>
                </a>
              </li>
              
            </ul>
          </li>
          <li class="nav-item">
                <a href="{{route('user.setting')}}" class="nav-link">
                  <i class="nav-icon fas fa-cogs"></i>
                  <p>
                     Settings
                    
                  </p>
                </a>
              </li>
          <li class="nav-item">
                <a href="{{route('user.sos.list')}}" class="nav-link">
                  <i class="nav-icon fas fa-envelope"></i>
                  <p>
                     Support
                    
                  </p>
                </a>
              </li>
          

              
             <!-- <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="nav-icon fa fa-life-ring"></i>
                  <p>
                    Support 
                    
                  </p>
                </a>
              </li>-->
          @endif
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>