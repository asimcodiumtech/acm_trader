@extends('layouts.app')

@section('dashboardcontent')
    <style>
        body {
            background: #f9f9fb;
        }

        .view-account {
            background: #FFFFFF;
            margin-top: 20px;
        }

        .view-account .pro-label {
            font-size: 13px;
            padding: 4px 5px;
            position: relative;
            top: -5px;
            margin-left: 10px;
            display: inline-block
        }

        .view-account .side-bar {
            padding-bottom: 30px;
            color: white;
        }

        .view-account .side-bar .hedge-info {
            text-align: center;
            margin-bottom: 15px;
            padding: 30px;
            color: #f3f3f3;
            border-bottom: 1px solid #f3f3f3
        }

        .view-account .side-bar .hedge-info .img-profile {
            width: 120px;
            height: 120px;
            margin-bottom: 15px
        }

        .view-account .side-bar .hedge-info .meta li {
            margin-bottom: 10px
        }

        .view-account .side-bar .hedge-info .meta li span {
            display: inline-block;
            width: 100px;
            margin-right: 5px;
            text-align: right
        }

        .view-account .side-bar .hedge-info .meta li a {
            color: #616670
        }

        .view-account .side-bar .hedge-info .meta li.activity {
            color: #a2a6af
        }

        .view-account .side-bar .side-menu {
            text-align: center
        }

        .view-account .side-bar .side-menu .nav {
            display: inline-block;
            margin: 0 auto
        }

        .view-account .side-bar .side-menu .nav > li {
            font-size: 14px;
            margin-bottom: 0;
            border-bottom: none;
            display: inline-block;
            float: left;
            margin-right: 15px;
            margin-bottom: 15px
        }

        .view-account .side-bar .side-menu .nav > li:last-child {
            margin-right: 0
        }

        .view-account .side-bar .side-menu .nav > li > a {
            display: inline-block;
            color: #9499a3;
            padding: 5px;
            border-bottom: 2px solid transparent
        }

        .view-account .side-bar .side-menu .nav > li > a:hover {
            color: #616670;
            background: none
        }

        .view-account .side-bar .side-menu .nav > li.active a {
            color: #40babd;
            border-bottom: 2px solid #40babd;
            background: none;
            border-right: none
        }

        .theme-2 .view-account .side-bar .side-menu .nav > li.active a {
            color: #6dbd63;
            border-bottom-color: #6dbd63
        }

        .theme-3 .view-account .side-bar .side-menu .nav > li.active a {
            color: #497cb1;
            border-bottom-color: #497cb1
        }

        .theme-4 .view-account .side-bar .side-menu .nav > li.active a {
            color: #ec6952;
            border-bottom-color: #ec6952
        }

        .view-account .side-bar .side-menu .nav > li .icon {
            display: block;
            font-size: 24px;
            margin-bottom: 5px
        }

        .view-account .content-panel {
            padding: 30px
        }

        .view-account .content-panel .title {
            margin-bottom: 15px;
            margin-top: 0;
            font-size: 18px
        }

        .view-account .content-panel .fieldset-title {
            padding-bottom: 15px;
            border-bottom: 1px solid #eaeaf1;
            margin-bottom: 30px;
            color: #f9f3ed;
            font-size: 16px
        }

        .view-account .content-panel .avatar .figure img {
            float: right;
            width: 64px
        }

        .view-account .content-panel .content-header-wrapper {
            position: relative;
            margin-bottom: 30px
        }

        .view-account .content-panel .content-header-wrapper .actions {
            position: absolute;
            right: 0;
            top: 0
        }

        .view-account .content-panel .content-utilities {
            position: relative;
            margin-bottom: 30px
        }

        .view-account .content-panel .content-utilities .btn-group {
            margin-right: 5px;
            margin-bottom: 15px
        }

        .view-account .content-panel .content-utilities .fa {
            font-size: 16px;
            margin-right: 0
        }

        .view-account .content-panel .content-utilities .page-nav {
            position: absolute;
            right: 0;
            top: 0
        }

        .view-account .content-panel .content-utilities .page-nav .btn-group {
            margin-bottom: 0
        }

        .view-account .content-panel .content-utilities .page-nav .indicator {
            color: #a2a6af;
            margin-right: 5px;
            display: inline-block
        }

        .view-account .content-panel .mails-wrapper .mail-item {
            position: relative;
            padding: 10px;
            border-bottom: 1px solid #f3f3f3;
            color: #616670;
            overflow: hidden
        }

        .view-account .content-panel .mails-wrapper .mail-item > div {
            float: left
        }

        .view-account .content-panel .mails-wrapper .mail-item .icheck {
            background-color: #fff
        }

        .view-account .content-panel .mails-wrapper .mail-item:hover {
            background: #f9f9fb
        }

        .view-account .content-panel .mails-wrapper .mail-item:nth-child(even) {
            background: #fcfcfd
        }

        .view-account .content-panel .mails-wrapper .mail-item:nth-child(even):hover {
            background: #f9f9fb
        }

        .view-account .content-panel .mails-wrapper .mail-item a {
            color: #616670
        }

        .view-account .content-panel .mails-wrapper .mail-item a:hover {
            color: #494d55;
            text-decoration: none
        }

        .view-account .content-panel .mails-wrapper .mail-item .checkbox-container,
        .view-account .content-panel .mails-wrapper .mail-item .star-container {
            display: inline-block;
            margin-right: 5px
        }

        .view-account .content-panel .mails-wrapper .mail-item .star-container .fa {
            color: #a2a6af;
            font-size: 16px;
            vertical-align: middle
        }

        .view-account .content-panel .mails-wrapper .mail-item .star-container .fa.fa-star {
            color: #f2b542
        }

        .view-account .content-panel .mails-wrapper .mail-item .star-container .fa:hover {
            color: #868c97
        }

        .view-account .content-panel .mails-wrapper .mail-item .mail-to {
            display: inline-block;
            margin-right: 5px;
            min-width: 120px
        }

        .view-account .content-panel .mails-wrapper .mail-item .mail-subject {
            display: inline-block;
            margin-right: 5px
        }

        .view-account .content-panel .mails-wrapper .mail-item .mail-subject .label {
            margin-right: 5px
        }

        .view-account .content-panel .mails-wrapper .mail-item .mail-subject .label:last-child {
            margin-right: 10px
        }

        .view-account .content-panel .mails-wrapper .mail-item .mail-subject .label a,
        .view-account .content-panel .mails-wrapper .mail-item .mail-subject .label a:hover {
            color: #fff
        }

        .view-account .content-panel .mails-wrapper .mail-item .mail-subject .label-color-1 {
            background: #f77b6b
        }

        .view-account .content-panel .mails-wrapper .mail-item .mail-subject .label-color-2 {
            background: #58bbee
        }

        .view-account .content-panel .mails-wrapper .mail-item .mail-subject .label-color-3 {
            background: #f8a13f
        }

        .view-account .content-panel .mails-wrapper .mail-item .mail-subject .label-color-4 {
            background: #ea5395
        }

        .view-account .content-panel .mails-wrapper .mail-item .mail-subject .label-color-5 {
            background: #8a40a7
        }

        .view-account .content-panel .mails-wrapper .mail-item .time-container {
            display: inline-block;
            position: absolute;
            right: 10px;
            top: 10px;
            color: #a2a6af;
            text-align: left
        }

        .view-account .content-panel .mails-wrapper .mail-item .time-container .attachment-container {
            display: inline-block;
            color: #a2a6af;
            margin-right: 5px
        }

        .view-account .content-panel .mails-wrapper .mail-item .time-container .time {
            display: inline-block;
            text-align: right
        }

        .view-account .content-panel .mails-wrapper .mail-item .time-container .time.today {
            font-weight: 700;
            color: #494d55
        }

        .drive-wrapper {
            padding: 15px;
            background: #f5f5f5;
            overflow: hidden
        }

        .drive-wrapper .drive-item {
            width: 130px;
            margin-right: 15px;
            display: inline-block;
            float: left
        }

        .drive-wrapper .drive-item:hover {
            box-shadow: 0 1px 5px rgba(0, 0, 0, .1);
            z-index: 1
        }

        .drive-wrapper .drive-item-inner {
            padding: 15px
        }

        .drive-wrapper .drive-item-title {
            margin-bottom: 15px;
            max-width: 100px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis
        }

        .drive-wrapper .drive-item-title a {
            color: #494d55
        }

        .drive-wrapper .drive-item-title a:hover {
            color: #40babd
        }

        .theme-2 .drive-wrapper .drive-item-title a:hover {
            color: #6dbd63
        }

        .theme-3 .drive-wrapper .drive-item-title a:hover {
            color: #497cb1
        }

        .theme-4 .drive-wrapper .drive-item-title a:hover {
            color: #ec6952
        }

        .drive-wrapper .drive-item-thumb {
            width: 100px;
            height: 80px;
            margin: 0 auto;
            color: #616670
        }

        .drive-wrapper .drive-item-thumb a {
            -webkit-opacity: .8;
            -moz-opacity: .8;
            opacity: .8
        }

        .drive-wrapper .drive-item-thumb a:hover {
            -webkit-opacity: 1;
            -moz-opacity: 1;
            opacity: 1
        }

        .drive-wrapper .drive-item-thumb .fa {
            display: inline-block;
            font-size: 36px;
            margin: 0 auto;
            margin-top: 20px
        }

        .drive-wrapper .drive-item-footer .utilities {
            margin-bottom: 0
        }

        .drive-wrapper .drive-item-footer .utilities li:last-child {
            padding-right: 0
        }

        .drive-list-view .name {
            width: 60%
        }

        .drive-list-view .name.truncate {
            max-width: 100px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis
        }

        .drive-list-view .type {
            width: 15px
        }

        .drive-list-view .date,
        .drive-list-view .size {
            max-width: 60px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis
        }

        .drive-list-view a {
            color: #494d55
        }

        .drive-list-view a:hover {
            color: #40babd
        }

        .theme-2 .drive-list-view a:hover {
            color: #6dbd63
        }

        .theme-3 .drive-list-view a:hover {
            color: #497cb1
        }

        .theme-4 .drive-list-view a:hover {
            color: #ec6952
        }

        .drive-list-view td.date,
        .drive-list-view td.size {
            color: #a2a6af
        }

        @media (max-width: 767px) {
            .view-account .content-panel .title {
                text-align: center
            }

            .view-account .side-bar .hedge-info {
                padding: 0
            }

            .view-account .side-bar .hedge-info .img-profile {
                width: 60px;
                height: 60px
            }

            .view-account .side-bar .hedge-info .meta li {
                margin-bottom: 5px
            }

            .view-account .content-panel .content-header-wrapper .actions {
                position: static;
                margin-bottom: 30px
            }

            .view-account .content-panel {
                padding: 0
            }

            .view-account .content-panel .content-utilities .page-nav {
                position: static;
                margin-bottom: 15px
            }

            .drive-wrapper .drive-item {
                width: 100px;
                margin-right: 5px;
                float: none
            }

            .drive-wrapper .drive-item-thumb {
                width: auto;
                height: 54px
            }

            .drive-wrapper .drive-item-thumb .fa {
                font-size: 24px;
                padding-top: 0
            }

            .view-account .content-panel .avatar .figure img {
                float: none;
                margin-bottom: 15px
            }

            .view-account .file-uploader {
                margin-bottom: 15px
            }

            .view-account .mail-subject {
                max-width: 100px;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis
            }

            .view-account .content-panel .mails-wrapper .mail-item .time-container {
                position: static
            }

            .view-account .content-panel .mails-wrapper .mail-item .time-container .time {
                width: auto;
                text-align: left
            }
        }

        @media (min-width: 768px) {
            .view-account .side-bar .hedge-info {
                padding: 0;
                padding-bottom: 15px
            }

            .view-account .mail-subject .subject {
                max-width: 200px;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis
            }
        }

        @media (min-width: 992px) {
            .view-account .content-panel {
                min-height: 800px;
                border-left: 1px solid #f3f3f7;
                margin-left: 200px
            }

            .view-account .mail-subject .subject {
                max-width: 280px;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis
            }

            .view-account .side-bar {
                position: absolute;
                width: 200px;
                min-height: 600px
            }

            .view-account .side-bar .hedge-info {
                margin-bottom: 0;
                border-bottom: none;
                padding: 30px
            }

            .view-account .side-bar .hedge-info .img-profile {
                width: 120px;
                height: 120px
            }

            .view-account .side-bar .side-menu {
                text-align: left
            }

            .view-account .side-bar .side-menu .nav {
                display: block
            }

            .view-account .side-bar .side-menu .nav > li {
                display: block;
                float: none;
                font-size: 14px;
                border-bottom: 1px solid #f3f3f7;
                margin-right: 0;
                margin-bottom: 0
            }

            .view-account .side-bar .side-menu .nav > li > a {
                display: block;
                color: #9499a3;
                padding: 10px 15px;
                padding-left: 30px
            }

            .view-account .side-bar .side-menu .nav > li > a:hover {
                background: #f9f9fb
            }

            .view-account .side-bar .side-menu .nav > li.active a {
                background: #f9f9fb;
                border-right: 4px solid #40babd;
                border-bottom: none
            }

            .theme-2 .view-account .side-bar .side-menu .nav > li.active a {
                border-right-color: #6dbd63
            }

            .theme-3 .view-account .side-bar .side-menu .nav > li.active a {
                border-right-color: #497cb1
            }

            .theme-4 .view-account .side-bar .side-menu .nav > li.active a {
                border-right-color: #ec6952
            }

            .view-account .side-bar .side-menu .nav > li .icon {
                font-size: 24px;
                vertical-align: middle;
                text-align: center;
                width: 40px;
                display: inline-block
            }

            .field-icon {
                float: right;
                margin-right: 17px;
                margin-top: -25px;
                position: relative;
                z-index: 14;
            }
        }
    </style>
    {{--        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />--}}
    <!-- Select2 -->
    <link rel="stylesheet" href="../../js/select2/css/select2.min.css">
    <link rel="stylesheet" href="../../js/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 style="color: white;">Hedge Management</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Hedge List</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="">
            <!-- SELECT2 EXAMPLE -->
            <div class="row">

                <!-- general form elements -->
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header " style="background-color: #35354B;color: white;">
                            <h3 class="card-title">Hedges</h3>
                        </div>
                        <div class="card-header" style="background-color: #35354B;color: white;">
                            <a class="btn btn-info" id="add_hedge_btn" href="#">Add New Hedge</a>

                        </div>
                        <!-- /.card-header -->
                        <div class="card-body" style="background-color: #35354B;color: white;">
                            <div class="table-responsive ">
                                <table id="example1" class="  table table-bordered table-striped ">
                                    <thead style="background-color:#131311; color:white;">
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Amount</th>
                                        <th>Slot</th>
                                        <th>Available Slot</th>
                                        <th>Code</th>
                                        <th>Total Invested</th>
                                        <th>Opening Date</th>
                                        <th>Closing Date</th>
                                        <th>Due Date</th>
                                        <th>Status</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody style="background-color: #35354B;color: white;">
                                    @foreach($hedge as $hed)
                                        <tr>
                                            <td>{{$hed['id']}}</td>
                                            <td>{{$hed['title']}}</td>
                                            <td>{{$hed['amount']}}</td>
                                            <td>{{$hed['slot']}}</td>
                                            <td>{{$hed['available_slot']}}</td>
                                            <td>{{$hed['code']}}</td>
                                            {{--                                    <script>--}}
                                            {{--                                        var app = @json($hed->roles);--}}
                                            {{--                                        app->name;--}}
                                            {{--                                    </script>--}}
                                            {{--                                    <td> {{$hed->getRoleNames()}}</td>--}}
                                            <td> {{$hed['total_invested']}}</td>
                                            <td> {{Carbon\Carbon::parse($hed['slot_opening_date'])->format('y-m-d')}}</td>
                                            <td> {{Carbon\Carbon::parse($hed['slot_closing_date'])->format('y-m-d')}}</td>
                                            <td> {{Carbon\Carbon::parse($hed['due_date'])->format('y-m-d')}}</td>
                                            <td> {{$hed['status']}}</td>
                                            {{--                                            <td> {{implode($hed->permissions->pluck('name')->toarray())}}</td>--}}
                                            {{--                                            <td> {{$hed->getAllPermissions()}}</td>--}}

                                            <td style="text-align:right;">
                                                {{--                                                <button class="btn btn-success  add_hedge"--}}
                                                {{--                                                        type="button" data-uid="{{$hed->id}}"--}}

                                                {{--                                                ><i class="fas fa-hedge-circle"></i> &nbsp;View Profile--}}
                                                {{--                                                </button>--}}

                                                <button class="btn btn-success  add_hedge" type="button"
                                                        data-hedgeid="{{$hed['id']}}"><i class="fas fa-user-circle"></i>
                                                    &nbsp;View Details
                                                </button>


                                            </td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot style="background-color:#131311; color:white;">
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Amount</th>
                                        <th>Slot</th>
                                        <th>Available Slot</th>
                                        <th>Code</th>
                                        <th>Total Invested</th>
                                        <th>Opening Date</th>
                                        <th>Closing Date</th>
                                        <th>Due Date</th>
                                        <th>Status</th>
                                        <th>Action</th>

                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- /.card -->
            </div>


        </div>


        <!-- /.row -->
        </div><!-- /.container-fluid -->
        </div><!-- /.container-fluid -->
    </section>


    <!-- Modal -->
    <div class="modal fade" id="add_hedge_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #42426a;color: white;">
                    <h5 class="modal-title" id="exampleModalLabel" style=" color:white;">Hedge Info</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="background-color:#35354B; color:white;">

                    {{--                    <div class="card-body">--}}
                    <input type="hidden" id="hedge_id" value="">
                    <div class="container">
                        <div class="view-account">
                            <section class="module " style="background-color:#35354B; color:white;">
                                <div class="module-inner">
                                    <div class="side-bar">
                                        <div class="hedge-info">
                                            {{--                                            <img class="img-profile img-circle img-responsive center-block"--}}
                                            {{--                                                 src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="">--}}
                                            Hedge Info
                                            <ul class="meta list list-unstyled">
                                                <li class="name mt-2">
                                                    <div id="header_title"></div>
                                                </li>
                                                <label class="label label-info">
                                                    Total Slots: <span id="header_slots"></span> </label>
                                                <label class="label label-info">
                                                    Available Slots: <span id="header_available"></span></label>
                                                <label class="label label-info">
                                                    Code: <span id="header_code"></span></label>
                                                <li class="email" id=""><a href="#"></a></li>
{{--                                                <li class="activity">Created at:--}}
{{--                                                    <span id="header_create_at" style="text-align: right;--}}
{{--    margin-right: 36px;"></span></li>--}}
                                                <li class="activity">Due Date:
                                                    <span id="header_Duedate" style="text-align: right;
    margin-right: 36px;"></span></li>

                                            </ul>
                                        </div>
                                        <nav class="side-menu">
                                            <nav>
                                                <div class="nav nav-tabs " id="nav-tab" role="tablist">
                                                    <a class="nav-item nav-link  " id="nav-profile-tab"
                                                       data-toggle="tab"
                                                       href="#nav-profile" role="tab" aria-controls="nav-profile"
                                                       aria-selected="false" style="color: white"><span
                                                            class="fa fa-edit"></span> &nbsp;Edit</a>
                                                    <a class="nav-item nav-link active " id="nav-setting-tab"
                                                       data-toggle="tab" href="#nav-setting" role="tab"
                                                       aria-controls="nav-setting" aria-selected="true"
                                                       style="color: white"><span class="fa fa-cog"></span> &nbsp;Set
                                                        Profit</a>

                                                    <a class="nav-item nav-link nav_activites_log"
                                                       id="nav-activates-tab" data-toggle="tab"
                                                       href="#nav-activates" role="tab" aria-controls="nav-activates"
                                                       aria-selected="false" style="color: white"><span
                                                            class="fa fa-credit-card"></span> &nbsp;Profit History</a>
                                                    <a class="nav-item nav-link nav-bookings"
                                                       id="nav-activates-tab" data-toggle="tab"
                                                       href="#nav-bookings" role="tab" aria-controls="nav-bookings"
                                                       aria-selected="false" style="color: white"><span
                                                            class="fa fa-book-open"></span> &nbsp;Bookings</a>
                                                </div>
                                            </nav>
                                            {{--                                            <ul class="nav">--}}
                                            {{--                                                <li><a href="#"><span class="fa fa-hedge"></span> Profile</a></li>--}}
                                            {{--                                                <li class="active"><a href="#"><span class="fa fa-cog"></span> Settings</a>--}}
                                            {{--                                                </li>--}}
                                            {{--                                                <li><a href="#"><span class="fa fa-credit-card"></span> Activates</a>--}}
                                            {{--                                                </li>--}}
                                            {{--                                                --}}{{--                                                <li><a href="#"><span class="fa fa-envelope"></span> Messages</a></li>--}}

                                            {{--                                                --}}{{--                                                <li><a href="hedge-drive.html"><span class="fa fa-th"></span> Drive</a></li>--}}
                                            {{--                                                --}}{{--                                                <li><a href="#"><span class="fa fa-clock-o"></span> Reminders</a></li>--}}
                                            {{--                                            </ul>--}}
                                        </nav>
                                    </div>
                                    <div class="content-panel">
                                        {{--                                        <h2 class="title">Profile<span class="pro-label label label-warning">PRO</span></h2>--}}
                                        <div class="tab-content" id="nav-tabContent">
                                            <div class="tab-pane fade active show " id="nav-setting" role="tabpanel"
                                                 aria-labelledby="nav-setting-tab">

                                                <fieldset class="fieldset mt-4">
                                                    <div class="card card-primary">
                                                        <div class="card-header"
                                                             style="background-color: #42426a;color: white;">
                                                            <h3 class="card-title">Hedge Profit</h3>
                                                        </div>
                                                        <!-- /.card-header -->
                                                        <!-- form start -->


                                                        <div class="card-body"
                                                             style="background-color:#35354B; color:white;">
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Set Daily Percentage to
                                                                    Give</label>
                                                                <input type="text" class="form-control"
                                                                       id="hedge_profit_perc"
                                                                       placeholder="Enter %age value"
                                                                       name="hedge_profit_perc"
                                                                       value=""
                                                                       style="background-color:#131311; color:white;">
                                                            </div>

                                                            <!-- /.card-body -->


                                                        </div>
                                                    </div>

                                                </fieldset>

                                                <hr>
                                                <div class="form-group">
                                                    <div
                                                        class="col-md-10 col-sm-9 col-xs-12 col-md-push-2 col-sm-push-3 col-xs-push-0">

                                                        <button type="submit" class="btn btn-primary"
                                                                id='profit_submit'
                                                                style="background-color: #35354B;color: white;">
                                                            Submit
                                                        </button>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="tab-pane fade " id="nav-profile" role="tabpanel"
                                                 aria-labelledby="nav-profile-tab">
                                                <form class="form-horizontal">
                                                    <fieldset class="fieldset">
                                                        <h3 class="fieldset-title ">Hedge Info</h3>
                                                        <div class="form-group avatar">

                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 col-sm-3 col-xs-12 control-label">
                                                                Title</label>
                                                            <div class="col-md-10 col-sm-9 col-xs-12">
                                                                <input type="text" class="form-control" id="title-m"
                                                                       value=""
                                                                       style="background-color: black ; color: white">
                                                                <span style="display: none"
                                                                      id="title_error_m"
                                                                      class="text-danger"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label
                                                                class="col-md-2 col-sm-3 col-xs-12 control-label">Amount</label>
                                                            <div class="col-md-10 col-sm-9 col-xs-12">
                                                                <input type="text" class="form-control"
                                                                       id="amount-m" value=""
                                                                       style="background-color: black ; color: white">
                                                                <span style="display: none"
                                                                      id="amount_error_m"
                                                                      class="text-danger"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label
                                                                class="col-md-2 col-sm-3 col-xs-12 control-label">Slot</label>
                                                            <div class="col-md-10 col-sm-9 col-xs-12">
                                                                <input id="slot-m" type="text"
                                                                       class="form-control"
                                                                       name="slot" value=""
                                                                       style="background-color: black ; color: white">

                                                                <span style="display: none" id="slot_error_m"
                                                                      class="text-danger"></span>


                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label
                                                                class="col-md-2 col-sm-3 col-xs-12 control-label">Slot Opening Date</label>
                                                            <div class="col-md-10 col-sm-9 col-xs-12">
                                                                <input id="slot_opening_date-m" type="text"
                                                                       class="form-control"
                                                                       name="slot_opening_date" value=""
                                                                       style="background-color: black ; color: white" readonly>
                                                                <span style="display: none"
                                                                      id="slot_opening_date_error_m"
                                                                      class="text-danger"></span>


                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label
                                                                class="col-md-2 col-sm-3 col-xs-12 control-label">Slot
                                                                Closing Date</label>
                                                            <div class="col-md-10 col-sm-9 col-xs-12">
                                                                <input id="slot_closing_date-m" type="text"
                                                                       class="form-control"
                                                                       name="slot_closing_date" value=""
                                                                       style="background-color: black ; color: white" readonly >
                                                                <span style="display: none"
                                                                      id="slot_closing_date_error_m"
                                                                      class="text-danger"></span>


                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label
                                                                class="col-md-2 col-sm-3 col-xs-12 control-label">Due
                                                                Date</label>
                                                            <div class="col-md-10 col-sm-9 col-xs-12">
                                                                <input id="due_date-m" type="text"
                                                                       class="form-control"
                                                                       name="due_date" value=""
                                                                       style="background-color: black ; color: white" readonly>

                                                                <span style="display: none" id="due_date_error_m"
                                                                      class="text-danger"></span>


                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                    <hr>
                                                    <div class="form-group">
                                                        <div
                                                            class="col-md-10 col-sm-9 col-xs-12 col-md-push-2 col-sm-push-3 col-xs-push-0">

                                                            <input class="btn btn-primary submit_hedge_form"
                                                                   type="submit"
                                                                   value="Update Info">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="tab-pane fade" id="nav-activates" role="tabpanel"
                                                 aria-labelledby="nav-activates-tab">
                                                <div class="table-responsive ">
                                                    <fieldset class="fieldset mt-4">
                                                        <h3 class="fieldset-title"><span>Hedge Profit History</span>
                                                        </h3>
                                                        <div class="row">
                                                            <div class="col-md-4 ">

                                                            </div>
                                                            <div class="col-md-4 ">

                                                            </div>
                                                            <div class="col-md-4 mb-3">


                                                                <select class="form-control" name="log_name"
                                                                        id="log_name">
                                                                    <option value="">Select Filter</option>
                                                                </select>
                                                            </div>
                                                        </div>


                                                        <table id="activities_table1"
                                                               class="  table table-bordered table-striped ">
                                                            <thead style="background-color:#131311; color:white;">
                                                            <tr>
                                                                {{--                                                            <th>ID</th>--}}
                                                                <th>Percentage</th>
                                                                <th>Profit Time</th>


                                                            </tr>
                                                            </thead>
                                                            <tbody style="background-color: #35354B;color: white;"
                                                                   id="profit_tbody">

                                                            </tbody>
                                                            <tfoot style="background-color:#131311; color:white;">
                                                            <tr>
                                                                {{--                                                            <th>ID</th>--}}
                                                                <th>Percentage</th>
                                                                <th>Profit Time</th>



                                                            </tr>
                                                            </tfoot>
                                                        </table>
                                                    </fieldset>
                                                </div>

                                            </div>
                                            <div class="tab-pane fade" id="nav-bookings" role="tabpanel"
                                                 aria-labelledby="nav-activates-tab">
                                                <div class="table-responsive ">
                                                    <fieldset class="fieldset mt-4">
                                                        <h3 class="fieldset-title"><span>Hedge Booking History</span>
                                                        </h3>
                                                        <div class="row">
                                                            <div class="col-md-4 ">

                                                            </div>
                                                            <div class="col-md-4 ">

                                                            </div>
                                                            <div class="col-md-4 mb-3">


                                                                <select class="form-control" name="booking_name"
                                                                        id="bookings_name">
                                                                    <option value="">Select Filter</option>
                                                                </select>
                                                            </div>
                                                        </div>


                                                        <table id="bookings_table"
                                                               class="  table table-bordered table-striped ">
                                                            <thead style="background-color:#131311; color:white;">
                                                            <tr>

                                                                <th>User Name</th>
                                                                <th>Email</th>
                                                                <th>Slot</th>
                                                                <th>Amount</th>
                                                                <th>BTC Rate</th>
                                                                <th>Type</th>
                                                                <th>Booking Time</th>



                                                            </tr>
                                                            </thead>
                                                            <tbody style="background-color: #35354B;color: white;"
                                                                   id="bookings_tbody">

                                                            </tbody>
                                                            <tfoot style="background-color:#131311; color:white;">
                                                            <tr>
                                                                {{--                                                            <th>ID</th>--}}
                                                                <th>User Name</th>
                                                                <th>Email</th>
                                                                <th>Slot</th>
                                                                <th>Amount</th>
                                                                <th>BTC Rate</th>
                                                                <th>Type</th>
                                                                <th>Booking Time</th>


                                                            </tr>
                                                            </tfoot>
                                                        </table>
                                                    </fieldset>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>


                    <!-- /.form-group -->
                    {{--                                <div class="form-group">--}}
                    {{--                                    <label>Disabled Result</label>--}}
                    {{--                                    <select class="form-control select2bs4" style="width: 100%;">--}}
                    {{--                                        <option selected="selected">Alabama</option>--}}
                    {{--                                        <option>Alaska</option>--}}
                    {{--                                        <option disabled="disabled">California (disabled)</option>--}}
                    {{--                                        <option>Delaware</option>--}}
                    {{--                                        <option>Tennessee</option>--}}
                    {{--                                        <option>Texas</option>--}}
                    {{--                                        <option>Washington</option>--}}
                    {{--                                    </select>--}}
                    {{--                                </div>--}}
                <!-- /.form-group -->

                    <!-- /.col -->

                    <!-- /.row -->
                    {{--                    </div>--}}


                </div>
                <div class="modal-footer">
                    {{--                    <button type="button" id="submit_hedge_form" class="btn btn-secondary">Submit--}}
                    {{--                    </button>--}}

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="new_hedge_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #42426a;color: white;">
                    <h5 class="modal-title" style=" color:white;">Add New Hedge</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                    <div class="modal-body" style="background-color:#35354B; color:white;">

                        <div class="form-group">
                            <label class="col-form-label" for="title"> Title</label>
                            <input type="text" class="form-control " name="title" id="title"
                                   placeholder="Enter Title" style="background-color: black ; color: white" autocomplete="off">
                            <span style="display: none" id="title_error"
                                  class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label" for="Email">Amount</label>
                            <input type="text" style="background-color: black ; color: white" class="form-control"
                                   name="amount" id="amount"
                                   placeholder="Enter Amount" autocomplete="off">
                            <span style="display: none" id="amount_error"
                                  class="text-danger"></span>

                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Slot</label>
                            <input type="text" class="form-control " name="slot" id="slot"
                                   placeholder="Enter Slot..." name="slot"
                                   style="background-color: black ; color: white" autocomplete="off">
                            <span style="display: none" id="slot_error"
                                  class="text-danger"></span>

                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Slot Openning Date</label>
                            <input type="text" class="form-control " id="slot_opening_date"
                                   placeholder="Enter Slot Openning Date" name="slot_opening_date"
                                   style="background-color: black ; color: white" autocomplete="off">
                            <span style="display: none" id="slot_opening_date_error"
                                  class="text-danger"></span>

                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Slot Closing Date</label>
                            <input type="text" class="form-control " id="slot_closing_date"
                                   placeholder="Enter Slot Closing Date" name="slot_closing_date"
                                   style="background-color: black ; color: white" autocomplete="off">
                            <span style="display: none" id="slot_closing_date_error"
                                  class="text-danger"></span>

                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Due Date</label>
                            <input type="text" class="form-control " id="due_date"
                                   placeholder="Enter Due Date of Hedge" name="due_date"
                                   style="background-color: black ; color: white" autocomplete="off">
                            <span style="display: none" id="due_date_error"
                                  class="text-danger"></span>

                        </div>
                    </div>
                    <div class="modal-footer" style="background-color: #42426a;color: white;">
                        <button type="button" id="add_hedge" class="btn btn-primary" style="background-color: #35354B;color: white;">
                            Submit
                        </button>
                        <div id="success_message" class="alert alert-success " style="display: none">
                            <p></p>
                        </div>

                    </div>


            </div>
        </div>
    </div>

    <!-- /.content -->
    {{--        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>--}}
    <!-- Select2 -->
    {{--    <script src="../../js/select2/js/select2.full.min.js"></script>--}}
    {{--    <script src="../../js/select2/js/select2.full.min.js"></script>--}}

    <script>
        $(document).ready(function () {
            var dateToday = new Date();
            // var dates = $("#slot_opening_date, #due_date").datepicker({
            //     defaultDate: "+1w",
            //     changeMonth: true,
            //     numberOfMonths: 1,
            //     minDate: dateToday,
            //     onSelect: function(selectedDate) {
            //         var option = this.id == "slot_opening_date" ? "minDate" : "maxDate",
            //             instance = $(this).data("datepicker"),
            //             date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
            //         dates.not(this).datepicker("option", option, date);
            //     }
            // });


            $('#slot_opening_date').datepicker({
                inline: true,
                minDate: dateToday,
                onSelect: function(selectedDate) {
                    var option = this.id == "slot_opening_date" ? "minDate" : "maxDate",
                        instance = $('#due_date').data("datepicker"),
                        date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                    $('#slot_closing_date').datepicker("option", 'minDate', date);
                }
            });
            $('#slot_closing_date').datepicker({
                inline: true,
                minDate:dateToday,
                // maxDate: $('#due_date').val()?$('#due_date').val(): '2050-02-10',
                onSelect: function(selectedDate) {
                    var option = this.id == "slot_opening_date" ? "minDate" : "maxDate",
                        instance = $('#due_date').data("datepicker"),
                        date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                    $('#slot_opening_date').datepicker("option", 'maxDate', date);
                    $('#due_date').datepicker("option", 'minDate', date);
                }
            });
            $('#due_date').datepicker({
                inline: true,
                minDate: dateToday,
                onSelect: function(selectedDate) {
                    var option = this.id == "slot_opening_date" ? "minDate" : "maxDate",
                        instance = $('#due_date').data("datepicker"),
                        date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                    $('#slot_closing_date').datepicker("option", 'maxDate', date);
                }
            });


            // $( "#due_date" ).datepicker();
            $('#add_hedge_btn').on('click', function (e) {
                e.preventDefault();
                $('#new_hedge_model').modal('show');


            });





            function topRight() {

                var values = [{
                    "positionClass": "toast-top-right",
                    timeOut: 5000,
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": true,
                    "preventDuplicates": true,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut",
                    "tapToDismiss": false
                }];
                return values[0];

            }


            // $('.select_role_drop').css('background-color: black !important; color: white ; width: 100%;');

            // $('.select_role_drop').css( style="background-color: black !important; color: white ; width: 100%;");
            $('#example1').DataTable({
                // "scrollX": true,
                responsive: true,
                sPaginationType: 'full_numbers',
                // "scrollCollapse": true,
            });

            $('#add_hedge').on('click', function (e) {
                e.preventDefault();
                var me  = $(this);
                if(me.data('requestRunning')){
                    return;

                }
                me.data('requestRunning',true);
                me.text('Request is Running');

                console.log('clicked ');
                var title = $('#title').val();
                var amount = $('#amount').val();
                var slot = $('#slot').val();
                var slot_opening_date = $('#slot_opening_date').val();
                var due_date = $('#due_date').val();
                var slot_closing_date = $('#slot_closing_date').val();
                $.ajax({
                    beforeSend: (function () {
                        $('#title_error').hide();
                        $('#slot_error').hide();
                        $('#amount_error').hide();
                        $('#slot_closing_date_error').hide();
                        $('#slot_opening_date_error').hide();
                        $('#due_date_error').hide();


                    }),

                    method: "post",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        title:title,
                        amount:amount,
                        slot:slot,
                        slot_opening_date:slot_opening_date,
                        slot_closing_date:slot_closing_date,
                        due_date:due_date,

                    },
                    url: "{{route('admin.add_hedge')}}",
                    success: function (data) {
                        toastr.success(data.message, "Hedge add", topRight());
                        // $('#success_message').html(data.message);
                        // $('#success_message').show();
                        $("#new_hedge_model").modal('hide');
                        window.setTimeout(function () {
                            window.location.reload()
                        }, 4600);
                    },
                    error: function (data) {
                        if (data.status === 422) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                // $('#category_id_error').show().append(value + "<br/>"); //this is my div with messages
                                if ($.isPlainObject(value)) {
                                    $.each(value, function (key, value) {

                                        if (key == 'title') {
                                            $('#title_error').html(value);
                                            $('#title_error').show();

                                        }
                                        if (key == 'slot') {
                                            $('#slot_error').html(value);
                                            $('#slot_error').show();
                                        }
                                        if (key == 'amount') {
                                            $('#amount_error').html(value);
                                            $('#amount_error').show();

                                        }
                                        if (key == 'slot_closing_date') {
                                            $('#slot_closing_date_error').html(value);
                                            $('#slot_closing_date_error').show();

                                        }
                                        if (key == 'slot_opening_date') {
                                            $('#slot_opening_date_error').html(value);
                                            $('#slot_opening_date_error').show();

                                        }
                                        if (key == 'due_date') {
                                            $('#due_date_error').html(value);
                                            $('#due_date_error').show();

                                        }
                                    });

                                }

                            });
                        }

                    },
                    complete:function () {
                        me.data('requestRunning', false);
                        me.text('Submit');

                    }

                });
            });

            $('#add_hedge_model').on('hidden.bs.modal', function () {
                // $('#permission').select2("val", ["1", "2"]);
                // $('#permission').select2("val", '[""]');
                // $('#nav-profile-tab').addClass("Active");

                // $('#permission').val('');
                // $('#selected_role').select2("val", '[""]');
                $('#hedge_id').val('');


            });
            $('#new_hedge_model').on('hidden.bs.modal', function () {
                // $('#permission').select2("val", ["1", "2"]);
                // $('#permission').select2("val", '[""]');
                // $('#nav-profile-tab').addClass("Active");

                // $('#permission').val('');
                // $('#selected_role').select2("val", '[""]');
                $('#title').val('');
                $('#amount').val('');
                $('#slot').val('');
                $('#slot_opening_date').val('');
                $('#slot_closing_date').val('');
                $('#due_date').val('');


            });


            $('#activities_table1').DataTable({
                // "scrollX": true,
                responsive: true,
                sPaginationType: 'full_numbers',
                // "scrollCollapse": true,
            });
            $('#example1').on('click', '.add_hedge', function (e) {
                var me = $(this);
                e.preventDefault();
                var id = $(this).data('hedgeid');

                if (me.data('requestRunning')) {
                    return;
                }

                me.data('requestRunning', true);
                // me.attr('disabled', true);
                //
                // me.text('RequestRunning');

                $('#hedge_id').val(id);
                console.log(id);
                $.ajax({
                    type: "POST",
                    url: "{{route('admin.view_hedge_info')}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        hedge_id: id,
                    },
                    success: function (response) {
                        if (response.error == null) {
                            var d = new Date(response.hedge.slot_opening_date);
                            var pening_date = d.getDate();

                            var pening_month = d.getMonth();

                            var pening_year = d.getFullYear();

                            pening_year = pening_year.toString().substr(2,2);
                            $('#slot_opening_date-m').val(pening_month+"-"+pening_date+"-"+pening_year);

                            var closing = new Date(response.hedge.slot_closing_date);
                            var closing_date = closing.getDate();

                            var closing_month = closing.getMonth();

                            var closing_year = closing.getFullYear();

                            closing_year = closing_year.toString().substr(2,2);
                            $('#slot_closing_date-m').val(closing_month+"-"+closing_date+"-"+closing_year);

                            var due_date = new Date(response.hedge.due_date);
                            var due_date_date = due_date.getDate();

                            var due_date_month = due_date.getMonth();

                            var due_date_year = due_date.getFullYear();

                            due_date_year = due_date_year.toString().substr(2,2);
                            // $('#due_date-m').datepicker('setDate', due_date_month+"-"+due_date_date+"-"+due_date_year);;
                            $('#due_date-m').val( due_date_month+"-"+due_date_date+"-"+due_date_year);
                            // document.write(curr_date+"-"+curr_month+"-"+curr_year);
                            // console.log(curr_date+"-"+curr_month+"-"+curr_year);
                            // console.log(curr_month+"-"+curr_date+"-"+curr_year);
                            // $('#slot_opening_date-m').val(d);

                            // $('#slot_opening_date-m').val(curr_month+"-"+curr_date+"-"+curr_year);
                            // $('#slot_opening_date-m').datepicker({
                            //     'setDate':curr_date+"/"+curr_month+"/"+curr_year,
                            //     dateFormat: 'dd/mm/yy'
                            // });

                            // var date = "2017-11-07";
                            // date = $

                            // $('#due_date-m').val( response.hedge.due_date );
// console.log(response.hedge.due_date);
                            $('#title-m').val(response.hedge.title);
                            $('#amount-m').val(response.hedge.amount);
                            $('#slot-m').val(response.hedge.slot);
                            // $('#slot_opening_date-m').val(response.hedge.slot_opening_date);
                            // $('#slot_closing_date-m').val(response.hedge.slot_closing_date);
                            // $('#due_date-m').val(response.hedge.due_date);
                            // $('#email-m').val(response.hedge.email);
                            $('#header_title').text(response.hedge.title);
                            $('#header_slots').text(response.hedge.slot);
                            $('#header_available').text(response.hedge.available_slot);
                            $('#header_code').text(response.hedge.code);
                            $('#header_Duedate').text(due_date_month+"-"+due_date_date+"-"+due_date_year);
                            // $('#header_create_at').text(response.hedge.created_at);

                            // // $('#password-field').val(response.hedge.password);
                            // $('#header_name').text(response.hedge.name);
                            // $('#header_email').text(response.hedge.email);
                            // $('#header_create_at').text(response.hedge.created_at);
                            //
                            // // $('.confirm').on('click', function () {
                            // //     // me.text('running');
                            // //     // window.location.reload();
                            // // });
                            // $('#log_name').find('option').not(':first').remove();
                            //
                            // if (response.activity_filters != null) {
                            //     $.each(response.activity_filters, function (key, value) {
                            //         $('#log_name').append(`<option value="${value.log_name}">
                            //            ${value.log_name}
                            //       </option>`);
                            //     });
                            //
                            //
                            // }
                            $("#add_hedge_model").modal('show');


                        } else {
                            if (response.error == 409) {

                                swal("Cancelled !!", response.message);

                            }
                            if (response.error == 421) {

                                swal("Cancelled !!", response.message);

                            }
                            if (response.error == 409) {

                                swal("Cancelled !!", response.message, "error");

                            } else {
                                // alert("Status can't be change.")
                            }
                        }
                    },
                    // error: function () {
                    //     alert('Error occured');
                    // },
                    complete: function () {
                        console.log('ok');
                        // me.text('completed');
                        // me.attr('disabled', true);

                        me.data('requestRunning', false);
                        // window.location.reload();

                    }
                });
                // $('#slot_opening_date-m').datepicker({
                //     inline: true,
                //     minDate: dateToday,
                //     onSelect: function(selectedDate) {
                //         var option = this.id == "slot_opening_date" ? "minDate" : "maxDate",
                //             instance = $('#due_date-m').data("datepicker"),
                //             date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                //         $('#slot_closing_date-m').datepicker("option", 'minDate', date);
                //     }
                // });
                // $('#slot_closing_date-m').datepicker({
                //     inline: true,
                //     minDate:dateToday,
                //     // maxDate: $('#due_date').val()?$('#due_date').val(): '2050-02-10',
                //     onSelect: function(selectedDate) {
                //         var option = this.id == "slot_opening_date" ? "minDate" : "maxDate",
                //             instance = $('#due_date-m').data("datepicker"),
                //             date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                //         $('#slot_opening_date-m').datepicker("option", 'maxDate', date);
                //         $('#due_date-m').datepicker("option", 'minDate', date);
                //     }
                // });
                // $('#due_date-m').datepicker({
                //     inline: true,
                //     minDate: dateToday,
                //     onSelect: function(selectedDate) {
                //         var option = this.id == "slot_opening_date" ? "minDate" : "maxDate",
                //             instance = $('#due_date-m').data("datepicker"),
                //             date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                //         $('#slot_closing_date-m').datepicker("option", 'maxDate', date);
                //     }
                // });


                $.ajax({
                    type: "get",
                    url: "{{route('admin.hedge_profit_history')}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        hedge_id: id,
                    },

                    success: function (data) {

                        // '<td>' + JSON.parse(JSON.stringify( value.properties.)) +'</td>'+
                        // '<td>' + JSON.parse(JSON.stringify( value.properties.toString())) +'</td>'+

                        console.log(data);
                        var res = '';
                        var pro = '';

                        $.each(data.hedge_profit_list, function (key, value) {
                            console.log(value.percentage);


                            res +=
                                '<tr>' +
                                '<td>' + value.percentage + '</td>' +
                                '<td>' + value.profit_time + '</td>' +



                                // '<td>' + JSON.stringify( value.properties.toString()) +'</td>'+

                                '</tr>';

                            pro = '';


                        });

                        $('#profit_tbody').html(res);
                    },

                    // error: function () {
                    //     alert('Error occured');
                    // },
                    complete: function () {
                        console.log('ok');
                        // me.text('completed');
                        // me.attr('disabled', true);

                        me.data('requestRunning', false);
                        // window.location.reload();

                    }
                });
                $.ajax({
                    type: "get",
                    url: "{{route('admin.hedge_booking_list')}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        hedge_id: id,
                    },

                    success: function (data) {

                        // '<td>' + JSON.parse(JSON.stringify( value.properties.)) +'</td>'+
                        // '<td>' + (JSON.parseJSON.stringify( value.properties.toString())) +'</td>'+


                      // console.log(myArr[0]);
                        var res = '';
                        var pro = '';
                        // console.log('booking',JSON.parse(data.hedgeBookingList));
                        // console.log(data);
                    //     var res = '';
                    //     var pro = '';
                    //
                    //     $.each(data.hedge_profit_list, function (key, value) {
                    //         console.log(value.percentage);
                    //
                    //
                    //         res +=
                    //             '<tr>' +
                    //             '<td>' + value.percentage + '</td>' +
                    //             '<td>' + value.profit_time + '</td>' +
                    //             '<td>' + value.created_at + '</td>' +
                    //
                    //
                    //             // '<td>' + JSON.stringify( value.properties.toString()) +'</td>'+
                    //             '<td>' + value.updated_at + '</td>' +
                    //             '</tr>';
                    //
                    //         pro = '';
                    //
                    //
                    //     });
                    //
                    //     $('#profit_tbody').html(res);
                    // },
                        $.each(data.hedgeBookingList, function (key, value) {



                            res +=
                                '<tr>' +
                                '<td>' + value.user_name + '</td>' +
                                '<td>' + value.email + '</td>' +
                                '<td>' + value.slot + '</td>' +
                                '<td>' + value.amount + '</td>' +
                                '<td>' + value.btc_rate + '</td>' +
                                '<td>' + value.type + '</td>' +
                                '<td>' + value.deposit_approved_time + '</td>' +



                                // '<td>' + JSON.stringify( value.properties.toString()) +'</td>'+

                                '</tr>';

                            pro = '';


                        });

                        $('#bookings_tbody').html(res);
                    },

                    // error: function () {
                    //     alert('Error occured');
                    // },
                    complete: function () {
                        console.log('ok');
                        // me.text('completed');
                        // me.attr('disabled', true);

                        me.data('requestRunning', false);
                        // window.location.reload();

                    }
                });

                // .val([1,2,3]).change();
                // $('#selected_role').val(u_hedge).trigger('change');
                // $('#permission').val(u_per).trigger('change');
                // $('#header_role').text(u_roles_name);

                $("#add_hedge_model").modal('show');
                //
                //
                // swal({
                //            title: "Are you sure you want to Approved ?",
                //            text: "",
                //            type: "warning",
                //            showCancelButton: true,
                //            confirmButtonColor: "#DD6B55",
                //            confirmButtonText: "Yes, Processed !!",
                //            cancelButtonText: "No, cancel it !!",
                //            closeOnConfirm: false,
                //            closeOnCancel: false
                //        },


                // function(isConfirm) {
                // if (isConfirm) {

                {{--                   var url: "{{route('admin.add_hedges')}}";--}}


                $("#add_hedge_model").on('change', '#log_name', function (e) {

                    console.log('hedge id', $('#hedge_id').val());
                    var hedge_id = $('#hedge_id').val();
                    console.log('logname', $('#log_name').val());
                    var filer_name = $('#log_name').val();
                    $.ajax({
                        type: "get",
                        {{--                    url: "{{route('admin.hedge_log_filer_byId')}}",--}}
                        data: {
                            "_token": "{{ csrf_token() }}",
                            hedge_id: hedge_id,
                            filer_name: filer_name,

                        },
                        success: function (data) {
                            // $("#add_hedge_model").modal('hide');
                            console.log(data);
                            var res = '';
                            var pro = '';

                            $.each(data.hedge_Log, function (key, value) {

                                $.each(value.properties, function (key, value) {

                                    pro += '<span class="badge badge-secondary">' + key + ' = ' + JSON.parse(JSON.stringify(value)) + '</span>&nbsp;,&nbsp;';

                                });
                                res +=
                                    '<tr>' +
                                    '<td>' + value.log_name + '</td>' +
                                    '<td>' + value.description + '</td>' +
                                    '<td>' + pro + '</td>' +

                                    // '<td>' + JSON.stringify( value.properties.toString()) +'</td>'+
                                    '<td>' + value.created_at + '</td>' +
                                    '</tr>';

                                pro = '';


                            });

                            $('#activites_tbody').html(res);


                        },

                    });


                });
                $("#add_hedge_model").on('click', '.submit_hedge_form', function (e) {
                    e.preventDefault();
                    var me = $(this);
                    var title = $('#title-m').val();
                    var amount = $('#amount-m').val();
                    var slot = $('#slot-m').val();
                    var slot_opening_date = $('#slot_opening_date-m').val();
                    var due_date = $('#due_date-m').val();
                    var slot_closing_date = $('#slot_closing_date-m').val();
                    var hedge_id = $('#hedge_id').val();

                    if (me.data('requestRunning')) {
                        return;
                    }

                    me.data('requestRunning', true);
                    // me.attr('disabled', true);
                    //

                    me.prop("value", "Request Running");

                    console.log('permission', $('#permission').val());
                    console.log('hedge id', $('#hedge_id').val());
                    console.log('hedge Role', $('#selected_role').val());
                    $.ajax({
                        beforeSend: (function () {
                            $('#title_error_m').hide();
                            $('#slot_error_m').hide();
                            $('#amount_error_m').hide();
                            $('#slot_closing_date_error_m').hide();
                            $('#slot_opening_date_error_m').hide();
                            $('#due_date_error_m').hide();


                        }),
                        type: "POST",
                        url: "{{route('admin.update_hedge_info')}}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            title: title,
                            hedge_id: hedge_id,
                            slot_opening_date: slot_opening_date,
                            slot_closing_date: slot_closing_date,
                            due_date: due_date,
                            slot: slot,
                            amount: amount,
                            action: "update_hedge",
                            // action: "removeRoles"
                        },
                        success: function (data) {
                            $("#add_hedge_model").modal('hide');

                            if (data.error == null) {
                                swal("Hey !!", "Hedge Info Successfully Updated", "success");
                                $('.confirm').on('click', function () {
                                    me.text('running');
                                    window.location.reload();
                                });

                            } else {
                                if (data.error == 409) {

                                    swal("Cancelled !!", data.message);

                                }

                            }
                        },
                        // error: function () {
                        //     alert('Error occured');
                        // },
                        error: function (data) {
                            if (data.status === 422) {
                                var errors = $.parseJSON(data.responseText);
                                $.each(errors, function (key, value) {
                                    // $('#category_id_error').show().append(value + "<br/>"); //this is my div with messages
                                    if ($.isPlainObject(value)) {
                                        $.each(value, function (key, value) {

                                            if (key == 'title') {
                                                $('#title_error_m').html(value);
                                                $('#title_error_m').show();

                                            }
                                            if (key == 'slot') {
                                                $('#slot_error_m').html(value);
                                                $('#slot_error_m').show();

                                            }
                                            if (key == 'amount') {
                                                $('#amount_error_m').html(value);
                                                $('#amount_error_m').show();
                                                slot_closing_date
                                            }
                                            if (key == 'slot_closing_date') {
                                                $('#slot_closing_date_error_m').html(value);
                                                $('#slot_closing_date_error_m').show();

                                            }
                                            if (key == 'slot_opening_date') {
                                                $('#slot_opening_date_error_m').html(value);
                                                $('#slot_opening_date_error_m').show();

                                            }
                                            if (key == 'due_date') {
                                                $('#due_date_error_m').html(value);
                                                $('#due_date_error_m').show();

                                            }
                                        });

                                    }

                                });
                            }

                        }
                        ,

                        complete: function () {
                            console.log('ok');
                            me.prop('value','Update Info');
                            // me.attr('disabled', true);


                            me.data('requestRunning', false);
                            // window.location.reload();

                        }
                    },);

                });
            });


            $("#profit_submit").on('click', function (e) {
                e.preventDefault();
                var me = $(this);
                // var hedge_id = $(this).data('hedgeid');

                var hedge_id = $('#hedge_id').val();
                var hedge_profit_perc = $('#hedge_profit_perc').val();

                // console.log('permission', $('#permission').val());
                console.log('hedge id', hedge_id);
                // console.log('hedge Role', $('#selected_role').val());
                swal({
                        title: "Are you sure you want to Set the Profit for this Hedge  ?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Processed !!",
                        cancelButtonText: "No, cancel it !!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },


                    function (isConfirm) {
                        if (isConfirm) {
                            // console.log(id);

                            var me = $('.confirm');
                            if (me.data('requestruning')) {
                                return;
                            }
                            me.data('requestruning', true)
                            me.text('Request is Runing');
                            $.ajax({
                                type: "POST",
                                url: "{{route('admin.profitstore')}}",
                                data: {
                                    "_token": "{{ csrf_token() }}",
                                    // permission: permission,
                                    hedge_id: hedge_id,
                                    hedge_profit_perc: hedge_profit_perc,
                                    // selected_role: selected_role,
                                    // action: "add_hedges"
                                    action: "set_profit"
                                },
                                success: function (response) {
                                    // $("#add_hedge_model").modal('hide');

                                    if (response.success) {
                                        swal("Hey !!", "Profit Set Successfully ", "success");
                                        $('.confirm').on('click', function () {
                                            me.text('running');
                                            window.location.reload();
                                        });

                                    } else {
                                        if (response.error == 409) {

                                            swal("Cancelled !!", response.message);

                                        }
                                        if (response.error == 421) {

                                            swal("Cancelled !!", response.message);

                                        }
                                        if (response.error == 417) {

                                            swal("Cancelled !!", response.message);

                                        }
                                        if (response.error == 409) {

                                            swal("Cancelled !!", response.message, "error");

                                        } else {
                                            // alert("Status can't be change.")
                                        }
                                    }
                                },
                                {{--                // error: function () {--}}
                                    {{--                //     alert('Error occured');--}}
                                    {{--                // },--}}
                                    {{--                complete: function () {--}}
                                    {{--                    console.log('ok');--}}
                                    {{--                    me.text('completed');--}}
                                    {{--                    me.attr('disabled', true);--}}

                                    {{--                    me.data('requestRunning', false);--}}
                                    {{--                    // window.location.reload();--}}

                                    {{--                }--}}
                                    {{--            });--}}
                                    {{--        }else {--}}
                                    {{--            swal("Cancelled !!", "Hey, Cancelled the Transaction !!", "error");--}}
                                    {{--        }--}}


                                complete: function () {
                                me.data('requestruning',false);
                                me.text('Completed');

                                }

                            });


                        } else {
                            swal("Cancelled !!", "Hey, Cancelled the Request !!", "error");
                        }
                    });
                {{--swal({--}}
                {{--        title: "Are you sure you want to Remove ?",--}}
                {{--        text: "",--}}
                {{--        type: "warning",--}}
                {{--        showCancelButton: true,--}}
                {{--        confirmButtonColor: "#DD6B55",--}}
                {{--        confirmButtonText: "Yes, Processed !!",--}}
                {{--        cancelButtonText: "No, cancel it !!",--}}
                {{--        closeOnConfirm: false,--}}
                {{--        closeOnCancel: false--}}
                {{--    },--}}
                {{--if (isConfirm) {--}}
                {{--    if (isConfirm){--}}
                {{--                $.ajax({--}}
                {{--                    type: "POST",--}}
                {{--                    url: "{{route('admin.add_hedges_permissions_ajax')}}",--}}
                {{--                    data: {--}}
                {{--                        "_token": "{{ csrf_token() }}",--}}
                {{--                        permission: permission,--}}
                {{--                        hedge_id: hedge_id,--}}
                {{--                        selected_role: selected_role,--}}
                {{--                        // action: "add_hedges"--}}
                {{--                        action: "removeRoles"--}}
                {{--                    },--}}
                {{--                    success: function (response) {--}}
                {{--                        $("#add_hedge_model").modal('hide');--}}

                {{--                        if (response.success) {--}}
                {{--                            // swal("Hey !!", "Deposit Approved Successfully ", "success");--}}
                {{--                            // $('.confirm').on('click', function () {--}}
                {{--                            //     me.text('running');--}}
                {{--                            //     // window.location.reload();--}}
                {{--                            // });--}}

                {{--                        } else {--}}
                {{--                            if (response.error == 409) {--}}

                {{--                                swal("Cancelled !!", response.message);--}}

                {{--                            }--}}
                {{--                            if (response.error == 421) {--}}

                {{--                                swal("Cancelled !!", response.message);--}}

                {{--                            }--}}
                {{--                            if (response.error == 409) {--}}

                {{--                                swal("Cancelled !!", response.message, "error");--}}

                {{--                            } else {--}}
                {{--                                // alert("Status can't be change.")--}}
                {{--                            }--}}
                {{--                        }--}}
                {{--                    },--}}
                {{--    --}}{{--                // error: function () {--}}
                {{--    --}}{{--                //     alert('Error occured');--}}
                {{--    --}}{{--                // },--}}
                {{--    --}}{{--                complete: function () {--}}
                {{--    --}}{{--                    console.log('ok');--}}
                {{--    --}}{{--                    me.text('completed');--}}
                {{--    --}}{{--                    me.attr('disabled', true);--}}

                {{--    --}}{{--                    me.data('requestRunning', false);--}}
                {{--    --}}{{--                    // window.location.reload();--}}

                {{--    --}}{{--                }--}}
                {{--    --}}{{--            });--}}
                {{--    --}}{{--        }else {--}}
                {{--    --}}{{--            swal("Cancelled !!", "Hey, Cancelled the Transaction !!", "error");--}}
                {{--    --}}{{--        }--}}




            });
            $("#example1").on('click', '.remove_role', function (e) {
                e.preventDefault();
                var me = $(this);
                var hedge_id = $(this).data('uid');
                // var selected_role = $('#selected_role').val();

                // console.log('permission', $('#permission').val());
                console.log('hedge id', hedge_id);
                // console.log('hedge Role', $('#selected_role').val());
                swal({
                        title: "Are you sure you want to Remove Role ?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Processed !!",
                        cancelButtonText: "No, cancel it !!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },


                    function (isConfirm) {
                        if (isConfirm) {
                            // console.log(id);
                            $.ajax({
                                type: "POST",
                                {{--                                url: "{{route('admin.add_hedges_permissions_ajax')}}",--}}
                                data: {
                                    "_token": "{{ csrf_token() }}",
                                    // permission: permission,
                                    hedge_id: hedge_id,
                                    // selected_role: selected_role,
                                    // action: "add_hedges"
                                    action: "removeRoles"
                                },
                                success: function (response) {
                                    // $("#add_hedge_model").modal('hide');

                                    if (response.success) {
                                        swal("Hey !!", "Role Remove Successfully ", "success");
                                        $('.confirm').on('click', function () {
                                            me.text('running');
                                            window.location.reload();
                                        });

                                    } else {
                                        if (response.error == 409) {

                                            swal("Cancelled !!", response.message);

                                        }
                                        if (response.error == 421) {

                                            swal("Cancelled !!", response.message);

                                        }
                                        if (response.error == 417) {

                                            swal("Cancelled !!", response.message);

                                        }
                                        if (response.error == 409) {

                                            swal("Cancelled !!", response.message, "error");

                                        } else {
                                            // alert("Status can't be change.")
                                        }
                                    }
                                },
                                {{--                // error: function () {--}}
                                {{--                //     alert('Error occured');--}}
                                {{--                // },--}}
                                {{--                complete: function () {--}}
                                {{--                    console.log('ok');--}}
                                {{--                    me.text('completed');--}}
                                {{--                    me.attr('disabled', true);--}}

                                {{--                    me.data('requestRunning', false);--}}
                                {{--                    // window.location.reload();--}}

                                {{--                }--}}
                                {{--            });--}}
                                {{--        }else {--}}
                                {{--            swal("Cancelled !!", "Hey, Cancelled the Transaction !!", "error");--}}
                                {{--        }--}}




                            });


                        } else {
                            swal("Cancelled !!", "Hey, Cancelled the Request !!", "error");
                        }
                    });
                {{--swal({--}}
                {{--        title: "Are you sure you want to Remove ?",--}}
                {{--        text: "",--}}
                {{--        type: "warning",--}}
                {{--        showCancelButton: true,--}}
                {{--        confirmButtonColor: "#DD6B55",--}}
                {{--        confirmButtonText: "Yes, Processed !!",--}}
                {{--        cancelButtonText: "No, cancel it !!",--}}
                {{--        closeOnConfirm: false,--}}
                {{--        closeOnCancel: false--}}
                {{--    },--}}
                {{--if (isConfirm) {--}}
                {{--    if (isConfirm){--}}
                {{--                $.ajax({--}}
                {{--                    type: "POST",--}}
                {{--                    url: "{{route('admin.add_hedges_permissions_ajax')}}",--}}
                {{--                    data: {--}}
                {{--                        "_token": "{{ csrf_token() }}",--}}
                {{--                        permission: permission,--}}
                {{--                        hedge_id: hedge_id,--}}
                {{--                        selected_role: selected_role,--}}
                {{--                        // action: "add_hedges"--}}
                {{--                        action: "removeRoles"--}}
                {{--                    },--}}
                {{--                    success: function (response) {--}}
                {{--                        $("#add_hedge_model").modal('hide');--}}

                {{--                        if (response.success) {--}}
                {{--                            // swal("Hey !!", "Deposit Approved Successfully ", "success");--}}
                {{--                            // $('.confirm').on('click', function () {--}}
                {{--                            //     me.text('running');--}}
                {{--                            //     // window.location.reload();--}}
                {{--                            // });--}}

                {{--                        } else {--}}
                {{--                            if (response.error == 409) {--}}

                {{--                                swal("Cancelled !!", response.message);--}}

                {{--                            }--}}
                {{--                            if (response.error == 421) {--}}

                {{--                                swal("Cancelled !!", response.message);--}}

                {{--                            }--}}
                {{--                            if (response.error == 409) {--}}

                {{--                                swal("Cancelled !!", response.message, "error");--}}

                {{--                            } else {--}}
                {{--                                // alert("Status can't be change.")--}}
                {{--                            }--}}
                {{--                        }--}}
                {{--                    },--}}
                {{--    --}}{{--                // error: function () {--}}
                {{--    --}}{{--                //     alert('Error occured');--}}
                {{--    --}}{{--                // },--}}
                {{--    --}}{{--                complete: function () {--}}
                {{--    --}}{{--                    console.log('ok');--}}
                {{--    --}}{{--                    me.text('completed');--}}
                {{--    --}}{{--                    me.attr('disabled', true);--}}

                {{--    --}}{{--                    me.data('requestRunning', false);--}}
                {{--    --}}{{--                    // window.location.reload();--}}

                {{--    --}}{{--                }--}}
                {{--    --}}{{--            });--}}
                {{--    --}}{{--        }else {--}}
                {{--    --}}{{--            swal("Cancelled !!", "Hey, Cancelled the Transaction !!", "error");--}}
                {{--    --}}{{--        }--}}




            });
            $("#example1").on('click', '.removePermission', function (e) {
                e.preventDefault();
                var me = $(this);
                var hedge_id = $(this).data('uid');
                // var selected_role = $('#selected_role').val();

                // console.log('permission', $('#permission').val());
                console.log('hedge id', hedge_id);
                // console.log('hedge Role', $('#selected_role').val());
                swal({
                        title: "Are you sure you want to Remove Extra Permissions ?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Processed !!",
                        cancelButtonText: "No, cancel it !!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },


                    function (isConfirm) {
                        if (isConfirm) {
                            // console.log(id);
                            $.ajax({
                                type: "POST",
                                {{--                                url: "{{route('admin.add_hedges_permissions_ajax')}}",--}}
                                data: {
                                    "_token": "{{ csrf_token() }}",
                                    // permission: permission,
                                    hedge_id: hedge_id,
                                    // selected_role: selected_role,
                                    // action: "add_hedges"
                                    action: "removePermission"
                                },
                                success: function (response) {
                                    // $("#add_hedge_model").modal('hide');

                                    if (response.success) {
                                        swal("Hey !!", "Extra Permissions Remove Successfully ", "success");
                                        $('.confirm').on('click', function () {
                                            me.text('running');
                                            window.location.reload();
                                        });

                                    } else {
                                        if (response.error == 409) {

                                            swal("Cancelled !!", response.message);

                                        }
                                        if (response.error == 421) {

                                            swal("Cancelled !!", response.message);

                                        }
                                        if (response.error == 417) {

                                            swal("Cancelled !!", response.message);

                                        }
                                        if (response.error == 409) {

                                            swal("Cancelled !!", response.message, "error");

                                        } else {
                                            // alert("Status can't be change.")
                                        }
                                    }
                                },
                                {{--                // error: function () {--}}
                                {{--                //     alert('Error occured');--}}
                                {{--                // },--}}
                                {{--                complete: function () {--}}
                                {{--                    console.log('ok');--}}
                                {{--                    me.text('completed');--}}
                                {{--                    me.attr('disabled', true);--}}

                                {{--                    me.data('requestRunning', false);--}}
                                {{--                    // window.location.reload();--}}

                                {{--                }--}}
                                {{--            });--}}
                                {{--        }else {--}}
                                {{--            swal("Cancelled !!", "Hey, Cancelled the Transaction !!", "error");--}}
                                {{--        }--}}




                            });


                        } else {
                            swal("Cancelled !!", "Hey, Cancelled the Request !!", "error");
                        }
                    });
                {{--swal({--}}
                {{--        title: "Are you sure you want to Remove ?",--}}
                {{--        text: "",--}}
                {{--        type: "warning",--}}
                {{--        showCancelButton: true,--}}
                {{--        confirmButtonColor: "#DD6B55",--}}
                {{--        confirmButtonText: "Yes, Processed !!",--}}
                {{--        cancelButtonText: "No, cancel it !!",--}}
                {{--        closeOnConfirm: false,--}}
                {{--        closeOnCancel: false--}}
                {{--    },--}}
                {{--if (isConfirm) {--}}
                {{--    if (isConfirm){--}}
                {{--                $.ajax({--}}
                {{--                    type: "POST",--}}
                {{--                    url: "{{route('admin.add_hedges_permissions_ajax')}}",--}}
                {{--                    data: {--}}
                {{--                        "_token": "{{ csrf_token() }}",--}}
                {{--                        permission: permission,--}}
                {{--                        hedge_id: hedge_id,--}}
                {{--                        selected_role: selected_role,--}}
                {{--                        // action: "add_hedges"--}}
                {{--                        action: "removeRoles"--}}
                {{--                    },--}}
                {{--                    success: function (response) {--}}
                {{--                        $("#add_hedge_model").modal('hide');--}}

                {{--                        if (response.success) {--}}
                {{--                            // swal("Hey !!", "Deposit Approved Successfully ", "success");--}}
                {{--                            // $('.confirm').on('click', function () {--}}
                {{--                            //     me.text('running');--}}
                {{--                            //     // window.location.reload();--}}
                {{--                            // });--}}

                {{--                        } else {--}}
                {{--                            if (response.error == 409) {--}}

                {{--                                swal("Cancelled !!", response.message);--}}

                {{--                            }--}}
                {{--                            if (response.error == 421) {--}}

                {{--                                swal("Cancelled !!", response.message);--}}

                {{--                            }--}}
                {{--                            if (response.error == 409) {--}}

                {{--                                swal("Cancelled !!", response.message, "error");--}}

                {{--                            } else {--}}
                {{--                                // alert("Status can't be change.")--}}
                {{--                            }--}}
                {{--                        }--}}
                {{--                    },--}}
                {{--    --}}{{--                // error: function () {--}}
                {{--    --}}{{--                //     alert('Error occured');--}}
                {{--    --}}{{--                // },--}}
                {{--    --}}{{--                complete: function () {--}}
                {{--    --}}{{--                    console.log('ok');--}}
                {{--    --}}{{--                    me.text('completed');--}}
                {{--    --}}{{--                    me.attr('disabled', true);--}}

                {{--    --}}{{--                    me.data('requestRunning', false);--}}
                {{--    --}}{{--                    // window.location.reload();--}}

                {{--    --}}{{--                }--}}
                {{--    --}}{{--            });--}}
                {{--    --}}{{--        }else {--}}
                {{--    --}}{{--            swal("Cancelled !!", "Hey, Cancelled the Transaction !!", "error");--}}
                {{--    --}}{{--        }--}}




            });


        });


    </script>
@endsection


