@extends('layouts.app')

@section('dashboardcontent')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profit Pending</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Profit Pending</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<section class="content">
      <div class="row">
        <div class="col-12">

      <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Profit Information</h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <table  class="table table-bordered table-striped example1">
                      <thead>
                      <tr>
                        <th>Currency</th>
                        <th>Amount</th>
                        
                        <th>Time</th>
                         <th>Profit Transaction Id</th>
                        
                        <th>Withdraw Status</th>
                        <th>Total Amount</th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($users as $user)
                             <tr>
                              <td>
                      {{$user->coin_type}}
                    </td>
                              <td>
                      {{$user->profit}}
                    </td>

                    <td>
                      {{$user->created_at}}
                    </td>
                    <td>
                      {{$user->profit_trans_id}}
                    </td>
                    
                    <td>
                      @if($user->withdraw_profit==1)
                        <button  class="btn btn-success"><b> Processeed </b></button>
                        @elseif($user->withdraw_profit==2)
                        <button  class="btn btn-warning pending" data-id="{{$user->id}}"><b> pending </b></button>
                        
                        @else
                       <button  class="btn btn-danger " ><b> No Applied </b></button>
                       @endif
                    </td>
                    <td>
                     total amount
                    </td>
                             </tr>
                      @endforeach
                      </tbody>
                      <tfoot>
                      <tr>
                        <th>Currency</th>
                        <th>Amount</th>
                        <th>Time</th>
                         <th>Profit Transaction Id</th>
                        <th>Withdraw Status</th>
                        <th>Total Amount</th>
                      </tr>
                      </tfoot>
                    </table>
                  </div>
                  <!-- /.card-body -->
      </div>
     </div>
    </div>
</section>  


<script>
  $(function () {
    $(".example1").DataTable();
    $('.example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
  $(document).ready(function() {
$('.example1').on('click', '.pending', function () {
  var id = $(this).data('id');
  



                 swal({
                            title: "Are you sure you want to Approved ?",
                            text: "",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes, Processed !!",
                            cancelButtonText: "No, cancel it !!",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },

  
                        function(isConfirm){
                            if (isConfirm) {
                              console.log(id);
                                var url = '<?= route('admin.profit.ajax')?>';
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {"_token": "{{ csrf_token() }}", id: id},
                    success: function (response) {
                        if (response.success) {
                            swal("Hey !!", "You Gave Withdraw amount Successfully " , "success");
                            $('.confirm').on('click', function () {
                                window.location.reload();
                            });
                            
                        } else {
                            if (response.error == 409) {
                               
                                swal("Cancelled !!", "Hey, your Approval is denied !!", "error");

                            }
                            if (response.error == 401) {
                               
                                swal("Cancelled !!", "This Amount is alreayd Withdraw", "error");

                            }
                             else {
                                alert("Status can't be change.")
                            }
                        }
                    },
                    error: function () {
                        alert('Error occured');
                    }
                });

                                
                            }
                            else {
                                swal("Cancelled !!", "Hey, Cancelled the Transaction !!", "error");
                            }
                        });


});
$('.example1').on('click', '.processed', function () {
  var id = $(this).data('id');
var id = $(this).data('id');
  



                 swal({
                            title: "Are you sure you want to Approved ?",
                            text: "",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes, Processed !!",
                            cancelButtonText: "No, cancel it !!",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },

  
                        function(isConfirm){
                            if (isConfirm) {
                              console.log(id);
                                var url = '<?= route('admin.pending.ajax')?>';
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {"_token": "{{ csrf_token() }}", id: id,deposite: "doPending"},
                    success: function (response) {
                        if (response.success) {
                            swal("Hey !!", "You Gave Withdraw amount Successfully " , "success");
                             $('.confirm').on('click', function () {
                                window.location.reload();
                            });
                        } else {
                            if (response.error == 409) {
                               
                                swal("Cancelled !!", "Hey, your Approval is denied !!", "error");

                            }
                            if (response.error == 401) {
                               
                                swal("Cancelled !!", "This Amount is alreayd Withdraw", "error");

                            }
                             else {
                                alert("Status can't be change.")
                            }
                        }
                    },
                    error: function () {
                        alert('Error occured');
                    }
                });

                                
                            }
                            else {
                                swal("Cancelled !!", "Hey, Cancelled the Transaction !!", "error");
                            }
                        });





});

  });
</script> 
@endsection