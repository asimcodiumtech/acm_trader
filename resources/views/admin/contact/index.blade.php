@extends('layouts.app')

@section('dashboardcontent')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 style="color: white;">SOS List</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">SOS List</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<section class="content">
      <div class="row">
        <div class="col-12">

      <div class="card">
                  <div class="card-header" style="background-color: #35354B;color: white;">
                    <h3 class="card-title">All SOS</h3>
                  </div>


                  <!-- /.card-header -->
                  <div class="card-body" style="background-color: #35354B;color: white;">
                    <div class="table-responsive">
                    <table  class="table table-bordered table-striped example1">
                      <thead style="background-color:#131311; color:white;">
                      <tr>
                        <th>Ticket NO</th>
                        <th>Username</th>
                        <th>Subject</th>
                        <th>Created At</th>
                        <th>Action</th>
                      </tr>
                      </thead>
                      <tbody style="background-color: #35354B;color: white;">
                        @foreach($users as $user)
                            <tr>
                                <td>
                                    {{$user->id}}
                                </td>
                                <td>
                                    {{$user->name($user->user_id)}}
                                </td>
                                <td>
                                    <a  class="" href="{{route('admin.sos.detail',$user->id)}}">{{$user->subject}} </a>
                                </td>
                                <td>
                                    {{$user->created_at}}
                                </td>
                                <td>
                                    @if($user->status==1)
                                      <button class="btn btn-success code" data-id="{{$user->id}}">Resolved</button>
                                      @elseif($user->status==2)
                                      <button class="btn btn-warning code" data-id="{{$user->id}}">In process</button>
                                      @else
                                      <button class="btn btn-danger code"data-id="{{$user->id}}">Unresolved</button>
                                      @endif
                                </td>
                            </tr>
                        @endforeach
                      </tbody>
                      <tfoot style="background-color:#131311; color:white;">
                      <tr>
                        <th>Ticket NO#</th>
                        <th>Username</th>
                        <th>Subject</th>
                        <th>Created At</th>
                          <th>Action</th>
                      </tr>
                      </tfoot>
                    </table>
                  </div>
                  </div>
                  <!-- /.card-body -->
      </div>
     </div>
    </div>
</section>


<script>

</script>

<script type="text/javascript">






  $(document).ready(function() {



      $(".example1").DataTable({
          responsive: true
      });


  $('.code').on('click', function () {





var id = $(this).data('id');





     html='<div class="FixedHeightContainer" style="">';
    html+='<div class="Content" style="height:30%;overflow:auto;">';

    html+='<select  name="coin_type" class="form-control coin_type" tabindex="3" required style="background-color:#131311;color:white;"><option value="">Please Select</option><option value="1">Resolved</option><option value="2">Inprocess</option></select>';

   html+=' </div>';
   html+=' </div>';
    var url = '<?= route('user.setting.store')?>';
    data={"_token": "{{ csrf_token() }}",ecode:'send'};
                  $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (response) {

                    },
                    error: function () {

                    }
                });

    swal({
            title: "Change Status",
            text: "Note : You can change it back Later",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Submit",
            animation: "slide-from-top",
            inputPlaceholder: "Write something",
            closeOnCancel: false
        });

    $('.showSweetAlert').css("margin-top","-350px");
    $('.showSweetAlert fieldset').html(html);
    $('.confirm').attr("disabled", true);

     $('.coin_type').on('change', function() {

               var vcoin_type = $("select.coin_type").children("option:selected").val();
              if(vcoin_type=="1" || vcoin_type=="2")
              {
                $('.confirm').removeAttr("disabled");

              }

              if(vcoin_type=='')
              {

                $('.confirm').attr("disabled", true);

              }
        });


    $('.confirm').click(function(){
    var vcoin_type = $("select.coin_type").children("option:selected").val();
    var url = '<?= route('admin.sos.status')?>';
    data={"_token": "{{ csrf_token() }}",status:vcoin_type,id:id};
                  $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (response) {
                      if (response.success=='201') {

                              $('body').find('.sweet-overlay').remove();
                                   $('body').find('.hideSweetAlert').remove();
                               swal("Status Change Successfully", "Successfully !!", "success");
                                $('.confirm').on('click', function () {
                                window.location.reload();
                            });


                        }


                    },
                    error: function () {

                    }
                });



    });

  $('.cancel').click(function()
    {
        $('body').find('.sweet-overlay').remove();
       // $(this).parent().parent().closest(".sweet-overlay").remove();
         $(this).parent().parent().remove();

    });

  });
});



</script>
@endsection
