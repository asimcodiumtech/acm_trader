@extends('layouts.app')

@section('dashboardcontent')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>DataTables</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Approved Request</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<section class="content">
      <div class="row">
        <div class="col-12">

			<div class="card">
			            <div class="card-header">
			              <h3 class="card-title">Approvd Request</h3>
			            </div>
			            <!-- /.card-header -->
			            <div class="card-body">
			              <table  class="table table-bordered table-striped example1">
			                <thead>
			                <tr>
			                  <th>Amount</th>
			                  <th>Coin Type</th>
			                  <th>Transaction Id</th>
			                  <th>Profit</th>
			                  <th>Deposite Status</th>
			                  <th>Withdraw Status</th>
			                </tr>
			                </thead>
			                <tbody>
			                @foreach($users as $user)
                             <tr>
                             	<td>
                             		@if($user->coin_type == "USD")
						          {{$user->amount}}$
						          @else
						          {{$user->amount}}BTC
						          @endif
						        </td>
						        <td>
						          {{$user->coin_type}}
						        </td>
                                <td>
						          {{$user->trans_id}}
						        
						        <td>
						        	<button  class="btn btn-info profit" data-id="{{$user->id}}"><b> {{$user->profit}}</b></button>
						          
						        </td>
						        <td>
						          @if($user->deposite_status==1)
						            <button  class="btn btn-success"><b> Approved </b></button>
						            @elseif($user->deposite_status==2)
						            <button  class="btn btn-warning"><b> pending </b></button>
						            
						            @else
						           <button  class="btn btn-danger"><b> Not Approved </b></button>
						           @endif
						        </td>
						        <td>
						          @if($user->withdraw_status==1)
						            <button  class="btn btn-success"><b> Approved </b></button>
						            @elseif($user->withdraw_status==2)
						            <button  class="btn btn-warning"><b> pending </b></button>
						            
						            @else
						           <button  class="btn btn-danger"><b> No Applied </b></button>
						           @endif
						        </td>
                             </tr>
			                @endforeach
			                </tbody>
			                <tfoot>
			                <tr>
			                  <th>Amount</th>
			                  <th>Coin Type</th>
			                  <th>Transaction Id</th>
			                  <th>Profit</th>
			                  <th>Deposite Status</th>
			                  <th>Withdraw Status</th>
			                </tr>
			                </tfoot>
			              </table>
			            </div>
			            <!-- /.card-body -->
			</div>
	   </div>
	  </div>
</section>	


<script>
  $(function () {
    $(".example1").DataTable();
    $('.example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
    $(document).ready(function() {
    	var val;
    	var id;



    	 
$('.example1').on('click', '.profit', function () {
	 id = $(this).data('id');
	 html='<input type="number" tabindex="3" placeholder="enter profit amount" name="profit" class="first">';
	               swal({
            title: "Enter Amount to gave",
            text: "Please complete all field !!",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            animation: "slide-from-top",
            inputPlaceholder: "Write something",
            closeOnCancel: false
        });
$('.showSweetAlert fieldset').html(html);


	               $('.confirm').click(function(){
	                var profit =	$('.first').val();

	                console.log(profit);
                      var url = '<?= route('admin.profit.approved')?>';
	                $.ajax({
                    type: "POST",
                    url: url,
                    data: {"_token": "{{ csrf_token() }}", id: id,profit: profit},
                    success: function (response) {
                        console.log(response);
                        if (response.success) {
                             $('body').find('.sweet-overlay').remove();
                                   $('body').find('.hideSweetAlert').remove();
                               swal("Request Submit to Admin", "Successfully !!", "success");
                                $('.confirm').on('click', function () {
                                window.location.reload();
                            });
                               
                        } else {
                            if (response.error == 409) {
                                
                                alert("User already has an active subscription.");
                            } else {
                                $(this).parent().parent().parent().remove();
                                   
                                   $('body').find('.sweet-overlay').remove();
                                   $('body').find('.hideSweetAlert').remove();

                               swal("Request Rejected", "You can't fill all fields !!", "error");
                             }
                        }
                    },
                    error: function () {
                        alert('Error occured');
                    }
                });

	});
	               $('.cancel').click(function(){
            $('body').find('.sweet-overlay').remove();
           // $(this).parent().parent().closest(".sweet-overlay").remove();
             $(this).parent().parent().remove();

             
          });


                 


});

});
</script>	
@endsection