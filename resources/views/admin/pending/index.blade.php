@extends('layouts.app')

@section('dashboardcontent')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 style="color: white;">Deposit Request</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Deposit Request</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="row">
            <div class="col-12">

                <div class="card">
                    <div class="card-header" style="background-color: #35354B;color: white;">
                        <h3 class="card-title"></h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body" style="background-color: #35354B;color: white;">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped example1">
                                <thead style="background-color:#131311; color:white;">
                                <tr>
                                    <th>Amount Deposit</th>
                                    <th>Admin fee</th>
                                    <th>Coin Type</th>
                                    <th>Reference</th>
                                    <th>BTC Address</th>
                                    <th>Account Name</th>
                                    <th>Branch Code</th>
                                    <th>Bank Type</th>
                                    <th>Swift Code</th>
                                    <th>Time</th>
                                    <th>Deposit Status</th>

                                </tr>
                                </thead>
                                <tbody style="background-color: #35354B;color: white;">
                                @foreach($users as $user)
                                    <tr>
                                        <td>
                                            {{$user->amount}}
                                        </td>
                                        <td>
                                            {{($user->amount * 0.1)}}
                                        </td>
                                        <td>
                                            {{$user->coin_type}}
                                        </td>
                                        <td>
                                            {{$user->refNo($user->u_id)}}
                                        </td>
                                        <td>
                                            {{$user->trans_id}}
                                        </td>
                                        </td>
                                        <td>
                                            {{$user->acc_name}}
                                        </td>
                                        </td>
                                        <td>
                                            {{$user->branch_code}}
                                        </td>
                                        </td>
                                        <td>
                                            {{$user->bank_type}}
                                        </td>
                                        </td>
                                        <td>
                                            {{$user->swift_code}}
                                        </td>
                                        <td>
                                            {{$user->created_at}}
                                        </td>

                                        <td>
                                            @if($user->deposite_status==1)
                                                <button class="btn btn-success " data-id="{{$user->id}}">processed
                                                </button>
                                            @elseif($user->deposite_status==2)
                                                <button class="btn btn-danger pending" data-id="{{$user->id}}">pending
                                                </button>

                                            @else
                                                <button class="btn btn-danger"><b> Not Approved </b></button>
                                            @endif
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot style="background-color:#131311; color:white;">
                                <tr>
                                    <th>Amount Deposit</th>
                                    <th>Admin fee</th>
                                    <th>Coin Type</th>
                                    <th>Reference</th>
                                    <th>BTC Address</th>
                                    <th>Account Name</th>
                                    <th>Branch Code</th>
                                    <th>Bank Type</th>
                                    <th>Swift Code</th>
                                    <th>Time</th>
                                    <th>Deposit Status</th>

                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
    </section>


    <script>
        $(function () {
            $(".example1").DataTable();
            $('.example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
        $(document).ready(function () {
            $('.example1').on('click', '.pending', function (e) {
                var me = $(this);
                e.preventDefault();

                var id = $(this).data('id');


                console.log(id);
                //
                //
                // swal({
                //            title: "Are you sure you want to Approved ?",
                //            text: "",
                //            type: "warning",
                //            showCancelButton: true,
                //            confirmButtonColor: "#DD6B55",
                //            confirmButtonText: "Yes, Processed !!",
                //            cancelButtonText: "No, cancel it !!",
                //            closeOnConfirm: false,
                //            closeOnCancel: false
                //        },


                // function(isConfirm) {
                // if (isConfirm) {
                var url = '<?= route('admin.pending.ajax')?>';
                if (me.data('requestRunning')) {
                    return;
                }

                me.data('requestRunning', true);
                me.attr('disabled', true);

                me.text('requestRunning');


            console.log('comp1');


            $.ajax({
                type: "POST",
                url: url,
                data: {"_token": "{{ csrf_token() }}", id: id, deposite: "doApproved"},
                success: function (response) {
                    if (response.success) {
                        // swal("Hey !!", "Deposit Approved Successfully ", "success");
                        $('.confirm').on('click', function () {
                            me.text('running');
                            // window.location.reload();
                        });

                    } else {
                        if (response.error == 409) {

                            swal("Cancelled !!", "Hey, your Approval is denied !!", "error");

                        }
                        if (response.error == 401) {

                            swal("Cancelled !!", "This Amount is already Withdraw", "error");

                        } else {
                            alert("Status can't be change.")
                        }
                    }
                },
                error: function () {
                    alert('Error occured');
                },
                complete: function () {
                    console.log('ok');
                    me.text('completed');
                    me.attr('disabled', true);

                    me.data('requestRunning', false);
                }
            });


            // else
            //     {
            //         swal("Cancelled !!", "Hey, Cancelled the process !!", "error");
            //     }
            // } );


        });
        $('.example1').on('click', '.processed', function () {
            var id = $(this).data('id');
            var id = $(this).data('id');


            swal({
                    title: "Are you sure you want to Approved ?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Processed !!",
                    cancelButtonText: "No, cancel it !!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },


                function (isConfirm) {
                    if (isConfirm) {
                        console.log(id);
                        var url = '<?= route('admin.pending.ajax')?>';
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: {"_token": "{{ csrf_token() }}", id: id, deposite: "doPending"},
                            success: function (response) {
                                if (response.success) {
                                    swal("Hey !!", "You Gave Withdraw amount Successfully ", "success");
                                    $('.confirm').on('click', function () {
                                        window.location.reload();
                                    });
                                } else {
                                    if (response.error == 409) {

                                        swal("Cancelled !!", "Hey, your Approval is denied !!", "error");

                                    }
                                    if (response.error == 401) {

                                        swal("Cancelled !!", "This Amount is alreayd Withdraw", "error");

                                    } else {
                                        alert("Status can't be change.")
                                    }
                                }
                            },
                            error: function () {
                                alert('Error occured');
                            }
                        });


                    } else {
                        swal("Cancelled !!", "Hey, Cancelled the Transaction !!", "error");
                    }
                });


        });

        })
        ;
    </script>
@endsection
