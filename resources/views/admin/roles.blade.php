@extends('layouts.app')

@section('dashboardcontent')
    <style>
        body {
            background: #f9f9fb;
        }

        .view-account {
            background: #FFFFFF;
            margin-top: 20px;
        }

        .view-account .pro-label {
            font-size: 13px;
            padding: 4px 5px;
            position: relative;
            top: -5px;
            margin-left: 10px;
            display: inline-block
        }

        .view-account .side-bar {
            padding-bottom: 30px;
            color: white;
        }

        .view-account .side-bar .user-info {
            text-align: center;
            margin-bottom: 15px;
            padding: 30px;
            color: #f3f3f3;
            border-bottom: 1px solid #f3f3f3
        }

        .view-account .side-bar .user-info .img-profile {
            width: 120px;
            height: 120px;
            margin-bottom: 15px
        }

        .view-account .side-bar .user-info .meta li {
            margin-bottom: 10px
        }

        .view-account .side-bar .user-info .meta li span {
            display: inline-block;
            width: 100px;
            margin-right: 5px;
            text-align: right
        }

        .view-account .side-bar .user-info .meta li a {
            color: #616670
        }

        .view-account .side-bar .user-info .meta li.activity {
            color: #a2a6af
        }

        .view-account .side-bar .side-menu {
            text-align: center
        }

        .view-account .side-bar .side-menu .nav {
            display: inline-block;
            margin: 0 auto
        }

        .view-account .side-bar .side-menu .nav > li {
            font-size: 14px;
            margin-bottom: 0;
            border-bottom: none;
            display: inline-block;
            float: left;
            margin-right: 15px;
            margin-bottom: 15px
        }

        .view-account .side-bar .side-menu .nav > li:last-child {
            margin-right: 0
        }

        .view-account .side-bar .side-menu .nav > li > a {
            display: inline-block;
            color: #9499a3;
            padding: 5px;
            border-bottom: 2px solid transparent
        }

        .view-account .side-bar .side-menu .nav > li > a:hover {
            color: #616670;
            background: none
        }

        .view-account .side-bar .side-menu .nav > li.active a {
            color: #40babd;
            border-bottom: 2px solid #40babd;
            background: none;
            border-right: none
        }

        .theme-2 .view-account .side-bar .side-menu .nav > li.active a {
            color: #6dbd63;
            border-bottom-color: #6dbd63
        }

        .theme-3 .view-account .side-bar .side-menu .nav > li.active a {
            color: #497cb1;
            border-bottom-color: #497cb1
        }

        .theme-4 .view-account .side-bar .side-menu .nav > li.active a {
            color: #ec6952;
            border-bottom-color: #ec6952
        }

        .view-account .side-bar .side-menu .nav > li .icon {
            display: block;
            font-size: 24px;
            margin-bottom: 5px
        }

        .view-account .content-panel {
            padding: 30px
        }

        .view-account .content-panel .title {
            margin-bottom: 15px;
            margin-top: 0;
            font-size: 18px
        }

        .view-account .content-panel .fieldset-title {
            padding-bottom: 15px;
            border-bottom: 1px solid #eaeaf1;
            margin-bottom: 30px;
            color: #f9f3ed;
            font-size: 16px
        }

        .view-account .content-panel .avatar .figure img {
            float: right;
            width: 64px
        }

        .view-account .content-panel .content-header-wrapper {
            position: relative;
            margin-bottom: 30px
        }

        .view-account .content-panel .content-header-wrapper .actions {
            position: absolute;
            right: 0;
            top: 0
        }

        .view-account .content-panel .content-utilities {
            position: relative;
            margin-bottom: 30px
        }

        .view-account .content-panel .content-utilities .btn-group {
            margin-right: 5px;
            margin-bottom: 15px
        }

        .view-account .content-panel .content-utilities .fa {
            font-size: 16px;
            margin-right: 0
        }

        .view-account .content-panel .content-utilities .page-nav {
            position: absolute;
            right: 0;
            top: 0
        }

        .view-account .content-panel .content-utilities .page-nav .btn-group {
            margin-bottom: 0
        }

        .view-account .content-panel .content-utilities .page-nav .indicator {
            color: #a2a6af;
            margin-right: 5px;
            display: inline-block
        }

        .view-account .content-panel .mails-wrapper .mail-item {
            position: relative;
            padding: 10px;
            border-bottom: 1px solid #f3f3f3;
            color: #616670;
            overflow: hidden
        }

        .view-account .content-panel .mails-wrapper .mail-item > div {
            float: left
        }

        .view-account .content-panel .mails-wrapper .mail-item .icheck {
            background-color: #fff
        }

        .view-account .content-panel .mails-wrapper .mail-item:hover {
            background: #f9f9fb
        }

        .view-account .content-panel .mails-wrapper .mail-item:nth-child(even) {
            background: #fcfcfd
        }

        .view-account .content-panel .mails-wrapper .mail-item:nth-child(even):hover {
            background: #f9f9fb
        }

        .view-account .content-panel .mails-wrapper .mail-item a {
            color: #616670
        }

        .view-account .content-panel .mails-wrapper .mail-item a:hover {
            color: #494d55;
            text-decoration: none
        }

        .view-account .content-panel .mails-wrapper .mail-item .checkbox-container,
        .view-account .content-panel .mails-wrapper .mail-item .star-container {
            display: inline-block;
            margin-right: 5px
        }

        .view-account .content-panel .mails-wrapper .mail-item .star-container .fa {
            color: #a2a6af;
            font-size: 16px;
            vertical-align: middle
        }

        .view-account .content-panel .mails-wrapper .mail-item .star-container .fa.fa-star {
            color: #f2b542
        }

        .view-account .content-panel .mails-wrapper .mail-item .star-container .fa:hover {
            color: #868c97
        }

        .view-account .content-panel .mails-wrapper .mail-item .mail-to {
            display: inline-block;
            margin-right: 5px;
            min-width: 120px
        }

        .view-account .content-panel .mails-wrapper .mail-item .mail-subject {
            display: inline-block;
            margin-right: 5px
        }

        .view-account .content-panel .mails-wrapper .mail-item .mail-subject .label {
            margin-right: 5px
        }

        .view-account .content-panel .mails-wrapper .mail-item .mail-subject .label:last-child {
            margin-right: 10px
        }

        .view-account .content-panel .mails-wrapper .mail-item .mail-subject .label a,
        .view-account .content-panel .mails-wrapper .mail-item .mail-subject .label a:hover {
            color: #fff
        }

        .view-account .content-panel .mails-wrapper .mail-item .mail-subject .label-color-1 {
            background: #f77b6b
        }

        .view-account .content-panel .mails-wrapper .mail-item .mail-subject .label-color-2 {
            background: #58bbee
        }

        .view-account .content-panel .mails-wrapper .mail-item .mail-subject .label-color-3 {
            background: #f8a13f
        }

        .view-account .content-panel .mails-wrapper .mail-item .mail-subject .label-color-4 {
            background: #ea5395
        }

        .view-account .content-panel .mails-wrapper .mail-item .mail-subject .label-color-5 {
            background: #8a40a7
        }

        .view-account .content-panel .mails-wrapper .mail-item .time-container {
            display: inline-block;
            position: absolute;
            right: 10px;
            top: 10px;
            color: #a2a6af;
            text-align: left
        }

        .view-account .content-panel .mails-wrapper .mail-item .time-container .attachment-container {
            display: inline-block;
            color: #a2a6af;
            margin-right: 5px
        }

        .view-account .content-panel .mails-wrapper .mail-item .time-container .time {
            display: inline-block;
            text-align: right
        }

        .view-account .content-panel .mails-wrapper .mail-item .time-container .time.today {
            font-weight: 700;
            color: #494d55
        }

        .drive-wrapper {
            padding: 15px;
            background: #f5f5f5;
            overflow: hidden
        }

        .drive-wrapper .drive-item {
            width: 130px;
            margin-right: 15px;
            display: inline-block;
            float: left
        }

        .drive-wrapper .drive-item:hover {
            box-shadow: 0 1px 5px rgba(0, 0, 0, .1);
            z-index: 1
        }

        .drive-wrapper .drive-item-inner {
            padding: 15px
        }

        .drive-wrapper .drive-item-title {
            margin-bottom: 15px;
            max-width: 100px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis
        }

        .drive-wrapper .drive-item-title a {
            color: #494d55
        }

        .drive-wrapper .drive-item-title a:hover {
            color: #40babd
        }

        .theme-2 .drive-wrapper .drive-item-title a:hover {
            color: #6dbd63
        }

        .theme-3 .drive-wrapper .drive-item-title a:hover {
            color: #497cb1
        }

        .theme-4 .drive-wrapper .drive-item-title a:hover {
            color: #ec6952
        }

        .drive-wrapper .drive-item-thumb {
            width: 100px;
            height: 80px;
            margin: 0 auto;
            color: #616670
        }

        .drive-wrapper .drive-item-thumb a {
            -webkit-opacity: .8;
            -moz-opacity: .8;
            opacity: .8
        }

        .drive-wrapper .drive-item-thumb a:hover {
            -webkit-opacity: 1;
            -moz-opacity: 1;
            opacity: 1
        }

        .drive-wrapper .drive-item-thumb .fa {
            display: inline-block;
            font-size: 36px;
            margin: 0 auto;
            margin-top: 20px
        }

        .drive-wrapper .drive-item-footer .utilities {
            margin-bottom: 0
        }

        .drive-wrapper .drive-item-footer .utilities li:last-child {
            padding-right: 0
        }

        .drive-list-view .name {
            width: 60%
        }

        .drive-list-view .name.truncate {
            max-width: 100px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis
        }

        .drive-list-view .type {
            width: 15px
        }

        .drive-list-view .date,
        .drive-list-view .size {
            max-width: 60px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis
        }

        .drive-list-view a {
            color: #494d55
        }

        .drive-list-view a:hover {
            color: #40babd
        }

        .theme-2 .drive-list-view a:hover {
            color: #6dbd63
        }

        .theme-3 .drive-list-view a:hover {
            color: #497cb1
        }

        .theme-4 .drive-list-view a:hover {
            color: #ec6952
        }

        .drive-list-view td.date,
        .drive-list-view td.size {
            color: #a2a6af
        }

        @media (max-width: 767px) {
            .view-account .content-panel .title {
                text-align: center
            }

            .view-account .side-bar .user-info {
                padding: 0
            }

            .view-account .side-bar .user-info .img-profile {
                width: 60px;
                height: 60px
            }

            .view-account .side-bar .user-info .meta li {
                margin-bottom: 5px
            }

            .view-account .content-panel .content-header-wrapper .actions {
                position: static;
                margin-bottom: 30px
            }

            .view-account .content-panel {
                padding: 0
            }

            .view-account .content-panel .content-utilities .page-nav {
                position: static;
                margin-bottom: 15px
            }

            .drive-wrapper .drive-item {
                width: 100px;
                margin-right: 5px;
                float: none
            }

            .drive-wrapper .drive-item-thumb {
                width: auto;
                height: 54px
            }

            .drive-wrapper .drive-item-thumb .fa {
                font-size: 24px;
                padding-top: 0
            }

            .view-account .content-panel .avatar .figure img {
                float: none;
                margin-bottom: 15px
            }

            .view-account .file-uploader {
                margin-bottom: 15px
            }

            .view-account .mail-subject {
                max-width: 100px;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis
            }

            .view-account .content-panel .mails-wrapper .mail-item .time-container {
                position: static
            }

            .view-account .content-panel .mails-wrapper .mail-item .time-container .time {
                width: auto;
                text-align: left
            }
        }

        @media (min-width: 768px) {
            .view-account .side-bar .user-info {
                padding: 0;
                padding-bottom: 15px
            }

            .view-account .mail-subject .subject {
                max-width: 200px;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis
            }
        }

        @media (min-width: 992px) {
            .view-account .content-panel {
                min-height: 800px;
                border-left: 1px solid #f3f3f7;
                margin-left: 200px
            }

            .view-account .mail-subject .subject {
                max-width: 280px;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis
            }

            .view-account .side-bar {
                position: absolute;
                width: 200px;
                min-height: 600px
            }

            .view-account .side-bar .user-info {
                margin-bottom: 0;
                border-bottom: none;
                padding: 30px
            }

            .view-account .side-bar .user-info .img-profile {
                width: 120px;
                height: 120px
            }

            .view-account .side-bar .side-menu {
                text-align: left
            }

            .view-account .side-bar .side-menu .nav {
                display: block
            }

            .view-account .side-bar .side-menu .nav > li {
                display: block;
                float: none;
                font-size: 14px;
                border-bottom: 1px solid #f3f3f7;
                margin-right: 0;
                margin-bottom: 0
            }

            .view-account .side-bar .side-menu .nav > li > a {
                display: block;
                color: #9499a3;
                padding: 10px 15px;
                padding-left: 30px
            }

            .view-account .side-bar .side-menu .nav > li > a:hover {
                background: #f9f9fb
            }

            .view-account .side-bar .side-menu .nav > li.active a {
                background: #f9f9fb;
                border-right: 4px solid #40babd;
                border-bottom: none
            }

            .theme-2 .view-account .side-bar .side-menu .nav > li.active a {
                border-right-color: #6dbd63
            }

            .theme-3 .view-account .side-bar .side-menu .nav > li.active a {
                border-right-color: #497cb1
            }

            .theme-4 .view-account .side-bar .side-menu .nav > li.active a {
                border-right-color: #ec6952
            }

            .view-account .side-bar .side-menu .nav > li .icon {
                font-size: 24px;
                vertical-align: middle;
                text-align: center;
                width: 40px;
                display: inline-block
            }

            .field-icon {
                float: right;
                margin-right: 17px;
                margin-top: -25px;
                position: relative;
                z-index: 14;
            }
        }
    </style>
    {{--        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />--}}
    <!-- Select2 -->
    <link rel="stylesheet" href="../../js/select2/css/select2.min.css">
    <link rel="stylesheet" href="../../js/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 style="color: white;">Roles Management</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">User List</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="">
            <!-- SELECT2 EXAMPLE -->
            <div class="row">

                <!-- general form elements -->
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header " style="background-color: #35354B;color: white;">
                            <h3 class="card-title">Users with Role and Extra Permissions</h3>
                        </div>
                        <div class="card-header" style="background-color: #35354B;color: white;">
                            <a class="btn btn-info" id="add_user_btn" href="#">Register New Admin</a>

                        </div>
                        <!-- /.card-header -->
                        <div class="card-body" style="background-color: #35354B;color: white;">
                            <div class="table-responsive ">
                                <table id="example1" class="  table table-bordered table-striped ">
                                    <thead style="background-color:#131311; color:white;">
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Extra Permissions</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody style="background-color: #35354B;color: white;">
                                    @foreach($admins as $adm)
                                        <tr>
                                            <td>{{$adm->id}}</td>
                                            <td>{{$adm->name}}</td>
                                            <td>{{$adm->email}}</td>
                                            {{--                                    <script>--}}
                                            {{--                                        var app = @json($adm->roles);--}}
                                            {{--                                        app->name;--}}
                                            {{--                                    </script>--}}
                                            {{--                                    <td> {{$adm->getRoleNames()}}</td>--}}
                                            <td> {{implode($adm->roles->pluck('name')->toarray())}}</td>
                                            <td> {{implode(', ',$adm->getDirectPermissions()->pluck('name')->toarray())}}</td>
                                            {{--                                            <td> {{implode($adm->permissions->pluck('name')->toarray())}}</td>--}}
                                            {{--                                            <td> {{$adm->getAllPermissions()}}</td>--}}

                                            <td style="text-align:right;">
                                                <button class="btn btn-success  add_role"
                                                        type="button" data-uid="{{$adm->id}}"
                                                        @if(implode(', ',$adm->getDirectPermissions()->pluck('id')->toarray()))
                                                        {{--                                                    data-u_per="{{implode(',',$adm->getDirectPermissions()->pluck('id')->toarray())}}"--}}
                                                        data-u_per="{{$adm->getDirectPermissions()->pluck('id')}}"
                                                        @endif
                                                        @if(implode($adm->roles->pluck('name')->toarray()))
                                                        data-u_roles="{{$adm->roles->pluck('id')}}"
                                                        @endif
                                                        @if(implode($adm->roles->pluck('name')->toarray()))
                                                        data-u_roles_name="{{implode($adm->roles->pluck('name')->toarray())}}"
                                                    @endif
                                                ><i class="fas fa-user-circle"></i> &nbsp;View Profile
                                                </button>
{{--                                                <div class="btn-group">--}}
{{--                                                    <button type="button" class="btn btn-secondary dropdown-toggle"--}}
{{--                                                            data-toggle="dropdown" aria-haspopup="true"--}}
{{--                                                            aria-expanded="false">--}}
{{--                                                        <i class="fas fa-compress-arrows-alt"></i> Action--}}
{{--                                                    </button>--}}
{{--                                                    <div class="dropdown-menu dropdown-menu-right">--}}

{{--                                                        <button class="btn btn-success dropdown-item add_role"--}}
{{--                                                                type="button" data-uid="{{$adm->id}}"--}}
{{--                                                                @if(implode(', ',$adm->getDirectPermissions()->pluck('id')->toarray()))--}}
{{--                                                                --}}{{--                                                    data-u_per="{{implode(',',$adm->getDirectPermissions()->pluck('id')->toarray())}}"--}}
{{--                                                                data-u_per="{{$adm->getDirectPermissions()->pluck('id')}}"--}}
{{--                                                                @endif--}}
{{--                                                                @if(implode($adm->roles->pluck('name')->toarray()))--}}
{{--                                                                data-u_roles="{{$adm->roles->pluck('id')}}"--}}
{{--                                                                @endif--}}
{{--                                                                @if(implode($adm->roles->pluck('name')->toarray()))--}}
{{--                                                                data-u_roles_name="{{implode($adm->roles->pluck('name')->toarray())}}"--}}
{{--                                                            @endif--}}
{{--                                                        ><i class="fas fa-user-circle"></i> &nbsp;View Profile--}}
{{--                                                        </button>--}}


{{--                                                        @if(!implode($adm->roles->pluck('name')->toarray()))--}}
{{--                                                            <button class="btn btn-success dropdown-item add_role"--}}
{{--                                                                    type="button" data-uid="{{$adm->id}}"--}}
{{--                                                                    @if(implode(', ',$adm->getDirectPermissions()->pluck('id')->toarray()))--}}
{{--                                                                    --}}{{--                                                    data-u_per="{{implode(',',$adm->getDirectPermissions()->pluck('id')->toarray())}}"--}}
{{--                                                                    data-u_per="{{$adm->getDirectPermissions()->pluck('id')}}"--}}
{{--                                                                @endif--}}
{{--                                                            ><i class="fas fa-folder-plus"></i> &nbsp;Add--}}
{{--                                                                Role--}}
{{--                                                            </button>--}}
{{--                                                        @elseif($adm->roles)--}}

{{--                                                            --}}{{--                                                    <button class="btn btn-danger edit_role" data-uid="{{$adm->id}}">--}}
{{--                                                            --}}{{--                                                        Edit--}}
{{--                                                            --}}{{--                                                    </button>--}}
{{--                                                            <button class="btn btn-danger dropdown-item remove_role"--}}
{{--                                                                    type="button" data-uid="{{$adm->id}}">--}}
{{--                                                                <b><i class="fas fa-user-times"></i> &nbsp; Remove Role--}}
{{--                                                                </b></button>--}}
{{--                                                        @else--}}

{{--                                                            <button class=" btn btn-danger dropdown-item remove_role "--}}
{{--                                                                    type="button"--}}
{{--                                                                    data-uid="{{$adm->id}}"><b> Remove Role else</b>--}}
{{--                                                            </button>--}}

{{--                                                        @endif--}}
{{--                                                        @if(!implode(', ',$adm->getDirectPermissions()->pluck('name')->toarray()))--}}
{{--                                                            <button class="btn btn-success dropdown-item add_role"--}}
{{--                                                                    data-uid="{{$adm->id}}" type="button"--}}

{{--                                                                    @if(implode($adm->roles->pluck('name')->toarray()))--}}
{{--                                                                    data-u_roles="{{$adm->roles->pluck('id')}}"--}}
{{--                                                                    @endif--}}
{{--                                                                    @if(implode($adm->roles->pluck('name')->toarray()))--}}
{{--                                                                    data-u_roles_name="{{implode($adm->roles->pluck('name')->toarray())}}"--}}
{{--                                                                @endif--}}

{{--                                                            ><i class="fas fa-puzzle-piece"></i> &nbsp; Add--}}
{{--                                                                Permissions--}}
{{--                                                            </button>--}}
{{--                                                        @elseif($adm->roles)--}}

{{--                                                            --}}{{--                                                    <button class="btn btn-danger edit_role" data-uid="{{$adm->id}}">--}}
{{--                                                            --}}{{--                                                        Edit--}}
{{--                                                            --}}{{--                                                    </button>--}}
{{--                                                            <button--}}
{{--                                                                class="btn btn-danger dropdown-item removePermission"--}}
{{--                                                                type="button"--}}
{{--                                                                data-uid="{{$adm->id}}">--}}
{{--                                                                <b><i class="fas fa-eraser"></i> &nbsp; Remove--}}
{{--                                                                    Permissions </b></button>--}}
{{--                                                        @endif--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}

                                            </td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot style="background-color:#131311; color:white;">
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Extra Permissions</th>
                                        <th>Action</th>


                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- /.card -->
            </div>

            <!-- general form elements disabled -->

        {{--            <div class="card bg-gradient-dark">--}}
        {{--                <div class="card-header">--}}
        {{--                    <h3 class="card-title">Add User</h3>--}}
        {{--                </div>--}}

        {{--                <!-- /.card-header -->--}}
        {{--                <form id="add_user">--}}

        {{--                    <div class="card-body bg-gradient-dark">--}}
        {{--                    @csrf--}}
        {{--                    <!-- input states -->--}}
        {{--                        <div class="form-group">--}}
        {{--                            <label class="col-form-label" for="name"><i class="fas fa-check"></i> Name</label>--}}
        {{--                            <input type="text" class="form-control is-valid" name="name" id="name"--}}
        {{--                                   placeholder="Enter Name">--}}
        {{--                            <span style="display: none" id="name_error"--}}
        {{--                                  class="text-danger"></span>--}}
        {{--                        </div>--}}
        {{--                        <div class="form-group">--}}
        {{--                            <label class="col-form-label" for="Email"><i class="far fa-bell"></i> Email</label>--}}
        {{--                            <input type="text" class="form-control is-warning" name="email" id="email"--}}
        {{--                                   placeholder="Enter Email">--}}
        {{--                            <span style="display: none" id="email_error"--}}
        {{--                                  class="text-danger"></span>--}}

        {{--                        </div>--}}
        {{--                        <div class="form-group">--}}
        {{--                            <label class="col-form-label" for="password"><i class="far fa-times-circle"></i>Password</label>--}}
        {{--                            <input type="password" class="form-control is-invalid" name="password" id="password"--}}
        {{--                                   placeholder="Enter ..." name="password">--}}

        {{--                        </div>--}}
        {{--                        <div class="form-group">--}}
        {{--                            <label class="col-form-label" for="password"><i class="far fa-times-circle"></i>Confirm--}}
        {{--                                Password</label>--}}
        {{--                            <input type="password" class="form-control is-invalid" id="password_again"--}}
        {{--                                   placeholder="Enter ..." name="password_confirmation">--}}
        {{--                            <span style="display: none" id="password_error"--}}
        {{--                                  class="text-danger"></span>--}}

        {{--                        </div>--}}


        {{--                    </div>--}}
        {{--                    <div class="card-footer">--}}
        {{--                        <button type="submit" class="btn btn-primary">Submit</button>--}}
        {{--                        <div id="success_message" class="alert alert-success " style="display: none">--}}
        {{--                            <p></p>--}}
        {{--                        </div>--}}
        {{--                    </div>--}}
        {{--                </form>--}}

        {{--                <!-- /.card-body -->--}}
        {{--            </div>--}}
        {{--            <!-- /.card -->--}}
        {{--            <!-- general form elements disabled -->--}}

        <!-- /.card -->


        </div>


        <!-- /.row -->
        </div><!-- /.container-fluid -->
        </div><!-- /.container-fluid -->
    </section>


    <!-- Modal -->
    <div class="modal fade" id="add_role_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #42426a;color: white;">
                    <h5 class="modal-title" id="exampleModalLabel" style=" color:white;">User Info</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="background-color:#35354B; color:white;">

                    {{--                    <div class="card-body">--}}
                    <input type="hidden" id="user_id" value="">
                    <div class="container">
                        <div class="view-account">
                            <section class="module " style="background-color:#35354B; color:white;">
                                <div class="module-inner">
                                    <div class="side-bar">
                                        <div class="user-info">
                                            {{--                                            <img class="img-profile img-circle img-responsive center-block"--}}
                                            {{--                                                 src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="">--}}
                                            Admin User
                                            <ul class="meta list list-unstyled">
                                                <li class="name mt-2">
                                                    <div id="header_name"></div>
                                                </li>
                                                <label class="label label-info" id="header_role">
                                                    No Role</label>

                                                <li class="email" id="header_email"><a href="#"></a></li>
                                                <li class="activity">Created at:
                                                    <span id="header_create_at" style="text-align: right;
    margin-right: 36px;">Today at 2:18pm </span></li>
                                            </ul>
                                        </div>
                                        <nav class="side-menu">
                                            <nav>
                                                <div class="nav nav-tabs " id="nav-tab" role="tablist">
                                                    <a class="nav-item nav-link " id="nav-profile-tab" data-toggle="tab"
                                                       href="#nav-profile" role="tab" aria-controls="nav-profile"
                                                       aria-selected="false" style="color: white"><span
                                                            class="fa fa-user"></span> Profile</a>
                                                    <a class="nav-item nav-link active " id="nav-setting-tab"
                                                       data-toggle="tab" href="#nav-setting" role="tab"
                                                       aria-controls="nav-setting" aria-selected="true"
                                                       style="color: white"><span class="fa fa-cog"></span> Settings</a>

                                                    <a class="nav-item nav-link nav_activites_log"
                                                       id="nav-activates-tab" data-toggle="tab"
                                                       href="#nav-activates" role="tab" aria-controls="nav-activates"
                                                       aria-selected="false" style="color: white"><span
                                                            class="fa fa-credit-card"></span> Activates</a>
                                                </div>
                                            </nav>
                                            {{--                                            <ul class="nav">--}}
                                            {{--                                                <li><a href="#"><span class="fa fa-user"></span> Profile</a></li>--}}
                                            {{--                                                <li class="active"><a href="#"><span class="fa fa-cog"></span> Settings</a>--}}
                                            {{--                                                </li>--}}
                                            {{--                                                <li><a href="#"><span class="fa fa-credit-card"></span> Activates</a>--}}
                                            {{--                                                </li>--}}
                                            {{--                                                --}}{{--                                                <li><a href="#"><span class="fa fa-envelope"></span> Messages</a></li>--}}

                                            {{--                                                --}}{{--                                                <li><a href="user-drive.html"><span class="fa fa-th"></span> Drive</a></li>--}}
                                            {{--                                                --}}{{--                                                <li><a href="#"><span class="fa fa-clock-o"></span> Reminders</a></li>--}}
                                            {{--                                            </ul>--}}
                                        </nav>
                                    </div>
                                    <div class="content-panel">
                                        {{--                                        <h2 class="title">Profile<span class="pro-label label label-warning">PRO</span></h2>--}}
                                        <div class="tab-content" id="nav-tabContent">
                                            <div class="tab-pane fade show " id="nav-setting" role="tabpanel"
                                                 aria-labelledby="nav-setting-tab">
                                                <form class="form-horizontal">
                                                    <fieldset class="fieldset mt-4">
                                                        <h3 class="fieldset-title"><span>Roles Settings</span></h3>
                                                        <div class="form-group">
                                                            <label class="col-md-2  col-sm-3 col-xs-12 control-label">Select
                                                                Role</label>
                                                            <div class="col-md-10 col-sm-9 col-xs-12"
                                                                 style="color: black">
                                                                <select class="form-control select_role_drop"
                                                                        placeholder="Select a Role"
                                                                        id="selected_role"
                                                                >
                                                                    <option>No Role</option>

                                                                    @foreach($roles as $role)
                                                                        <option
                                                                            value="{{$role->id}}">{{$role->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <p class="help-block mt-2" style="color: white">User
                                                                    Curent Role </p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3  col-sm-3 col-xs-12 control-label">Direct
                                                                Permissions</label>
                                                            <div class="col-md-10 col-sm-9 col-xs-12"
                                                                 style="color: black">
                                                                <select class="select_role_drop" id="permission"
                                                                        name="permission"
                                                                        multiple="multiple"
                                                                        data-placeholder="Select Extra Permissions"
                                                                >
                                                                    @foreach($permissions as $permission)
                                                                        {{--                                            <option value="{{$permission->id}}">{{$permission->name}}</option>--}}
                                                                        <option
                                                                            value="{{$permission->id}}">{{$permission->name}}</option>
                                                                    @endforeach

                                                                </select>

                                                                <p class="help-block mt-2" style="color: white">User
                                                                    Direct Permissions
                                                                    (Other then
                                                                    Role)</p>
                                                            </div>
                                                        </div>
                                                        {{--                                                <div class="form-group">--}}
                                                        {{--                                                    <label class="col-md-2  col-sm-3 col-xs-12 control-label">Linkedin</label>--}}
                                                        {{--                                                    <div class="col-md-10 col-sm-9 col-xs-12">--}}
                                                        {{--                                                        <input type="url" class="form-control" value="https://www.linkedin.com/in/lorem">--}}
                                                        {{--                                                        <p class="help-block">eg. https://www.linkedin.com/in/yourname</p>--}}
                                                        {{--                                                    </div>--}}
                                                        {{--                                                </div>--}}
                                                    </fieldset>

                                                    <hr>
                                                    <div class="form-group">
                                                        <div
                                                            class="col-md-10 col-sm-9 col-xs-12 col-md-push-2 col-sm-push-3 col-xs-push-0">

                                                            <input class="btn btn-primary submit_roles_form"
                                                                   type="submit"
                                                                   value="Update Profile">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="tab-pane fade " id="nav-profile" role="tabpanel"
                                                 aria-labelledby="nav-profile-tab">
                                                <form class="form-horizontal">
                                                    <fieldset class="fieldset">
                                                        <h3 class="fieldset-title ">Personal Info</h3>
                                                        <div class="form-group avatar">
                                                            {{--                                                    <figure class="figure col-md-2 col-sm-3 col-xs-12">--}}
                                                            {{--                                                        <img class="img-rounded img-responsive"--}}
                                                            {{--                                                             src="https://bootdey.com/img/Content/avatar/avatar1.png"--}}
                                                            {{--                                                             alt="">--}}
                                                            {{--                                                    </figure>--}}
                                                            {{--                                                    <div class="form-inline col-md-10 col-sm-9 col-xs-12">--}}
                                                            {{--                                                        <input type="file" class="file-uploader pull-left">--}}
                                                            {{--                                                        <button type="" class="btn btn-sm btn-default-alt pull-left"--}}
                                                            {{--                                                                disabled>--}}
                                                            {{--                                                            Update Image--}}
                                                            {{--                                                        </button>--}}
                                                            {{--                                                    </div>--}}
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 col-sm-3 col-xs-12 control-label">User
                                                                Name</label>
                                                            <div class="col-md-10 col-sm-9 col-xs-12">
                                                                <input type="text" class="form-control" id="name-m"
                                                                       value=""
                                                                       readonly
                                                                       style="background-color: black ; color: white">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label
                                                                class="col-md-2 col-sm-3 col-xs-12 control-label">Email</label>
                                                            <div class="col-md-10 col-sm-9 col-xs-12">
                                                                <input type="text" class="form-control"
                                                                       id="email-m" value="" readonly
                                                                       style="background-color: black ; color: white">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label
                                                                class="col-md-2 col-sm-3 col-xs-12 control-label">Password</label>
                                                            <div class="col-md-10 col-sm-9 col-xs-12">
                                                                <input id="password-m" type="password"
                                                                       class="form-control"
                                                                       name="password" value=""
                                                                       style="background-color: black ; color: white">
                                                                <span toggle="#password-m"
                                                                      class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                                                <span style="display: none" id="password_error_m"
                                                                      class="text-danger"></span>
                                                                <p class="help-block mt-2">*Leave This Filed Empty If
                                                                    You Don't
                                                                    Want To Change Password.</p>

                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                    <hr>
                                                    <div class="form-group">
                                                        <div
                                                            class="col-md-10 col-sm-9 col-xs-12 col-md-push-2 col-sm-push-3 col-xs-push-0">

                                                            <input class="btn btn-primary submit_roles_form"
                                                                   type="submit"
                                                                   value="Update Profile">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="tab-pane fade" id="nav-activates" role="tabpanel"
                                                 aria-labelledby="nav-activates-tab">
                                                <div class="table-responsive ">
                                                    <fieldset class="fieldset mt-4">
                                                        <h3 class="fieldset-title"><span>User Activities</span></h3>
                                                        <div class="row">
                                                            <div class="col-md-4 ">

                                                            </div>
                                                            <div class="col-md-4 ">

                                                            </div>
                                                            <div class="col-md-4 mb-3">


                                                                <select class="form-control" name="log_name" id="log_name">
                                                                    <option value="">Select Filter</option>
                                                                </select>
                                                            </div>
                                                            </div>




                                                        <table id="activities_table1"
                                                               class="  table table-bordered table-striped ">
                                                            <thead style="background-color:#131311; color:white;">
                                                            <tr>
                                                                {{--                                                            <th>ID</th>--}}
                                                                <th>Log Name</th>
                                                                <th>Description</th>
                                                                <th>Proprities</th>
                                                                <th>Created At</th>

                                                            </tr>
                                                            </thead>
                                                            <tbody style="background-color: #35354B;color: white;"
                                                                   id="activites_tbody">

                                                            </tbody>
                                                            <tfoot style="background-color:#131311; color:white;">
                                                            <tr>
                                                                {{--                                                            <th>ID</th>--}}
                                                                <th>Log Name</th>
                                                                <th>Description</th>
                                                                <th>Proprities</th>
                                                                <th>Created At</th>


                                                            </tr>
                                                            </tfoot>
                                                        </table>
                                                    </fieldset>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>


                    <!-- /.form-group -->
                    {{--                                <div class="form-group">--}}
                    {{--                                    <label>Disabled Result</label>--}}
                    {{--                                    <select class="form-control select2bs4" style="width: 100%;">--}}
                    {{--                                        <option selected="selected">Alabama</option>--}}
                    {{--                                        <option>Alaska</option>--}}
                    {{--                                        <option disabled="disabled">California (disabled)</option>--}}
                    {{--                                        <option>Delaware</option>--}}
                    {{--                                        <option>Tennessee</option>--}}
                    {{--                                        <option>Texas</option>--}}
                    {{--                                        <option>Washington</option>--}}
                    {{--                                    </select>--}}
                    {{--                                </div>--}}
                <!-- /.form-group -->

                    <!-- /.col -->

                    <!-- /.row -->
                    {{--                    </div>--}}


                </div>
                <div class="modal-footer">
                    {{--                    <button type="button" id="submit_roles_form" class="btn btn-secondary">Submit--}}
                    {{--                    </button>--}}

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="add_user_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #42426a;color: white;">
                    <h5 class="modal-title" style=" color:white;">Register New Admin</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="add_user">
                    <div class="modal-body" style="background-color:#35354B; color:white;">
                        @csrf
                        <div class="form-group">
                            <label class="col-form-label" for="name"><i class="fas fa-check"></i> Name</label>
                            <input type="text" class="form-control " name="name" id="name"
                                   placeholder="Enter Name" style="background-color: black ; color: white">
                            <span style="display: none" id="name_error"
                                  class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label" for="Email"><i class="far fa-bell"> </i> Email</label>
                            <input type="text" style="background-color: black ; color: white" class="form-control"
                                   name="email" id="email"
                                   placeholder="Enter Email">
                            <span style="display: none" id="email_error"
                                  class="text-danger"></span>

                        </div>
                        <div class="form-group">
                            <label class="col-form-label"><i
                                    class="far fa-times-circle"></i>Password</label>
                            <input type="password" class="form-control " name="password" id="password"
                                   placeholder="Enter ..." name="password"
                                   style="background-color: black ; color: white">

                        </div>
                        <div class="form-group">
                            <label class="col-form-label"><i class="far fa-times-circle"></i>Confirm
                                Password</label>
                            <input type="password" class="form-control " id="password_again"
                                   placeholder="Enter ..." name="password_confirmation"
                                   style="background-color: black ; color: white">
                            <span style="display: none" id="password_error"
                                  class="text-danger"></span>

                        </div>
                    </div>
                    <div class="modal-footer" style="background-color: #42426a;color: white;">
                        <button type="submit" class="btn btn-primary" style="background-color: #35354B;color: white;">
                            Submit
                        </button>
                        <div id="success_message" class="alert alert-success " style="display: none">
                            <p></p>
                        </div>

                    </div>
                </form>

            </div>
        </div>
    </div>

    <!-- /.content -->
    {{--        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>--}}
    <!-- Select2 -->
    <script src="../../js/select2/js/select2.full.min.js"></script>
    <script src="../../js/select2/js/select2.full.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#add_user_btn').on('click', function (e) {
                e.preventDefault();
                $('#add_user_model').modal('show');


            })
            $(".toggle-password").click(function () {

                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });

            function topRight() {

                var values = [{
                    "positionClass": "toast-top-right",
                    timeOut: 5000,
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": true,
                    "preventDuplicates": true,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut",
                    "tapToDismiss": false
                }];
                return values[0];

            }

            $('.select2').select2();
            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            });
            $('.select_role_drop').select2({
                theme: 'bootstrap4',

            });
            // $('.select_role_drop').css('background-color: black !important; color: white ; width: 100%;');

            // $('.select_role_drop').css( style="background-color: black !important; color: white ; width: 100%;");
            $('#example1').DataTable({
                // "scrollX": true,
                responsive: true,
                sPaginationType: 'full_numbers',
                // "scrollCollapse": true,
            });

            $('#add_user').on('submit', function (e) {
                e.preventDefault();
                console.log('clicked ');
                $.ajax({
                    beforeSend: (function () {
                        $('#name_error').hide();
                        $('#password_error').hide();
                        $('#email_error').hide();


                    }),
                    cache: false,
                    contentType: false,
                    processData: false,
                    method: "post",
                    data: new FormData(this),
                    url: "{{route('admin.add_Users')}}",
                    success: function (data) {
                        toastr.success(data.message, "User add", topRight());
                        // $('#success_message').html(data.message);
                        // $('#success_message').show();
                        $("#add_user_model").modal('hide');
                        window.setTimeout(function () {
                            window.location.reload()
                        }, 4600);
                    },
                    error: function (data) {
                        if (data.status === 422) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                // $('#category_id_error').show().append(value + "<br/>"); //this is my div with messages
                                if ($.isPlainObject(value)) {
                                    $.each(value, function (key, value) {

                                        if (key == 'name') {
                                            $('#name_error').html(value);
                                            $('#name_error').show();

                                        }
                                        if (key == 'email') {
                                            $('#email_error').html(value);
                                            $('#email_error').show();
                                        }
                                        if (key == 'password') {
                                            $('#password_error').html(value);
                                            $('#password_error').show();

                                        }
                                    });

                                }

                            });
                        }

                    }

                });
            });

            $('#add_role_model').on('hidden.bs.modal', function () {
                // $('#permission').select2("val", ["1", "2"]);
                $('#permission').select2("val", '[""]');
                // $('#nav-profile-tab').addClass("Active");

                // $('#permission').val('');
                $('#selected_role').select2("val", '[""]');
                $('#user_id').val('');


            });


            $('#activities_table1').DataTable({
                // "scrollX": true,
                responsive: true,
                sPaginationType: 'full_numbers',
                // "scrollCollapse": true,
            });
            $('#example1').on('click', '.add_role', function (e) {
                var me = $(this);
                e.preventDefault();
                var id = $(this).data('uid');
                var u_per = $(this).data('u_per');
                var u_roles = $(this).data('u_roles');
                var u_roles_name = $(this).data('u_roles_name');
                $('#user_id').val(id);
                console.log(id);
                console.log('add_role');
                // console.log('permission', $('#permission').val());
                // console.log('user id', $('#user_id').val());
                // console.log('user Role', $('#selected_role').val());
                console.log('u_per', u_per);
                $.ajax({
                    type: "POST",
                    url: "{{route('admin.view_user_info')}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        user_id: id,
                    },
                    success: function (response) {
                        if (response.error == null) {
                            $('#name-m').val(response.user.name);
                            $('#email-m').val(response.user.email);
                            // $('#header_name').innerText=response.user.name;
                            // $('#password-field').val(response.user.password);
                            $('#header_name').text(response.user.name);
                            $('#header_email').text(response.user.email);
                            $('#header_create_at').text(response.user.created_at);

                            // $('.confirm').on('click', function () {
                            //     // me.text('running');
                            //     // window.location.reload();
                            // });
                            $('#log_name').find('option').not(':first').remove();

                            if(response.activity_filters != null){
                                $.each(response.activity_filters, function (key, value) {
                                    $('#log_name').append(`<option value="${value.log_name}">
                                       ${value.log_name}
                                  </option>`);
                                });


                            }


                        } else {
                            if (response.error == 409) {

                                swal("Cancelled !!", response.message);

                            }
                            if (response.error == 421) {

                                swal("Cancelled !!", response.message);

                            }
                            if (response.error == 409) {

                                swal("Cancelled !!", response.message, "error");

                            } else {
                                // alert("Status can't be change.")
                            }
                        }
                    },
                    // error: function () {
                    //     alert('Error occured');
                    // },
                    complete: function () {
                        console.log('ok');
                        // me.text('completed');
                        // me.attr('disabled', true);

                        me.data('requestRunning', false);
                        // window.location.reload();

                    }
                });
                $.ajax({
                    type: "get",
                    url: "{{route('admin.view_user_logs')}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        user_id: id,
                    },
                    {{--                    success: function (data) {--}}
                        {{--                        var html = '';--}}
                        {{--                        var i;--}}
                        {{--                     var info ='{{json_encode({data})}}';--}}
                        {{--                        for (i = 1; i < info.length; i++) {--}}

                        {{--                            html += '<tr>'+--}}
                        {{--                                // + '<td><img style="width: 80px;  height: 76px;"  class="sm" src="http://' + window.location.host +--}}
                        {{--                                // '/Library_Management_System_Codeigniter3/assets/images/Members/' + data[i].profile_photo +--}}
                        {{--                                // '" alt=" No Cover "></td>' +--}}
                        {{--                                // '<td></td> ' +--}}

                        {{--                                // '<td>' + data.activity[i].id + '</td>' +--}}
                        {{--                                '<td>' + info.activity[i].log_name + '</td>' +--}}
                        {{--                                '<td>' + info.activity[i].description + '</td>' +--}}
                        {{--                                {{implode($adm->roles->pluck('name')->toarray())}}--}}
                        {{--                                '<td>' + info.activity[i].properties + '</td>' +--}}
                        {{--                                '<td>'+" {!!json_decode(+'data'.activity[i])!!} " +--}}
                        {{--                                // +data.activity[i].properties[0]+--}}
                        {{--                                '</td>' +--}}
                        {{--                                '<td>' + info.activity[i].created_at + '</td>' +--}}


                        {{--                                // '<td style="text-align:right;">' +--}}
                        {{--                                // '<a href="javascript:void(0);" class=" btn btn-info btn-sm item_edit" data-member_id="' + data.activity[i].id + '" > <i class="fas fa-edit"></i> </a>' + ' ' +--}}
                        {{--                                // '<a href="javascript:void(0);" class=" btn btn-danger btn-sm  item_delete" data-member_id="' + data.activity[i].id + '">   <i class="fas fa-trash"></i> </a>' +--}}
                        {{--                                // '</td>' +--}}
                        {{--                                '</tr>';--}}
                        {{--                        }--}}

                        {{--                        $('#activites_tbody').html(html);--}}



                        {{--                        // if (response.error == null) {--}}
                        {{--                        //     $('#name-m').val(response.user.name);--}}
                        {{--                        //     $('#email-m').val(response.user.email);--}}
                        {{--                        //     // $('#header_name').innerText=response.user.name;--}}
                        {{--                        //     // $('#password-field').val(response.user.password);--}}
                        {{--                        //     $('#header_name').text(response.user.name);--}}
                        {{--                        //     $('#header_email').text(response.user.email);--}}
                        {{--                        //     $('#header_create_at').text(response.user.created_at);--}}
                        {{--                        //--}}
                        {{--                        //     // $('.confirm').on('click', function () {--}}
                        {{--                        //     //     // me.text('running');--}}
                        {{--                        //     //     // window.location.reload();--}}
                        {{--                        //     // });--}}
                        {{--                        //--}}
                        {{--                        // } else {--}}
                        {{--                        //     if (response.error == 409) {--}}
                        {{--                        //--}}
                        {{--                        //         swal("Cancelled !!", response.message);--}}
                        {{--                        //--}}
                        {{--                        //     }--}}
                        {{--                        //     if (response.error == 421) {--}}
                        {{--                        //--}}
                        {{--                        //         swal("Cancelled !!", response.message);--}}
                        {{--                        //--}}
                        {{--                        //     }--}}
                        {{--                        //     if (response.error == 409) {--}}
                        {{--                        //--}}
                        {{--                        //         swal("Cancelled !!", response.message, "error");--}}
                        {{--                        //--}}
                        {{--                        //     } else {--}}
                        {{--                        //         // alert("Status can't be change.")--}}
                        {{--                        //     }--}}
                        {{--                        // }--}}
                        {{--                    },--}}
                        {{--success: function(data){--}}
                        {{--    {{print_r(json_decode(data))}}--}}
                        {{--    jQuery.each(data,function(key,row){--}}
                        {{--        jQuery('#activities_table1').append('<tr>');--}}
                        {{--        jQuery.each(row,function(key,column){--}}
                        {{--            jQuery('#activities_table1').append('<td>'+column+'</td>');--}}
                        {{--        });--}}
                        {{--        jQuery('#activities_table1').append('</tr>');--}}
                        {{--    });--}}
                        {{--}--}}
                    success: function (data) {

                        // '<td>' + JSON.parse(JSON.stringify( value.properties.)) +'</td>'+
                        // '<td>' + JSON.parse(JSON.stringify( value.properties.toString())) +'</td>'+

                        console.log(data);
                        var res = '';
                        var pro = '';

                        $.each(data.user_Log, function (key, value) {

                            $.each(value.properties, function (key, value) {

                                pro += '<span class="badge badge-secondary">'+key+' = ' + JSON.parse(JSON.stringify(value)) + '</span>&nbsp;,&nbsp;';

                            });
                            res +=
                                '<tr>' +
                                '<td>' + value.log_name + '</td>' +
                                '<td>' + value.description + '</td>' +
                                '<td>' + pro + '</td>' +

                                // '<td>' + JSON.stringify( value.properties.toString()) +'</td>'+
                                '<td>' + value.created_at + '</td>' +
                                '</tr>';

                            pro = '';


                        });

                        $('#activites_tbody').html(res);
                    },

                    // error: function () {
                    //     alert('Error occured');
                    // },
                    complete: function () {
                        console.log('ok');
                        // me.text('completed');
                        // me.attr('disabled', true);

                        me.data('requestRunning', false);
                        // window.location.reload();

                    }
                });

                // .val([1,2,3]).change();
                $('#selected_role').val(u_roles).trigger('change');
                $('#permission').val(u_per).trigger('change');
                $('#header_role').text(u_roles_name);

                $("#add_role_model").modal('show');
                //
                //
                // swal({
                //            title: "Are you sure you want to Approved ?",
                //            text: "",
                //            type: "warning",
                //            showCancelButton: true,
                //            confirmButtonColor: "#DD6B55",
                //            confirmButtonText: "Yes, Processed !!",
                //            cancelButtonText: "No, cancel it !!",
                //            closeOnConfirm: false,
                //            closeOnCancel: false
                //        },


                // function(isConfirm) {
                // if (isConfirm) {

                {{--                   var url: "{{route('admin.add_Users')}}";--}}

                if (me.data('requestRunning')) {
                    return;
                }

                me.data('requestRunning', true);
                // me.attr('disabled', true);
                //
                // me.text('requestRunning');



            $("#add_role_model").on('change', '#log_name', function (e) {

                console.log('user id', $('#user_id').val());
               var user_id=  $('#user_id').val();
                console.log('logname', $('#log_name').val());
               var filer_name = $('#log_name').val();
                $.ajax({
                    type: "get",
                    url: "{{route('admin.user_log_filer_byId')}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        user_id: user_id,
                        filer_name:filer_name,

                    },
                    success: function (data) {
                        // $("#add_role_model").modal('hide');
                        console.log(data);
                        var res = '';
                        var pro = '';

                        $.each(data.user_Log, function (key, value) {

                            $.each(value.properties, function (key, value) {

                                pro += '<span class="badge badge-secondary">'+key+' = ' + JSON.parse(JSON.stringify(value)) + '</span>&nbsp;,&nbsp;';

                            });
                            res +=
                                '<tr>' +
                                '<td>' + value.log_name + '</td>' +
                                '<td>' + value.description + '</td>' +
                                '<td>' + pro + '</td>' +

                                // '<td>' + JSON.stringify( value.properties.toString()) +'</td>'+
                                '<td>' + value.created_at + '</td>' +
                                '</tr>';

                            pro = '';


                        });

                        $('#activites_tbody').html(res);


                    },
                    {{--                // error: function () {--}}
                    {{--                //     alert('Error occured');--}}
                    {{--                // },--}}
                    {{--                complete: function () {--}}
                    {{--                    console.log('ok');--}}
                    {{--                    me.text('completed');--}}
                    {{--                    me.attr('disabled', true);--}}

                    {{--                    me.data('requestRunning', false);--}}
                    {{--                    // window.location.reload();--}}

                    {{--                }--}}
                    {{--            });--}}
                    {{--        }else {--}}
                    {{--            swal("Cancelled !!", "Hey, Cancelled the Transaction !!", "error");--}}
                    {{--        }--}}




                });



            });
            $("#add_role_model").on('click', '.submit_roles_form', function (e) {
                e.preventDefault();
                var me = $(this);
                var permission = $('#permission').val();
                var user_id = $('#user_id').val();
                var selected_role = $('#selected_role').val();
                var password = $('#password-m').val();

                console.log('permission', $('#permission').val());
                console.log('user id', $('#user_id').val());
                console.log('user Role', $('#selected_role').val());
                $.ajax({
                    beforeSend: (function () {
                        $('#password_error_m').hide();

                    }),
                    type: "POST",
                    url: "{{route('admin.add_roles_permissions_ajax')}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        permission: permission,
                        user_id: user_id,
                        selected_role: selected_role,
                        action: "add_roles",
                        password: password
                        // action: "removeRoles"
                    },
                    success: function (data) {
                        $("#add_role_model").modal('hide');

                        if (data.error == null) {
                            swal("Hey !!", "Roles and Permissions are Successfully Updated", "success");
                            $('.confirm').on('click', function () {
                                me.text('running');
                                window.location.reload();
                            });

                        } else {
                            if (data.error == 409) {

                                swal("Cancelled !!", data.message);

                            }
                            if (data.error == 421) {

                                swal("Cancelled !!", data.message);

                            }
                            if (data.error == 409) {

                                swal("Cancelled !!", data.message, "error");

                            } else {
                                // alert("Status can't be change.")
                            }
                        }
                    },
                    // error: function () {
                    //     alert('Error occured');
                    // },
                    error: function (data) {
                        if (data.status === 422) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                // $('#category_id_error').show().append(value + "<br/>"); //this is my div with messages
                                if ($.isPlainObject(value)) {
                                    $.each(value, function (key, value) {

                                        if (key == 'password') {
                                            $('#password_error_m').html(value);
                                            $('#password_error_m').show();

                                        }
                                    });

                                }

                            });
                        }

                    }
                    ,

                    complete: function () {
                        console.log('ok');
                        // me.text('completed');
                        // me.attr('disabled', true);

                        me.data('requestRunning', false);
                        // window.location.reload();

                    }
                },);

            });
            });


            $("#example1").on('click', '.remove_role', function (e) {
                e.preventDefault();
                var me = $(this);
                var user_id = $(this).data('uid');
                // var selected_role = $('#selected_role').val();

                // console.log('permission', $('#permission').val());
                console.log('user id', user_id);
                // console.log('user Role', $('#selected_role').val());
                swal({
                        title: "Are you sure you want to Remove Role ?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Processed !!",
                        cancelButtonText: "No, cancel it !!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },


                    function (isConfirm) {
                        if (isConfirm) {
                            // console.log(id);
                            $.ajax({
                                type: "POST",
                                url: "{{route('admin.add_roles_permissions_ajax')}}",
                                data: {
                                    "_token": "{{ csrf_token() }}",
                                    // permission: permission,
                                    user_id: user_id,
                                    // selected_role: selected_role,
                                    // action: "add_roles"
                                    action: "removeRoles"
                                },
                                success: function (response) {
                                    // $("#add_role_model").modal('hide');

                                    if (response.success) {
                                        swal("Hey !!", "Role Remove Successfully ", "success");
                                        $('.confirm').on('click', function () {
                                            me.text('running');
                                            window.location.reload();
                                        });

                                    } else {
                                        if (response.error == 409) {

                                            swal("Cancelled !!", response.message);

                                        }
                                        if (response.error == 421) {

                                            swal("Cancelled !!", response.message);

                                        }
                                        if (response.error == 417) {

                                            swal("Cancelled !!", response.message);

                                        }
                                        if (response.error == 409) {

                                            swal("Cancelled !!", response.message, "error");

                                        } else {
                                            // alert("Status can't be change.")
                                        }
                                    }
                                },
                                {{--                // error: function () {--}}
                                {{--                //     alert('Error occured');--}}
                                {{--                // },--}}
                                {{--                complete: function () {--}}
                                {{--                    console.log('ok');--}}
                                {{--                    me.text('completed');--}}
                                {{--                    me.attr('disabled', true);--}}

                                {{--                    me.data('requestRunning', false);--}}
                                {{--                    // window.location.reload();--}}

                                {{--                }--}}
                                {{--            });--}}
                                {{--        }else {--}}
                                {{--            swal("Cancelled !!", "Hey, Cancelled the Transaction !!", "error");--}}
                                {{--        }--}}




                            });


                        } else {
                            swal("Cancelled !!", "Hey, Cancelled the Request !!", "error");
                        }
                    });
                {{--swal({--}}
                {{--        title: "Are you sure you want to Remove ?",--}}
                {{--        text: "",--}}
                {{--        type: "warning",--}}
                {{--        showCancelButton: true,--}}
                {{--        confirmButtonColor: "#DD6B55",--}}
                {{--        confirmButtonText: "Yes, Processed !!",--}}
                {{--        cancelButtonText: "No, cancel it !!",--}}
                {{--        closeOnConfirm: false,--}}
                {{--        closeOnCancel: false--}}
                {{--    },--}}
                {{--if (isConfirm) {--}}
                {{--    if (isConfirm){--}}
                {{--                $.ajax({--}}
                {{--                    type: "POST",--}}
                {{--                    url: "{{route('admin.add_roles_permissions_ajax')}}",--}}
                {{--                    data: {--}}
                {{--                        "_token": "{{ csrf_token() }}",--}}
                {{--                        permission: permission,--}}
                {{--                        user_id: user_id,--}}
                {{--                        selected_role: selected_role,--}}
                {{--                        // action: "add_roles"--}}
                {{--                        action: "removeRoles"--}}
                {{--                    },--}}
                {{--                    success: function (response) {--}}
                {{--                        $("#add_role_model").modal('hide');--}}

                {{--                        if (response.success) {--}}
                {{--                            // swal("Hey !!", "Deposit Approved Successfully ", "success");--}}
                {{--                            // $('.confirm').on('click', function () {--}}
                {{--                            //     me.text('running');--}}
                {{--                            //     // window.location.reload();--}}
                {{--                            // });--}}

                {{--                        } else {--}}
                {{--                            if (response.error == 409) {--}}

                {{--                                swal("Cancelled !!", response.message);--}}

                {{--                            }--}}
                {{--                            if (response.error == 421) {--}}

                {{--                                swal("Cancelled !!", response.message);--}}

                {{--                            }--}}
                {{--                            if (response.error == 409) {--}}

                {{--                                swal("Cancelled !!", response.message, "error");--}}

                {{--                            } else {--}}
                {{--                                // alert("Status can't be change.")--}}
                {{--                            }--}}
                {{--                        }--}}
                {{--                    },--}}
                {{--    --}}{{--                // error: function () {--}}
                {{--    --}}{{--                //     alert('Error occured');--}}
                {{--    --}}{{--                // },--}}
                {{--    --}}{{--                complete: function () {--}}
                {{--    --}}{{--                    console.log('ok');--}}
                {{--    --}}{{--                    me.text('completed');--}}
                {{--    --}}{{--                    me.attr('disabled', true);--}}

                {{--    --}}{{--                    me.data('requestRunning', false);--}}
                {{--    --}}{{--                    // window.location.reload();--}}

                {{--    --}}{{--                }--}}
                {{--    --}}{{--            });--}}
                {{--    --}}{{--        }else {--}}
                {{--    --}}{{--            swal("Cancelled !!", "Hey, Cancelled the Transaction !!", "error");--}}
                {{--    --}}{{--        }--}}




            });
            $("#example1").on('click', '.removePermission', function (e) {
                e.preventDefault();
                var me = $(this);
                var user_id = $(this).data('uid');
                // var selected_role = $('#selected_role').val();

                // console.log('permission', $('#permission').val());
                console.log('user id', user_id);
                // console.log('user Role', $('#selected_role').val());
                swal({
                        title: "Are you sure you want to Remove Extra Permissions ?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Processed !!",
                        cancelButtonText: "No, cancel it !!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },


                    function (isConfirm) {
                        if (isConfirm) {
                            // console.log(id);
                            $.ajax({
                                type: "POST",
                                url: "{{route('admin.add_roles_permissions_ajax')}}",
                                data: {
                                    "_token": "{{ csrf_token() }}",
                                    // permission: permission,
                                    user_id: user_id,
                                    // selected_role: selected_role,
                                    // action: "add_roles"
                                    action: "removePermission"
                                },
                                success: function (response) {
                                    // $("#add_role_model").modal('hide');

                                    if (response.success) {
                                        swal("Hey !!", "Extra Permissions Remove Successfully ", "success");
                                        $('.confirm').on('click', function () {
                                            me.text('running');
                                            window.location.reload();
                                        });

                                    } else {
                                        if (response.error == 409) {

                                            swal("Cancelled !!", response.message);

                                        }
                                        if (response.error == 421) {

                                            swal("Cancelled !!", response.message);

                                        }
                                        if (response.error == 417) {

                                            swal("Cancelled !!", response.message);

                                        }
                                        if (response.error == 409) {

                                            swal("Cancelled !!", response.message, "error");

                                        } else {
                                            // alert("Status can't be change.")
                                        }
                                    }
                                },
                                {{--                // error: function () {--}}
                                {{--                //     alert('Error occured');--}}
                                {{--                // },--}}
                                {{--                complete: function () {--}}
                                {{--                    console.log('ok');--}}
                                {{--                    me.text('completed');--}}
                                {{--                    me.attr('disabled', true);--}}

                                {{--                    me.data('requestRunning', false);--}}
                                {{--                    // window.location.reload();--}}

                                {{--                }--}}
                                {{--            });--}}
                                {{--        }else {--}}
                                {{--            swal("Cancelled !!", "Hey, Cancelled the Transaction !!", "error");--}}
                                {{--        }--}}




                            });


                        } else {
                            swal("Cancelled !!", "Hey, Cancelled the Request !!", "error");
                        }
                    });
                {{--swal({--}}
                {{--        title: "Are you sure you want to Remove ?",--}}
                {{--        text: "",--}}
                {{--        type: "warning",--}}
                {{--        showCancelButton: true,--}}
                {{--        confirmButtonColor: "#DD6B55",--}}
                {{--        confirmButtonText: "Yes, Processed !!",--}}
                {{--        cancelButtonText: "No, cancel it !!",--}}
                {{--        closeOnConfirm: false,--}}
                {{--        closeOnCancel: false--}}
                {{--    },--}}
                {{--if (isConfirm) {--}}
                {{--    if (isConfirm){--}}
                {{--                $.ajax({--}}
                {{--                    type: "POST",--}}
                {{--                    url: "{{route('admin.add_roles_permissions_ajax')}}",--}}
                {{--                    data: {--}}
                {{--                        "_token": "{{ csrf_token() }}",--}}
                {{--                        permission: permission,--}}
                {{--                        user_id: user_id,--}}
                {{--                        selected_role: selected_role,--}}
                {{--                        // action: "add_roles"--}}
                {{--                        action: "removeRoles"--}}
                {{--                    },--}}
                {{--                    success: function (response) {--}}
                {{--                        $("#add_role_model").modal('hide');--}}

                {{--                        if (response.success) {--}}
                {{--                            // swal("Hey !!", "Deposit Approved Successfully ", "success");--}}
                {{--                            // $('.confirm').on('click', function () {--}}
                {{--                            //     me.text('running');--}}
                {{--                            //     // window.location.reload();--}}
                {{--                            // });--}}

                {{--                        } else {--}}
                {{--                            if (response.error == 409) {--}}

                {{--                                swal("Cancelled !!", response.message);--}}

                {{--                            }--}}
                {{--                            if (response.error == 421) {--}}

                {{--                                swal("Cancelled !!", response.message);--}}

                {{--                            }--}}
                {{--                            if (response.error == 409) {--}}

                {{--                                swal("Cancelled !!", response.message, "error");--}}

                {{--                            } else {--}}
                {{--                                // alert("Status can't be change.")--}}
                {{--                            }--}}
                {{--                        }--}}
                {{--                    },--}}
                {{--    --}}{{--                // error: function () {--}}
                {{--    --}}{{--                //     alert('Error occured');--}}
                {{--    --}}{{--                // },--}}
                {{--    --}}{{--                complete: function () {--}}
                {{--    --}}{{--                    console.log('ok');--}}
                {{--    --}}{{--                    me.text('completed');--}}
                {{--    --}}{{--                    me.attr('disabled', true);--}}

                {{--    --}}{{--                    me.data('requestRunning', false);--}}
                {{--    --}}{{--                    // window.location.reload();--}}

                {{--    --}}{{--                }--}}
                {{--    --}}{{--            });--}}
                {{--    --}}{{--        }else {--}}
                {{--    --}}{{--            swal("Cancelled !!", "Hey, Cancelled the Transaction !!", "error");--}}
                {{--    --}}{{--        }--}}




            });


        });


    </script>
@endsection


