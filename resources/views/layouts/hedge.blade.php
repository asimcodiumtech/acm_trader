<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@if(!empty(auth()->user()->admin))
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ACM-Traders</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/acm_trader.png')}}">
        <link rel="stylesheet" href="{{ asset('refral/plugins/fontawesome-free/css/all.min.css')}}">
        <!-- Ionicons -->
{{--        <link rel="stylesheet" href="{{ asset('refral/ionicons/2.0.1/css/ionicons.min.css')}}">--}}
        <!-- Tempusdominus Bbootstrap 4 -->
{{--        <link rel="stylesheet" href="{{ asset('refral/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">--}}
        <!-- iCheck -->
{{--        <link rel="stylesheet" href="{{ asset('refral/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">--}}
        <!-- JQVMap -->
{{--        <link rel="stylesheet" href="{{ asset('refral/plugins/jqvmap/jqvmap.min.css')}}">--}}
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ asset('refral/dist/css/adminlte.min.css')}}">
        <!-- overlayScrollbars -->
{{--        <link rel="stylesheet" href="{{ asset('refral/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">--}}
        <!-- Daterange picker -->
        <link rel="stylesheet" href="{{ asset('refral/plugins/daterangepicker/daterangepicker.css')}}">
        <!-- summernote -->
{{--        <link rel="stylesheet" href="{{ asset('refral/plugins/summernote/summernote-bs4.css')}}">--}}
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('refral/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">

        <link rel="stylesheet" href="{{ asset('css/sweetalert/sweetalert.css')}}">
{{--        <link rel="stylesheet" href="{{ asset('css/toastr/toastr.min.css')}}">--}}



{{--        <link href="{{ asset('refral/dist/css/jquery.orgchart.css')}}" media="all" rel="stylesheet" type="text/css" />--}}






    </head>
{{--<head>--}}
{{--  <meta charset="utf-8">--}}
{{--  <meta http-equiv="X-UA-Compatible" content="IE=edge">--}}
{{--  <title>ACM-Traders</title>--}}
{{--  <!-- Tell the browser to be responsive to screen width -->--}}
{{--  <meta name="viewport" content="width=device-width, initial-scale=1">--}}
{{--  <!-- Font Awesome -->--}}
{{--  <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/acm_trader.png')}}">--}}
{{--  <link rel="stylesheet" href="{{ asset('refral/plugins/fontawesome-free/css/all.min.css')}}">--}}
{{--  <!-- Ionicons -->--}}
{{--  <link rel="stylesheet" href="{{ asset('refral/ionicons/2.0.1/css/ionicons.min.css')}}">--}}
{{--  <!-- Tempusdominus Bbootstrap 4 -->--}}
{{--    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css">--}}
{{--  <link rel="stylesheet" href="{{ asset('refral/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">--}}
{{--  <!-- iCheck -->--}}
{{--  <link rel="stylesheet" href="{{ asset('refral/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">--}}
{{--  <!-- JQVMap -->--}}
{{--  <link rel="stylesheet" href="{{ asset('refral/plugins/jqvmap/jqvmap.min.css')}}">--}}
{{--  <!-- Theme style -->--}}
{{--  <link rel="stylesheet" href="{{ asset('refral/dist/css/adminlte.min.css')}}">--}}
{{--  <!-- overlayScrollbars -->--}}
{{--  <link rel="stylesheet" href="{{ asset('refral/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">--}}
{{--  <!-- Daterange picker -->--}}
{{--  <link rel="stylesheet" href="{{ asset('refral/plugins/daterangepicker/daterangepicker.css')}}">--}}
{{--  <!-- summernote -->--}}
{{--  <link rel="stylesheet" href="{{ asset('refral/plugins/summernote/summernote-bs4.css')}}">--}}
{{--  <!-- Google Font: Source Sans Pro -->--}}
{{--  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">--}}
{{--  <link rel="stylesheet" href="{{ asset('refral/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">--}}

{{--  <link rel="stylesheet" href="{{ asset('css/sweetalert/sweetalert.css')}}">--}}
{{--  <link rel="stylesheet" href="{{ asset('css/toastr/toastr.min.css')}}">--}}



{{--  <link href="{{ asset('refral/dist/css/jquery.orgchart.css')}}" media="all" rel="stylesheet" type="text/css" />--}}
{{--    <meta charset="utf-8">--}}
{{--    <meta http-equiv="X-UA-Compatible" content="IE=edge">--}}
{{--    <title>ACM-Traders</title>--}}
{{--    <!-- Tell the browser to be responsive to screen width -->--}}
{{--    <meta name="viewport" content="width=device-width, initial-scale=1">--}}
{{--    <!-- Font Awesome -->--}}
{{--    <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/acm_trader.png')}}">--}}
{{--    <link rel="stylesheet" href="{{ asset('refral/plugins/fontawesome-free/css/all.min.css')}}">--}}
{{--    <!-- Ionicons -->--}}
{{--    <link rel="stylesheet" href="{{ asset('refral/ionicons/2.0.1/css/ionicons.min.css')}}">--}}
{{--    <!-- Tempusdominus Bbootstrap 4 -->--}}
{{--    <link rel="stylesheet" href="{{ asset('refral/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">--}}
{{--    <!-- iCheck -->--}}
{{--    <link rel="stylesheet" href="{{ asset('refral/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">--}}
{{--    <!-- JQVMap -->--}}
{{--    <link rel="stylesheet" href="{{ asset('refral/plugins/jqvmap/jqvmap.min.css')}}">--}}
{{--    <!-- Theme style -->--}}
{{--    <link rel="stylesheet" href="{{ asset('refral/dist/css/adminlte.min.css')}}">--}}
{{--    <!-- overlayScrollbars -->--}}
{{--    <link rel="stylesheet" href="{{ asset('refral/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">--}}
{{--    <!-- Daterange picker -->--}}
{{--    <link rel="stylesheet" href="{{ asset('refral/plugins/daterangepicker/daterangepicker.css')}}">--}}
{{--    <!-- summernote -->--}}
{{--    <link rel="stylesheet" href="{{ asset('refral/plugins/summernote/summernote-bs4.css')}}">--}}
{{--    <!-- Google Font: Source Sans Pro -->--}}
{{--    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">--}}
{{--    <link rel="stylesheet" href="{{ asset('refral/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">--}}

{{--    <link rel="stylesheet" href="{{ asset('css/sweetalert/sweetalert.css')}}">--}}
{{--    <link rel="stylesheet" href="{{ asset('css/toastr/toastr.min.css')}}">--}}



{{--    <link href="{{ asset('refral/dist/css/jquery.orgchart.css')}}" media="all" rel="stylesheet" type="text/css" />--}}












{{--</head>--}}
<body class="hold-transition sidebar-mini layout-fixed">
    @include('partials.hedge_scripts')
<div class="wrapper">

  <!-- Navbar -->
 @include('partials.hedge_navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  @include('partials.hedge_sidebar')


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- /.content-header -->

    <!-- Main content -->
    @yield('dashboardcontent')

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
   <!-- <center><strong>Copyright &copy; 2014-2019 <a href="">Codium Technologies</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.2
    </div>
    </center>-->

  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->

</body>

@else






@endif
</html>
