@extends('layouts.hedge')

@section('dashboardcontent')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 style="color: white;">My Hedge Information</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Hedge</a></li>
                        <li class="breadcrumb-item active">My Hedge</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="row">
            <div class="col-12">

                <div class="card">
                    <div class="card-header" style="background-color: #35354B;color: white;">
                        <h3 class="card-title">Hedge Booking List</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body" style="background-color: #35354B;color: white;">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped example1">
                                <thead style="background-color:#131311; color:white;">
                                <tr>
                                    <th>Hedge Title</th>
                                    <th>Hedge Code</th>
                                    <th>Slot</th>
                                    <th>Invested Amount</th>

                                    <th>Total Profit</th>
{{--                                    <th>Type</th>--}}

{{--                                    <th>Status</th>--}}

{{--                                    <th>Total Amount</th>--}}
                                </tr>
                                </thead>
                                <tbody style="background-color: #35354B;color: white;">

                                @foreach($check as $user)
                                    <tr>
                                        <td>
                                            {{$user['hedge_title']}}
                                        </td>
                                        <td>
                                            {{$user['hedge_code']}}
                                        </td>
                                        <td>
                                            {{$user['slot']}}
                                        </td>
                                        <td>
                                            {{$user['invested_amount']}}
                                        </td>

                                        <td>
{{--                                            {{Carbon\Carbon::parse($user['created_at'])->format('Y-m-d')}}--}}
                                            {{$user['total_profit']}}
                                        </td>
{{--                                        <td>--}}
{{--                                            {{$user['type']}}--}}
{{--                                        </td>--}}

{{--                                        <td>--}}
{{--                                            @if($user['deposite_status']==1)--}}
{{--                                                <button class="btn btn-success"><b> Processeed </b></button>--}}
{{--                                            @elseif($user['deposite_status']==2)--}}
{{--                                                <button class="btn btn-danger"><b> pending </b></button>--}}

{{--                                            @else--}}
{{--                                                <button class="btn btn-danger " data-id="{{$user['id']}}"><b> No--}}
{{--                                                        Applied </b></button>--}}
{{--                                            @endif--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            @if($user['deposite_status']==1)--}}
{{--                                                {{$user['total_amount']}}--}}


{{--                                            @else--}}
{{--                                                <button class="btn btn-success " data-id="{{$user['id']}}"><b>--}}
{{--                                                        Withdraw </b></button>--}}
{{--                                            @endif--}}
{{--                                        </td>--}}
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot style="background-color:#131311; color:white;">
                                <tr>
                                    <th>Hedge Title</th>
                                    <th>Hedge Code</th>
                                    <th>Slot</th>
                                    <th>Invested Amount</th>

                                    <th>Total Profit</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
    </section>


    <script>
        $(function () {
            $(".example1").DataTable({
                responsive: true
            });
            $('.example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });

        $(document).ready(function () {
            var val;
            var id;


            $('.example1').on('click', '.apply', function () {
                id = $(this).data('id');

                swal({
                    title: "Enter Your BTC/ Account No",
                    text: "Please complete all field !!",
                    type: "input",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    animation: "slide-from-top",
                    inputPlaceholder: "eg. 1312373",
                    closeOnCancel: false
                }, function (inputValue) {
                    if (inputValue === false) return false;
                    if (inputValue === "") {
                        swal.showInputError("You Enter your BTC/Acct NO");
                        return false
                    }
                    sweatalert(inputValue);
                });


                function sweatalert(inputValue) {
                    var url = '<?= route('withdraw.request')?>';
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {"_token": "{{ csrf_token() }}", id: id, withdrawId: inputValue, withdrawStatus: 2},
                        success: function (response) {
                            console.log(response);
                            if (response.success) {

                                swal("Request Submit to Admin", "Successfully !!", "success");
                                $('.confirm').on('click', function () {
                                    window.location.reload();
                                });

                            } else {
                                if (response.error == 409) {

                                    alert("User already has an active subscription.");
                                } else {
                                    $(this).parent().parent().parent().remove();

                                    $('body').find('.sweet-overlay').remove();
                                    $('body').find('.hideSweetAlert').remove();

                                    swal("Request Rejected", "You can't fill all fields !!", "error");
                                }
                            }
                        },
                        error: function () {
                            alert('Error occured');
                        }
                    });


                }


                $('.cancel').click(function () {
                    $('body').find('.sweet-overlay').remove();
                    // $(this).parent().parent().closest(".sweet-overlay").remove();
                    $(this).parent().parent().remove();


                });


            });

        });

    </script>
@endsection
