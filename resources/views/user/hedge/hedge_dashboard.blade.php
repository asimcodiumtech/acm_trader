@extends('layouts.hedge')

@section('dashboardcontent')
    <style>
        body {
            margin: 0;
        }

        .content {
            padding: 10px 20px;
        }

        .content p {
            font-family: sans-serif;
        }

        /******/

        .ticker-container {
            width: 100%;
            overflow: hidden;
        }

        .ticker-canvas {
            width: calc((200px * 6) + 2px);
            /*
            200px = minimum width of ticker item before widget script starts removing ticker codes
            15 = number of ticker codes
            2px = accounts for 1px external border
            */
            -webkit-animation-iteration-count: infinite;
            animation-iteration-count: infinite;
            -webkit-animation-timing-function: linear;
            animation-timing-function: linear;
            -webkit-animation-name: ticker-canvas;
            animation-name: ticker-canvas;
            -webkit-animation-duration: 20s;
            animation-duration: 20s;
        }

        .ticker-canvas:hover {
            animation-play-state: paused
        }

        @-webkit-keyframes ticker-canvas {
            0% {
                -webkit-transform: translate3d(0, 0, 0);
                transform: translate3d(0, 0, 0);
                visibility: visible;
            }
            100% {
                -webkit-transform: translate3d(-100%, 0, 0);
                transform: translate3d(-100%, 0, 0);
            }
        }

        @keyframes ticker-canvas {
            0% {
                -webkit-transform: translate3d(0, 0, 0);
                transform: translate3d(0, 0, 0);
                visibility: visible;
            }
            100% {
                -webkit-transform: translate3d(-100%, 0, 0);
                transform: translate3d(-100%, 0, 0);
            }
        }

        .tradingview-widget-container {
            position: relative;
        }

        .tradingview-widget-container iframe {
            position: absolute;
            top: 0;
        }

        .tradingview-widget-container iframe:nth-of-type(2) {
            left: 100%;
        }

        .sweet-alert select {
            width: 100%;
            box-sizing: border-box;
            border-radius: 3px;
            border: 1px solid #d7d7d7;
            height: 43px;
            margin-top: 10px;
            margin-bottom: 17px;
            font-size: 18px;
            background-color: #131311;
            color: white;
        }
    </style>
    <div class="container">
        {{--        <div class="row">--}}
        {{--            <div class="col-md-12">--}}
        {{--                <a href="{{route('userlaunchnotice')}}"> <img class="banner" alt="banner" src="../whitelanding/assets/images/banner.jpeg" width="100%"></a>--}}
        {{--            </div>--}}
        {{--        </div>--}}
    </div>
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 " style="color: white;">Hedge Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Hedge</a></li>
                        <li class="breadcrumb-item active">WithDrawls List</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success" style="background-color: #35354b!important;">
                        <div class="inner">
                            <div class="row">
                                <div class="col-lg-12 col-6">
                                    <p>No of Hedges</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-4">
                                    {{--                                    <span class="btn btn-sm btn-info "--}}
                                    {{--                                          style="background-color: #00c245;border-color: #00c245;"> </span>--}}
                                    {{--                                    <i class="fa fa-bar-chart" aria-hidden="true"></i>--}}
                                </div>
                                <div class="col-lg-6 col-4">
                                    <p>{{$hedgeCount}}<br><span style="font-size: 13px;">Hedges</span></p>
                                </div>

                            </div>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>

                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success" style="background-color: #35354b!important;">
                        <div class="inner">
                            <div class="row">
                                <div class="col-lg-12 col-6">
                                    <p>Total Withdrawable</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-4">
                                    {{--                                    <span class="btn btn-sm btn-info " style="background-color: red;border-color: red;">$ </span>--}}

                                </div>
                                <div class="col-lg-6 col-4">
                                    <p>{{$hedgeWithdrawable}}<br><span style="font-size: 13px;">USD</span></p>
                                </div>

                            </div>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>

                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info" style="background-color: #35354b!important;">
                        <div class="inner">
                            <div class="row">
                                <div class="col-lg-3 col-6">
                                    <p>Balance</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-4">
                                    {{--                                    <p>{{$todayP}} USD<br><span style="font-size: 13px;">Today Profit</span></p>--}}

                                </div>
                                <div class="col-lg-6 col-4">
                                    <p>{{round($balance, 2)}}<br><span style="font-size: 13px;">USD</span></p>
                                </div>

                            </div>
                            <!-- <div class="row">
                               <div class="col-lg-6 col-4">
                                       <p>Today Profit<br>USD</p>
                               </div>
                               <div class="col-lg-6 col-4">
                                       <p>USD</p>
                               </div>
                            </div> -->
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>

                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-danger" style="background-color: #35354b!important;">
                        <div class="inner">
                            <div class="row">
                                <div class="col-lg-12 col-12">
                                    <p>Total Profit</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-4">
                                    {{--                                    <p>{{$leftwithdrawable}} USD <br><span style="font-size: 13px;">Left</span></p>--}}

                                </div>
                                <div class="col-lg-6 col-4">
                                    <p>{{$total_profit}} <br><span style="font-size: 13px;">USD</span></p>
                                </div>

                            </div>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>

                    </div>
                </div>
                <!-- ./col -->
            </div>
            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <div class="col-md-8 col-lg-8 col-xl-8 col-xxl-8">


                    <div class="card card-primary card-outline card-outline-tabs"
                         style="background-color: #35354b!important;">
                        <div class="card-header  border-bottom-0">
                            {{--                            <h5 class="card-title">Card title</h5>--}}
                            <h1 class=" card-title" style="color: white;">Upcoming Hedges</h1>
                        </div>
                        <div class="card-body">
                            <table id="example1" class="  table table-bordered table-striped ">
                                <thead style="background-color:#131311; color:white;">
                                <tr>

                                    <th>Title</th>
                                    <th>Amount</th>
                                    <th>Total Invested</th>

                                    <th>Available Slot</th>
                                    <th>Code</th>
                                    {{--                                    <th>Invested</th>--}}
                                    <th>Opening Date</th>
                                    <th>Closing Date</th>
                                    <th>Due Date</th>
                                    <th>Action</th>

                                </tr>
                                </thead>
                                <tbody style="background-color: #35354B;color: white;">
                                @foreach($hedge as $hed)
                                    <tr>

                                        <td>{{$hed['title']}}</td>
                                        <td>{{$hed['amount']}}</td>
                                        <td>{{$hed['total_invested']}}</td>
                                        <td>{{$hed['available_slot']}}</td>
                                        <td>{{$hed['code']}}</td>
                                        {{--                                    <script>--}}
                                        {{--                                        var app = @json($hed->roles);--}}
                                        {{--                                        app->name;--}}
                                        {{--                                    </script>--}}
                                        {{--                                    <td> {{$hed->getRoleNames()}}</td>--}}
                                        {{--                                        <td> {{$hed->total_invested}}</td>--}}
                                        <td> {{Carbon\Carbon::parse($hed['slot_opening_date'])->format('y-m-d')}}</td>
                                        <td> {{\Carbon\Carbon::parse($hed['slot_closing_date'])->format('y-m-d')}}</td>
                                        <td> {{Carbon\Carbon::parse($hed['due_date'])->format('y-m-d')}}</td>

                                        {{--                                            <td> {{implode($hed->permissions->pluck('name')->toarray())}}</td>--}}
                                        {{--                                            <td> {{$hed->getAllPermissions()}}</td>--}}

                                        <td style="text-align:right;">
                                            {{--                                                <button class="btn btn-success  add_hedge"--}}
                                            {{--                                                        type="button" data-uid="{{$hed->id}}"--}}

                                            {{--                                                ><i class="fas fa-hedge-circle"></i> &nbsp;View Profile--}}
                                            {{--                                                </button>--}}

                                            <button class="btn btn-success  book_hedge" type="button"
                                                    data-hedgeid="{{$hed['id']}}"
                                                    data-hedge_amount="{{$hed['amount']}}"
                                                    data-hedge_available_slot="{{$hed['available_slot']}}"

                                            >
                                                &nbsp;Booking
                                            </button>


                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot style="background-color:#131311; color:white;">
                                <tr>

                                    <th>Title</th>
                                    <th>Amount</th>
                                    <th>Total Invested</th>
                                    <th>Slot</th>
                                    <th>Code</th>
                                    {{--                                    <th>Invested</th>--}}
                                    <th>Opening Date</th>
                                    <th>Closing Date</th>
                                    <th>Due Date</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card -->
                    </div>


                </div>
                <div class="col-md-4 col-lg-4 col-xl-4 col-xxl-4" style="">
                    <div class="card">
                        <div class="card-header" style="background-color: #35354b;color:white">
                            <h3 class="card-title">STATS</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <!-- /.card-header -->

                        <!-- /.card-body -->
                        <div class="card-footer bg-white p-0">
                            <ul class="nav nav-pills flex-column" style="background-color: #35354b;">
                                <li class="nav-item">
                                    <h class="nav-link">
                                        TOTAL USD INVESTED

                                    </h>
                                    <p href="#" class="nav-link" style="font-weight: bold;">
{{--                                        @if(in_array($hedgeInvested,old('hedgeInvested',[])))@endif--}}
                                        {{$hedgeInvested}}
{{--                                        {{number_format($invested_amount,2)}}--}}
{{--                                        {{round($invested_amount, 2)}}--}}

                                    </p>

                                </li>
{{--                                <li class="nav-item">--}}
{{--                                    <h8 class="nav-link">--}}
{{--                                        Total <span style="color:#07c1ca">USD</span> IN ASSETS ON EXCHANGE :--}}

{{--                                    </h8>--}}
{{--                                    <h3 class="nav-link">--}}
{{--                                        @if(round($avg, 2)< 0)--}}
{{--                                            <i class="fas fa-wallet" style="color:red"></i> <span--}}
{{--                                                style="color:red;text-align: center;">{{round($exchange2ndtime,2)}}({{round($avg, 2)}} %)--}}
{{--                              </span>--}}
{{--                                        @else--}}
{{--                                            <i class="fas fa-wallet" style="color:#07c887"></i> <span--}}
{{--                                                style="color:#07c887;text-align: center;">{{round($exchange2ndtime,2)}}({{round($avg, 2)}} %)--}}
{{--                              </span>--}}

{{--                                        @endif--}}


{{--                                    </h3>--}}


{{--                                </li>--}}
                                <li class="nav-item">

                                    <h4 class="nav-link">
                                        <i class="fa fa-receipt" style="color:red"></i>
                                        My Hedges Today's Profits

{{--                                    </h4>--}}
                                    @foreach($myHedgeListPer as $myhedge)
                                    <h5 class="nav-link">
                                        <i class="fa fa-receipt" style="color:#07c1ca;display:none;"></i>
                                        {{$myhedge['title']}} : <span style="font-weight: bold;color:  @if($myhedge['percentage']>=0)green;@else red; @endif}}">{{$myhedge['percentage']}}%</span>

                                    </h5>
                                   @endforeach
                                    <p href="#" class="nav-link" style="text-align: center;margin-top: 15%;">
                                        <button class="btn btn-primary withdraw" data-id="{{auth()->user()->id}}"
                                                style="color: black;
    background: #e7efef;
    border: #d4dad9 1px solid;" disabled>Withdraw
                                        </button>
{{--                                        <button class="btn btn-primary deposit" data-id="{{auth()->user()->id}}" style="color: black;--}}
{{--    background: #e7efef;--}}
{{--    border: #d4dad9 1px solid;">Fund Account--}}
{{--                                        </button>--}}

                                    </p>
                                </li>


                            </ul>
                        </div>
                        <!-- /.footer -->
                    </div>
                </div>

                <!--   <div class="row">

                   <div class="col-md-6 col-lg-6 col-xl-6 col-xxl-6">
                  <div class="card-body" style="    background-color: #00800059;
            height: 192px;
            width: 715px;
        ">

                      </div>
                  </div>
              </div> --></div>
        </div><!-- /.container-fluid -->
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12 col-xxl-12">
{{--                @if(round($avg, 2) >= 0 )--}}
                    <div class="card"
                         style="    background-color: #00800059;border: #09950b 1px solid; text-align: center;margin-left: 0%;margin-top: 4%;">
                        <h style="font-size: 42px;
    color: green;"> Total Today's Profit
                        </h>
                        <br>
                        <h style="font-size: 42px;
    color: green;"> {{round($avg, 2)}} USD
                        </h>

                    </div>

            </div>
            {{--            <div class="col-md-4 col-lg-4 col-xl-4 col-xxl-4">--}}
            {{--                <div class="card" style="margin-top: 6%; height:19%;background-color: #35354b;">--}}
            {{--                    <!-- <div class="card-header" style="background-color: #35354b;color:white">--}}



            {{--                    </div> -->--}}
            {{--                    <ul class="nav nav-pills flex-column" style="background-color: #35354b;">--}}
            {{--                        <li class="nav-item" style="background-color: #35354b;color:white">--}}

            {{--                            <h4 class="nav-link">--}}

            {{--                                Referral Link--}}

            {{--                            </h4>--}}


            {{--                            <p href="#" class="nav-link" style="text-align: center;">--}}

            {{--                            <div class="input-group">--}}
            {{--                                <!-- <p type="text" class="form-control" >Helo </p> -->--}}
            {{--                                <input type="text" class="form-control"--}}
            {{--                                       value="{{route('register')}}/?id={{$reffKey}}" id="myInput" readonly>--}}
            {{--                                <span class="input-group-btn">--}}
            {{--    <button class="btn btn-success" type="button" onclick="myFunction()">Copy</button>--}}
            {{--  </span>--}}
            {{--                            </div>--}}


            {{--                            </p>--}}
            {{--                        </li>--}}
            {{--                    </ul>--}}


            {{--                </div>--}}

            {{--            </div>--}}
        </div>


        <div class="row">
            <!-- Left col -->
            {{--            <div class="col-md-12 col-lg-12 col-xl-12 col-xxl-12">--}}
            {{--                <!-- TradingView Widget BEGIN -->--}}
            {{--                <div class="ticker-container">--}}
            {{--                    <div class="ticker-canvas">--}}
            {{--                        <div class="tradingview-widget-container">--}}
            {{--                            <div class="tradingview-widget-container__widget"></div>--}}
            {{--                            <script type="text/javascript"--}}
            {{--                                    src="https://s3.tradingview.com/external-embedding/embed-widget-tickers.js">--}}
            {{--                                {--}}
            {{--                                    "symbols"--}}
            {{--                                :--}}
            {{--                                    [--}}
            {{--                                        {--}}
            {{--                                            "proName": "OANDA:SPX500USD",--}}
            {{--                                            "title": "S&P 500"--}}
            {{--                                        },--}}
            {{--                                        {--}}
            {{--                                            "proName": "OANDA:NAS100USD",--}}
            {{--                                            "title": "Nasdaq 100"--}}
            {{--                                        },--}}
            {{--                                        {--}}
            {{--                                            "proName": "FX_IDC:EURUSD",--}}
            {{--                                            "title": "EUR/USD"--}}
            {{--                                        },--}}
            {{--                                        {--}}
            {{--                                            "proName": "BITSTAMP:BTCUSD",--}}
            {{--                                            "title": "BTC/USD"--}}
            {{--                                        },--}}
            {{--                                        {--}}
            {{--                                            "proName": "BITSTAMP:ETHUSD",--}}
            {{--                                            "title": "ETH/USD"--}}
            {{--                                        }--}}
            {{--                                    ],--}}
            {{--                                        "colorTheme"--}}
            {{--                                :--}}
            {{--                                    "dark",--}}
            {{--                                        "isTransparent"--}}
            {{--                                :--}}
            {{--                                    false,--}}
            {{--                                        "locale"--}}
            {{--                                :--}}
            {{--                                    "en"--}}
            {{--                                }--}}
            {{--                            </script>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--                <!-- TradingView Widget END -->--}}

            {{--            </div>--}}
        </div>


        <!-- /.row (main row) -->
        <!-- Bootstrap4 CSS - -->
    {{--        <link rel="stylesheet" href="https://bootswatch.com/4/superhero/bootstrap.css" crossorigin="anonymous">--}}

    <!-- Note - If your website not use Bootstrap4 CSS as main style, please use custom css style below and delete css line above.
        It isolate Bootstrap CSS to a particular class 'bootstrapiso' to avoid css conflicts with your site main css style -->
        <!-- <link rel="stylesheet" href="css/superhero.min.css" crossorigin="anonymous"> -->


        <!-- JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"
                crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
                crossorigin="anonymous"></script>
    {{--        <script defer src="https://use.fontawesome.com/releases/v5.12.0/js/all.js" crossorigin="anonymous"></script>--}}
                <script src="<?php echo CRYPTOBOX_JS_FILES_PATH; ?>support.min.js" crossorigin="anonymous"></script>

    <!-- CSS for Payment Box -->
        <style>
            html {
                font-size: 14px;
            }

            @media (min-width: 768px) {
                html {
                    font-size: 16px;
                }

                .tooltip-inner {
                    max-width: 350px;
                }
            }

            .mncrpt .container {
                max-width: 980px;
            }

            .mncrpt .box-shadow {
                box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .05);
            }

            img.radioimage-select {
                padding: 7px;
                border: solid 2px #ffffff;
                margin: 7px 1px;
                cursor: pointer;
                box-shadow: none;
            }

            img.radioimage-select:hover {
                border: solid 2px #a5c1e5;
            }

            img.radioimage-select.radioimage-checked {
                border: solid 2px #7db8d9;
                background-color: #f4f8fb;
            }
            .sweet-alert {
                box-sizing : border-box;
                max-height : 100% !important;
                overflow-y : auto !important;
                padding : 0 17px 17px !important;
                width : 512px;
                overflow-y: hidden;

            //As default top and left are 50%, so it will transform those values and will set the modal exactly in the middle
            transform: translate(-50%, -50%);

            //remove margins
            margin: 0;
            }
        </style>


    </section>
    {{--    <script src="{{ asset('js/chart/tv.js')}}"></script>--}}
    {{--    <script src="{{ asset('js/chart/bundle.js')}}"></script>--}}
    <script type="text/javascript">
        // $(document).ready(function () {
        //     $(".tradingview-widget-container iframe").clone().appendTo(".tradingview-widget-container");
        //
        //
        // });

        function myFunction() {
            var copyText = document.getElementById("myInput");
            copyText.select();
            copyText.setSelectionRange(0, 99999)
            document.execCommand("copy");
            alert("Link is copied ");
        }

        $(document).ready(function () {

            // $('#team_finance_amount1').on("focusout",function () {
            //     alert('focus');
            //     console.log('change');
            //
            // });
            var data = {!! json_encode($coin) !!};
            console.log(data);
                {{--            var noti ={!! json_encode($notify) !!};--}}
            var dataz = {!! json_encode($usercoin) !!};


            var qr_path = '{!! url('images/qrcodes') !!}';
            var img_path = qr_path + '/' + data['qr_code'];
            var gift_path = qr_path + '/gift.png';

            // var val;
            var user_id = {{auth()->user()->id}};
            var id;
            var coin_type;


            $('.book_hedge').on('click', function () {
                // Team Finance
                {{--var Team_Finance_total = {{$lbalance}}+{{$rbalance}} ;--}}
                if ({{$allowed_withdraw}}) {


                    id = $(this).data('id');

                    html = '<div class="FixedHeightContainer" style="">' +
                        '<input type="hidden" value="" id="selected_hedge_id">' +
                        '<input type="hidden" value="" id="hedge_amount_model">';

                    html += '<p class="mt-2" style="text-align:left;font-weight: bold;">Available Slots :</p>';
                    html += '<input type="text" tabindex="3" placeholder="" name="available_slot" id="available_slot_model"  readonly>';
                    html += '<p class="mt-2 mr-5" style="text-align:left;font-weight: bold;">Enter Slots : <span class="ml-5" id="perslot" >22 </span></p>';
                    html += '<input type="text" tabindex="3" placeholder="Please Enter Number of Slots you want to Book" name="booking_slot_input" id="booking_slot_input"   >';
                    html += '<span  id="booking_slot_input_error"style="text-align:left;font-weight: normal; color: red; display: none;"></span>';
                    html += '<p style="text-align:left;font-weight: bold; display: none" id="total_amount_lable">Total Amount :</p>';
                    html += '<input type="text" tabindex="3" placeholder="" name="total_amount" id="total_amount" style="display: none" readonly>';
                    html += '<div class="Content" style="height:auto 100px;overflow:auto;">';

                    html += '<select  name="coin_type" class="coin_type" tabindex="3" required disabled style="display: none">' +
                        '<option value="">Please Select Payment Type</option>' +
                        // '<option value="usd">From Invested USD</option>' +
                        // '<option value="profit">From Profit</option>' +
                        // '<option value="team_finance">From Team Finance </option>' +
                        @if(auth()->user()->id ==68)
                        '<option value="bitcoin">From Bitcoin </option>' +
                        @endif
                        '<option value="gift">From Gift Code </option>' +
                        '</select> ';


                    html += '<div class="btc" style="display:none;">';
                    // html += '<p style="text-align:left;font-weight: bold;">Your Frozen Balance is :' + dataz['frozen'] + ' Will clear after 60 days </p>';
                    html += '<p style="text-align:left;font-weight: bold;">Withdraw Amount Available in Invested USD :</p>';
                    html += '<input type="text" tabindex="3" placeholder="" name="profit" class="acc_name"  value="' + '{{$usdwithdrawable}}' + '"  readonly>';
                    html += ' @if($usdwithdrawable>=20) @else <p style="text-align:left;font-weight: bold; color: red;">Your are not Allowed to WithDraw Less then $20 :</p>@endif ';
                    html += '<span  id="btc_amount_error"style="text-align:left;font-weight: normal; color: red; display: none;"></span>';

                    html += '</div>';

                    html += '<div class="profit" style="display:none;">';
                    html += '<p style="text-align:left;font-weight: bold;">Amount Available in Profit Balance:</p>';
                    html += '<input type="text" tabindex="3" id="profbalance" placeholder="" name="profit" class="acc_name"  value="{{round($profitdrawable,2)}}"  readonly>';
                    html += '@if($profitdrawable >=20)' +
                        ' @else <p style="text-align:left;font-weight: bold; color: red;">Your are not Allowed to WithDraw Less then $20 :</p>@endif';
                    html += '<span  id="profit_amount_error"style="text-align:left;font-weight: normal; color: red; display: none;"></span>';
                    html += '</div>';


                    html += '<div class="gift" style="display:none;">';
                    html += '<p style="text-align:left;font-weight: bold;">Enter Gift Code:</p>';
                    html += '<input type="text" tabindex="3" id="giftcode" placeholder="Enter Code" name="giftcode" class="acc_name"  value="" >';
                    html += '<span  id="giftcode_error"style="text-align:left;font-weight: normal; color: red; display: none;"></span>';
                    html += '</div>';


                    html += '<div class="team_finance" style="display:none;">';
                    /* var currwithdraw = {!! file_get_contents('https://blockchain.info/ticker') !!};
  var dwithraw =dataz['withdraw_amount'] /currwithdraw['USD']['sell'] ;*/

                    html += '<ul class="nav nav-tabs" id="myTab" role="tablist">\n' +
                        '  <li class="nav-item">\n' +
                        '    <a class="nav-link active" id="left_tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Left</a>\n' +
                        '  </li>\n' +
                        '  <li class="nav-item">\n' +
                        '    <a class="nav-link" id="right-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Right</a>\n' +
                        '  </li>\n' +
                        '</ul>' +
                        '<div class="tab-content" id="myTabContent">\n' +

                        '  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">' +
                        '<p style="text-align:left;font-weight: bold;">Balance Available in Left Team Finance:</p>' +
                        '<input type="text" tabindex="3" placeholder="" name="profit" class="acc_name"  value="{{$leftwithdrawable}}"  readonly>' +
                        {{--                        '@if($leftwithdrawable>=20)'+--}}
                            '</div>' +
                        '<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">' +
                        '<p style="text-align:left;font-weight: bold;">Balance Amount Available in Right Team Finance:</p>' +
                        '<input type="text" tabindex="3" placeholder="" name="profit" class="acc_name"  value="{{$rightwithdrawable}}"  readonly>' +
                        {{--                        '@if($rightwithdrawable>=20)' +--}}
                            '</div>' +
                        '</div>' +
                        '</div>';
                    html += '<div class="bitcoin" style="display:none;">\n' +
                        '                                    <div class="form-group">\n' +
                         '<p style="text-align:left;font-weight: bold;">Amount After 10% Admin fee:</p>';
                        html += '<input type="number" tabindex="3" placeholder="Enter Amount" name="profit" id="after_fee"  readonly>' +
                        '</div>' +
                        '<div class="form-group btcData">' +
                        '</div>' +
                        '</div>';

                    html += '<div class="usd" style="display:none;">';
                    html += '<p style="text-align:left;font-weight: bold;">invested  Bank Name:</p>';
                    html += '<input type="text" tabindex="3" placeholder="enter your Bank Name" name="profit" class="acc_name"  value="' + dataz['acc_name'] + '">';
                    html += '<p style="text-align:left;font-weight: bold;">Bank Account No:</p>';
                    html += '<input type="number" tabindex="3" placeholder="enter your Bank Account No" name="profit" class="usd_id"  value="' + dataz['acc_no'] + '">';
                    html += '<p style="text-align:left;font-weight: bold;">Bank Branch Code: </p>';
                    html += '<input type="text" tabindex="3" placeholder="enter your Branch Code " name="profit" class="branch_code"  value="' + dataz['branch_code'] + '">';
                    html += '<p style="text-align:left;font-weight: bold;">Bank Type:</p>';
                    html += '<input type="text" tabindex="3" placeholder="enter your Branch Code " name="profit" class="acc_type"  value="' + dataz['acc_type'] + '">';
                    html += '<p style="text-align:left;font-weight: bold;">Swift Code:</p>';
                    html += '<input type="text" tabindex="3" placeholder="enter your Swift Code" name="profit" class="swift_code"  value="' + dataz['swift_code'] + '">';
                    html += '<p style="text-align:left;font-weight: bold;">Reference :</p>';
                    html += '<input type="text" tabindex="3" placeholder="" name="sur_name" class=""  value="' + dataz['name'] + '"  readonly>';
                    html += '<input type="number" tabindex="3" placeholder="Enter amount to Withdraw" id="usd_amount1"  value="0" class="amount1" required>';


                    html += '</div>';
                    html += ' </div>';
                    html += ' </div>';

                    swal({
                        title: "Booking",
                        text: "Note : All Withdrawals are processed within 72 hours. Your allowed only one Withdraw Request until approved it",
                        type: "input",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        animation: "slide-from-top",
                        inputPlaceholder: "Write something",
                        closeOnCancel: false,

                    });
                    $('.showSweetAlert').css("margin-top", "-350px");
                    $('.showSweetAlert fieldset').html(html);

                    let hedgeid = $(this).data('hedgeid');
                    let available_slot = $(this).data('hedge_available_slot');
                    let hedge_amount = $(this).data('hedge_amount');
                    console.log('available_slot', available_slot);
                    console.log('hedge_amount', hedge_amount);
                    $('#selected_hedge_id').val(hedgeid);
                    $('#hedge_amount_model').val(hedge_amount);
                    $('#available_slot_model').val(available_slot);
                    $('#perslot').text('1 Slot =' + hedge_amount);
                    // $('.confirm').prop('disabled', false);
                    $('.confirm').prop('disabled', true);


                    console.log('hedge id', $('#selected_hedge_id').val());
                    // console.log('input slots', inputslots);
                    $('#booking_slot_input').on('focus', function () {

                        $('.confirm').prop('disabled', true);

                        let inputslots = $('#booking_slot_input').val();
                        $('.profit').hide();
                        $('.bitcoin').hide();
                        $('.team_finance').hide();
                        $('.usd').hide();
                        $('.btc').hide();
                        $(".coin_type").prop('disabled', true);
                        $(".coin_type").hide();
                        $('#total_amount').hide();
                        $('#total_amount_lable').hide();
                        console.log(' focus in input slots', inputslots);

                    });

                    $('#booking_slot_input').on('focusout', function () {
                        let inputslots = $('#booking_slot_input').val();

                        console.log('input slots', inputslots);

                        // team_finance
                        // $leftwithdrawable
                        // $rightwithdrawable

                        if (available_slot >= inputslots) {
                            var profitdrawable = {{(int)$profitdrawable}};
                            var usdwithdrawable = {{(int)$usdwithdrawable}};
                            var leftwithdrawable = {{(int)$leftwithdrawable}};
                            var rightwithdrawable = {{(int)$rightwithdrawable}};
                            $('#booking_slot_input_error').hide();
                            var total_amount = inputslots * hedge_amount;
                            console.log('total_amount', total_amount);
                            console.log('left+right = ', rightwithdrawable + leftwithdrawable)



                                $(".coin_type option[value='team_finance']").hide();

                                if (leftwithdrawable < total_amount) {
                                    $('#left_tab').hide();
                                    $('#home').hide();

                                    //hide leftleft_tab
                                    // right-tab
                                    // home
                                    // profile
                                } else {
                                    $('#left_tab').show();
                                    $('#home').show();
                                    $(".coin_type option[value='team_finance']").show();


                                }

                                if (rightwithdrawable < total_amount) {
                                    //hide right
                                    $('#right-tab').hide();
                                    $('#profile').hide();

                                } else {
                                    // show right
                                    $('#right-tab').show();
                                    $('#profile').show();
                                    $(".coin_type option[value='team_finance']").show();
                                }



                            if (profitdrawable <= total_amount) {
                                $(".coin_type option[value='profit']").hide();
                            } else {
                                $(".coin_type option[value='profit']").show();

                            }
                            {{--  if ({{$profitdrawable}}<= total_amount)--}}
                                {{-- {--}}
                                {{--$(".coin_type option[value='profit']").hide();--}}
                                {{--  }--}}
                                {{--     else--}}
                                {{--           {--}}
                                {{--                $(".coin_type option[value='profit']").show();--}}

                                {{--            }--}}

                            if ({{(int)$usdwithdrawable}} <= total_amount) {
                                $(".coin_type option[value='usd']").hide();
                            } else {
                                $(".coin_type option[value='usd']").show();

                            }


                            $('#total_amount').val(total_amount);
                            $('#total_amount').show();
                            $('#total_amount_lable').show();
                            $(".coin_type").show();

                            $(".coin_type").prop('disabled', false);

                        } else {

                            $('#booking_slot_input_error').html("Please Enter a value less than and Equal to available Slots ");
                            $('#booking_slot_input_error').show();
                            $('#total_amount').hide();
                            $('#total_amount_lable').hide();
                            $(".coin_type").prop('disabled', true);
                            $(".coin_type").hide();

                        }
                    });

                    $('.coin_type').on('change', function () {

                        var vcoin_type = $("select.coin_type").children("option:selected").val();
                        if (vcoin_type == "usd") {
                            $('.usd').hide();
                            $('.bitcoin').hide();
                            $('.btc').show();
                            $('.confirm').prop('disabled', false);
                            $('.gift').hide();

                            $('.profit').hide();
                            $('.team_finance').hide();

                        }
                        if (vcoin_type == "gift") {

                            $('.gift').show();
                            $('.usd').hide();
                            $('.btc').hide();
                            $('.profit').hide();
                            $('.bitcoin').hide();
                            $('.team_finance').hide();
                            $('.confirm').prop('disabled', false);

                        }
                        if (vcoin_type == 'profit') {
                            // profit_amount
                            $('.profit').show();
                            $('.gift').hide();

                            $('.team_finance').hide();
                            $('.usd').hide();
                            $('.btc').hide();
                            $('.bitcoin').hide();
                            $('.confirm').prop('disabled', false);


                        }
                        if (vcoin_type == 'team_finance') {
                            $('.team_finance').show();
                            $('.gift').hide();

                            $('.profit').hide()
                            $('.usd').hide();
                            $('.bitcoin').hide();
                            $('.btc').hide();
                            $('.confirm').prop('disabled', false);

                        }
                        if (vcoin_type == '') {
                            $('.gift').hide();

                            $('.profit').hide();
                            $('.team_finance').hide();
                            $('.usd').hide();
                            $('.bitcoin').hide();
                            $('.btc').hide();
                            $('.confirm').prop('disabled', true);

                        }
                        if (vcoin_type == 'bitcoin') {
                            $('.gift').hide();

                            $('.profit').hide();
                            $('.team_finance').hide();
                            $('.usd').hide();
                            $('.btc').hide();
                            $('.bitcoin').show();
                            $('.confirm').prop('disabled', true);

                            var total_amount = $('#total_amount').val();
                            var input_slots = $('#booking_slot_input').val();
                            var u_id = id;
                            var hedgeid = $('#selected_hedge_id').val();
                            // var afterFee = Number(parseFloat(total_amount) + parseFloat(total_amount) * 0.1).toFixed(3);
                            // console.log(afterFee);
                            console.log('total_amount in type change', total_amount);

                            // $('#after_fee').val(afterFee);
                            var url = '<?= route('user.hedge.hedgeBookingBtcAjax')?>';
                            data = {
                                "_token": "{{ csrf_token() }}",
                                type: 'btc_hedge_booking',
                                input_slots: input_slots,
                                hedgeid: hedgeid,
                            };
                            $.ajax({
                                type: "POST",
                                url: url,
                                data: data,
                                success: function (response) {
                                    // console.log(response);
                                    $('.bootstrapiso').html('');
                                    $('.bootstrapiso').html('');
                                    $('.bootstrapiso').html('');
                                    $('.btcData').fadeOut().html(response.data).fadeIn();

                                },
                                error: function () {

                                }
                            });

                        }
                    });


                    $('.confirm').click(function () {


                        var curr = {!! file_get_contents('https://blockchain.info/ticker') !!};

                        var rate = curr['USD']['sell'];
                        var vcoin_type = $("select.coin_type").children("option:selected").val();
                        var total_amount = $('#total_amount').val();
                        let input_slots = $('#booking_slot_input').val();
                        var u_id = id;
                        var hedgeid = $('#selected_hedge_id').val();
                        var data;


                        if (vcoin_type == 'profit') {


                            data = {
                                "_token": "{{ csrf_token() }}",
                                u_id: u_id,
                                hedgeid: hedgeid,
                                total_amount: total_amount,
                                input_slots: input_slots,
                                coin_type: 'profit',
                                btc_rate: rate
                            };
                        }

                        if (vcoin_type == 'team_finance') {
                            var coin_type;
                            $('#left_tab').hasClass('active') ? coin_type = "left" : coin_type = "right";

                            data = {
                                "_token": "{{ csrf_token() }}",
                                u_id: u_id,
                                hedgeid: hedgeid,
                                total_amount: total_amount,
                                input_slots: input_slots,
                                coin_type: coin_type,
                                btc_rate: rate

                            };
                        }

                        {{--if (vcoin_type == 'BTC') {--}}

                            {{--    data = {--}}
                            {{--        "_token": "{{ csrf_token() }}",--}}
                            {{--        u_id: u_id,--}}
                            {{--        hedgeid: hedgeid,--}}
                            {{--        total_amount: total_amount,--}}
                            {{--        input_slots: input_slots,--}}
                            {{--        coin_type: coin_type,--}}
                            {{--        btc_rate: rate--}}

                            {{--    };--}}
                            {{--}--}}
                        if (vcoin_type == 'usd') {
                            data = {
                                "_token": "{{ csrf_token() }}",

                                u_id: u_id,
                                hedgeid: hedgeid,
                                total_amount: total_amount,
                                input_slots: input_slots,
                                coin_type: 'usd',
                                btc_rate: rate


                            };
                        }
                        if (vcoin_type == 'gift') {
                            var giftcode = $('#giftcode').val();

                            data = {
                                "_token": "{{ csrf_token() }}",

                                u_id: u_id,
                                hedgeid: hedgeid,
                                total_amount: total_amount,
                                input_slots: input_slots,
                                coin_type: 'gift',
                                btc_rate: rate,
                                giftcode:giftcode

                            };
                        }


                        var url = '<?= route('user.hedge.bookingajax')?>';
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: data,
                            success: function (response) {

                                if (response.success) {
                                    $('.withdraw').prop('disabled', true);

                                    $('body').find('.sweet-overlay').remove();
                                    $('body').find('.hideSweetAlert').remove();
                                    swal("Your Booking Request Submitted", "Successfully", "success");
                                    $('.confirm').on('click', function () {
                                        window.location.reload();
                                    });

                                } else {


                                    if (response.error ) {

                                        $(this).parent().parent().parent().remove();

                                        $('body').find('.sweet-overlay').remove();
                                        $('body').find('.hideSweetAlert').remove();

                                        swal("Request Rejected", response.message, "error");
                                        $('.confirm').on('click', function () {
                                            window.location.reload();
                                        });
                                    }
                                    }




                            },
                            error: function () {
                                $(this).parent().parent().parent().remove();

                                $('body').find('.sweet-overlay').remove();
                                $('body').find('.hideSweetAlert').remove();

                                swal("Request Rejected", "", "error");
                            }
                        });
                    });
                    $('.cancel').click(function () {
                        $('body').find('.sweet-overlay').remove();
                        // $(this).parent().parent().closest(".sweet-overlay").remove();
                        $(this).parent().parent().remove();

                    });

                } else {

                    swal({
                        title: "Withdraw",
                        text: "Note : You can send only one request at a time\n" +
                            "A Withdrawl Request is already pending",
                        // type: "input",
                        // showCancelButton: true,
                        closeOnConfirm: true,
                        animation: "slide-from-top",
                        // inputPlaceholder: "Write something",
                        // closeOnCancel: false
                    });
                    $('.confirm').on('click', function () {
                        // window.location.reload();
                    });
                }
            });

            $("#makepayment").on('click', function () {
                console.log('makepayment', $('#total_amount').val());


                if (true) {


                    var url = '<?= route('user.hedge.hedgeBookingBtcAjax')?>';
                    data = {"_token": "{{ csrf_token() }}", amount: 11, type: 'gift'};
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function (response) {
                            console.log(response);
                            $('.bootstrapiso').html('');
                            $('.bootstrapiso').html('');
                            $('.bootstrapiso').html('');
                            $('.btcData').fadeOut().html(response.data).fadeIn();

                        },
                        error: function () {

                        }
                    });

                } else {

                    $('.btcData').fadeOut().html('').fadeIn();
                    $('#amountInputfee').val(0);

                }
            });
            // $(".amount").on('change keyup', function () {
            //
            //     if ($(this).val()) {
            //         var checkamount = parseInt($(this).val());
            //         var fee = Number(parseFloat($(this).val()) + parseFloat($(this).val()) * 0.1).toFixed(3);
            //         $('.amountfee').val(fee);
            //
            //     } else {
            //         $('.amountfee').val(0);
            //     }
            // });
        });


    </script>




@endsection
