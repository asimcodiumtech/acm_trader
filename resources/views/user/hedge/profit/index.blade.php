@extends('layouts.hedge')

@section('dashboardcontent')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 style="color: white;">Profit Information</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Hedge</a></li>
                        <li class="breadcrumb-item active">Profit List</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="row">
            <div class="col-12">

                <div class="card">
                    <div class="card-header" style="background-color: #35354B;color: white;">
                        <h3 class="card-title">Profit List</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body" style="background-color: #35354B;color: white;">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped example1">
                                <thead style="background-color:#131311; color:white;">
                                <tr>

                                    <th>Hedge Title</th>
                                    <th>Code</th>
                                    <th>Amount</th>
                                    <th>% profit</th>

                                    <th>Time</th>

                                    <th>Status</th>
                                    <th>Total Amount</th>
                                </tr>
                                </thead>
                                <tbody style="background-color: #35354B;color: white;">

                                @foreach($check as $profit)
                                    <tr>

                                        <td>
                                            {{$profit['hedge_title']}}
                                        </td>
                                        <td>
                                            {{$profit['hedge_code']}}
                                        </td>
                                        <td>
                                            {{$profit['amount']}}
                                        </td>
                                        <td>
                                            {{$profit['percentage']}}
                                        </td>


                                        <td>
                                            {{Carbon\Carbon::parse($profit['profit_time'])->format('y-m-d')}}
                                        </td>


                                        <td>
                                            @if($profit['percentage']>0)
                                                <button class="btn btn-success"><b> Win </b></button>
                                            @elseif($profit['percentage']<0)
                                                <button class="btn btn-danger"><b> Loss </b></button>

                                            @else
                                                <button class="btn btn-danger " data-id="{{$profit['id']}}"><b> No
                                                        Applied </b></button>
                                            @endif
                                        </td>
                                        <td>
                                            {{$profit['total_amount']}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot style="background-color:#131311; color:white;">
                                <tr>

                                    <th>Hedge Title</th>
                                    <th>Code</th>
                                    <th>Amount</th>
                                    <th>% profit</th>

                                    <th>Time</th>

                                    <th>Status</th>
                                    <th>Total Amount</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
    </section>


    <script>

        $(document).ready(function () {
            $(".example1").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });


            $('.example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });


    </script>
@endsection
