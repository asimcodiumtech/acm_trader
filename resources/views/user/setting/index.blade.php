@extends('layouts.app')

@section('dashboardcontent')
    <style>
    </style>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 style="color: white;">Settings</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Setting</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <div class="card card-primary">
                        <div class="card-header" style="background-color: #42426a;color: white;">
                            <h3 class="card-title">Your Personal Info</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->


                        <div class="card-body" style="background-color:#35354B; color:white;">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Your Email</label>
                                <input type="email" class="form-control" id="email"
                                       placeholder="Enter Email" name="email" value="{{$user->email}}" required
                                       style="background-color:#131311; color:white;" readonly>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Name and Sur Name</label>
                                <input type="text" class="form-control" id="name"
                                       placeholder="Enter your name and sur name " name="name"
                                       value="{{$user->name}}" required
                                       style="background-color:#131311; color:white;" readonly>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Your Bitcoin Address</label>
                                <input type="text" class="form-control" id="btc_address"
                                       placeholder="Enter BTC address" name="btc_address"
                                       value="{{$user->btc_address}}" required
                                       style="background-color:#131311; color:white;">
                                <span id="btc_address_error"
                                      style="text-align:left;font-weight: normal; color: red; display: none;">
                                     Your BTC Address is an invalid.
                                    </span>
                            </div>


                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="button" class="btn btn-primary" id="setting_submit"
                                        style="background-color: #35354B;color: white;">Submit
                                </button>
                                <button type="button" class="btn btn-success code float-right"
                                        style="background-color: #35354B;color: white;">Change Password
                                </button>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">


        $(document).ready(function () {
            function topRight() {

                var values= [{"positionClass": "toast-top-right",
                    timeOut: 5000,
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": true,
                    "preventDuplicates": true,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut",
                    "tapToDismiss": false}];
                return values[0];

            }
            $('#btc_address').on('focusin', function () {
                $("#btc_address_error").hide();
                // $("#setting_submit").text("Submit");

            });
            $('#setting_submit').on('click', function (e) {
                var me = $(this);
                e.preventDefault();
                var address = $('#btc_address').val();
                var isInvalid = false;
                if (me.data('requestRunning')) {
                    return;
                }
                me.text('Request Running');

                me.data('requestRunning', true);
                $.get({
                    method: 'GET',
                    url: 'https://api.smartbit.com.au/v1/blockchain/address/'+address,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    // query parameters go under "data" as an Object

                    success: function (response) {
                        $("btc_address_error").hide();

                        var url = '<?= route('user.setting.store')?>';

                        data = {
                            "_token": "{{ csrf_token() }}",
                            name: $('#name').val(),
                            email: $('#email').val(),
                            btc_address: $('#btc_address').val()
                        };
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: data,
                            success: function (response) {
                                console.log(response.notification);
                                setTimeout(location.reload.bind(location), 4000);
                                toastr.info( 'Your BTC Address Update Successfully',"{{ Session::get('heading') }}",  topRight() );




                            },
                            error: function () {

                            }
                        });

                            console.log(response.success);
                    },
                    error: function (data) {
                        $("#btc_address_error").show();
                        console.log('btc_address_error');

                        swal("Please put a valid BTC Address", "", "error");


                    },
                    complete: function () {
                        console.log('ok');
                        me.text('completed');
                        // me.attr('disabled', true);

                        me.data('requestRunning', false);
                        // window.location.reload();

                    }
                });


            });


            $('.code').on('click', function () {


                html = '<div class="FixedHeightContainer" style="">';
                html += '<div class="Content" style="height:400px;overflow:auto;">';

                html += '<input type="text" tabindex="3" placeholder="Enter Code" name="ecode" class="ecode"  >';


                html += '<div class="usd" style="display:none;">';

                html += '<p style="text-align:left;font-weight: bold;">New Password:</p>';
                html += '<input type="password" tabindex="3" placeholder="Password" name="profit" class="pass"  value="">';
                html += '<p style="text-align:left;font-weight: bold;">Re Type New Password:</p>';
                html += '<input type="password" tabindex="3" placeholder="Confirm Password" name="profit" class="cpass"  value="">';
                html += '</div>';


                html += ' </div>';
                html += ' </div>';
                var url = '<?= route('user.setting.store')?>';
                data = {"_token": "{{ csrf_token() }}", ecode: 'send'};
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (response) {

                    },
                    error: function () {

                    }
                });

                swal({
                    title: "Change Password",
                    text: "Note : Code Send to your Email",
                    type: "input",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonText: "Submit",
                    animation: "slide-from-top",
                    inputPlaceholder: "Write something",
                    closeOnCancel: false
                });

                $('.showSweetAlert').css("margin-top", "-350px");
                $('.showSweetAlert fieldset').html(html);


                $('.confirm').attr("disabled", true);

                $(".ecode").on('change keyup', function () {


                    var url = '<?= route('user.setting.store')?>';
                    acc_name:$('.ecode').val()
                    data = {"_token": "{{ csrf_token() }}", ecode: 'verify', ecodev: $('.ecode').val()};
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function (response) {
                            if (response.success == '201') {
                                $('.usd').show();

                                $(".cpass").on('change keyup', function () {

                                    var pass = $('.pass').val();
                                    var cpass = $('.cpass').val();
                                    if (pass == cpass && pass != '' && cpass != '') {

                                        $('.confirm').removeAttr("disabled");
                                    } else {
                                        $('.confirm').attr("disabled", true);

                                    }


                                });
                                $(".pass").on('change keyup', function () {

                                    var pass = $('.pass').val();
                                    var cpass = $('.cpass').val();
                                    if (pass == cpass && pass != '' && cpass != '') {

                                        $('.confirm').removeAttr("disabled");
                                    } else {
                                        $('.confirm').attr("disabled", true);

                                    }


                                });


                            }


                        },
                        error: function () {

                        }
                    });


                });
                $('.confirm').click(function () {


                    var url = '<?= route('user.setting.store')?>';
                    acc_name:$('.ecode').val()
                    data = {"_token": "{{ csrf_token() }}", ecode: 'password', ecodev: $('.pass').val()};
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function (response) {
                            if (response.success == '201') {

                                $('body').find('.sweet-overlay').remove();
                                $('body').find('.hideSweetAlert').remove();
                                swal("Password Change Successfully", "Successfully !!", "success");
                                $('.confirm').on('click', function () {
                                    window.location.reload();
                                });


                            }


                        },
                        error: function () {

                        }
                    });


                });

                $('.cancel').click(function () {
                    $('body').find('.sweet-overlay').remove();
                    // $(this).parent().parent().closest(".sweet-overlay").remove();
                    $(this).parent().parent().remove();

                });

            });
        });


    </script>
    <script>
        function topRight() {

            var values = [{
                "positionClass": "toast-top-right",
                timeOut: 5000,
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut",
                "tapToDismiss": false
            }];
            return values[0];

        }

        /*toastr.success("{{ Session::get('success') }}", topRight());*/
        var type = "{{ Session::get('alert-type') }}";
        switch (type) {
            case 'info':
                toastr.info("{{ Session::get('message') }}", "{{ Session::get('heading') }}", topRight());
                break;

            case 'warning':
                toastr.warning("{{ Session::get('message') }}", "{{ Session::get('heading') }}", topRight());
                break;

            case 'success':
                toastr.success("{{ Session::get('message') }}", "{{ Session::get('heading') }}", topRight());
                break;

            case 'error':
                toastr.error("{{ Session::get('message') }}", "{{ Session::get('heading') }}", topRight());
                break;
        }

    </script>



@endsection
