@extends('layouts.app')

@section('dashboardcontent')
    <style>
        body {
            margin: 0;
        }

        .content {
            padding: 10px 20px;
        }

        .content p {
            font-family: sans-serif;
        }

        /******/

        .ticker-container {
            width: 100%;
            overflow: hidden;
        }

        .ticker-canvas {
            width: calc((200px * 6) + 2px);
            /*
            200px = minimum width of ticker item before widget script starts removing ticker codes
            15 = number of ticker codes
            2px = accounts for 1px external border
            */
            -webkit-animation-iteration-count: infinite;
            animation-iteration-count: infinite;
            -webkit-animation-timing-function: linear;
            animation-timing-function: linear;
            -webkit-animation-name: ticker-canvas;
            animation-name: ticker-canvas;
            -webkit-animation-duration: 20s;
            animation-duration: 20s;
        }

        .ticker-canvas:hover {
            animation-play-state: paused
        }

        @-webkit-keyframes ticker-canvas {
            0% {
                -webkit-transform: translate3d(0, 0, 0);
                transform: translate3d(0, 0, 0);
                visibility: visible;
            }
            100% {
                -webkit-transform: translate3d(-100%, 0, 0);
                transform: translate3d(-100%, 0, 0);
            }
        }

        @keyframes ticker-canvas {
            0% {
                -webkit-transform: translate3d(0, 0, 0);
                transform: translate3d(0, 0, 0);
                visibility: visible;
            }
            100% {
                -webkit-transform: translate3d(-100%, 0, 0);
                transform: translate3d(-100%, 0, 0);
            }
        }

        .tradingview-widget-container {
            position: relative;
        }

        .tradingview-widget-container iframe {
            position: absolute;
            top: 0;
        }

        .tradingview-widget-container iframe:nth-of-type(2) {
            left: 100%;
        }

        .sweet-alert select {
            width: 100%;
            box-sizing: border-box;
            border-radius: 3px;
            border: 1px solid #d7d7d7;
            height: 43px;
            margin-top: 10px;
            margin-bottom: 17px;
            font-size: 18px;
            background-color: #131311;
            color: white;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
{{--                <a href="{{route('userlaunchnotice')}}"> <img class="banner" alt="banner" src="../whitelanding/assets/images/banner.jpeg" width="100%"></a>--}}
                <video class="myHTMLvideo" width="100%" height="100%" playsinline autoplay muted loop controls style="    margin-top: 1%;">
                    <source src="../videos/ACM Trader - HEDGE FUND Spokesperson Video.mp4" type="video/mp4">
                </video>
            </div>
        </div>
    </div>
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 " style="color: white;">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success" style="background-color: #35354b!important;">
                        <div class="inner">
                            <div class="row">
                                <div class="col-lg-12 col-6">
                                    <p>Left Team</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-4">
                                    <span class="btn btn-sm btn-info "
                                          style="background-color: #00c245;border-color: #00c245;">100% </span>
                                    <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                </div>
                                <div class="col-lg-6 col-4">
                                    <p>{{$leftTeam}}<br><span style="font-size: 13px;">User</span></p>
                                </div>

                            </div>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>

                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success" style="background-color: #35354b!important;">
                        <div class="inner">
                            <div class="row">
                                <div class="col-lg-12 col-6">
                                    <p>Right Team</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-4">
                                    <span class="btn btn-sm btn-info " style="background-color: red;border-color: red;">100% </span>

                                </div>
                                <div class="col-lg-6 col-4">
                                    <p>{{$rightTeam}}<br><span style="font-size: 13px;">User</span></p>
                                </div>

                            </div>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>

                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info" style="background-color: #35354b!important;">
                        <div class="inner">
                            <div class="row">
                                <div class="col-lg-3 col-6">
                                    <p>Balance</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-4">
                                    <p>{{$todayP}} USD<br><span style="font-size: 13px;">Today Profit</span></p>

                                </div>
                                <div class="col-lg-6 col-4">
                                    <p>{{round($balance, 2)}}<br><span style="font-size: 13px;">USD</span></p>
                                </div>

                            </div>
                            <!-- <div class="row">
                               <div class="col-lg-6 col-4">
                                       <p>Today Profit<br>USD</p>
                               </div>
                               <div class="col-lg-6 col-4">
                                       <p>USD</p>
                               </div>
                            </div> -->
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>

                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-danger" style="background-color: #35354b!important;">
                        <div class="inner">
                            <div class="row">
                                <div class="col-lg-12 col-12">
                                    <p>Team Finance</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-4">
                                    <p>{{$leftwithdrawable}} USD <br><span style="font-size: 13px;">Left</span></p>

                                </div>
                                <div class="col-lg-6 col-4">
                                    <p>{{$rightwithdrawable}} USD<br><span style="font-size: 13px;">Right</span></p>
                                </div>

                            </div>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>

                    </div>
                </div>
                <!-- ./col -->
            </div>
            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <div class="col-md-8 col-lg-8 col-xl-8 col-xxl-8">


                    <div class="card card-primary card-outline card-outline-tabs"
                         style="background-color: #35354b!important;">
                        <div class="card-header p-0 border-bottom-0">
                            <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill"
                                       href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home"
                                       aria-selected="true">Chart</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill"
                                       href="#custom-tabs-three-profile" role="tab"
                                       aria-controls="custom-tabs-three-profile" aria-selected="false">Market</a>
                                </li>

                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-three-tabContent">
                                <div class="tab-pane fade active show" id="custom-tabs-three-home" role="tabpanel"
                                     aria-labelledby="custom-tabs-three-home-tab">
                                    <div class="ttradingview-widget-container">
                                        <div id="tradingview_ca3fa"></div>
                                        <!--<div class="tradingview-widget-copyright"><a href="" rel="noopener" target="_blank"><span class="blue-text">ETHBTC Chart</span></a></div>-->
                                        <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
                                        <script type="text/javascript">
                                            new TradingView.widget(
                                                {

                                                    "symbol": "BINANCE:ETHBTC",
                                                    "interval": "30",
                                                    "height": 510,
                                                    "width": "100%",
                                                    "timezone": "Etc/UTC",
                                                    "theme": "Dark",
                                                    "style": "1",
                                                    "locale": "en",
                                                    "toolbar_bg": "#f1f3f6",
                                                    "enable_publishing": false,
                                                    "allow_symbol_change": true,
                                                    "container_id": "tradingview_ca3fa"
                                                }
                                            );
                                        </script>
                                    </div>
                                    <!-- TradingView Widget END -->
                                </div>
                                <div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel"
                                     aria-labelledby="custom-tabs-three-profile-tab">
                                    <!-- TradingView Widget BEGIN -->
                                    <!-- TradingView Widget BEGIN -->
                                    <div class="ttradingview-widget-container">
                                        <div class="tradingview-widget-container__widget"></div>
                                        <div class="tradingview-widget-copyright"></div>
                                        <script type="text/javascript"
                                                src="https://s3.tradingview.com/external-embedding/embed-widget-screener.js"
                                                async>
                                            {
                                                "width"
                                            :
                                                "101%",
                                                    "height"
                                            :
                                                512,
                                                    "defaultColumn"
                                            :
                                                "overview",
                                                    "defaultScreen"
                                            :
                                                "general",
                                                    "market"
                                            :
                                                "crypto",
                                                    "showToolbar"
                                            :
                                                false,
                                                    "colorTheme"
                                            :
                                                "dark",
                                                    "locale"
                                            :
                                                "en"
                                            }
                                        </script>
                                    </div>
                                    <!-- TradingView Widget END -->
                                    <!-- TradingView Widget END -->
                                </div>


                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                    @if(round($avg, 2) >= 0 )
                        <div class="card"
                             style="    background-color: #00800059;border: #09950b 1px solid; text-align: center;margin-left: 0%;margin-top: 4%;">
                            <h style="font-size: 42px;
    color: green;"> Total Average Profit
                            </h>
                            <br>
                            <h style="font-size: 42px;
    color: green;"> {{round($avg, 2)}} %
                            </h>

                        </div>
                    @else
                        <div class="card" style="background-color: #80000059;
    border: #fa0a0a 1px solid; text-align: center;margin-left: 0%;margin-top: 4%;">
                            <h style="font-size: 42px;
        color: #d90909;"> Total Average Profit
                            </h>
                            <br>
                            <h style="font-size: 42px;
        color: #d90909;"> {{round($avg, 2)}} %
                            </h>

                        </div>
                    @endif

                </div>
                <div class="col-md-4 col-lg-4 col-xl-4 col-xxl-4" style="">
                    <div class="card">
                        <div class="card-header" style="background-color: #35354b;color:white">
                            <h3 class="card-title">STATS</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <!-- /.card-header -->

                        <!-- /.card-body -->
                        <div class="card-footer bg-white p-0">
                            <ul class="nav nav-pills flex-column" style="background-color: #35354b;">
                                <li class="nav-item">
                                    <h class="nav-link">
                                        USD INVESTED

                                    </h>
                                    <p href="#" class="nav-link" style="font-weight: bold;">
                                        {{round($exchange, 2)}}

                                    </p>
                                    <h class="nav-link">
                                        Total USD return

                                    </h>
                                    <p href="#" class="nav-link" style="font-weight: bold;">
                                        {{$todayP}}

                                    </p>
                                </li>
                                <li class="nav-item">
                                    <h8 class="nav-link">
                                        Total <span style="color:#07c1ca">USD</span> IN ASSETS ON EXCHANGE :

                                    </h8>
                                    <h3 class="nav-link">
                                        @if(round($avg, 2)< 0)
                                            <i class="fas fa-wallet" style="color:red"></i> <span
                                                style="color:red;text-align: center;">{{round($exchange2ndtime,2)}}({{round($avg, 2)}} %)
                              </span>
                                        @else
                                            <i class="fas fa-wallet" style="color:#07c887"></i> <span
                                                style="color:#07c887;text-align: center;">{{round($exchange2ndtime,2)}}({{round($avg, 2)}} %)
                              </span>

                                        @endif


                                    </h3>


                                </li>
                                <li class="nav-item">

                                    <h4 class="nav-link">
                                        <i class="fa fa-receipt" style="color:red"></i>
                                        Trading API Exchange

                                    </h4>
                                    <h5 class="nav-link">
                                        <i class="fa fa-receipt" style="color:#07c1ca;display:none;"></i>
                                        Account : <span style="font-weight: bold;">Default</span>

                                    </h5>
                                    <h5 class="nav-link">
                                        <i class="fa fa-receipt" style="color:#07c1ca;display:none;"></i>
                                        Exchange : <span style="font-weight: bold;">Binance</span>

                                    </h5>
                                    <p href="#" class="nav-link" style="text-align: center;margin-top: 15%;">
                                        <button class="btn btn-primary withdraw" data-id="{{auth()->user()->id}}"
                                                style="color: black;
    background: #e7efef;
    border: #d4dad9 1px solid;">Withdraw
                                        </button>
                                        <button class="btn btn-primary deposit" data-id="{{auth()->user()->id}}" style="color: black;
    background: #e7efef;
    border: #d4dad9 1px solid;">Fund Account
                                        </button>

                                    </p>
                                </li>


                            </ul>
                        </div>
                        <!-- /.footer -->
                    </div>
                    <div class="card" style="margin-top: 6%; height:19%;background-color: #35354b;">
                        <!-- <div class="card-header" style="background-color: #35354b;color:white">



                        </div> -->
                        <ul class="nav nav-pills flex-column" style="background-color: #35354b;">
                            <li class="nav-item" style="background-color: #35354b;color:white">

                                <h4 class="nav-link">

                                    Referral Link

                                </h4>


                                <p href="#" class="nav-link" style="text-align: center;">

                                <div class="input-group">
                                    <!-- <p type="text" class="form-control" >Helo </p> -->
                                    <input type="text" class="form-control"
                                           value="{{route('register')}}/?id={{$reffKey}}" id="myInput" readonly>
                                    <span class="input-group-btn">
    <button class="btn btn-success" type="button" onclick="myFunction()">Copy</button>
  </span>

                                </div>


                                </p>
                            </li>
                        </ul>


                    </div>
                </div>

                <!--   <div class="row">

                   <div class="col-md-6 col-lg-6 col-xl-6 col-xxl-6">
                  <div class="card-body" style="    background-color: #00800059;
            height: 192px;
            width: 715px;
        ">

                      </div>
                  </div>
              </div> -->
                <div class="row">
                    <!-- Left col -->
                    <div class="col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                        <!-- TradingView Widget BEGIN -->
                        <div class="ticker-container">
                            <div class="ticker-canvas">
                                <div class="tradingview-widget-container">
                                    <div class="tradingview-widget-container__widget"></div>
                                    <script type="text/javascript"
                                            src="https://s3.tradingview.com/external-embedding/embed-widget-tickers.js">
                                        {
                                            "symbols"
                                        :
                                            [
                                                {
                                                    "proName": "OANDA:SPX500USD",
                                                    "title": "S&P 500"
                                                },
                                                {
                                                    "proName": "OANDA:NAS100USD",
                                                    "title": "Nasdaq 100"
                                                },
                                                {
                                                    "proName": "FX_IDC:EURUSD",
                                                    "title": "EUR/USD"
                                                },
                                                {
                                                    "proName": "BITSTAMP:BTCUSD",
                                                    "title": "BTC/USD"
                                                },
                                                {
                                                    "proName": "BITSTAMP:ETHUSD",
                                                    "title": "ETH/USD"
                                                }
                                            ],
                                                "colorTheme"
                                        :
                                            "dark",
                                                "isTransparent"
                                        :
                                            false,
                                                "locale"
                                        :
                                            "en"
                                        }
                                    </script>
                                </div>
                            </div>
                        </div>
                        <!-- TradingView Widget END -->

                    </div>
                </div>


                <!-- /.row (main row) -->
            </div><!-- /.container-fluid -->
            <!-- Bootstrap4 CSS - -->
            <link rel="stylesheet" href="https://bootswatch.com/4/superhero/bootstrap.css" crossorigin="anonymous">

            <!-- Note - If your website not use Bootstrap4 CSS as main style, please use custom css style below and delete css line above.
            It isolate Bootstrap CSS to a particular class 'bootstrapiso' to avoid css conflicts with your site main css style -->
            <!-- <link rel="stylesheet" href="css/superhero.min.css" crossorigin="anonymous"> -->


            <!-- JS -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"
                    crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"
                    crossorigin="anonymous"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
                    crossorigin="anonymous"></script>
            <script defer src="https://use.fontawesome.com/releases/v5.12.0/js/all.js" crossorigin="anonymous"></script>
            <script src="<?php echo CRYPTOBOX_JS_FILES_PATH; ?>support.min.js" crossorigin="anonymous"></script>

            <!-- CSS for Payment Box -->
            <style>
                html {
                    font-size: 14px;
                }

                @media (min-width: 768px) {
                    html {
                        font-size: 16px;
                    }

                    .tooltip-inner {
                        max-width: 350px;
                    }
                }

                .mncrpt .container {
                    max-width: 980px;
                }

                .mncrpt .box-shadow {
                    box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .05);
                }

                img.radioimage-select {
                    padding: 7px;
                    border: solid 2px #ffffff;
                    margin: 7px 1px;
                    cursor: pointer;
                    box-shadow: none;
                }

                img.radioimage-select:hover {
                    border: solid 2px #a5c1e5;
                }

                img.radioimage-select.radioimage-checked {
                    border: solid 2px #7db8d9;
                    background-color: #f4f8fb;
                }
            </style>


    </section>
    <script src="{{ asset('js/chart/tv.js')}}"></script>
    <script src="{{ asset('js/chart/bundle.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".tradingview-widget-container iframe").clone().appendTo(".tradingview-widget-container");


        });
        //document.querySelector('video').play();
        // $(document).ready(function() {
        //     $('.myHTMLvideo').click(function() {
        //         this.paused ? this.play() : this.pause();
        //     });
        // });
        function myFunction() {
            var copyText = document.getElementById("myInput");
            copyText.select();
            copyText.setSelectionRange(0, 99999)
            document.execCommand("copy");
            alert("Link is copied ");
        }

        $(document).ready(function () {

            // $('#team_finance_amount1').on("focusout",function () {
            //     alert('focus');
            //     console.log('change');
            //
            // });
            var data = {!! json_encode($coin) !!};
            console.log(data);
            var noti ={!! json_encode($notify) !!};
            var dataz = {!! json_encode($usercoin) !!};


            var qr_path = '{!! url('images/qrcodes') !!}';
            var img_path = qr_path + '/' + data['qr_code'];
            var gift_path = qr_path + '/gift.png';

            var val;
            var id;
            var coin_type;


            html = '<div class="FixedHeightContainer" style="">';
            html += '<div class="Content" style="height:400px;overflow:auto;">';
            $.each(noti, function (index, value) {
                html += '<div class="media" style="background-color: #131311;margin-top: 1%;">';
                html += ' <div class="media-body">';
                html += '<h2 class="dropdown-item-title">';

                html += '<p class="text-sm">' + value['data']['message'] + '</p>';
                html += ' </div>';
                html += ' </div>';
            });


            html += ' </div>';
            html += ' </div>';

            if (noti.length != 0) {

                swal({
                        title: "A message From Admin",
                        text: html,

                        html: true,
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",

                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            var url = '<?= route('notifi.read')?>';
                            var data = {"_token": "{{ csrf_token() }}", notifi: 1};
                            $.ajax({
                                type: "POST",
                                url: url,
                                data: data,
                                success: function (response) {
                                    console.log(response);
                                    if (response.success) {
                                        swal("Readed", "You read All notifications", "success");


                                    }
                                },
                                error: function () {


                                    swal("Request Rejected", "You can't fill all fields !!", "error");
                                }
                            });


                        }

                    });
                $('.showSweetAlert').css("margin-top", "-350px");

            }


            $('.deposit').on('click', function () {


                id = $(this).data('id');
                html = '<div class="FixedHeightContainer" style="">';
                html += '<div class="Content" style="height:400px;overflow:auto;">';

                html += '<select  name="coin_type" class="coin_type" tabindex="3" required>' +
                    '<option value="">Please Select</option>' +
                    @if(auth()->user()->id ==68)
                        '<option value="BTC">BTC</option>' +
                    @endif
                    '<option value="gift">Gift</option>' +
                    // '<option value="cash">Cash</option>' +
                    '</select>';


                html += '<div class="btc" style="display:none;">';
                html += '<p style="text-align:left;font-weight: bold; color:red">When Change Amount wait for 10 second to Update QR code</p>';
                html += '<input type="number" tabindex="3" placeholder="Enter Amount" name="profit" class="amountInput"  value=""> ';
                html += '<span  class="form-control about" style="text-align:left;font-weight: bold; color:red;display:none; background-color:#35354B">Amount Must be Equal or greater than 100</span>';
                html += '<p style="text-align:left;font-weight: bold;">Amount After 10% Admin fee:</p>';
                html += '<input type="number" tabindex="3" placeholder="Enter Amount" name="profit" class="amountInputfee"  value="" readonly> ';

                html += '<div class="form-group">';
                html += '<button type="button" class="btn btn-info float-right payment" style="color: white;">Make Payment</button>';
                html += '</div>';

                html += '<div class="form-group btcData" style="display:none; margin-top: 20%;">';
                html += "";

                html += '</div>';
                html += '<div class="form-group btcmessage" style="display: none; float:left; margin-top: -15%;" ><p>NB: Do not pay this from a Coinbase wallet or a bitcoin exchange account. Exchanges, and especially Coinbase, do not broadcast the bitcoin payment in time, thus this result in the payment not reflect immediately</p></div>';

                html += '</div>';
                html += '<div class="btcstop" style="display:none;">';
                html += '<p style="text-align:left;font-weight: bold; color:red">BTC payments currently not available kindly use gift code</p>';
                html += '</div>';

                html += '<div class="gift" style="display:none;">';
                html += '<div class="card" style="width: 18rem;height: 15rem;margin-left: 14%;">';
                html += '<img src="' + gift_path + '" class="card-img-top" alt="..." style="width: 18rem;height: 15rem;background-color: #35354B;">';
                html += '</div>';


                html += '<p style="text-align:left;font-weight: bold;">Gift Code:</p>';
                html += '<input type="text" tabindex="3" placeholder="Enter Gift code" name="profit" class="gift_code"  value=""> ';


                html += '</div>';

                html += '<div class="usd" style="display:none;">';
                html += '<p style="text-align:left;font-weight: bold;">Amount to send:</p>';
                html += '<input type="number" tabindex="3" placeholder="Enter amount in USD to send" name="amount" class="amount"  >';
                // html+='<p style="text-align:left;font-weight: bold;">Amount After fee:</p>';
                // html+='<input type="text" tabindex="3" placeholder="after 10% fee" name="profit"  class="total_amount"   value="" readonly> ';


                // html+='<p style="text-align:left;font-weight: bold;">Bank Name:</p>';
                // html+='<input type="text" tabindex="3" placeholder="comming soon" name="profit" class="first" readonly value="'+ data['acc_name']+'">';
                // html+='<p style="text-align:left;font-weight: bold;">Bank Account No:</p>';
                // html+='<input type="number" tabindex="3" placeholder="comming soon" name="profit" class="first" readonly value="'+ data['usd_id']+'">';
                // html+='<p style="text-align:left;font-weight: bold;">Branch Code: </p>';
                // html+='<input type="number" tabindex="3" placeholder="comming soon" name="profit" class="first" readonly value="'+ data['branch_code']+'">';
                // html+='<p style="text-align:left;font-weight: bold;">Bank Type: </p>';
                // html+='<input type="text" tabindex="3" placeholder="comming soonaaa" name="profit" class="first" readonly value="'+ data['bank_type']+'">';
                // html+='<p style="text-align:left;font-weight: bold;">Swift Code:</p>';
                // html+='<input type="number" tabindex="3" placeholder="comming soon" name="profit" class="first" readonly value="'+ data['swift_code']+'">';

                // html+='<p style="text-align:left;font-weight: bold;">Reference :</p>';
                // html+='<input type="text" tabindex="3" placeholder="" name="sur_name" class=""  value="'+ dataz['name']+'"  readonly>';

                html += '<p style="text-align:left;font-weight: bold;">NB: User your full names as reference number and click process only when deposit is made.</p>';

                html += '<p style="text-align:left;font-weight: bold; color:red">Note that this is our affiliate bank account to assist people that are not enlighten about bitcoin.</p>';

                html += '</div>';

                html += ' </div>';
                html += ' </div>';

                swal({
                    title: "Deposit (Fund Account)",
                    text: "Note : All deposit require a 10% managment Fee",
                    type: "input",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonText: "Submit",
                    animation: "slide-from-top",
                    inputPlaceholder: "Write something",
                    closeOnCancel: false
                });
                $('.showSweetAlert').css("margin-top", "-350px");
                $('.showSweetAlert fieldset').html(html);
                $('.coin_type').on('change', function () {

                    var vcoin_type = $("select.coin_type").children("option:selected").val();
                    if (vcoin_type == "BTC") {
                        $('.usd').hide();
                        $('.btc').show();
                        $('.btcstop').hide();
                        $('.confirm').attr("disabled", true);
                        $('.gift').hide();

                    }
                    if (vcoin_type == "USD") {
                        console.log(vcoin_type);
                        $('.usd').show();
                        $('.btc').hide();
                        $('.btcstop').hide();

                        $('.gift').hide();
                    }
                    if (vcoin_type == "gift") {
                        console.log(vcoin_type);
                        $('.gift').show();
                        $('.confirm').removeAttr("disabled");
                        $('.btc').hide();
                        $('.usd').hide();
                        $('.btcstop').hide();

                    }

                    if (vcoin_type == '') {
                        console.log(vcoin_type);
                        $('.usd').hide();
                        $('.btc').hide();
                        $('.gift').hide();
                        $('.btcstop').hide();

                    }
                    if (vcoin_type == 'cash') {
                        console.log(vcoin_type);
                        $('.usd').show();
                        $('.btc').hide();
                        $('.btcstop').hide();

                        $('.gift').hide();
                        $('.confirm').removeAttr("disabled");

                    }
                });
                //call to get btc scan code
                $(".payment").click(function () {

                    var amountInput = $('.amountInput').val();
                    if (amountInput >= 100) {
                        $('.about').hide();
                        var amount = parseFloat(amountInput);
                        var afterFee = Number(parseFloat(amountInput) + parseFloat(amountInput) * 0.1).toFixed(3);


                        $('.amountInputfee').val(afterFee);

                        var url = '<?= route('user.ajax.btc')?>';
                        data = {"_token": "{{ csrf_token() }}", amount: amount, type: 'deposit', amountfee: afterFee};
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: data,
                            success: function (response) {
                                console.log(response);
                                $('.bootstrapiso').html('');
                                $('.bootstrapiso').html('');
                                $('.bootstrapiso').html('');

                                $('.btcData').fadeOut().html(response.data).fadeIn();
                                $('.btcmessage').show();

                            },
                            error: function () {

                            }
                        });

                    } else {

                        $('.about').show();
                        $('.btcData').fadeOut().html('').fadeIn();
                        $('.amountInputfee').val(0);


                    }
                });
                $(".amount").on('change keyup', function () {

                    if ($(this).val()) {
                        var checkamount = parseInt($(this).val());
                        var fee;
                        var vcoin_type = $("select.coin_type").children("option:selected").val();
                        if (vcoin_type == 'BTC') {
                            var curr = {!! file_get_contents('https://blockchain.info/ticker') !!};

                            var convert = curr['USD']['sell'] * checkamount;
                            fee = convert * 0.1;

                            var total = Number(convert + fee).toFixed(2) + ' $ ';
                            console.log(total);
                            $("#convert").val(convert);
                            $("#total_amount").val(total);
                        }
                        if (vcoin_type == 'USD') {
                            fee = checkamount * 0.1;

                            total = checkamount + fee + ' $ ';
                            $(".total_amount").val(total);
                        }


                    } else {
                        total = 0 + ' $ ';
                        $(".total_amount").val(total);
                    }
                });
                $(".amountbtc").on('change keyup', function () {
                    /*console.log($(this).val());*/
                    if ($(this).val()) {
                        var checkamount = parseFloat($(this).val());
                        var fee;
                        var vcoin_type = $("select.coin_type").children("option:selected").val();
                        if (vcoin_type == 'BTC') {
                            var curr = {!! file_get_contents('https://blockchain.info/ticker') !!};

                            var convert = curr['USD']['sell'] * checkamount;
                            fee = convert * 0.1;

                            var total = Number(convert + fee).toFixed(2) + ' $ ';

                            $("#convert").val(Number(convert).toFixed(2) + ' $ ');
                            $("#total_amount").val(total);
                        }
                        if (vcoin_type == 'USD') {
                            fee = checkamount * 0.1;

                            total = checkamount - fee + ' $ ';
                            $(".total_amount").val(total);
                        }


                    } else {
                        total = 0 + ' $ ';
                        $("#convert").val(total);
                        $(".total_amount").val(total);
                    }
                });
                $('.confirm').attr("disabled", true);
                $('.confirma').click(function () {
                    $('body').find('.sweet-overlay').remove();
                    $('body').find('.hideSweetAlert').remove();
                    swal("Processed Complete", "Successfully !!", "success");
                    window.location.reload(true);

                });

                $('.confirm').click(function () {


                    var vcoin_type = $("select.coin_type").children("option:selected").val();
                    var amount;


                    var data;

                    if (vcoin_type == 'BTCaa') {
                        var u_id = id;
                        amount = $('.amountbtc').val();
                        var trans_id = $('.trans_id').val();
                        data = {
                            "_token": "{{ csrf_token() }}",
                            deposite_status: 2,
                            u_id: u_id,
                            amount: amount,
                            trans_id: trans_id,
                            coin_type: vcoin_type
                        };

                    }
                    if (vcoin_type == 'BTC') {
                        $('body').find('.sweet-overlay').remove();
                        $('body').find('.hideSweetAlert').remove();
                        swal("Processed Complete", "Successfully !!", "success");
                        window.location.reload(true);


                    }
                    if (vcoin_type == 'cash') {
                        var data1 = {!! json_encode($coin) !!};
                        var u_id = id;
                        amount = $('.amount').val();

                        data = {
                            "_token": "{{ csrf_token() }}",
                            u_id: u_id,
                            // acc_name:data1['acc_name'],
                            //  usd_id:data1['usd_id'],
                            //  branch_code:data1['branch_code'],
                            //  bank_type:data1['bank_type'],
                            //  swift_code:data1['swift_code'],
                            amount: amount,
                            coin_type: vcoin_type,
                            deposite_status: 2

                        };
                        //deposite.cash
                        var url = '<?= route('deposite.cash')?>';
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: data,
                            success: function (response) {
                                console.log(response);
                                if (response.success) {
                                    if (response.success == '203') {
                                        $('body').find('.sweet-overlay').remove();
                                        $('body').find('.hideSweetAlert').remove();
                                        swal("Gift code verified , kindly pay the affiliate(seller) to activate your funds", "Successfully !!", "success");
                                        $('.confirm').on('click', function () {
                                            window.location.reload();
                                        });

                                    } else {
                                        $('body').find('.sweet-overlay').remove();
                                        $('body').find('.hideSweetAlert').remove();
                                        swal("Request Submit to Admin", "Successfully !!", "success");
                                        $('.confirm').on('click', function () {
                                            window.location.reload();
                                        });

                                    }

                                } else {
                                    if (response.error == 4091) {

                                        alert("User already has an active subscription.");

                                    } else {
                                        if (response.error == "303") {
                                            $(this).parent().parent().parent().remove();

                                            $('body').find('.sweet-overlay').remove();
                                            $('body').find('.hideSweetAlert').remove();

                                            swal("Request Rejected", "You are not the right Person!!", "error");


                                        }
                                        if (response.error == "301") {
                                            $(this).parent().parent().parent().remove();

                                            $('body').find('.sweet-overlay').remove();
                                            $('body').find('.hideSweetAlert').remove();

                                            swal("Request Rejected", "Token not valid or Processed!!", "error");


                                        }
                                        if (response.error == '409') {
                                            {
                                                $(this).parent().parent().parent().remove();

                                                $('body').find('.sweet-overlay').remove();
                                                $('body').find('.hideSweetAlert').remove();

                                                swal("Request Rejected", "You can't fill all fields !!", "error");

                                            }

                                        }
                                    }
                                }
                            },
                            error: function () {
                                $(this).parent().parent().parent().remove();

                                $('body').find('.sweet-overlay').remove();
                                $('body').find('.hideSweetAlert').remove();

                                swal("Request Rejected", "You can't fill all fields !!", "error");
                            }
                        });


                    }
                    if (vcoin_type == 'gift') {
                        var u_id = id;
                        var gift_code;
                        gift_code = $('.gift_code').val();

                        data = {
                            "_token": "{{ csrf_token() }}",
                            deposite_status: 2,
                            gift_code: gift_code,
                            coin_type: vcoin_type
                        };

//gift call
                        var url = '<?= route('deposite.store')?>';
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: data,
                            success: function (response) {
                                console.log(response);
                                if (response.success) {
                                    if (response.success == '203') {
                                        $('body').find('.sweet-overlay').remove();
                                        $('body').find('.hideSweetAlert').remove();
                                        swal("Gift code verified , kindly pay the affiliate(seller) to activate your funds", "Successfully !!", "success");
                                        $('.confirm').on('click', function () {
                                            window.location.reload();
                                        });

                                    } else {
                                        $('body').find('.sweet-overlay').remove();
                                        $('body').find('.hideSweetAlert').remove();
                                        swal("Request Submit to Admin", "Successfully !!", "success");
                                        $('.confirm').on('click', function () {
                                            window.location.reload();
                                        });

                                    }

                                } else {
                                    if (response.error == 4091) {

                                        alert("User already has an active subscription.");

                                    } else {
                                        if (response.error == "303") {
                                            $(this).parent().parent().parent().remove();

                                            $('body').find('.sweet-overlay').remove();
                                            $('body').find('.hideSweetAlert').remove();

                                            swal("Request Rejected", "You are not the right Person!!", "error");


                                        }
                                        if (response.error == "301") {
                                            $(this).parent().parent().parent().remove();

                                            $('body').find('.sweet-overlay').remove();
                                            $('body').find('.hideSweetAlert').remove();

                                            swal("Request Rejected", "Token not valid or Processed!!", "error");


                                        }
                                        if (response.error == '409') {
                                            {
                                                $(this).parent().parent().parent().remove();

                                                $('body').find('.sweet-overlay').remove();
                                                $('body').find('.hideSweetAlert').remove();

                                                swal("Request Rejected", "You can't fill all fields !!", "error");

                                            }

                                        }
                                    }
                                }
                            },
                            error: function () {
                                $(this).parent().parent().parent().remove();

                                $('body').find('.sweet-overlay').remove();
                                $('body').find('.hideSweetAlert').remove();

                                swal("Request Rejected", "You can't fill all fields !!", "error");
                            }
                        });
                    }
                });
                $('.cancel').click(function () {
                    $('body').find('.sweet-overlay').remove();
                    // $(this).parent().parent().closest(".sweet-overlay").remove();
                    $(this).parent().parent().remove();
                    window.location.reload(true);


                });
            });


            $('.withdraw').on('click', function () {
                    // Team Finance
                    {{--var Team_Finance_total = {{$lbalance}}+{{$rbalance}} ;--}}
                    if ({{$allowed_withdraw}}) {


                        id = $(this).data('id');
                        html = '<div class="FixedHeightContainer" style="">';
                        html += '<div class="Content" style="height:400px;overflow:auto;">';

                        html += '<select  name="coin_type" class="coin_type" tabindex="3" required><option value="">Please Select</option>' +
                            '<option value="BTC">Invested USD</option>' +
                            '<option value="profit">Profit</option>' +
                            '<option value="team_finance">Team Finance </option>' +
                            '</select>';


                        html += '<div class="btc" style="display:none;">';
                        html += '<p style="text-align:left;font-weight: bold;">Your Frozen Balance is :' + dataz['frozen'] + ' Will clear after 60 days </p>';
                        html += '<p style="text-align:left;font-weight: bold;">Withdraw Amount Available in USD :</p>';
                        html += '<input type="text" tabindex="3" placeholder="" name="profit" class="acc_name"  value="' + '{{$usdwithdrawable}}' + '"  readonly>';
                        html += ' @if($usdwithdrawable>=20)<input type="number" tabindex="3" placeholder="Enter amount to Withdraw" id="btc_amount" class="amount1" required>@else <p style="text-align:left;font-weight: bold; color: red;">Your are not Allowed to WithDraw Less then $20 :</p>@endif ';
                        html += '<span  id="btc_amount_error"style="text-align:left;font-weight: normal; color: red; display: none;"></span>';

                        html += '<input type="text" tabindex="3" placeholder="BTC address" name="profit" class="btc_id"  value="' + dataz['btc_address'] + '" readonly>';
                        html += '<p style="text-align:left;font-weight: bold;">Reference :</p>';
                        html += '<input type="text" tabindex="3" placeholder="" name="sur_name" class=""  value="' + dataz['name'] + '"  readonly>';

                        html += '</div>';

                        html += '<div class="profit" style="display:none;">';
                        html += '<p style="text-align:left;font-weight: bold;">Withdraw Amount Available in Profit:</p>';
                        html += '<input type="text" tabindex="3" id="profbalance" placeholder="" name="profit" class="acc_name"  value="{{round($profitdrawable,2)}}"  readonly>';
                        html += '@if($profitdrawable >=20)<input type="number" tabindex="3" placeholder="Enter amount to Withdraw" id="profit_amount" class="amount1" required>@else <p style="text-align:left;font-weight: bold; color: red;">Your are not Allowed to WithDraw Less then $20 :</p>@endif';
                        html += '<span  id="profit_amount_error"style="text-align:left;font-weight: normal; color: red; display: none;"></span>';

                        html += '<input type="text" tabindex="3" placeholder="BTC address" name="profit" class="btc_id"  value="' + dataz['btc_address'] + '" readonly>';
                        html += '<p style="text-align:left;font-weight: bold;">Reference :</p>';
                        html += '<input type="text" tabindex="3" placeholder="" name="sur_name" class=""  value="' + dataz['name'] + '"  readonly>';

                        html += '</div>';


                        html += '<div class="team_finance" style="display:none;">';
                        /* var currwithdraw = {!! file_get_contents('https://blockchain.info/ticker') !!};
  var dwithraw =dataz['withdraw_amount'] /currwithdraw['USD']['sell'] ;*/

                        html += '<ul class="nav nav-tabs" id="myTab" role="tablist">\n' +
                            '  <li class="nav-item">\n' +
                            '    <a class="nav-link active" id="left_tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Left</a>\n' +
                            '  </li>\n' +
                            '  <li class="nav-item">\n' +
                            '    <a class="nav-link" id="right-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Right</a>\n' +
                            '  </li>\n' +
                            '</ul>' +
                            '<div class="tab-content" id="myTabContent">\n' +

                            '  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">' +
                            '<p style="text-align:left;font-weight: bold;">Withdraw Amount Available in Left Team Finance:</p>' +
                            '<input type="text" tabindex="3" placeholder="" name="profit" class="acc_name"  value="{{$leftwithdrawable}}"  readonly>' +
                            '@if($leftwithdrawable>=20)<p style="text-align:left;font-weight: bold;">Enter Withdraw Amunt:</p>' +
                            '<input type="number" tabindex="3" placeholder="Enter amount to Withdraw" id="finance_amount_left"   class="amount1" required>@else <p style="text-align:left;font-weight: bold; color: red;">Your are not Allowed to WithDraw Less then $20 :</p>@endif  ' +
                            '<span  id="finance_amount_left_error"style="text-align:left;font-weight: normal; color: red; display: none;">Please Enter a value less than and Equall to available Balance.</span>' +
                            '<p style="text-align:left;font-weight: bold;">BTC address :</p>' +
                            '<input type="text" tabindex="3" placeholder="BTC address" name="profit" class="btc_id"  value="' + dataz['btc_address'] + '" readonly >' +
                            '<p style="text-align:left;font-weight: bold;">Reference :</p>' +
                            '<input type="text" tabindex="3" placeholder="" name="sur_name" class=""  value="' + dataz['name'] + '"  readonly>' +
                            '</div> ' +
                            '<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">' +
                            '<p style="text-align:left;font-weight: bold;">Withdraw Amount Available in Right Team Finance:</p>' +
                            '<input type="text" tabindex="3" placeholder="" name="profit" class="acc_name"  value="{{$rightwithdrawable}}"  readonly>' +
                            '@if($rightwithdrawable>=20)<p style="text-align:left;font-weight: bold;">Enter Withdraw Amunt:</p>' +
                            '<input type="number" tabindex="3" placeholder="Enter amount to Withdraw" id="finance_amount_right"   class="amount1" required>@else  <p style="text-align:left;font-weight: bold; color: red;">Your are not Allowed to WithDraw Less then $20 :</p>@endif ' +
                            '<span  id="finance_amount_right_error"style="text-align:left;font-weight: normal; color: red; display: none;">Please Enter a value less than and Equall to available Balance </span>' +
                            '<p style="text-align:left;font-weight: bold;">BTC address :</p>' +
                            '<input type="text" tabindex="3" placeholder="BTC address" name="profit" class="btc_id"  value="' + dataz['btc_address'] + '" readonly>' +
                            '<p style="text-align:left;font-weight: bold;">Reference :</p>'
                        '<input type="text" tabindex="3" placeholder="" name="sur_name" class=""  value="' + dataz['name'] + '"  readonly>' +
                        '</div>' +
                        '</div>' +
                        '</div>';


                        html += '<div class="usd" style="display:none;">';
                        html += '<p style="text-align:left;font-weight: bold;">invested  Bank Name:</p>';
                        html += '<input type="text" tabindex="3" placeholder="enter your Bank Name" name="profit" class="acc_name"  value="' + dataz['acc_name'] + '">';
                        html += '<p style="text-align:left;font-weight: bold;">Bank Account No:</p>';
                        html += '<input type="number" tabindex="3" placeholder="enter your Bank Account No" name="profit" class="usd_id"  value="' + dataz['acc_no'] + '">';
                        html += '<p style="text-align:left;font-weight: bold;">Bank Branch Code: </p>';
                        html += '<input type="text" tabindex="3" placeholder="enter your Branch Code " name="profit" class="branch_code"  value="' + dataz['branch_code'] + '">';
                        html += '<p style="text-align:left;font-weight: bold;">Bank Type:</p>';
                        html += '<input type="text" tabindex="3" placeholder="enter your Branch Code " name="profit" class="acc_type"  value="' + dataz['acc_type'] + '">';
                        html += '<p style="text-align:left;font-weight: bold;">Swift Code:</p>';
                        html += '<input type="text" tabindex="3" placeholder="enter your Swift Code" name="profit" class="swift_code"  value="' + dataz['swift_code'] + '">';
                        html += '<p style="text-align:left;font-weight: bold;">Reference :</p>';
                        html += '<input type="text" tabindex="3" placeholder="" name="sur_name" class=""  value="' + dataz['name'] + '"  readonly>';
                        html += '<input type="number" tabindex="3" placeholder="Enter amount to Withdraw" id="usd_amount1"  value="0" class="amount1" required>';


                        html += '</div>';
                        html += ' </div>';
                        html += ' </div>';

                        swal({
                            title: "Withdraw",
                            text: "Note : All Withdrawals are processed within 72 hours. Your allowed only one Withdraw Request until approved it",
                            type: "input",
                            showCancelButton: true,
                            closeOnConfirm: false,
                            animation: "slide-from-top",
                            inputPlaceholder: "Write something",
                            closeOnCancel: false
                        });
                        $('.showSweetAlert').css("margin-top", "-350px");
                        $('.showSweetAlert fieldset').html(html);
                        $('.coin_type').on('change', function () {

                            var vcoin_type = $("select.coin_type").children("option:selected").val();
                            if (vcoin_type == "BTC") {
                                $('.usd').hide();
                                $('.btc').show();
                                $('.confirm').prop('disabled', true);
                                $('#btc_amount').on('focusout', function () {
                                    var new_amount = $('#btc_amount').val();
                                    var bal = {{$usdwithdrawable}};
                                    if (bal >= new_amount && 20 <= new_amount) {
                                        $('#btc_amount_error').hide();
                                        $('.confirm').prop('disabled', false);

                                    } else {
                                        if (20 >= new_amount) {
                                            $('#btc_amount_error').html("Your minimum withdrawal limit is 20$ ");
                                        } else {
                                            $('#btc_amount_error').html("Please Enter a value less than and Equal to available Balance ");
                                        }

                                        $('#btc_amount_error').show();
                                        $('.confirm').prop('disabled', true);

                                    }
                                    // if ( 20 <= new_amount) {
                                    //     $('#btc_amount_error').hide();
                                    //     $('.confirm').prop('disabled', false);
                                    //
                                    // } else {
                                    //
                                    //     $('#btc_amount_error').html("Please Enter a value Greater than and Equal to $20 ");
                                    //     $('#btc_amount_error').show();
                                    //     $('.confirm').prop('disabled', true);
                                    //
                                    // }
                                    // console.log(  $('#team_finance_amount1').val());
                                    // $('.confirm').prop('disabled',false);
                                });


                                $('.profit').hide();
                                $('.team_finance').hide();

                            }
                            if (vcoin_type == "usd") {

                                $('.usd').show();
                                $('.btc').hide();
                                $('.profit').hide();
                                $('.team_finance').hide();
                            }
                            if (vcoin_type == 'profit') {
                                // profit_amount
                                $('.profit').show()
                                $('.team_finance').hide();
                                $('.usd').hide();
                                $('.btc').hide();
                                $('.confirm').prop('disabled', true);
                                $('#profit_amount').on('focusout', function () {
                                    var new_amount = $('#profit_amount').val();
                                    var bal = {{$profitdrawable}};
                                    if (bal >= new_amount && 20 <= new_amount) {
                                        $('#profit_amount_error').hide();
                                        $('.confirm').prop('disabled', false);

                                    } else {
                                        if (20 >= new_amount) {
                                            $('#profit_amount_error').html("Your minimum withdrawal limit is 20$ ");
                                        } else {
                                            $('#profit_amount_error').html("Please Enter a value less than and Equal to available Balance ");
                                        }
                                        $('#profit_amount_error').show();
                                        $('.confirm').prop('disabled', true);

                                    }
                                    // console.log(  $('#team_finance_amount1').val());
                                    // $('.confirm').prop('disabled',false);
                                });

                            }
                            if (vcoin_type == 'team_finance') {
                                $('.team_finance').show();
                                $('#finance_amount_right').val('');
                                $('#finance_amount_left').val('');
                                $('.profit').hide()
                                $('.usd').hide();
                                $('.btc').hide();
                                // $('.confirm').prop('disabled',true);
                                // $('#team_finance_amount1').on ('focus',function(){
                                //     $('#team_finance_amount1').val(null);
                                // });
                                var flag = false;

                                $('#finance_amount_left').on('focusout', function () {
                                    var new_amount = $('#finance_amount_left').val();
                                    var b = {{$leftwithdrawable}};
                                    console.log(new_amount <= b);
                                    if (new_amount <= b && 20 <= new_amount) {
                                        flag = true;

                                        $('#finance_amount_left_error').hide();
                                        $('.confirm').prop('disabled', false);

                                    } else {
                                        flag = false;
                                        if (20>= new_amount){
                                            $('#finance_amount_left_error').html("Your minimum withdrawal limit is 20$ ")
                                        }else{
                                            $('#finance_amount_left_error').html("Please Enter a value less than and Equal to available Balance ")

                                        }
                                            $('#finance_amount_left_error').show();
                                        $('.confirm').prop('disabled', true);

                                    }
                                    // console.log(  $('#team_finance_amount1').val());
                                    // $('.confirm').prop('disabled',false);
                                });

                                $('#finance_amount_right').on('focusout', function () {
                                    console.log('focusout');
                                    var new_amount = $('#finance_amount_right').val();
                                    if (new_amount <= {{$rightwithdrawable}} && 20<=new_amount) {
                                        $('#finance_amount_right_error').hide();
                                        $('.confirm').prop('disabled', false);
                                        flag = true;
                                    } else {
                                        if(20 >= new_amount){
                                            $('#finance_amount_right_error').html("Your minimum withdrawal limit is 20$");

                                        }else{
                                            $('#finance_amount_right_error').html("Please Enter a value less than and Equal to available Balance ");
                                        }
                                        $('#finance_amount_right_error').show();
                                        $('.confirm').prop('disabled', true);
                                        flag = false;

                                    }
                                    // console.log(  $('#team_finance_amount1').val());
                                    // $('.confirm').prop('disabled',false);
                                });


                                if(flag){
                                    $('#finance_amount_left_error').hide();
                                    $('#finance_amount_right_error').hide();
                                    $('.confirm').prop('disabled',false);
                                }else {

                                    $('.confirm').prop('disabled',true);
                                }

                                // $('.coin_type').on('change', function ()


                                // $('.confirm').prop('disabled',true);


                            }
                            if (vcoin_type == '') {
                                $('.profit').hide();
                                $('.team_finance').hide();
                                $('.usd').hide();
                                $('.btc').hide();
                            }
                        });


                        $('.confirm').click(function () {


                            var vcoin_type = $("select.coin_type").children("option:selected").val();


                            // var amount = $('.amount1').val();

                            var data;
                            if (vcoin_type == 'profit') {
                                var u_id = id;
                                var btc_id = $('.btc_id').val();
                                var amount = $("#profit_amount").val();


                                var curr = {!! file_get_contents('https://blockchain.info/ticker') !!};

                                var rate = curr['USD']['sell'];

                                data = {
                                    "_token": "{{ csrf_token() }}",
                                    is_withdraw: 2,
                                    u_id: u_id,
                                    withdraw_amount: amount,
                                    btc_id: btc_id,
                                    coin_type: 'profit',
                                    btc_rate: rate
                                };
                            }

                            if (vcoin_type == 'team_finance') {
                                var u_id = id;
                                var btc_id = $('.btc_id').val();
                                $('#left_tab').hasClass('active') ? coin_type = "left" : coin_type = "right";
                                $('#left_tab').hasClass('active') ? amount = $("#finance_amount_left").val() : amount = $("#finance_amount_right").val();


                                var curr = {!! file_get_contents('https://blockchain.info/ticker') !!};

                                var rate = curr['USD']['sell'];

                                data = {
                                    "_token": "{{ csrf_token() }}",
                                    is_withdraw: 2,
                                    u_id: u_id,
                                    withdraw_amount: amount,
                                    btc_id: btc_id,
                                    coin_type: coin_type,
                                    btc_rate: rate

                                };
                            }

                            if (vcoin_type == 'BTC') {
                                var u_id = id;
                                var btc_id = $('.btc_id').val();
                                var amount = $("#btc_amount").val();


                                var curr = {!! file_get_contents('https://blockchain.info/ticker') !!};

                                var rate = curr['USD']['sell'];


                                data = {
                                    "_token": "{{ csrf_token() }}",
                                    is_withdraw: 2,
                                    u_id: u_id,
                                    withdraw_amount: amount,
                                    btc_id: btc_id,
                                    coin_type: 'usd',
                                    btc_rate: rate

                                };
                            }
                            if (vcoin_type == 'usd') {
                                var u_id = id;
                                var amount = $("#usd_amount1").val();

                                var curr = {!! file_get_contents('https://blockchain.info/ticker') !!};

                                var rate = curr['USD']['sell'];
                                data = {
                                    "_token": "{{ csrf_token() }}",

                                    u_id: u_id,
                                    acc_name: $('.acc_name').val(),
                                    usd_id: $('.usd_id').val(),
                                    branch_code: $('.branch_code').val(),
                                    bank_type: $('.acc_type').val(),
                                    swift_code: $('.swift_code').val(),
                                    withdraw_amount: amount,
                                    coin_type: 'usd',
                                    is_withdraw: 2,
                                    btc_rate: rate


                                };
                            }


                            var url = '<?= route('withdraw.request')?>';
                            $.ajax({
                                type: "POST",
                                url: url,
                                data: data,
                                success: function (response) {

                                    if (response.success) {
                                        $('.withdraw').prop('disabled', true);

                                        $('body').find('.sweet-overlay').remove();
                                        $('body').find('.hideSweetAlert').remove();
                                        swal("Request Submit to Admin", "Successfully !!", "success");
                                        $('.confirm').on('click', function () {
                                            window.location.reload();
                                        });

                                    } else {
                                        if (response.error == 409) {

                                            $(this).parent().parent().parent().remove();

                                            $('body').find('.sweet-overlay').remove();
                                            $('body').find('.hideSweetAlert').remove();

                                            swal("Request Rejected", "You have not Filled All fields !!", "error");
                                            $('.confirm').on('click', function () {
                                                window.location.reload();
                                            });

                                        }
                                        if (response.error == 408) {

                                            $(this).parent().parent().parent().remove();

                                            $('body').find('.sweet-overlay').remove();
                                            $('body').find('.hideSweetAlert').remove();

                                            swal("Request Rejected", "You don't have  enough balance !!", "error");
                                            $('.confirm').on('click', function () {
                                                window.location.reload();
                                            });
                                        }
                                        if (response.error == 405) {

                                            $(this).parent().parent().parent().remove();

                                            $('body').find('.sweet-overlay').remove();
                                            $('body').find('.hideSweetAlert').remove();

                                            swal("Request Rejected", "You don't have sufficient fund !!", "error");
                                            $('.confirm').on('click', function () {
                                                window.location.reload();
                                            });
                                        }
                                        if (response.error == 1010) {

                                            $(this).parent().parent().parent().remove();

                                            $('body').find('.sweet-overlay').remove();
                                            $('body').find('.hideSweetAlert').remove();

                                            swal("Request Rejected", "You can send only one request at a time\n" +
                                                "A Withdrawl Request is already pending !!", "error");
                                            $('.confirm').on('click', function () {
                                                window.location.reload();
                                            });
                                        }
                                        if (response.error == 1212) {

                                            $(this).parent().parent().parent().remove();

                                            $('body').find('.sweet-overlay').remove();
                                            $('body').find('.hideSweetAlert').remove();

                                            swal("Request Rejected", "Your minimum withdrawal limit is 20$!!", "error");
                                            $('.confirm').on('click', function () {
                                                // window.location.reload();
                                            });
                                        }

                                    }
                                },
                                error: function () {
                                    $(this).parent().parent().parent().remove();

                                    $('body').find('.sweet-overlay').remove();
                                    $('body').find('.hideSweetAlert').remove();

                                    swal("Request Rejected", "", "error");
                                }
                            });
                        });
                        $('.cancel').click(function () {
                            $('body').find('.sweet-overlay').remove();
                            // $(this).parent().parent().closest(".sweet-overlay").remove();
                            $(this).parent().parent().remove();

                        });

                    } else {

                        swal({
                            title: "Withdraw",
                            text: "Note : You can send only one request at a time\n" +
                                "A Withdrawl Request is already pending",
                            // type: "input",
                            // showCancelButton: true,
                            closeOnConfirm: true,
                            animation: "slide-from-top",
                            // inputPlaceholder: "Write something",
                            // closeOnCancel: false
                        });
                        $('.confirm').on('click', function () {
                            // window.location.reload();
                        });
                    }
                });

        });


    </script>




@endsection
