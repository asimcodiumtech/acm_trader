@extends('layouts.app')

@section('dashboardcontent')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>DataTables</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<section class="content">
      <div class="row">
        <div class="col-12">

			<div class="card">
			            <div class="card-header">
			              <h3 class="card-title">DataTable with default features</h3>
			            </div>
			            <!-- /.card-header -->
			            <div class="card-body">
			              <table  class="table table-bordered table-striped example1">
			                <thead>
			                <tr>
			                  <th>Amount</th>
			                  <th>Coin Type</th>
			                  <th>Transaction Id</th>
			                  <th>Profit</th>
			                  <th>Deposite Status</th>
			                  <th>Withdraw Status</th>
			                </tr>
			                </thead>
			                <tbody>
			                @foreach($users as $user)
                             <tr>
                             	<td>
						          {{$user->amount}}
						        </td>
						        <td>
						          {{$user->coin_type}}
						        </td>
                                <td>
						          {{$user->trans_id}}
						        
						        <td>
						          {{$user->profit}}
						        </td>
						        <td>
						          @if($user->deposite_status==1)
						            <button  class="btn btn-success"><b> Approved </b></button>
						            @elseif($user->deposite_status==2)
						            <button  class="btn btn-warning"><b> pending </b></button>
						            
						            @else
						           <button  class="btn btn-danger"><b> Not Approved </b></button>
						           @endif
						        </td>
						        <td>
						          @if($user->withdraw_status==1)
						            <button  class="btn btn-success"><b> Approved </b></button>
						            @elseif($user->withdraw_status==2)
						            <button  class="btn btn-warning"><b> pending </b></button>
						            
						            @else
						           <button  class="btn btn-danger"><b> No Applied </b></button>
						           @endif
						        </td>
                             </tr>
			                @endforeach
			                </tbody>
			                <tfoot>
			                <tr>
			                  <th>Amount</th>
			                  <th>Coin Type</th>
			                  <th>Transaction Id</th>
			                  <th>Profit</th>
			                  <th>Deposite Status</th>
			                  <th>Withdraw Status</th>
			                </tr>
			                </tfoot>
			              </table>
			            </div>
			            <!-- /.card-body -->
			</div>
	   </div>
	  </div>
</section>	


<script>
  $(function () {
    $(".example1").DataTable();
    $('.example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>	
@endsection