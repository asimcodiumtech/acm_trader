@extends('layouts.app')

@section('dashboardcontent')
    <style>
        .bg {

            background-repeat: no-repeat;

            background-position: center;
            height: 20vh;
            width: 20vh;
        }
    </style>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 style="color: white;">Gift Add</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Gift Add</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <div class="card card-primary">
                        <div class="card-header" style="background-color: #42426a;color: white;">
                            <h3 class="card-title">Purchase Gift Code</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form method="POST" action="#" enctype="">
                            @csrf
                            <div class="card-body" style="background-color:#35354B; color:white;">
                                <div class="form-group">
                                    <select name="coin_type" class="form-control coin_type" tabindex="3" required
                                            style="background-color:#131311; color:white;">
                                        <option value="">Please Select</option>
                                        @if(auth()->user()->id ==68)
                                        <option value="BTC">From BTC</option>
                                        @endif
                                        <option value="profit">From Profit</option>
                                        <option value="finance">From Team Finance</option>
                                        <option value="usd">From USD Invested</option>
                                        {{--                                        <option value="cash">From Cash</option>--}}
                                    </select>
                                </div>
                                <div class="cash" style="display:none;">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Set Amount</label>
                                        <input type="number" class="form-control amountaa" id="cash_amount"
                                               placeholder="Enter amount in USD " name="amount" value="" required
                                               style="background-color:#131311; color:white;">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" class="form-control gift_code_cash" id=""
                                               placeholder="Enter acount Name " name="gift_code" value="{{$hash}}"
                                               required style="background-color:#131311; color:white;" readonly>
                                    </div>

                                    <div class="">
                                        <button type="button" class="btn btn-primary"
                                                style="background-color: #35354B;color: white; " id='cash_payment'>
                                            Submit
                                        </button>
                                    </div>
                                </div>
                                <div class="profit" style="display:none;">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Profit Available</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder=""
                                               name="" value="$ {{round($profitwithdrawable)}} " required
                                               style="background-color:#131311; color:white;" readonly>
                                    </div>


                                    <div class="form-group">
                                        <input type="hidden" class="form-control gift_code_cash" id="exampleInputEmail1"
                                               placeholder="Enter acount Name " name="gift_code" value="{{$hash}}"
                                               required style="background-color:#131311; color:white;" readonly>
                                    </div>
                                    <div class="form-group">
                                        @if($profitwithdrawable>=100)
                                            <label for="exampleInputEmail1">Set Amount</label>

                                            <input type="number" class="form-control amountaa" id="profitwithdrawable"
                                                   placeholder="Enter amount in USD " name="amount" value="" required
                                                   style="background-color:#131311; color:white;">@else <p
                                            style="text-align:left;font-weight: bold; color: red;">Your are not Allowed
                                            to Create Gift code Less then $100 :</p>@endif

                                        <span id="profitwithdrawable_error"
                                              style="text-align:left;font-weight: normal; color: red; display: none;">Please Enter a value less than and Equal to available Balance </span>

                                    </div>


                                </div>
                                <div class="finance" style="display:none;">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="left_tab" data-toggle="tab" href="#home"
                                               role="tab" aria-controls="home" aria-selected="true">Left</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="right-tab" data-toggle="tab" href="#profile"
                                               role="tab" aria-controls="profile" aria-selected="false">Right</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="myTabContent">

                                        <div class="tab-pane fade show active" id="home" role="tabpanel"
                                             aria-labelledby="home-tab">
                                            <p style="text-align:left;font-weight: bold;">Withdraw Amount Available in
                                                Left Team Finance:</p>
                                            <input type="text" tabindex="3" placeholder="" name="profit"
                                                   class="acc_name" value="{{$leftwithdrawable}}" readonly>
                                            @if($leftwithdrawable>=100)
                                                <p style="text-align:left;font-weight: bold;">Enter Withdraw Amunt:</p>
                                                <input type="number" tabindex="3" placeholder="Enter amount to Withdraw"
                                                       id="finance_amount_left" class="amount1" required>
                                            @else<p style="text-align:left;font-weight: bold; color: red;">Your are not
                                                Allowed to Create Gift code Less then $100 :</p>@endif
                                            <span id="finance_amount_left_error"
                                                  style="text-align:left;font-weight: normal; color: red; display: none;">Please Enter a value less than and Equal to available Balance.</span>
                                        </div>
                                        <div class="tab-pane fade" id="profile" role="tabpanel"
                                             aria-labelledby="profile-tab">
                                            <p style="text-align:left;font-weight: bold;">Withdraw Amount Available in
                                                Right Team Finance:</p>
                                            <input type="text" tabindex="3" placeholder="" name="profit"
                                                   class="acc_name" value="{{$rightwithdrawable}}" readonly>
                                            @if($rightwithdrawable>=100)
                                                <p style="text-align:left;font-weight: bold;">Enter Withdraw Amunt:</p>
                                                <input type="number" tabindex="3" placeholder="Enter amount to Withdraw"
                                                       id="finance_amount_right" class="finance_amount_right" required>
                                            @else<p style="text-align:left;font-weight: bold; color: red;">Your are not
                                                Allowed to Create Gift code Less then $100 :</p>@endif

                                            <span id="finance_amount_right_error"
                                                  style="text-align:left;font-weight: normal; color: red; display: none;">Please Enter a value less than and Equal to available Balance </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="usd" style="display:none;">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">USD Available</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder=""
                                               name="" value="$ {{round($usdwithdrawable)}} " required
                                               style="background-color:#131311; color:white;" readonly>
                                    </div>


                                    <div class="form-group">
                                        <input type="hidden" class="form-control" id="exampleInputEmail1"
                                               placeholder="Enter acount Name " name="gift_code" value="{{$hash}}"
                                               required style="background-color:#131311; color:white;" readonly>
                                    </div>
                                    <div class="form-group">
                                        @if($usdwithdrawable>=100)
                                            <label for="exampleInputEmail1">Set Amount</label>
                                            <input type="number" class="form-control usdwithdrawable"
                                                   id="exampleInputEmail2"
                                                   placeholder="Enter amount in USD " name="amount" value="" required
                                                   style="background-color:#131311; color:white;">
                                        @else<p style="text-align:left;font-weight: bold; color: red;">Your are not
                                            Allowed to Create Gift code Less then $100 :</p>@endif

                                        <span id="usdwithdrawable_error"
                                              style="text-align:left;font-weight: normal; color: red; display: none;">Please Enter a value less than and Equal to available Balance </span>

                                    </div>


                                </div>


                                <style>
                                    html {
                                        font-size: 14px;
                                    }

                                    @media (min-width: 768px) {
                                        html {
                                            font-size: 16px;
                                        }

                                        .tooltip-inner {
                                            max-width: 350px;
                                        }
                                    }

                                    .mncrpt .container {
                                        max-width: 980px;
                                    }

                                    .mncrpt .box-shadow {
                                        box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .05);
                                    }

                                    img.radioimage-select {
                                        padding: 7px;
                                        border: solid 2px #ffffff;
                                        margin: 7px 1px;
                                        cursor: pointer;
                                        box-shadow: none;
                                    }

                                    img.radioimage-select:hover {
                                        border: solid 2px #a5c1e5;
                                    }

                                    img.radioimage-select.radioimage-checked {
                                        border: solid 2px #7db8d9;
                                        background-color: #f4f8fb;
                                    }
                                </style>
                                <div class="btc" style="display:none;">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Set Amount</label>
                                        <input type="number" class="form-control amountInput" id="amountInput"
                                               placeholder="Enter amount in USD " name="as" value=""
                                               style="background-color:#131311; color:white;">
                                    </div>
                                    <div class="form-group">

                                        <button type="button" class="btn btn-primary float-right payment"
                                                style="background-color: #35354B;color: white;">Make Payment
                                        </button>
                                    </div>

                                    <div class="form-group btcData">
                                        <!-- Bootstrap4 CSS - -->

                                    </div>
                                </div>
                                <div class="btcstop" style="display:none;">
                                    <p style="text-align:left;font-weight: bold; color:red">BTC payments currently not available </p>
                                </div>
                            </div>
                            <div class="card-footer confirm" style="display:none; background-color: #42426a !important;color: white;">
                                <button type="button" class="btn btn-primary"
                                        style="background-color: #42426a !important;color: white;">Submit
                                </button>
                            </div>
                            <link rel="stylesheet" href="https://bootswatch.com/4/superhero/bootstrap.css"
                                  crossorigin="anonymous">

                            <!-- Note - If your website not use Bootstrap4 CSS as main style, please use custom css style below and delete css line above.
                            It isolate Bootstrap CSS to a particular class 'bootstrapiso' to avoid css conflicts with your site main css style -->
                            <!-- <link rel="stylesheet" href="css/superhero.min.css" crossorigin="anonymous"> -->


                            <!-- JS -->
                            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"
                                    crossorigin="anonymous"></script>
                            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"
                                    crossorigin="anonymous"></script>
                            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
                                    crossorigin="anonymous"></script>
                            <script defer src="https://use.fontawesome.com/releases/v5.12.0/js/all.js"
                                    crossorigin="anonymous"></script>
                            <script src="<?php echo CRYPTOBOX_JS_FILES_PATH; ?>support.min.js"
                                    crossorigin="anonymous"></script>

                            <!-- CSS for Payment Box -->


                            <!-- /.card-body -->
                        </form>
                    </div>
                    {{--                        </form>--}}
                    <meta name="csrf-token" content="{{ csrf_token() }}"/>
                </div>
            </div>
        </div>
        </div>
    </section>

    <script type="text/javascript">
        $('.coin_type').on('change', function () {

            var vcoin_type = $("select.coin_type").children("option:selected").val();
            if (vcoin_type == "BTC") {

                $('.btc').show();
                // $('.btcstop').show();
                $('.gift').hide();
                $('.cash').hide();
                $('.card-footer').hide();
                $('.profit').hide();
                $('.usd').hide();
                $('.finance').hide();

            }

            if (vcoin_type == "gift") {
                console.log(vcoin_type);
                $('.btcstop').hide();

                $('.gift').show();
                $('.card-footer').show();
                $('.btc').hide();
                $('.cash').hide();
                $('.usd').hide();
                $('.finance').hide();
                $('.profit').hide();


            }
            if (vcoin_type == "usd") {
                console.log(vcoin_type);
                $('.btcstop').hide();

                $('.usd').show();
                $('.gift').hide();
                $('.card-footer').hide();
                $('.finance').hide();
                $('.btc').hide();
                $('.cash').hide();
                $('.profit').hide();

                $('.usdwithdrawable').on('focusout', function () {
                    var new_amount = $('.usdwithdrawable').val();
                    var bal = {{$usdwithdrawable}};
                    if (bal >= new_amount && 100 <= new_amount) {
                        $('#usdwithdrawable_error').hide();
                        $('.card-footer').show();

                    } else {
                        if (100 >= new_amount) {
                            $('#usdwithdrawable_error').html("Your minimum withdrawal limit is 100$ ");
                        } else {
                            $('#usdwithdrawable_error').html("Please Enter a value less than and Equal to available Balance ");

                        }
                        $('#usdwithdrawable_error').show();
                        $('.card-footer').hide();

                    }
                    // console.log(  $('#team_finance_amount1').val());
                    // $('.confirm').prop('disabled',false);
                });


            }


            if (vcoin_type == 'cash') {
                console.log(vcoin_type);
                $('.gift').hide();
                $('.cash').show();
                $('.btcstop').hide();

                $('.btc').hide();
                $('.usd').hide();
                $('.profit').hide();
                $('.finance').hide();
                $('.card-footer').hide();
            }
            if (vcoin_type == 'finance') {
                console.log(vcoin_type);
                $('.gift').hide();
                $('.finance').show();
                $('.cash').hide();
                $('.usd').hide();
                $('.btc').hide();
                $('.btcstop').hide();

                $('.profit').hide();
                $('.card-footer').hide();
                $('#finance_amount_right').val('');
                $('#finance_amount_left').val('');
                var flag = false;

                $('#finance_amount_left').on('focusout', function () {
                    var new_amount = $('#finance_amount_left').val();
                    var b = {{$leftwithdrawable}};
                    console.log(new_amount <= b);
                    if (new_amount <= b && 100 <= new_amount) {
                        flag = true;

                        $('#finance_amount_left_error').hide();
                        $('.card-footer').show();


                    } else {
                        flag = false;
                        if (100 >= new_amount) {
                            $('#finance_amount_left_error').html("Your minimum withdrawal limit is 100$ ")
                        } else {
                            $('#finance_amount_left_error').html("Please Enter a value less than and Equal to available Balance ")

                        }
                        $('#finance_amount_left_error').show();
                        $('.card-footer').hide();

                    }

                    // console.log(  $('#team_finance_amount1').val());
                    // $('.confirm').prop('disabled',false);
                });

                {{--$('#finance_amount_right').on('focusout', function () {--}}
                {{--    console.log('focusout');--}}
                {{--    var new_amount = $('#finance_amount_right').val();--}}
                {{--    if (new_amount <= {{$rightwithdrawable}}) {--}}
                {{--        $('#finance_amount_right_error').hide();--}}
                {{--        $('.card-footer').show();--}}

                {{--        flag = true;--}}
                {{--    } else {--}}
                {{--        $('#finance_amount_right_error').show();--}}
                {{--        $('.card-footer').hide();--}}

                {{--        flag = false;--}}

                {{--    }--}}
                {{--    // console.log(  $('#team_finance_amount1').val());--}}
                {{--    // $('.confirm').prop('disabled',false);--}}
                {{--});--}}
                $('#finance_amount_right').on('focusout', function () {
                    console.log('focusout');
                    var new_amount = $('#finance_amount_right').val();
                    if (new_amount <= {{$rightwithdrawable}} && 100 <= new_amount) {
                        $('#finance_amount_right_error').hide();
                        $('.card-footer').show();

                        $('.confirm').prop('disabled', false);
                        flag = true;
                    } else {
                        if (100 >= new_amount) {
                            $('#finance_amount_right_error').html("Your minimum withdrawal limit is 100$");

                        } else {
                            $('#finance_amount_right_error').html("Please Enter a value less than and Equal to available Balance ");
                        }
                        $('#finance_amount_right_error').show();
                        $('.card-footer').hide();

                        $('.confirm').prop('disabled', true);
                        flag = false;

                    }
                    // console.log(  $('#team_finance_amount1').val());
                    // $('.confirm').prop('disabled',false);
                });
            }
            if (vcoin_type == 'profit') {
                console.log(vcoin_type);
                $('.gift').hide();
                $('.cash').hide();
                $('.usd').hide();
                $('.btcstop').hide();

                $('.btc').hide();
                $('.finance').hide();

                $('.profit').show();
                $('.card-footer').hide();
                $('#profitwithdrawable').on('focusout', function () {
                    var new_amount = $('#profitwithdrawable').val();
                    var bal = {{$profitwithdrawable}};
                    if (bal >= new_amount && 100 <= new_amount) {
                        $('#profitwithdrawable_error').hide();
                        $('.card-footer').show();

                    } else {
                        if (100 >= new_amount) {
                            $('#profitwithdrawable_error').html("Your minimum withdrawal limit is 100$ ");
                        } else {
                            $('#profitwithdrawable_error').html("Please Enter a value less than and Equal to available Balance ");
                        }
                        $('#profitwithdrawable_error').show();
                        $('.card-footer').hide();

                    }
                    // console.log(  $('#team_finance_amount1').val());
                    // $('.confirm').prop('disabled',false);
                });

            }
            if (vcoin_type == '') {
                console.log(vcoin_type);
                $('.gift').hide();
                $('.btc').hide();
                $('.usd').hide();
                $('.btcstop').hide();

                $('.cash').hide();
                $('.finance').hide();
                $('.profit').hide();

                $('.card-footer').hide();
            }
        });
        // var click_count =0;
        // var click_count_after =0;
        $('.confirm').click(function (e) {
            e.preventDefault();
            // click_count++;
            var me = $(this);
            me.attr('disabled', true);
            // console.log('before clicked',click_count,'clicked after',click_count_after)
            if ($('.confirm').data('requestRunning')) {
                return;
            }
            // click_count_after++;
            // console.log('clicked after',click_count_after)

            me.text('Request Running');
            $('.confirm').data('requestRunning', true);
            // $('.confirm').attr('disabled', true);

            $('.confirm').text('requestRunning');

            var vcoin_type = $("select.coin_type").children("option:selected").val();


            // var amount = $('.amount1').val();
            var id ={{auth()->user()->id}};
            var data;
            if (vcoin_type == 'profit') {
                var u_id = id;
                var btc_id = $('.btc_id').val();
                var amount = $("#profitwithdrawable").val();
                var gift_code = $(".gift_code_cash").val();

                console.log(gift_code);

                var curr = {!! file_get_contents('https://blockchain.info/ticker') !!};

                var rate = curr['USD']['sell'];

                data = {

                    is_withdraw: 1,
                    u_id: u_id,
                    withdraw_amount: amount,
                    coin_type: 'profit',
                    btc_rate: rate,
                    is_gift: 1,
                    gift_code: gift_code
                };
            }

            if (vcoin_type == 'finance') {
                var u_id = id;
                var btc_id = $('.btc_id').val();
                var gift_code = $(".gift_code_cash").val();

                // console.log( u_id);

                $('#left_tab').hasClass('active') ? coin_type = "left" : coin_type = "right";
                $('#left_tab').hasClass('active') ? amount = $("#finance_amount_left").val() : amount = $("#finance_amount_right").val();

                var curr = {!! file_get_contents('https://blockchain.info/ticker') !!};

                var rate = curr['USD']['sell'];

                data = {

                    is_withdraw: 1,
                    u_id: u_id,
                    withdraw_amount: amount,
                    btc_id: btc_id,
                    coin_type: coin_type,
                    btc_rate: rate,
                    is_gift: 1,
                    gift_code: gift_code


                };
            }

            if (vcoin_type == 'BTC') {
                var u_id = id;
                var btc_id = $('.btc_id').val();
                var amount = $("#btc_amount").val();
                var gift_code = $(".gift_code_cash").val();


                var curr = {!! file_get_contents('https://blockchain.info/ticker') !!};

                var rate = curr['USD']['sell'];


                data = {

                    is_withdraw: 1,
                    u_id: u_id,
                    withdraw_amount: amount,
                    btc_id: btc_id,
                    coin_type: 'usd',
                    btc_rate: rate,
                    is_gift: 1,
                    gift_code: gift_code


                };
            }
            if (vcoin_type == 'usd') {
                var u_id = id;
                var amount = $(".usdwithdrawable").val();
                var gift_code = $(".gift_code_cash").val();


                var curr = {!! file_get_contents('https://blockchain.info/ticker') !!};

                var rate = curr['USD']['sell'];
                data = {


                    u_id: u_id,
                    // acc_name: $('.acc_name').val(),
                    // usd_id: $('.usd_id').val(),
                    // branch_code: $('.branch_code').val(),
                    // bank_type: $('.acc_type').val(),
                    // swift_code: $('.swift_code').val(),
                    withdraw_amount: amount,
                    coin_type: 'usd',
                    is_withdraw: 1,
                    btc_rate: rate,
                    is_gift: 1,
                    gift_code: gift_code


                };
            }


            var url = '<?= route('user.gift.store')?>';
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function (response) {

                    if (response.success) {
                        $('body').find('.sweet-overlay').remove();
                        $('body').find('.hideSweetAlert').remove();
                        swal("Gift Created ", "Successfully !!", "success");
                        $('.confirm').on('click', function () {
                            window.location.reload();
                        });

                    } else {
                        if (response.error == 409) {

                            $(this).parent().parent().parent().remove();

                            $('body').find('.sweet-overlay').remove();
                            $('body').find('.hideSweetAlert').remove();

                            swal("Request Rejected", "You have not Filled All fields !!", "error");
                            $('.confirm').on('click', function () {
                                window.location.reload();
                            });

                        }
                        if (response.error == 408) {

                            $(this).parent().parent().parent().remove();

                            $('body').find('.sweet-overlay').remove();
                            $('body').find('.hideSweetAlert').remove();

                            swal("Request Rejected", "You don't have  enough balance !!", "error");
                            $('.confirm').on('click', function () {
                                window.location.reload();
                            });
                        }
                        if (response.error == 405) {

                            $(this).parent().parent().parent().remove();

                            $('body').find('.sweet-overlay').remove();
                            $('body').find('.hideSweetAlert').remove();

                            swal("Request Rejected", "You don't have sufficient fund !!", "error");
                            $('.confirm').on('click', function () {
                                window.location.reload();
                            });
                        }
                        if (response.error == 1212) {

                            $(this).parent().parent().parent().remove();

                            $('body').find('.sweet-overlay').remove();
                            $('body').find('.hideSweetAlert').remove();

                            swal("Request Rejected", "Your minimum withdrawal limit is 100$!!", "error");
                            $('.confirm').on('click', function () {
                                // window.location.reload();
                            });
                        }
                    }
                },
                error: function () {
                    $(this).parent().parent().parent().remove();

                    $('body').find('.sweet-overlay').remove();
                    $('body').find('.hideSweetAlert').remove();

                    swal("Request Rejected", "", "error");
                },
                complete: function () {
                    $('.confirm').text('completed');

                    me.text('completed');
                    me.attr('disabled', true);
                    // $('.confirm').attr('disabled', true);

                    // me.data('requestRunning', false);
                }
            });
        });

        $(".payment").click(function () {

            var amountInput = $('.amountInput').val();
            if (amountInput) {
                var amount = parseFloat(amountInput);
                var afterFee = Number(parseFloat(amountInput) + parseFloat(amountInput) * 0.1).toFixed(3);


                $('.amountInputfee').val(afterFee);

                var url = '<?= route('user.ajax.btc')?>';
                data = {"_token": "{{ csrf_token() }}", amount: amount, type: 'gift'};
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (response) {
                        console.log(response);
                        $('.bootstrapiso').html('');
                        $('.bootstrapiso').html('');
                        $('.bootstrapiso').html('');
                        $('.btcData').fadeOut().html(response.data).fadeIn();

                    },
                    error: function () {

                    }
                });

            } else {

                $('.btcData').fadeOut().html('').fadeIn();
                $('.amountInputfee').val(0);

            }
        });
        $("#cash_payment").click(function () {

            var cash_amount = $('#cash_amount').val();
            var gift_code = $('#gift_code_cash').val();
            if (cash_amount) {
                var amount = parseFloat(cash_amount);
                // var afterFee = Number(parseFloat(cash_amount) + parseFloat(cash_amount) * 0.1).toFixed(3);


                // $('.cash_amount').val(afterFee);
                // if(cash_amount != '' )
                var url = '<?= route('user.ajax.cash')?>';
                data = {"_token": "{{ csrf_token() }}", amount: amount, type: 'cash', gift_code: gift_code};
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (response) {
                        console.log(response);
                        swal("Added", "Gift Created Successfully", "success");
                        // $('.bootstrapiso').html('');
                        // $('.bootstrapiso').html('');
                        // $('.bootstrapiso').html('');
                        // $('.btcData').fadeOut().html(response.data).fadeIn();
                        window.Location.replace('<?= route('user.gift.list')?>');
                    },
                    error: function () {

                    }
                });

            } else {

                // $('.btcData').fadeOut().html('').fadeIn();
                $('#cash_amount').val(0);

            }
        });

        $(".amount").on('change keyup', function () {

            if ($(this).val()) {
                var checkamount = parseInt($(this).val());
                var fee = Number(parseFloat($(this).val()) + parseFloat($(this).val()) * 0.1).toFixed(3);
                $('.amountfee').val(fee);

            } else {
                $('.amountfee').val(0);
            }
        });

    </script>


@endsection
