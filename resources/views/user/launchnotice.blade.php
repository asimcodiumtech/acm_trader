@extends('layouts.app')

@section('dashboardcontent')
    <style>
        @media (max-width:  758px) {
            .center {
               background-color: red;
                margin-left: -25%;

            }
        }
        body {
            margin: 0;
            background-color: #010001;
        }

        h1, h4 {
            color: #f32323;
        }

        .content {
            padding: 10px 20px;
        }

        .content p {
            font-family: sans-serif;
        }

        .size {
            font-size: 18px;

            font-weight: 500;
            background-color: #0d151d;
            color: white;
        }

        .center {
            margin-left: 25%;
        }

        /******/

        .ticker-container {
            width: 100%;
            overflow: hidden;
        }

        .ticker-canvas {
            width: calc((200px * 6) + 2px);
            /*
            200px = minimum width of ticker item before widget script starts removing ticker codes
            15 = number of ticker codes
            2px = accounts for 1px external border
            */
            -webkit-animation-iteration-count: infinite;
            animation-iteration-count: infinite;
            -webkit-animation-timing-function: linear;
            animation-timing-function: linear;
            -webkit-animation-name: ticker-canvas;
            animation-name: ticker-canvas;
            -webkit-animation-duration: 20s;
            animation-duration: 20s;
        }

        .ticker-canvas:hover {
            animation-play-state: paused
        }

        @-webkit-keyframes ticker-canvas {
            0% {
                -webkit-transform: translate3d(0, 0, 0);
                transform: translate3d(0, 0, 0);
                visibility: visible;
            }
            100% {
                -webkit-transform: translate3d(-100%, 0, 0);
                transform: translate3d(-100%, 0, 0);
            }
        }

        @keyframes ticker-canvas {
            0% {
                -webkit-transform: translate3d(0, 0, 0);
                transform: translate3d(0, 0, 0);
                visibility: visible;
            }
            100% {
                -webkit-transform: translate3d(-100%, 0, 0);
                transform: translate3d(-100%, 0, 0);
            }
        }

        .tradingview-widget-container {
            position: relative;
        }

        .tradingview-widget-container iframe {
            position: absolute;
            top: 0;
        }

        .tradingview-widget-container iframe:nth-of-type(2) {
            left: 100%;
        }

        .sweet-alert select {
            width: 100%;
            box-sizing: border-box;
            border-radius: 3px;
            border: 1px solid #d7d7d7;
            height: 43px;
            margin-top: 10px;
            margin-bottom: 17px;
            font-size: 18px;
            background-color: #131311;
            color: white;
        }
    </style>

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 " style="color: white;">Launch Notice</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Launch Notice</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <section class="content">
        <div class="container">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                <div class="title_default_dark ">
                    <div class="col-md-12   mt-3">

                            <video class="myHTMLvideo" width="100%" height="100%" playsinline autoplay muted loop>
                                <source src="../videos/WhatsApp Video 2020-09-23 at 1.25.18 PM.mp4" type="video/mp4">
                            </video>


                    </div>
                    <h1 class=" animation text-center " data-animation="fadeInUp" data-animation-delay="0.2s">This
                        notice contains information about technology upgrades, mobile application, incentives structure
                        and our business model going forward. </h1>
                    <div class="col-md-12 pb-4" style="border-bottom: 3px solid red"></div>
                    <h4 class="animation mt-4" data-animation="fadeInUp" data-animation-delay="0.2s">Technology Upgrades
                        and Mobile App</h4>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">In our sincerest efforts
                        to provide an excellent service to all of our users, we are currently in the works of upgrading
                        your trading software to enable the most-up-to-date features that will improve efficiency and
                        user control.</p>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">With our technology
                        upgrade, there are some changes that will affect the products and
                        services we provide to you. We want you to be aware of these changes and ask that you
                        please read this notice carefully to help ensure the transition to our new system causes you as
                        little disruption as possible.</p>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">We have finally reached
                        the completion of our testing sessions and thus far, the results are
                        outstanding. Based on stats produced by our data, we have reason to believe that your money
                        has been working for you. We are confident that at least to a satisfactory extent, it has been
                        helpful in solidifying your finances and has quenched your thirst for financial independence
                        without any further experience of losses or unreasonable returns.
                    </p>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">During the past months we
                        have been on a prelaunch stage, where we were testing the
                        system’s efficiency and its overall ability to trade for multiple accounts simultaneously. These
                        testing sessions were also helpful in developing our mobile application that has improved user
                        control and secure access to accounts. </p>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">The launch will introduce
                        new products offering residual income structures and an incentive
                        program that requires little networking efforts. This will broaden your earning horizons and
                        give you better control of your profit intake. However, these new applications that have been
                        put in place will run at a minimal expense, which will now recognize users as subscribers that
                        are subject to paying recurring subscriptions to ensure efficient program functionality, since
                        these new features run at a high maintenance cost. </p>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">The mobile app will
                        consist of the following features: </p>
                    <ul style="list-style-type:none; padding-left: 0% !important;">
                        <li>User Friendly Interface</li>
                        <li>Tracking Technology</li>
                        <li>Dynamic Tech 24/7</li>
                        <li>Added Security</li>
                        <li>Dollar Averages</li>

                    </ul>
                    <h4 class="mt-3">Incentives </h4>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">We are always looking
                        forward to building solid networks with people who aspire to get the
                        most out of opportunities. During the course of our prelaunch we experienced a remarkable
                        growth and we are looking to expand our team so we can magnify the culture of our business
                        and commercial presence. </p>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">We are excited to explore
                        new concepts and share effective networking strategies. That is
                        why we have an affiliate program that monitors users marketing skills and leadership aptitude
                        based on how they build cooperative teams with mindful individuals, who are in this for the
                        long haul. </p>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Performing members will
                        be recognised with an invitation to a team building trips, where we
                        will have networking sessions and activities that will help cultivate independent thinking and
                        leadership skills. </p>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Our aim is to have an
                        expansive affiliate program that offers incentives that help you grow as
                        a business and as a brand that is affiliated with successful people. </p>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">We believe that this is
                        necessary and will be very helpful for our business going forward. </p>

                    <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Business Model</h4>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Our business model
                        entails investment portfolios that are based on preserving and growing
                        sustainable wealth. We are well prepared to build long term wealth for our clients and have
                        many resources to identify undervalued digital assets with promising future value. Building
                        long term wealth requires patience, commitment and involvement; that’s the reason why we
                        have created a space for investors who are ready to focus on creating financial security. </p>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Having said that, in
                        order for the trading bot to operate at maximum efficiency, it requires
                        mathematical precision, which is vital if we are to pursue long term wealth without taking
                        undue risks. </p>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">During testing sessions
                        we discovered that, the software performs at its best when it’s trading
                        for users with committed accounts. Hence from the launch date, the trading software will
                        only be available to users with long term financial goals; this will increase daily profit
                        margins
                        and will minimize seldom technical distractions allowing the program to run swiftly and evade
                        errors caused by multiple user actions. </p>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Evidently ACM Trader has
                        delivered a profit reaping performance and users have been
                        withdrawing successfully with no hassles or delays on our side. We are happy to continue with
                        current users, however as a business going forward, we require patience, commitment and
                        involvement from you. We guarantee you better results and real wealth. </p>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">The standard will be
                        raised significantly and profits will be increasingly stable, since the
                        trading software will be responsible only to a select number of accounts.
                    </p>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Our directive as a
                        business going forward aims at operating as a digital private banking service
                        working alongside established financial institutions. </p>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">We are ready to begin
                        this promising journey with you and are willing to clarify anything you
                        are unclear with. Please report any issues to support should you experience errors during
                        technical transitions. </p>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Best Regards </p>

                    <div class="col-md-12">
                        <video class="myHTMLvideo" width="100%" height="100%" controls>
                            <source src="../videos/My Video35.mp4" type="video/mp4">
                        </video>
                    </div>

                </div>


            </div>
            <!-- /.row -->
            <!-- Main row -->


            <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
        <!-- Bootstrap4 CSS - -->
        <link rel="stylesheet" href="https://bootswatch.com/4/superhero/bootstrap.css" crossorigin="anonymous">

        <!-- Note - If your website not use Bootstrap4 CSS as main style, please use custom css style below and delete css line above.
        It isolate Bootstrap CSS to a particular class 'bootstrapiso' to avoid css conflicts with your site main css style -->
        <!-- <link rel="stylesheet" href="css/superhero.min.css" crossorigin="anonymous"> -->


        <!-- JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"
                crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
                crossorigin="anonymous"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.12.0/js/all.js" crossorigin="anonymous"></script>


        <!-- CSS for Payment Box -->


    </section>


    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script>
        //document.querySelector('video').play();
        $(document).ready(function() {
            $('.myHTMLvideo').click(function() {
                this.paused ? this.play() : this.pause();
            });
        });
    </script>

@endsection
