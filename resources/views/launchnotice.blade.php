<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Cryptocash is Professional Creative Template" />
    <!-- SITE TITLE -->
    <title>ACM-Traders</title>
    <!-- Favicon Icon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/acm_trader.png')}}">
    <!-- Animation CSS -->
    <link rel="stylesheet" href="{{ asset('landing/assets/css/animate.css')}}" >
    <!-- Latest Bootstrap min CSS -->
    <link rel="stylesheet" href="{{ asset('landing/assets/bootstrap/css/bootstrap.min.css')}}">
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="{{ asset('landing/assets/css/font-awesome.min.css')}}" >
    <!-- ionicons CSS -->
    <link rel="stylesheet" href="{{ asset('landing/assets/css/ionicons.min.css')}}">
    <!--- owl carousel CSS-->
    <link rel="stylesheet" href="{{ asset('landing/assets/owlcarousel/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('landing/assets/owlcarousel/css/owl.theme.default.min.css')}}">
    <!-- Magnific Popup CSS -->
    <link rel="stylesheet" href="{{ asset('landing/assets/css/magnific-popup.css')}}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{ asset('landing/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/responsive.css')}}">
    <!-- Color CSS -->
    <link id="layoutstyle" rel="stylesheet" href="{{ asset('landing/assets/color/theme.css')}}">


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106310707-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-106310707-1', { 'anonymize_ip': true });
    </script>

    <!-- Start of StatCounter Code -->
    <script>
        <!--
        var sc_project=11921154;
        var sc_security="6c07f98b";
        var scJsHost = (("https:" == document.location.protocol) ?
            "https://secure." : "http://www.");
        //-->

        document.write("<sc"+"ript src='" +scJsHost +"statcounter.com/counter/counter.js'></"+"script>");
    </script>
    <noscript><div class="statcounter"><a title="web analytics" href="https://statcounter.com/"><img class="statcounter" src="https://c.statcounter.com/11921154/0/6c07f98b/0/" alt="web analytics" /></a></div></noscript>
    <!-- End of StatCounter Code -->

</head>
<style>
    .size{
        font-size: 18px;

        font-weight: 500;
        background-color: #0d151d;
        color: white;
    }
    .center {
        margin-left: 25%;
    }
    .box{
        width: 30% !important;
        height: 200px !important;
        /*border: 5px dashed #f7a239 !important;*/
    }
    img{
        width: 100% !important;
        height: 100% !important;
    }
</style>

<body>



<!-- START LOADER -->
<div id="loader-wrapper" class="">
    <div id="loading-center-absolute" class="">
        <div class="object " id="object_four" style="background-color: black;"></div>
        <div class="object " id="object_three" style="background-color: black;"></div>
        <div class="object " id="object_two" style="background-color: black;"></div>
        <div class="object " id="object_one" style="background-color: black;"></div>
    </div>
    <div class="loader-section section-left "  style="background-color: black;"></div>
    <div class="loader-section section-right " style="background-color: black;"></div>

</div>
<!-- END LOADER -->

<!-- START HEADER -->
<header class="header_wrap fixed-top">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand animation" href="{{url('/')}}" data-animation="fadeInDown" data-animation-delay="1s">
                <img src="{{asset('images/acm_trader.png')}}" alt="logoimage" / style="height:152px !important; width:150px !important;">
                <img class="logo_dark" src="{{asset('images/acm_trader.png')}}" alt="logoimage" / style="height:24px !important; width:50px !important;">
            </a>
            <button class="navbar-toggler animation" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" data-animation="fadeInDown" data-animation-delay="1.1s">
                <span class="ion-android-menu"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav m-auto">
                    <li class="dropdown animation" data-animation="fadeInDown" data-animation-delay="1.1s">
                        <a  class="nav-link dropdown-toggle" href="{{url('/')}}">Home</a>

                    </li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.2s"><a class="nav-link page-scroll nav_item" href="{{url('/')}}#service">Services</a></li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.3s"><a class="nav-link page-scroll nav_item" href="{{route('about')}}">About</a></li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.8s"><a class="nav-link page-scroll nav_item" href="#">Affiliated Program</a></li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.8s"><a class="nav-link page-scroll nav_item" href="{{url('/')}}#faq">FAQs</a></li>

                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.8s"><a class="nav-link page-scroll nav_item" href="{{url('/')}}#contact">Contact</a></li>




                </ul>
                <ul class="navbar-nav nav_btn align-items-center">

                    <li class="animation" data-animation="fadeInDown" data-animation-delay="2s"><a class="btn btn-default btn-radius " href="{{route('login')}}">Login</a></li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="2s"><a class="btn btn-default btn-radius" href="{{route('register')}}">Register</a></li>
                </ul>
            </div>
        </nav>
    </div>
</header>
<!-- END HEADER -->

<!-- START SECTION BANNER -->
<section class="section_breadcrumb blue_light_bg" data-z-index="1" data-parallax="scroll" data-image-src="assets/images/home_banner_bg.png">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="banner_text text-center">
                    <h1 class="animation" data-animation="fadeInUp" data-animation-delay="1.1s">Notice</h1>
                    <ul class="breadcrumb bg-transparent justify-content-center animation m-0 p-0" data-animation="fadeInUp" data-animation-delay="1.3s">
                        <li><a href="{{url('/')}}">Home</a> </li>
                        <li><span>Launch Notice</span></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</section>


<section class="small_pb">
    <div class="container">

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="res_md_mb_30 res_sm_mb_20">
                    <!--<div class="box">
                   <img class="" data-animation="" data-animation-delay="0.2s" src="http://codiumtech.com/referral/public/images/acm_trader.png" alt="aboutimg2"
                   style="
   margin-left: 100%;
   "
                   /> </div>-->
                    <div class="title_default_dark ">
                        <div class="col-md-6 center mt-3" >
                            <video class="myHTMLvideo" width="100%" height="100%"  autoplay  loop>
                                <source src="videos/WhatsApp Video 2020-09-23 at 1.25.18 PM.mp4" type="video/mp4">
                            </video>
                        </div>
                        <h1 class=" animation text-center " data-animation="fadeInUp" data-animation-delay="0.2s">This notice contains information about technology upgrades, mobile application, incentives structure and our business model going forward. </h1>
                        <div class="col-md-12 pb-4" style="border-bottom: 3px solid red"></div>
                        <h4 class="animation mt-4" data-animation="fadeInUp" data-animation-delay="0.2s">Technology Upgrades and Mobile App</h4>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">In our sincerest efforts to provide an excellent service to all of our users, we are currently in the works of upgrading your trading software to enable the most-up-to-date features that will improve efficiency and user control.</p>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">With our technology upgrade, there are some changes that will affect the products and
                            services we provide to you. We want you to be aware of these changes and ask that you
                            please read this notice carefully to help ensure the transition to our new system causes you as
                            little disruption as possible.</p>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">We have finally reached the completion of our testing sessions and thus far, the results are
                            outstanding. Based on stats produced by our data, we have reason to believe that your money
                            has been working for you. We are confident that at least to a satisfactory extent, it has been
                            helpful in solidifying your finances and has quenched your thirst for financial independence
                            without any further experience of losses or unreasonable returns.
                        </p>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">During the past months we have been on a prelaunch stage, where we were testing the
                            system’s efficiency and its overall ability to trade for multiple accounts simultaneously. These
                            testing sessions were also helpful in developing our mobile application that has improved user
                            control and secure access to accounts. </p>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">The launch will introduce new products offering residual income structures and an incentive
                            program that requires little networking efforts. This will broaden your earning horizons and
                            give you better control of your profit intake. However, these new applications that have been
                            put in place will run at a minimal expense, which will now recognize users as subscribers that
                            are subject to paying recurring subscriptions to ensure efficient program functionality, since
                            these new features run at a high maintenance cost. </p>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">The mobile app will consist of the following features: </p>
                        <ul style="list-style-type:none; padding-left: 0% !important;">
                            <li>User Friendly Interface</li>
                            <li>Tracking Technology</li>
                            <li>Dynamic Tech 24/7</li>
                            <li>Added Security</li>
                            <li>Dollar Averages </li>

                        </ul>
                        <h4 class="mt-3">Incentives </h4>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">We are always looking forward to building solid networks with people who aspire to get the
                            most out of opportunities. During the course of our prelaunch we experienced a remarkable
                            growth and we are looking to expand our team so we can magnify the culture of our business
                            and commercial presence. </p>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">We are excited to explore new concepts and share effective networking strategies. That is
                            why we have an affiliate program that monitors users marketing skills and leadership aptitude
                            based on how they build cooperative teams with mindful individuals, who are in this for the
                            long haul. </p>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Performing members will be recognised with an invitation to a team building trips, where we
                            will have networking sessions and activities that will help cultivate independent thinking and
                            leadership skills. </p>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Our aim is to have an expansive affiliate program that offers incentives that help you grow as
                            a business and as a brand that is affiliated with successful people. </p>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">We believe that this is necessary and will be very helpful for our business going forward. </p>

                        <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Business Model</h4>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Our business model entails investment portfolios that are based on preserving and growing
                            sustainable wealth. We are well prepared to build long term wealth for our clients and have
                            many resources to identify undervalued digital assets with promising future value. Building
                            long term wealth requires patience, commitment and involvement; that’s the reason why we
                            have created a space for investors who are ready to focus on creating financial security. </p>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Having said that, in order for the trading bot to operate at maximum efficiency, it requires
                            mathematical precision, which is vital if we are to pursue long term wealth without taking
                            undue risks. </p>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">During testing sessions we discovered that, the software performs at its best when it’s trading
                            for users with committed accounts. Hence from the launch date, the trading software will
                            only be available to users with long term financial goals; this will increase daily profit margins
                            and will minimize seldom technical distractions allowing the program to run swiftly and evade
                            errors caused by multiple user actions. </p>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Evidently ACM Trader has delivered a profit reaping performance and users have been
                            withdrawing successfully with no hassles or delays on our side. We are happy to continue with
                            current users, however as a business going forward, we require patience, commitment and
                            involvement from you. We guarantee you better results and real wealth. </p>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">The standard will be raised significantly and profits will be increasingly stable, since the
                            trading software will be responsible only to a select number of accounts.
                        </p>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Our directive as a business going forward aims at operating as a digital private banking service
                            working alongside established financial institutions. </p>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">We are ready to begin this promising journey with you and are willing to clarify anything you
                            are unclear with. Please report any issues to support should you experience errors during
                            technical transitions. </p>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Best Regards </p>

                        <div class="col-md-12">
                            <video  class="myHTMLvideo" width="100%" height="100%" controls>
                                <source src="videos/My Video35.mp4" type="video/mp4">
                            </video>
                        </div>

                    </div>



                </div>
            </div>

        </div>
    </div>
</section>


<!-- START FOOTER SECTION -->
<footer>
    <div class="top_footer v_dark" data-z-index="1" data-parallax="scroll" data-image-src="assets/images/footer_bg.png">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-12">
                    <div class="newsletter_form">
                        <h4 class="footer_title border_title animation" data-animation="fadeInUp" data-animation-delay="0.2s">Newsletter</h4>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">By subscribing to our mailing list you will always be update with the latest news from us.</p>
                        <form class="subscribe_form animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                            <input class="input-rounded" type="text" required placeholder="Enter Email Address"/>
                            <button type="submit" title="Subscribe" class="btn-info" name="submit" value="Submit"> Subscribe </button>
                        </form>
                    </div>
                </div>
                <div class="col-lg-3 offset-lg-1 col-md-6 col-sm-8 res_md_mt_30 res_sm_mt_20">
                    <h4 class="footer_title border_title animation" data-animation="fadeInUp" data-animation-delay="0.2s">Quick Links</h4>
                    <ul class="footer_link half_link list_none">
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.2s"><a href="{{url('/')}}#service">SERVICES</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.3s"><a href="{{route('about')}}">ABOUT</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.4s"><a href="#">AFFILIATED PROGRAM</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.5s"><a href="{{url('/')}}#faq">FAQ</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.6s"><a href="{{url('/')}}#contact">Contact</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 offset-lg-1 col-md-6 col-sm-4 res_md_mt_30 res_sm_mt_20">
                    <h4 class="footer_title border_title animation" data-animation="fadeInUp" data-animation-delay="0.2s">Social</h4>
                    <ul class="footer_social list_none">
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.2s"><a href="#service"><i class="ion-social-facebook"></i> Facebook</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.3s"><a href="#service"><i class="ion-social-twitter"></i> Twitter</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.4s"><a href="#service"><i class="ion-social-googleplus"></i> Google Plus</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.5s"><a href="#service"><i class="ion-social-pinterest"></i> Pintrest</a></li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.6s"><a href="#service"><i class="ion-social-instagram-outline"></i> Instagram</a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
    <div class="bottom_footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p class="copyright">Copyright &copy; 2018 All Rights Reserved.</p>
                </div>
                <div class="col-md-6">
                    <ul class="list_none footer_menu">
                        <li><a href="#service">Privacy Policy</a></li>
                        <li><a href="#about">Terms & Conditions</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- END FOOTER SECTION -->
<script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script>
    //document.querySelector('video').play();
    $(document).ready(function() {
        $('.myHTMLvideo').click(function() {
            this.paused ? this.play() : this.pause();
        });
    });
</script>
</script>
<a href="#" class="scrollup btn-default"><i class="ion-ios-arrow-up"></i></a>

<!-- Latest jQuery -->
<script src="{{ asset('landing/assets/js/jquery-1.12.4.min.js')}}"></script>
<!-- Latest compiled and minified Bootstrap -->
<script src="{{ asset('landing/assets/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- owl-carousel min js  -->
<script src="{{ asset('landing/assets/owlcarousel/js/owl.carousel.min.js')}}"></script>
<!-- magnific-popup min js  -->
<script src="{{ asset('landing/assets/js/magnific-popup.min.js')}}"></script>
<!-- waypoints min js  -->
<script src="{{ asset('landing/assets/js/waypoints.min.js')}}"></script>
<!-- parallax js  -->
<script src="{{ asset('landing/assets/js/parallax.js')}}"></script>
<!-- countdown js  -->
<script src="{{ asset('landing/assets/js/jquery.countdown.min.js')}}"></script>
<!-- particles min js  -->
<script src="{{ asset('landing/assets/js/particles.min.js')}}"></script>
<!-- scripts js -->
<script src="{{ asset('landing/assets/js/jquery.dd.min.js')}}"></script>
<!-- jquery.counterup.min js -->
<script src="{{ asset('landing/assets/js/jquery.counterup.min.js')}}"></script>
<!-- scripts js -->
<script src="{{ asset('landing/assets/js/scripts.js')}}"></script>
</body>

<!-- Mirrored from bestwebcreator.com/cryptocash/demo/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 21 Feb 2020 10:29:30 GMT -->
</html>

