<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HedgeWithdraw extends Model
{
    protected $fillable = [
        'user_id',
        'hedge_id',
        'amount',
        'btc_rate',
        'type',
        'withdraw_status',
        'withdraw_approved_time',
    ];
}
