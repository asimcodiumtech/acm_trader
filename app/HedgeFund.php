<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HedgeFund extends Model
{
    protected $fillable = [
        'user_id',
        'hedge_id',
        'slot',
        'per_slot',
        'amount',
        'btc_rate',
        'type',
        'gift_id',
        'deposite_status',
        'deposit_approved_time'
    ];
}
