<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_info extends Model
{
    protected $table = 'user_info';
    public $timestamps = false;
    protected $fillable = [
        'u_id',
        'name',
        'b_withdrow_able',
        'a_withdrow_able',
        'b_blance',
        'after_blance',
        'b_gift_sum_used',
        'after_gift_sum_used',
        'b_left_team',
        'after_left_team',
        'b_team_right',
        'after_team_right' ,
        'b_total_return' ,
        'after_total_return',
        'b_usd_invested',
        'after_usd_invested',
        'b_assets',
        'after_assets',
        'b_total_deposit',
        'after_total_deposit',
        'b_total_profit',
        'after_total_profit',
        'b_total_withdrow',
        'after_total_withdrow'
    ];
}
