<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Comments;
class Contact extends Model
{
    protected $table="contacts";
	protected $fillable = [
        'name', 'email', 'message','phone_number','subject','user_id','read_issue','contact_pic'
    ];

public function emailCheck($email)
    {
    	$check=User::where('email',$email)->first();

    	if(!empty($check))
    	{
        	return 1;
    	}
    	else
    	{
        	return 0;
    	}
    }

    public function name($id)
    {   
        if(!empty($id))
        {
            $model=User::select('name')->where('id',$id)->first();

            return $model->name;

        }
        else
        {
            return '';
        }
    }

    public  static function unread()
    {   
        
            $model=self::where('read_issue',0)->get()->count();

            return $model;
    }
    public  static function markAsRead()
    {   
        $update=self::where('read_issue',0)->update(['read_issue'=>1]);
        return $update;
    }
    public function adminReply($contact_id)
    {
        $latest=Comments::where('contact_id',$contact_id)->where('u_id',2)->where("admin_reply",1)->latest('created_at')->first();
        
        if (!empty($latest)) {
           return 1;
        }
        else
        {    
            return 0;
        }
    }
}