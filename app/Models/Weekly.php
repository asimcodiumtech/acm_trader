<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\CausesActivity;
use Spatie\Activitylog\Traits\LogsActivity;

class Weekly extends Model
{
//    use LogsActivity, CausesActivity;
    protected $table="weekly";
    protected $fillable=['weekly_age','is_active','date'];
//    protected static  $logAttributes = ['weekly_age','is_active','date'];
//    protected static $logName = 'Set Percentage';
//    protected static  $logFillable  =true;
//    protected static $recordEvents  = (['created']);
//    public function getDescriptionForEvent(string $eventName): string
//    {
////        return "This :causer.name has been Set Profite {$eventName}";
//        if($eventName=='created'){
//        return "Set Daily Percentage";
//        }elseif($eventName=='updated'){
//                    return "This :causer.name has been update Daily Percentage {$eventName}";
//
//        }elseif ($eventName=='deleted'){
//            return "This :causer.name has been deleted Daily Percentage {$eventName}";
//
//        }
//    }
}
