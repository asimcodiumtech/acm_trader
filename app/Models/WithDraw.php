<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class WithDraw extends Model
{
    protected $table="with_draws";
    protected $fillable=['u_id','profit_id','deposite_id','withdraw_trans_id','coin_type','withdraw_amount','is_withdraw' ,'is_gift','branch_code','bank_type','is_app','btc_rate','btc_id','type','swift_code'];



    public static function refNo($id)
    {
        $model=User::where('id',$id)->first();

        return $model->name;
    }

    public  function usdToBtc($amount , $rate)
    {
        $model= $amount/ $rate;

        return $model;
    }
}
