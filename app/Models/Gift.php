<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Gift extends Model
{
    protected $table="gifts";
    protected $fillable=['user_id','gift_code','amount','status','btc_rate','coin_type','hedge_id','slots','total_slots_amount','created_by'];

    protected static $logName = 'gifts';
//    protected static  $logFillable  =true;
//    protected static $recordEvents  = (['created']);
    public function name($id)
    {
    	if(!empty($id))
    	{
    		$model=User::select('name')->where('id',$id)->first();

    		return $model->name;

    	}
    	else
    	{
    		return '';
    	}


    }
}
