<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HedgeProfitHistory extends Model
{
    protected $fillable = [
        'hedge_id',
        'percentage',
        'profit_time',
    ];
}
