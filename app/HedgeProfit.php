<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HedgeProfit extends Model
{
    protected $fillable = [
        'id', 'percentage', 'amount', 'user_id', 'hedge_id', 'profit_time'
    ];
}
