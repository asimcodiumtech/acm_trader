<?php

namespace App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\AdminCoin;
use App\Models\FundDetail;
use App\Models\Profit;
use App\Models\WithDraw;
use App\Models\Weekly;
use Illuminate\Support\Facades\Session;
use App\Models\Gift;


/**
 * ##########################################
 * ###  PLEASE DO NOT MODIFY THIS FILE !  ###
 * ##########################################
 *
 *
 * Cryptobox Server Callbacks
 *
 * @package     Cryptobox callbacks
 * @copyright   2014-2020 Delta Consultants
 * @category    Libraries
 * @website     https://gourl.io
 * @version     2.2.0
 *
 *
 * This file processes call-backs from Cryptocoin Payment Box server when new payment
 * from your users comes in. Please link this file in your cryptobox configuration on
 * gourl.io - Callback url: http://yoursite.com/cryptobox.callback.php
 *
 * Usually user will see on bottom of payment box button 'Click Here if you have already sent coins'
 * and when he will click on that button, script will connect to our remote cryptocoin payment box server
 * and check user payment.
 *
 * As backup, our server will also inform your server automatically every time when payment is
 * received through this callback file. I.e. if the user does not click on button, your website anyway
 * will receive notification about a given user and save it in your database. And when your user next time
 * comes on your website/reload page he will automatically will see message that his payment has been
 * received successfully.
 *
 *
 */
/*$data=run_sql($sql1);*/

        /* $data = new App\User;

print_r($data);exit;
*/          /* $model = new App\Models\FundDetail;
  echo "<pre>";
  print_r($model->paymentVerify(addslashes($_POST["user"]),));exit;*/

if(!defined("CRYPTOBOX_WORDPRESS")) define("CRYPTOBOX_WORDPRESS", false);

if (!CRYPTOBOX_WORDPRESS) require_once( "cryptobox.class.php" );
elseif (!defined('ABSPATH')) exit; // Exit if accessed directly in wordpress


// a. check if private key valid
$valid_key = false;
if (isset($_POST["private_key_hash"]) && strlen($_POST["private_key_hash"]) == 128 && preg_replace('/[^A-Za-z0-9]/', '', $_POST["private_key_hash"]) == $_POST["private_key_hash"])
{
    $keyshash = array();
    $arr = explode("^", CRYPTOBOX_PRIVATE_KEYS);
    foreach ($arr as $v) $keyshash[] = strtolower(hash("sha512", $v));
    if (in_array(strtolower($_POST["private_key_hash"]), $keyshash)) $valid_key = true;
}


// b. alternative - ajax script send gourl.io json data
if (!$valid_key && isset($_POST["json"]) && $_POST["json"] == "1")
{
    $data_hash = $boxID = "";
    if (isset($_POST["data_hash"]) && strlen($_POST["data_hash"]) == 128 && preg_replace('/[^A-Za-z0-9]/', '', $_POST["data_hash"]) == $_POST["data_hash"]) { $data_hash = strtolower($_POST["data_hash"]); unset($_POST["data_hash"]); }
    if (isset($_POST["box"]) && is_numeric($_POST["box"]) && $_POST["box"] > 0) $boxID = intval($_POST["box"]);

    if ($data_hash && $boxID)
    {
        $private_key = "";
        $arr = explode("^", CRYPTOBOX_PRIVATE_KEYS);
        foreach ($arr as $v) if (strpos($v, $boxID."AA") === 0) $private_key = $v;

        if ($private_key)
        {
            $data_hash2 = strtolower(hash("sha512", $private_key.json_encode($_POST).$private_key));
            if ($data_hash == $data_hash2) $valid_key = true;
        }
        unset($private_key);
    }

    if (!$valid_key) die("Error! Invalid Json Data sha512 Hash!");

}


// c.
if ($_POST) foreach ($_POST as $k => $v) if (is_string($v)) $_POST[$k] = trim($v);



// d.
if (isset($_POST["plugin_ver"]) && !isset($_POST["status"]) && $valid_key)
{
	echo "cryptoboxver_" . (CRYPTOBOX_WORDPRESS ? "wordpress_" . GOURL_VERSION : "php_" . CRYPTOBOX_VERSION);
	die;
}


// e.
if (isset($_POST["status"]) && in_array($_POST["status"], array("payment_received", "payment_received_unrecognised")) &&
		$_POST["box"] && is_numeric($_POST["box"]) && $_POST["box"] > 0 && $_POST["amount"] && is_numeric($_POST["amount"]) && $_POST["amount"] > 0 && $valid_key)
{

	foreach ($_POST as $k => $v)
	{
		if ($k == "datetime") 						$mask = '/[^0-9\ \-\:]/';
		elseif (in_array($k, array("err", "date", "period")))		$mask = '/[^A-Za-z0-9\.\_\-\@\ ]/';
		else								$mask = '/[^A-Za-z0-9\.\_\-\@]/';
		if ($v && preg_replace($mask, '', $v) != $v) 	$_POST[$k] = "";
	}

	if (!$_POST["amountusd"] || !is_numeric($_POST["amountusd"]))	$_POST["amountusd"] = 0;
	if (!$_POST["confirmed"] || !is_numeric($_POST["confirmed"]))	$_POST["confirmed"] = 0;


	$dt			= gmdate('Y-m-d H:i:s');
	$obj 		= run_sql("select paymentID, txConfirmed from crypto_payments where boxID = ".intval($_POST["box"])." && orderID = '".addslashes($_POST["order"])."' && userID = '".addslashes($_POST["user"])."' && txID = '".addslashes($_POST["tx"])."' && amount = ".floatval($_POST["amount"])." && addr = '".addslashes($_POST["addr"])."' limit 1");


	$paymentID		= ($obj) ? $obj->paymentID : 0;
	$txConfirmed	= ($obj) ? $obj->txConfirmed : 0;

	// Save new payment details in local database
	if (!$paymentID)
	{
		$sql = "INSERT INTO crypto_payments (boxID, boxType, orderID, userID, countryID, coinLabel, amount, amountUSD, unrecognised, addr, txID, txDate, txConfirmed, txCheckDate, recordCreated)
				VALUES (".intval($_POST["box"]).", '".addslashes($_POST["boxtype"])."', '".addslashes($_POST["order"])."', '".addslashes($_POST["user"])."', '".addslashes($_POST["usercountry"])."', '".addslashes($_POST["coinlabel"])."', ".floatval($_POST["amount"]).", ".floatval($_POST["amountusd"]).", ".($_POST["status"]=="payment_received_unrecognised"?1:0).", '".addslashes($_POST["addr"])."', '".addslashes($_POST["tx"])."', '".addslashes($_POST["datetime"])."', ".intval($_POST["confirmed"]).", '$dt', '$dt')";

           /*$data=FundDetail::create(['amount'=>floatval($_POST["amountusd"]),'u_id'=>auth()->user()->id,'coin_type'=>'speed']);*/


           /*echo "<pre>";
           print_r($_POST["order"]);exit;*/
           if($_POST["order"]== 'deposit')
           {
               try {
                   /*$minfee= $_POST["amountusd"] *100/110;*/
                   $minfee = floatval($_POST["amountusd"]) * 100 / 110;
                   $time_deposited = date('Y-m-d');
                   $time = date('Y-m-d H:i:s');

                   $rounded = 0.01 * (int)($minfee * 100);
                   $sql1 = "INSERT INTO fund_details (u_id,coin_type,amount,deposite_status,deposit_approved_time,deposite_initial_status,created_at,updated_at)
				VALUES ('" . addslashes($_POST["user"]) . "','BTC'," . $rounded . ",'" . addslashes(1) . "','" . addslashes($time_deposited) . "','" . addslashes(0) . "','$time','$time')";
                   $data = run_sql($sql1);

                   $user_id = addslashes($_POST["user"]);
                   //update frozen balance
//               $frozen = User::where('id', addslashes($_POST["user"]))->first();
                   $obj = run_sql("select * from users where id = '" . addslashes($_POST["user"]) . "'  limit 1");
                   $frozen = ($obj->frozen) ? $obj->frozen : 0;


                   $newfrozen = $frozen + $rounded;
//               User::where('id',addslashes($_POST["user"]))->update(['frozen' => $newfrozen]);
//               User::where('id',68)->update(['branch_code' => 'btc orm work']);
                   //give commotions to referrals
                   $updatefrozen = "UPDATE  users SET frozen ='" . $newfrozen . "' WHERE id = '" . $user_id . "'";
                   run_sql($updatefrozen);
//               $sql1 = "INSERT INTO gifts (gift_code,amount,created_by)
//				VALUES ('".$hash."',".$minfeegift.",'".addslashes($_POST["user"])."')";
//               $data=run_sql($sql1);
                   $refferby = run_sql("select * from users where id = '" . addslashes($_POST["user"]) . "'  limit 1");
                   $refferby = $refferby->reff_by;
                   $amount = $rounded;

                   for ($i = 1; $i <= 3; $i++) {
//                   $relativefirst = User::where('reff_key', $refferby)->first();
                       $relativefirst = run_sql("select * from users where reff_key = '" . $refferby . "'  limit 1");;
                       if (!empty($refferby)) {
                           if ($i == 3) {
                               $percentage = 0.05;
                           } else {
                               $percentage = 0.1;
                           }
//                                        $withdraw = $relativefirst->withdraw_amount;
//                                        $balance = $relativefirst->remain_balance;
                           $financeleft = $relativefirst->financeleft;

//                                        $finalWithamount = $withdraw + $amount * $percentage;
//                                        $finalamount = $balance + $amount * $percentage;
                           $finalfinanceleft = $financeleft + $amount * $percentage;

                           $update = run_sql("UPDATE users SET financeleft = '" . $finalfinanceleft . "' where reff_key = '" . $refferby . "'");
//                       $update = User::where('reff_key', $refferby)->update(['financeleft' => $finalfinanceleft]);
                           $refferby = $relativefirst->reff_by;

                       }
                   }
                   $system = run_sql("select * from users where id = '" . addslashes($_POST["user"]) . "'  limit 1");
                   if (!empty($system->system_position)) {
                       $endingLimit = $system->system_position - 3;
                       $positionwise = run_sql("Select * from users where system_position < " . $system->system_position . " AND  system_position >= " . $endingLimit . "  ORDER BY system_position DESC ");
//                   User::where('system_position', '<', $system->system_position)->where('system_position', '>=', $endingLimit)->orderBy('system_position', 'DESC')->get();
//                   $positionwise = User::where('system_position', '<', $system->system_position)->where('system_position', '>=', $endingLimit)->orderBy('system_position', 'DESC')->get();

                       if (!empty($check = $positionwise)) {
                           $cc = 1;
                           foreach ($positionwise as $users) {
                               switch ($cc) {
                                   case 1:
                                       $percentage = 0.05;
                                       $cc++;
                                       break;
                                   case 2:
                                       $percentage = 0.03;
                                       $cc++;
                                       break;
                                   case 3:
                                       $percentage = 0.02;
                                       $cc++;
                                       break;
                               }
//                                            $withdraw = $users->withdraw_amount;
//                                            $balance = $users->remain_balance;
                               $financeright = $users->financeright;

//                                            $finalWithamount = $withdraw + $amount * $percentage;
//                                            $finalamount = $balance + $amount * $percentage;
                               $finalfinanceright = $financeright + $amount * $percentage;
                               $update = run_sql("UPDATE users SET financeright ='" . $finalfinanceright . "' where id = '" . $users->id . "'");

//                           $update = User::where('id', $users->id)->update(['financeright' => $finalfinanceright]);
                           }
                       }
                   }
               } catch(Exception $e) {
                   $error_message = $e->getMessage();


// path of the log file where errors need to be logged
                   $log_file = "./cryptobox_callBack_error.log";

// setting error logging to be active
                   ini_set("log_errors", TRUE);

// setting the logging file in php.ini
                   ini_set('error_log', $log_file);

// logging the error
                   error_log($error_message);
               }
               //end commotions

           }elseif($_POST["order"]=='gift')
           {
                $minfeegift= floatval($_POST["amountusd"]);
           	 /*echo "<pre>"
           	 print_r($_POST["amountusd"]);exit;*/
           	 $rounded1 = 0.01 * (int)($minfeegift*100);

               $hash = bin2hex(random_bytes(16));


           	$sql1 = "INSERT INTO gifts (gift_code,amount,created_by,coin_type,created_at)
				VALUES ('".$hash."',".$minfeegift.",'".addslashes($_POST["user"])."','BTC','".$dt."')";
				$data=run_sql($sql1);



           }elseif( $_POST["order"]=='hedge_booking'){

//               auth()->user()->id."_".$input_slots."_".$hedgeid."";
//               $_POST["order"]
//                 parse_str(,$array);
               $array = explode("_", $_POST["user"]);
//              $array [0] = user_id
//              $array [1] = $input_slots
//              $array [2] = $hedgeid

//                dd($array['user_id'] , $array['input_slots'],$array['hedgeid']);
//               $hedge = Hedge::where('id', $array['hedgeid'])->first();
               $hedge = run_sql("select * from hedges where id = '" . $array['2'] . "'  limit 1");



               $hedge_details = run_sql("select * from hedge_details where hedge_id = '" . $array['2'] . "' AND user_id = '".$array['0']."' limit 1");
//               $hedge_details = self::where('hedge_id', $array['2'])->where('user_id', $array['user_id'])->first();

               if ($hedge_details !== null) {
                   $invested_new = $hedge_details->invested_amount + floatval($_POST["amountusd"]);
//                   $hedge_details->update(['invested_amount' => $invested_new]);

                  $update= run_sql( "UPDATE  hedge_details SET invested_amount ='" . $invested_new . "' WHERE hedge_id = '" . $array['2'] . "' AND user_id = '".$array['0']."'");
               } else {

                   $sql1 = "INSERT INTO hedge_details (hedge_id,user_id,invested_amount,created_at)
				VALUES ('".$array['2']."',". $array['0'].",'".floatval($_POST["amountusd"]).",'".$dt.")";
                   $data=run_sql($sql1);
//                   HedgeDetails::create(['hedge_id' => $array['2'], 'user_id' => $array['0'], 'invested_amount' => floatval($_POST["amountusd"])]);
               }


               $hedge_slots = $hedge->available_slot - $array['1'];
               $hedge_update = run_sql( "UPDATE  hedges SET available_slot ='" . $hedge_slots . "' WHERE id = '" . $array['2'] . "'");

               $btc = file_get_contents('https://blockchain.info/ticker');

               $rate = json_decode($btc)->USD->sell;


//               select * from hedges where id = '" . $array['2'] . "'  limit 1
//               $hedge_update = Hedge::where('id', $hedge_id)->update(['available_slot' => $hedge_slots]);
               $sql1 = "INSERT INTO hedge_funds (user_id,hedge_id,slot,per_slot,btc_rate,type,deposite_status,deposit_approved_time,amount,created_at)
				VALUES ('".$array['0']."',".$array['2'].",'".$array['1']."','".$hedge->amount."','".$rate."','"."BTC_PAY"."',1,'".$dt."','".floatval($_POST["amountusd"]).",'".$dt.")";

//               $hedge_fund = HedgeFund::create(['user_id' => $user_id, 'hedge_id' => $hedge_id, 'slot' => $input_slot, 'per_slot' => $hedge->amount, 'amount' => $total_amount, 'btc_rate' => $btc_rate, 'type' => $coin_type, 'deposite_status' => '1', 'deposit_approved_time' => $time_deposited]);
               ////
            //commission
               $refferby = run_sql("select * from users where id = '" .$array['0']. "'  limit 1");
               $refferby = $refferby->reff_by;
               $amount = floatval($_POST["amountusd"]);

               for ($i = 1; $i <= 3; $i++) {
//                   $relativefirst = User::where('reff_key', $refferby)->first();
                   $relativefirst = run_sql("select * from users where reff_key = '" . $refferby . "'  limit 1");;
                   if (!empty($refferby)) {
                       if ($i == 3) {
                           $percentage = 0.05;
                       } else {
                           $percentage = 0.1;
                       }
//                                        $withdraw = $relativefirst->withdraw_amount;
//                                        $balance = $relativefirst->remain_balance;
                       $financeleft = $relativefirst->financeleft;

//                                        $finalWithamount = $withdraw + $amount * $percentage;
//                                        $finalamount = $balance + $amount * $percentage;
                       $finalfinanceleft = $financeleft + $amount * $percentage;

                       $update = run_sql("UPDATE users SET financeleft = '" . $finalfinanceleft . "' where reff_key = '" . $refferby . "'");
//                       $update = User::where('reff_key', $refferby)->update(['financeleft' => $finalfinanceleft]);
                       $refferby = $relativefirst->reff_by;

                   }
               }
               $system = run_sql("select * from users where id = '" . $array['0'] . "'  limit 1");
               if (!empty($system->system_position)) {
                   $endingLimit = $system->system_position - 3;
                   $positionwise = run_sql("Select * from users where system_position < " . $system->system_position . " AND  system_position >= " . $endingLimit . "  ORDER BY system_position DESC ");
//                   User::where('system_position', '<', $system->system_position)->where('system_position', '>=', $endingLimit)->orderBy('system_position', 'DESC')->get();
//                   $positionwise = User::where('system_position', '<', $system->system_position)->where('system_position', '>=', $endingLimit)->orderBy('system_position', 'DESC')->get();

                   if (!empty($check = $positionwise)) {
                       $cc = 1;
                       foreach ($positionwise as $users) {
                           switch ($cc) {
                               case 1:
                                   $percentage = 0.05;
                                   $cc++;
                                   break;
                               case 2:
                                   $percentage = 0.03;
                                   $cc++;
                                   break;
                               case 3:
                                   $percentage = 0.02;
                                   $cc++;
                                   break;
                           }
//                                            $withdraw = $users->withdraw_amount;
//                                            $balance = $users->remain_balance;
                           $financeright = $users->financeright;

//                                            $finalWithamount = $withdraw + $amount * $percentage;
//                                            $finalamount = $balance + $amount * $percentage;
                           $finalfinanceright = $financeright + $amount * $percentage;
                           $update = run_sql("UPDATE users SET financeright ='" . $finalfinanceright . "' where id = '" . $users->id . "'");

//                           $update = User::where('id', $users->id)->update(['financeright' => $finalfinanceright]);
                       }
                   }
               }

           }




		$paymentID = run_sql($sql);

		$box_status = "cryptobox_newrecord";
	}
	// Update transaction status to confirmed
	elseif ($_POST["confirmed"] && !$txConfirmed)
	{
		$sql = "UPDATE crypto_payments SET txConfirmed = 1, txCheckDate = '$dt' WHERE paymentID = ".intval($paymentID)." LIMIT 1";
		run_sql($sql);

		$box_status = "cryptobox_updated";
	}
	else
	{
		$box_status = "cryptobox_nochanges";
	}


	/**
	 *  User-defined function for new payment - cryptobox_new_payment(...)
	 *  For example, send confirmation email, update database, update user membership, etc.
	 *  You need to modify file - cryptobox.newpayment.php
	 *  Read more - https://gourl.io/api-php.html#ipn
         */

	if (in_array($box_status, array("cryptobox_newrecord", "cryptobox_updated")) && function_exists('cryptobox_new_payment')) cryptobox_new_payment($paymentID, $_POST, $box_status);
}

else
	$box_status = "Only POST Data Allowed";


	echo $box_status; // don't delete it

?>
