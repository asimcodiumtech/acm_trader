<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Cache;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\User;

class LastUserActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if(Auth::check())
        {
                $expiresAt=Carbon::now()->addMinutes(10);
                Cache::put('active'.Auth::user()->id,true,$expiresAt);
                $token=bin2hex(random_bytes(16));
                $id=Auth::user()->id;
                $update=User::where('id',$id)->update(['remember_token'=>$token]);
                
        }
        else
        {
            
        }
        return $next($request);
    }
}
