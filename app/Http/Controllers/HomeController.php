<?php

namespace App\Http\Controllers;

use App\Models\AdminCoin;
use App\Models\FundDetail;
use App\Models\Gift;
use App\Models\Profit;
use App\Models\Weekly;
use App\Models\WithDraw;
use App\User;
use App\User_info;
use Cryptobox;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Models\Activity;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function commands()
    {

//      return  Activity::inLog( 'Add Reply')->where('causer_id',2)->get();
        return    DB::table('activity_log')->select('log_name')->distinct('log_name')->where('causer_id',2)->get();
      return  Activity::distinct( 'log_name')->where('causer_id',2)->get();

//         $lastActivity = Activity::all()->last();
//        return $lastActivity->description;
//        return $lastActivity->causer;
//        $user = User::where('id',2)->first();
//       return $user->actions;

        $arr = ['key' => 'value', 'key2' => 'value2','key3' => 'value3'
        ];

//        activity()->log('Look mum, I logged something');
        activity()
//            ->causedBy()
//            ->performedOn()
            ->withProperties( $arr)
            ->log('edited');





     //trigger exception in a "try" block
//        try {
//            throw new Exception("Value must be 1 or below");
////            //If the exception is thrown, this text will not be shown
//            echo 'If you see this, the number is 1 or below';
//        }
//
////catch exception
//        catch(Exception $e) {
//            $error_message = $e->getMessage();
//
//
//// path of the log file where errors need to be logged
//            $log_file = "./cryptobox_callBack_error.log";
//
//// setting error logging to be active
//            ini_set("log_errors", TRUE);
//
//// setting the logging file in php.ini
//            ini_set('error_log', $log_file);
//
//// logging the error
//            error_log($error_message);
//        }
////        dd(Permission::create(['name' => 'Users Roles Management']));
//       $role =  Role::findById(3);
//       $per =  Permission::findById(9);
//       $res = $role->givePermissionTo($per);
//        return $res;
//        $user = User::where('id',659)->first();
//        return $user->assignRole('finance department');
//return auth()->user()->getPermissionsViaRoles();
        //assignRole();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {

        if(auth()->user()->admin ==1)
        {

           $nousers=User::where('admin',2)->get()->count();
           $nodeposite=FundDetail::where('deposite_status',1)->sum('amount');
           $nowithdraw=Withdraw::where('is_withdraw',1)->sum('withdraw_amount');
           $noActive=User::where('admin',2)->where('remember_token','!=',null)->get()->count();
           $userss=User::where('admin','!=',1)->get();
           foreach ($userss as $user) {
                 if($user->isOnline())
                 {


                 }
                 else
                 {

                }
            }

           /*print_r($nowithdraw);exit;*/
                      $request->session()->forget('user');
           return view('admin.dashboard')->withNousers($nousers)->withNodeposite($nodeposite)->withNoWithdraw($nowithdraw)->withActive($noActive);
        }
        elseif(auth()->user()->admin==2)
        {
            $userinfomodel = User::user_Balance();
            $model_user=User::where('id',auth()->user()->id)->first();


//            $exchangeProfit=Profit::where('u_id',auth()->user()->id)->sum('profit');
////            $withdraw_amunt =
//            $exchangeShow =  $exchangeInvested  ;
//            $balance=$model->remain_balance;
//            $userss=User::where('admin','!=',1)->get();
//            foreach ($userss as $user) {
//                 if($user->isOnline())
//                 {
//                     /*echo "<pre>";
//                     print_r('active');*/
//
//                 }
//                 else
//                 {
//
//                     /*echo "<pre>";
//                     print_r('not active')*/;
//                }
//            }
//
//
//            $withdraw2=Withdraw::where('u_id',auth()->user()->id)->where('is_withdraw',1)->sum('withdraw_amount');
//
//            $minus= $withdraw2 - ($model->financeleft + 	$model->financeright);
//            if($minus<=0)
//            {
//                $minus=0;
//            }
//
//
//            $exchange2ndtime= $exchangeShow  + $exchangeProfit -$minus;
////            $exchange2ndtime= $exchangeShow  + $exchangeProfit;
//            /*echo "<pre>";
//            print_r($balance);exit;*/
////            echo "<pre>";
////            print_r($exchange2ndtime);
////            exit();
//                    $startDate= date('Y-m-d');
//                       $endDate= date('Y-m-d',strtotime("-60 day",strtotime($startDate))) ;
//                 $validWithdraw= FundDetail::where('u_id',auth()->user()->id)->where('deposite_status',1)->where('deposit_approved_time','<=',$endDate)->where('deposite_initial_status',0)->get();
//                 $array=FundDetail::where('u_id',auth()->user()->id)->where('deposite_status',1)->where('deposit_approved_time','<=',$endDate)->where('deposite_initial_status',0)->get()->toArray();
//                 $withdrawUpdate=User::where('id',auth()->user()->id)->first();
//
//                    if(sizeof($array) == 0)
//                    {
//
//                    }
//                    else
//                    {
//                        foreach($validWithdraw as $user)
//                                    {
//
//                                          $finalamount= $withdrawUpdate->withdraw_amount + $user->amount;
//                                          $withdrawUpdate=User::where('id',auth()->user()->id)->update(['withdraw_amount'=>$finalamount]);
//
//                                          $updateDeposit=FundDetail::where('id',$user->id)->update(['deposite_initial_status'=>1]);
//                                    }
//
//
//                    }



                    $startDate= date('Y-m-d');
                    $tprofit=Profit::where('u_id',auth()->user()->id)->sum('profit');
                    $todayProfit=Profit::where('u_id',auth()->user()->id)->first();
                    $todayProfit=Profit::where('u_id',auth()->user()->id)->where('profit_time',$startDate)->sum('profit');
                    $todayProfit=(int)$todayProfit;


                     $withdraw=Withdraw::where('u_id',auth()->user()->id)->first();
                    $withdraw=Withdraw::where('u_id',auth()->user()->id)->where('is_withdraw',1)->sum('withdraw_amount');
                    $withdraw=(int)$withdraw;
            $balance = $userinfomodel['balance'];
            $fund=FundDetail::where('u_id',auth()->user()->id)->where('deposite_status',1)->sum('amount');
                      if($balance>0)
                      {
                        $total_percent=$tprofit/$balance *100;
                      }
                      else{
                        $total_percent=0;
                      }



                    //print_r($total_percent);exit;



            $weekly=Weekly::where('is_active',1)->first();

            if(!empty($weekly))
            {
               /* $model=date('Y-m-d');
                 $weekly=date('Y-m-d',strtotime("-1 day",strtotime((string)$model)));
                $endDate= date('Y-m-d',strtotime("-7 day",strtotime((string)$model)));
                 //print_r($endDate);exit;
                $profitLastWeekAvgPercent=Profit::where('u_id',auth()->user()->id)->whereBetween('profit_time',[$endDate,$weekly])->sum('percentage');
                $profitLastWeekCount=Profit::where('u_id',auth()->user()->id)->whereBetween('profit_time',[$endDate,$weekly])->count();

                if($profitLastWeekCount<=0)
                {
                  $profitLastWeekCount=1;
                }
                $totalAvgAmount=$profitLastWeekAvgPercent /$profitLastWeekCount;*/
                $model=date('Y-m-d');
                 $daily=date('Y-m-d',strtotime("0 day",strtotime((string)$model)));
                 $profitLastDayPercent=Profit::where('u_id',auth()->user()->id)->where('profit_time','=',$daily)->first();

                 if(empty($profitLastDayPercent))
                 {
                  $totalAvgAmount=0;
                 }
                 else
                 {
                  $totalAvgAmount=$profitLastDayPercent->percentage;
                 }


            }
            else
            {
                $totalAvgAmount=0;
            }



            $coin= AdminCoin::where('is_active',1)->first();
            /*echo "<pre>";
            print_r($coin);exit;*/
            $usercoin= User::where('id',auth()->user()->id)->first();

        $notification=auth()->user()->unReadNotifications;
          /*auth()->user()->unReadNotifications->markAsRead();*/


             /*for($i=1;$i<=3;$i++)
             {
                if($i==3)
                {
                    $percentage=0.05;
                }
                else
                {
                     $percentage=0.1;
                }
            }

            echo "<pre>";
            print_r($refferby->level);exit;*/
            $currentId = auth()->user()->id;
        $data = User::where('id', $currentId)->first();
            // --------right Team Finance Code -----------
                    $lteambalance= $data->financeleft;
                    if(empty($lteambalance))
                    {
                        $lteambalance=0;
                    }


            // --------right Team Finance  Code End -----------



               // --------left Team Finance Code -----------
                       $rteambalance= $data->financeright;
                       if(empty($rteambalance))
                    {
                        $rteambalance=0;
                    }

            // --------left Team Finance  Code End -----------



            // --------left Team count Code ------------


        $reffkey = $data->reff_key;


        $datainner = User::where('reff_by', $reffkey)->get();
        $leftCountTeam=0;


        if (!empty($check1=$datainner->toArray()))
        {
            foreach ($datainner as $value)
            {
                $datainner1 = User::where('reff_by', $value->reff_key)
                    ->get();

                $leftCountTeam++;

                if (!empty($check2=$datainner1->toArray()))
                {
                    foreach ($datainner1 as  $value1)
                    {



                        $datainner2 = User::where('reff_by', $value1->reff_key)
                            ->get();

                        $leftCountTeam++;
                        if (!empty($datainner2->toArray()))
                        {
                            foreach ($datainner2 as  $value2)
                            { $leftCountTeam++;

                            }
                        }
                    }
                }
            }
        }


          /*echo "<pre>";
            print_r($leftCountTeam);exit;*/

            // --------left Team count Code End ------------


            // --------right Team count Code ------------

              $rightTeam = 0;
            if(!empty($data->system_position))
            {
                $afterSystem = User::where('system_position','>', $data->system_position)
                    ->get();

                if (!empty($check2=$afterSystem->toArray()))
                {
                    foreach ($afterSystem as  $value7)
                    {

                        $rightTeam++;
                    }
                }
            }
            // --------right Team count Code End ------------


           $reffral_key=$data->reff_key;

//approve all panding request in fund details model

//
//             $allUnapproveddeposite=FundDetail::where('u_id',auth()->user()->id)->where('deposite_status', 0)->get();
//            foreach ($allUnapproveddeposite as $key => $value)
//            {
//              $doApproved=FundDetail::paymentVerify($value->id);
//            }



            /**
         * @category    Main Example - Custom Payment Box ((json, bootstrap4, mobile friendly, white label product, your own logo)
         * @package     GoUrl Cryptocurrency Payment API
         * copyright    (c) 2014-2020 Delta Consultants
         * @desc        GoUrl Crypto Payment Box Example (json, bootstrap4, mobile friendly, optional - free White Label Product - Bitcoin/altcoin Payments with your own logo and all payment requests through your server, open source)
         * @crypto      Supported Cryptocoins - Bitcoin, BitcoinCash, BitcoinSV, Litecoin, Dash, Dogecoin, Speedcoin, Reddcoin, Potcoin, Feathercoin, Vertcoin, Peercoin, MonetaryUnit, UniversalCurrency
         * @website     https://gourl.io/bitcoin-payment-gateway-api.html#p8
         * @live_demo   https://gourl.io/lib/examples/example_customize_box.php
         * @note    You can delete folders - 'Examples', 'Screenshots' from this archive
         */


        /********************** NOTE - 2018-2020 YEARS *************************************************************************/
        /*****                                                                                                             *****/
        /*****     This is NEW 2018-2020 latest Bitcoin Payment Box Example  (mobile friendly JSON payment box)            *****/
        /*****                                                                                                             *****/
        /*****     You can generate php payment box code online - https://gourl.io/lib/examples/example_customize_box.php  *****/
        /*****         White Label Product - https://gourl.io/lib/test/example_customize_box.php?method=curl&logo=custom   *****/
        /*****         Light Theme - https://gourl.io/lib/examples/example_customize_box.php?theme=black                   *****/
        /*****         Black Theme - https://gourl.io/lib/examples/example_customize_box.php?theme=default             *****/
        /*****         Your Own Logo - https://gourl.io/lib/examples/example_customize_box.php?theme=default&logo=custom   *****/
        /*****                                                                                                             *****/
        /***********************************************************************************************************************/



     /* echo "<pre>";
*/




    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

        /**
         * @category    Main Example - Custom Payment Box ((json, bootstrap4, mobile friendly, white label product, your own logo)
         * @package     GoUrl Cryptocurrency Payment API
         * copyright    (c) 2014-2020 Delta Consultants
         * @desc        GoUrl Crypto Payment Box Example (json, bootstrap4, mobile friendly, optional - free White Label Product - Bitcoin/altcoin Payments with your own logo and all payment requests through your server, open source)
         * @crypto      Supported Cryptocoins - Bitcoin, BitcoinCash, BitcoinSV, Litecoin, Dash, Dogecoin, Speedcoin, Reddcoin, Potcoin, Feathercoin, Vertcoin, Peercoin, MonetaryUnit, UniversalCurrency
         * @website     https://gourl.io/bitcoin-payment-gateway-api.html#p8
         * @live_demo   https://gourl.io/lib/examples/example_customize_box.php
         * @note    You can delete folders - 'Examples', 'Screenshots' from this archive
         */


        /********************** NOTE - 2018-2020 YEARS *************************************************************************/
        /*****                                                                                                             *****/
        /*****     This is NEW 2018-2020 latest Bitcoin Payment Box Example  (mobile friendly JSON payment box)            *****/
        /*****                                                                                                             *****/
        /*****     You can generate php payment box code online - https://gourl.io/lib/examples/example_customize_box.php  *****/
        /*****         White Label Product - https://gourl.io/lib/test/example_customize_box.php?method=curl&logo=custom   *****/
        /*****         Light Theme - https://gourl.io/lib/examples/example_customize_box.php?theme=black                   *****/
        /*****         Black Theme - https://gourl.io/lib/examples/example_customize_box.php?theme=default             *****/
        /*****         Your Own Logo - https://gourl.io/lib/examples/example_customize_box.php?theme=default&logo=custom   *****/
        /*****                                                                                                             *****/
        /***********************************************************************************************************************/

        // Change path to your files
        // --------------------------------------
        DEFINE("CRYPTOBOX_PHP_FILES_PATH", app_path() . '/lib');

        // path to directory with files: cryptobox.class.php / cryptobox.callback.php / cryptobox.newpayment.php;
        // cryptobox.newpayment.php will be automatically call through ajax/php two times - payment received/confirmed
        DEFINE("CRYPTOBOX_IMG_FILES_PATH", asset("/images").'/');      // path to directory with coin image files (directory 'images' by default)
        DEFINE("CRYPTOBOX_JS_FILES_PATH", asset("/cryptojs").'/');            // path to directory with files: ajax.min.js/support.min.js

        // Change values below
        // --------------------------------------
        DEFINE("CRYPTOBOX_LANGUAGE_HTMLID", "alang");    // any value; customize - language selection list html id; change it to any other - for example 'aa';  default 'alang'
        DEFINE("CRYPTOBOX_COINS_HTMLID", "acoin");        // any value;  customize - coins selection list html id; change it to any other - for example 'bb';   default 'acoin'
        DEFINE("CRYPTOBOX_PREFIX_HTMLID", "acrypto_");    // any value; prefix for all html elements; change it to any other - for example 'cc';    default 'acrypto_'

        // Open Source Bitcoin Payment Library
        // ---------------------------------------------------------------
        //dd(CRYPTOBOX_PHP_FILES_PATH . "/cryptobox.class.php");

       require_once(CRYPTOBOX_PHP_FILES_PATH . "/cryptobox.class.php");
    /*********************************************************/
    /****  PAYMENT BOX CONFIGURATION VARIABLES  ****/
    /*********************************************************/
    // IMPORTANT: Please read description of options here - https://gourl.io/api-php.html#options
    $userID                 = auth()->user()->id;            // place your registered userID or md5(userID) here (user1, user7, uo43DC, etc).
                                    // You can use php $_SESSION["userABC"] for store userID, amount, etc
                                    // You don't need to use userID for unregistered website visitors - $userID = "";
                                    // if userID is empty, system will autogenerate userID and save it in cookies
    $userFormat     = "COOKIE";     // save userID in cookies (or you can use IPADDRESS, SESSION, MANUAL)
    $orderID            = "deposit";           // invoice number - 000383
    $amountUSD      = 0.01;         // invoice amount - 2.21 USD; or you can use - $amountUSD = convert_currency_live("EUR", "USD", 22.37); // convert 22.37EUR to USD
    $period         = "1 MINUTE";   // one time payment, not expiry
    $def_language   = "en";         // default Language in payment box
    $def_coin           = "bitcoin";      // default Coin in payment box
    // List of coins that you accept for payments
    //$coins = array('bitcoin', 'bitcoincash', 'bitcoinsv', 'litecoin', 'dogecoin', 'dash', 'speedcoin', 'reddcoin', 'potcoin', 'feathercoin', 'vertcoin', 'peercoin', 'monetaryunit', 'universalcurrency');
    $coins = array('bitcoin');  // for example, accept payments in bitcoin, bitcoincash, litecoin, dash, speedcoin
    // Create record for each your coin - https://gourl.io/editrecord/coin_boxes/0 ; and get free gourl keys
    // It is not bitcoin wallet private keys! Place GoUrl Public/Private keys below for all coins which you accept
    $all_keys = array(  "bitcoin"  =>       array("public_key" => "-your public key for Bitcoin box-",  "private_key" => "-your private key for Bitcoin box-"),
                    "bitcoincash"  =>   array("public_key" => "-your public key for BitcoinCash box-",  "private_key" => "-your private key for BitcoinCash box-"),
                    "litecoin" =>       array("public_key" => "-your public key for Litecoin box-", "private_key" => "-your private key for Litecoin box-")); // etc.
    // Demo Keys; for tests (example - 5 coins)
    $all_keys = array(
                      "bitcoin" => array( "public_key" => "50135AAiWAInBitcoin77BTCPUBuIS97PyiG1pptLIw5N3ekj6",
                                        "private_key" => "50135AAiWAInBitcoin77BTCPRVwNygkqzetvtaDlV1oNU5bYt")); // Demo keys!
    //  IMPORTANT: Add in file /lib/cryptobox.config.php your database settings and your gourl.io coin private keys (need for Instant Payment Notifications) -
    /* if you use demo keys above, please add to /lib/cryptobox.config.php -
        $cryptobox_private_keys = array("25654AAo79c3Bitcoin77BTCPRV0JG7w3jg0Tc5Pfi34U8o5JE", "25678AACxnGODogecoin77DOGEPRVFvl6IDdisuWHVJLo5m4eq",
                    "25656AAeOGaPBitcoincash77BCHPRV8quZcxPwfEc93ArGB6D", "25657AAOwwzoLitecoin77LTCPRV7hmp8s3ew6pwgOMgxMq81F",
                    "25678AACxnGODogecoin77DOGEPRVFvl6IDdisuWHVJLo5m4eq", "25658AAo79c3Dash77DASHPRVG7w3jg0Tc5Pfi34U8o5JEiTss",
                    "20116AA36hi8Speedcoin77SPDPRVNOwjzYNqVn4Sn5XOwMI2c");
        Also create table "crypto_payments" in your database, sql code - https://github.com/cryptoapi/Payment-Gateway#mysql-table
        Instruction - https://gourl.io/api-php.html
    */
    // Re-test - all gourl public/private keys

//            $cryptobox_private_keys = array("51386AAVIjZFSpeedcoin77SPDPRVgh2qffqBATqDdTXGyyQYH");
    $def_coin = strtolower($def_coin);
    if (!in_array($def_coin, $coins)) $coins[] = $def_coin;
    foreach($coins as $v)
    {
        if (!isset($all_keys[$v]["public_key"]) || !isset($all_keys[$v]["private_key"])) die("Please add your public/private keys for '$v' in \$all_keys variable");
        elseif (!strpos($all_keys[$v]["public_key"], "PUB"))  die("Invalid public key for '$v' in \$all_keys variable");
        elseif (!strpos($all_keys[$v]["private_key"], "PRV")) die("Invalid private key for '$v' in \$all_keys variable");
        elseif (strpos(CRYPTOBOX_PRIVATE_KEYS, $all_keys[$v]["private_key"]) === false)
                die("Please add your private key for '$v' in variable \$cryptobox_private_keys, file /lib/cryptobox.config.php.");
    }

    // Current selected coin by user
    $coinName = cryptobox_selcoin($coins, $def_coin);
    // Current Coin public/private keys
    $public_key  = $all_keys[$coinName]["public_key"];
    $private_key = $all_keys[$coinName]["private_key"];

    /** PAYMENT BOX **/
    $options = array(
        "public_key"    => $public_key, // your public key from gourl.io
        "private_key"   => $private_key,    // your private key from gourl.io
        "webdev_key"    => "",          // optional, gourl affiliate key
        "orderID"       => $orderID,        // order id or product name
        "userID"            => $userID,         // unique identifier for every user
        "userFormat"    => $userFormat,     // save userID in COOKIE, IPADDRESS, SESSION  or MANUAL
        "amount"        => 0,           // product price in btc/bch/bsv/ltc/doge/etc OR setup price in USD below

        "amountUSD"     => $amountUSD,  // we use product price in USD
        "period"            => $period,         // payment valid period
        "language"      => $def_language  // text on EN - english, FR - french, etc
    );

    // Initialise Payment Class
    $box = new Cryptobox ($options);

    // coin name
    $coinName = $box->coin_name();

        // php code end :)
        // ---------------------

        // NOW PLACE IN FILE "lib/cryptobox.newpayment.php", function cryptobox_new_payment(..) YOUR ACTIONS -
        // WHEN PAYMENT RECEIVED (update database, send confirmation email, update user membership, etc)
        // IPN function cryptobox_new_payment(..) will automatically appear for each new payment two times - payment received and payment confirmed
        // Read more - https://gourl.io/api-php.html#ipn

        $html=$box->display_cryptobox_bootstrap($coins, $def_coin, $def_language, null, 200, 200, false, "default", "default", 250, "", "ajax", false);

        $myVar = htmlspecialchars($html, ENT_QUOTES);
     $check= htmlentities($html);
        /*echo $check;exit;*/
   /*print_r($myVar);exit;*/
          /*  if(auth()->user()->acc_name== 'update_user_info'){


            $all_user =  User::all();

            foreach ($all_user as $loopUser){

                    $lo_gift = Gift::where('user_id',$loopUser->id)->where('status',1)->sum('amount');
                    $model_user=User::where('id',$loopUser->id)->first();


                    $total_deposite=FundDetail::where('u_id',$loopUser->id)->where('deposite_status',1)->sum('amount');

                    $exchangeProfit=Profit::where('u_id',$loopUser->id)->sum('profit');
//            $withdraw_amunt =
//                    $exchangeShow =  $exchangeInvested  ;
                    $balance=$model_user->remain_balance;
                    $userss=User::where('admin','!=',1)->get();


                    $total_withdraw=Withdraw::where('u_id',$loopUser->id)->where('is_withdraw',1)->sum('withdraw_amount');

                    $minus= $total_withdraw - $model_user->financeleft + 	$model_user->financeright;
                    if($minus<=0)
                    {
                        $minus=0;
                    }
                    $exchange= $total_deposite  + $exchangeProfit -$minus;
//                    if($balance>0)
//                    {
//                        $total_percent=$tprofit/$balance *100;
//                    }
//                    else{
//                        $total_percent=0;
//                    }

//                    after
                $after_exchange= ($total_deposite  + $exchangeProfit) - $total_withdraw;
               $a_blance =   ($model_user->financeleft + $model_user->financeright + $exchangeProfit + $total_deposite )- $total_withdraw ;


                User_info::Create([
                    'u_id'=>$loopUser->id,
                    'name'=>$loopUser->name,
                    'b_blance'=>$balance,
                    'after_blance'=>$a_blance,
                    'b_withdrow_able'=> $model_user->withdraw_amount ,
                    'a_withdrow_able',
//                    'b_gift_sum_used'=>$lo_gift,
                    'after_gift_sum_used',
                    'b_left_team'=>$model_user->financeleft,
                    'after_left_team',
                    'b_team_right'=>$model_user->financeright,
                    'after_team_right' ,
                    'b_total_return' ,
                    'after_total_return',
                    'b_usd_invested',
                    'after_usd_invested',
                    'b_assets'=>$exchange,
                    'after_assets'=>$after_exchange,
                    'b_total_deposit'=>$total_deposite,
                    'after_total_deposit',
                    'b_total_profit'=>$exchangeProfit,
                    'after_total_profit',
                    'b_total_withdrow'=>$total_withdraw,
                    'after_total_withdrow'
                ]);

                }

                var_dump('x');
                exit();
            }*/
            // error message to be logged
            $allowed_withdraw = $userinfomodel['allowed_withdraw'];
            if(auth()->user()->acc_name== 'sdkjfkasjfjjsldkj'){
                $all_user =  User::all();

//                $startDate= date('Y-m-d');
//                $endDate= date('Y-m-d',strtotime("-60 day",strtotime($startDate))) ;
//                var_dump($endDate);
//                exit();
                foreach ($all_user as $loopUser){

                    $lo_gift = Gift::where('user_id',$loopUser->id)->where('status',1)->sum('amount');
                    $model_user=User::where('id',$loopUser->id)->first();
                    $total_deposite=FundDetail::where('u_id',$loopUser->id)->where('deposite_status',1)->sum('amount');
                    $exchangeProfit=Profit::where('u_id',$loopUser->id)->sum('profit');

//            $withdraw_amunt =
//                    $exchangeShow =  $exchangeInvested  ;

//                    $balance=$model_user->remain_balance;
//                    $userss=User::where('admin','!=',1)->get();
                    $total_withdraw=Withdraw::where('u_id',$loopUser->id)->where('is_withdraw',1)->sum('withdraw_amount');

//                    $minus= $total_withdraw ;
//                    if($minus<=0)
//                    {
//                        $minus=0;
//                    }
//                    $exchange= ($total_deposite  + $exchangeProfit )-$minus;
//                    if($balance>0)
//                    {
//                        $total_percent=$tprofit/$balance *100;
//                    }
//                    else{
//                        $total_percent=0;
//                    }

//                    after
                    if($total_withdraw<=0)
                    {
                        $total_withdraw = 0.0;
                    }
//                        frozen = frozen deposits
                        $model_user->update(['frozen'=>$total_deposite]);

                    $startDate= date('Y-m-d');
                    $endDate= date('Y-m-d',strtotime("-60 day",strtotime($startDate))) ;
                    $validWithdraw= FundDetail::where('u_id',$loopUser->id)->where('deposite_status',1)->where('deposit_approved_time','<=',$endDate)->where('deposite_initial_status',0)->get();
                    $array=FundDetail::where('u_id',$loopUser->id)->where('deposite_status',1)->where('deposit_approved_time','<=',$endDate)->where('deposite_initial_status',0)->get()->toArray();
                    $withdrawUpdate=User::where('id',$loopUser->id)->first();

                    if(sizeof($array) == 0)
                    {
//                        User::where('id',$loopUser->id)->update(['unfrozen'=>0]);

                    }
                    else
                    {
                        foreach($validWithdraw as $transc)
                        {
                            $frozzen= $withdrawUpdate->frozen - $transc->amount;
                            $withdrawUpdate->update(['frozen'=>$frozzen]);

                            // unfrozen_amount = unfrozen
                            $unfrozen_amount= $withdrawUpdate->unfrozen + $transc->amount;
//                            $withdrawUpdate=User::where('id',$loopUser->id)->update(['unfrozen'=>$finalamount]);
                            $withdrawUpdate->update(['unfrozen'=>$unfrozen_amount]);

                            //frozen =  frozen blance
                            $updateDeposit=FundDetail::where('id',$transc->id)->update(['deposite_initial_status'=>1]);
                        }


                    }
                    $usd_withdrawed  =  WithDraw::where('u_id',$loopUser->id)->where('coin_type', 'usd')->sum('withdraw_amount');
                    $profit_withdrawed  =  WithDraw::where('u_id',$loopUser->id)->where('coin_type', 'profit')->sum('withdraw_amount');
                    $left_withdrawed  =  WithDraw::where('u_id',$loopUser->id)->where('coin_type', 'left')->sum('withdraw_amount');
                    $right_withdrawed  =  WithDraw::where('u_id',$loopUser->id)->where('coin_type', 'right')->sum('withdraw_amount');

//                      deposit totall = frozen + non frozen
//                    $exchange_assets= ($total_deposite + $exchangeProfit) - $total_withdraw;
                    $exchange_assets= ($model_user->frozen + $model_user->unfrozen + $exchangeProfit) - ($usd_withdrawed+$profit_withdrawed);

//                    $exchange_assets= ($total_deposite  + $exchangeProfit) - $total_withdraw;
                    // blance will calculte and return to user dahsbord on run time will update late on user table remaing blance
                    //blance =  prof
                    $blance =   ($model_user->financeleft + $model_user->financeright + $exchangeProfit  + $withdrawUpdate->unfrozen +$withdrawUpdate-> frozen ) - ($usd_withdrawed + $left_withdrawed + $right_withdrawed + $profit_withdrawed) ;
//                    $usd_invested = $total_deposite - $total_withdraw ;
                    $usd_invested =( $withdrawUpdate->unfrozen + $withdrawUpdate-> frozen) - $usd_withdrawed ;

                    User_account_info::Create([
                        'user_id'=>$loopUser->id,
                        'usd_invested'=>$usd_invested,
                        'exchange_assets'=>$exchange_assets,
                        'unfrozen_amount'=>$withdrawUpdate->unfrozen ,
                        'frozen_amount' => $withdrawUpdate-> frozen,
                        'allowed_withdraw_check'=>1,
                        'name'=>$model_user->name,
                        'total_deposits'=>$total_deposite,
                        'total_profit'=>$exchangeProfit,
                        'financeleft'=>$model_user->financeleft ,
                        'financeright'=>$model_user->financeright,
                        'total_withdraws'=>$total_withdraw,
                        'remaning_blance'=>$blance,
                        'difference'=>($total_deposite + $exchangeProfit + $model_user->financeleft + $model_user->financeright)-$total_withdraw
                         ]);

                }
                var_dump('user_account_info');
                exit();
            }
           return view('user.dashboard', [
            'box' => $box,
            'coins' => $coins,
            'def_coin' => $def_coin,
            'def_language' => $def_language,
            'roo'=> $check
        ],compact('allowed_withdraw'))->withModel($model_user)->withCoin($coin)
           ->withPercent($total_percent)->withBalance($balance)->withTodayP($todayProfit)->withWithdraw($withdraw)->withFund($fund)->withAvg($totalAvgAmount)->withNotify($notification)->withRightTeam($rightTeam)
                ->withLeftTeam($leftCountTeam)->withLbalance($lteambalance)->withRbalance($rteambalance)->withReffKey($reffral_key)->withExchange($userinfomodel['usd_invested'])->withUsercoin($usercoin)
                ->withExchange2ndtime($userinfomodel['exchange_assets'])->withprofitdrawable($userinfomodel['profit_withdraw_able'])->withleftwithdrawable($userinfomodel['left_withdraw_able'])->withrightwithdrawable($userinfomodel['right_withdraw_able'])->withusdwithdrawable($userinfomodel['usd_withdraw_able']);
        }
        else
        {
            return view('home');
        }
    }





}
