<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\User;
use \App\Mail\SendMail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Exceptions\Handler;
class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    public function showLinkRequestForm()
    {
        return view('auth.passwords.email');
    }
    public function sendResetLinkEmail(Request $request)
    {

        $user = DB::table('users')->where('email', '=', $request->email)
            ->first();

        //Check if the user exists
        if ($user == null) {
            return redirect()->back()->withErrors(['email' => trans('User does not exist')]);
        }
        $tokenData = DB::table('password_resets')
            ->where('email', $request->email)->first();
        // dd(Str::random(60));
        //Create Password Reset
        if ($tokenData == null) {
            $token = Str::random(60);
            DB::table('password_resets')->insert([
                'email' => $request->email,
                'token' => bcrypt($token),
                'created_at' => Carbon::now()
            ]);
        } else {
            DB::table('password_resets')->where('email', '=', $request->email)->delete();
            $token = Str::random(60);
            DB::table('password_resets')->insert([
                'email' => $request->email,
                'token' => bcrypt($token),
                'created_at' => Carbon::now()
            ]);
        }


        //Get the token just created above

        // dd($tokenData);
        if ($this->sendResetEmail($request->email, $token)) {
            
           $notification = array(
        'message' => 'Kindly Check your Email for Password Reset',
        'alert-type' => 'success',
        'heading' => 'Successed',
            );
            
            return redirect()->route('login')->with($notification);
            
            
            return "opk";
            //redirect()->back()->with('status', trans('A reset link has been sent to your email address.'));
        } else {
            $notification = array(
        'message' => 'Try Again for Password Reset',
        'alert-type' => 'error',
        'heading' => 'Successed',
            );
            
            return redirect()->route('login')->with($notification);

            return "error";
            //redirect()->back()->withErrors(['error' => trans('A Network Error occurred. Please try again.')]);
        }
    }
    private function sendResetEmail($email, $token)
    {
        //Retrieve the user from the database
        $user = DB::table('users')->where('email', $email)->select('name', 'email')->first();
        //Generate, the password reset link. The token generated is embedded in the link
        $link = url('/') . '/password/reset/' . $token . '?email=' . urlencode($user->email);

        try {
            $this->mailsend($email, $link);
            return true;
        } catch (Throwable $e) {
            //report($e);
            return false;
        }
    }
    public function mailsend($to, $link)
    {
        $details = [
            'title' => 'Kindly click on the following link to reset your password',
            'body' => $link
        ];
        //dd("123");

        Mail::to($to)->send(new SendMail($details));
        return view('emails.thanks');
    }
    public function sendResetLinkEmailAPi(Request $request)
    {

        $user = DB::table('users')->where('email', '=', $request->email)
            ->first();

        //Check if the user exists
        if ($user == null) {
            return response()->json(['error'=>'User does not exist'], 401);
        }
        $tokenData = DB::table('password_resets')
            ->where('email', $request->email)->first();
        // dd(Str::random(60));
        //Create Password Reset
        if ($tokenData == null) {
            $token = Str::random(60);
            DB::table('password_resets')->insert([
                'email' => $request->email,
                'token' => bcrypt($token),
                'created_at' => Carbon::now()
            ]);
        } else {
            DB::table('password_resets')->where('email', '=', $request->email)->delete();
            $token = Str::random(60);
            DB::table('password_resets')->insert([
                'email' => $request->email,
                'token' => bcrypt($token),
                'created_at' => Carbon::now()
            ]);
        }


        //Get the token just created above

        //Retrieve the user from the database
        $user = DB::table('users')->where('email', $request->email)->select('name', 'email')->first();
        //Generate, the password reset link. The token generated is embedded in the link
        $link = url('/') . '/password/reset/' . $token . '?email=' . urlencode($user->email);

        $details = [
            'title' => 'Kindly click on the following link to reset your password',
            'body' => $link
        ];
        //dd("123");

        Mail::to($user->email)->send(new SendMail($details));

        return ["error" => null, 'success' => '200','info'=>'reset password Link send to email'];
    }


    //use SendsPasswordResetEmails;
}
