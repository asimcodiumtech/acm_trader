<?php

namespace App\Http\Controllers;

use App\Hedge;
use App\HedgeDetails;
use App\HedgeFund;
use App\Models\AdminCoin;
use App\Models\Comments;
use App\Models\Contact;
use App\Models\FundDetail;
use App\Models\Gift;
use App\Models\NewsLetter;
use App\Models\Profit;
use App\Models\Weekly;
use App\Models\WithDraw;
use App\Notifications\MessageFromAdmin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Mail;
use Spatie\Activitylog\Models\Activity;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AdminController extends Controller
{
    public function newsletter(Request $request)
    {
        return view('admin.newsletter.index');
    }

    public function newsletterStore(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->get('msg'))) {
                $messagetext = $request->get('msg');
                $email = 'admin@acmtrader.com';

                $NewsLetter = NewsLetter::all();
                foreach ($NewsLetter as $user) {
                    $email_to = $user->email;
                    Mail::send('user.mail.newsletter', ['messagetext' => $messagetext], function ($message) use ($email, $email_to) {
                        $message->from($email);
                        $message->to($email_to);

                        $message->subject('ACM Newsletter');
                    });
                }
                activity('Newsletter Send')
                    ->causedBy(auth()->user())
//                    ->performedOn($user)
                    ->withProperties(['text' => $messagetext])
                    ->log('Newsletter Send');
//                    ->log_name('Newsletter Send');


                return ["error" => null, 'success' => '201'];

            }
        }
    }

    public function pendingIndex()
    {
        $model = FundDetail::where('deposite_status', "!=", 0)->get();
        return view('admin.pending.index')->withUsers($model);
    }

    public function pendingAjax(Request $request)
    {
        if ($request->ajax()) {

            if (!empty($request->get('id')) && $request->get('deposite') == 'doApproved') {

                $id = $request->get('id');
                $time_deposited = date('Y-m-d');
                $model = FundDetail::where('id', $id)->update(['deposite_status' => 1, 'deposit_approved_time' => $time_deposited, 'deposite_initial_status' => 0]);
                $u_id = FundDetail::where('id', $id)->first();
                //u_id from transctions
                $userthis = User::where('id', $u_id->u_id)->first();

                $newfrozen = $userthis->frozen + $u_id->amount;

                User::where('id', $u_id->u_id)->update(['frozen' => $newfrozen]);
//                $balance = User::select('remain_balance')->where('id', $u_id->u_id)->first();
//                $total = $balance->remain_balance + $u_id->amount;


                $refferby = User::where('id', $u_id->u_id)->first();
                $refferby = $refferby->reff_by;
                $amount = $u_id->amount;

                for ($i = 1; $i <= 3; $i++) {
                    $relativefirst = User::where('reff_key', $refferby)->first();
                    if (!empty($refferby)) {
                        if ($i == 3) {
                            $percentage = 0.05;
                        } else {
                            $percentage = 0.1;
                        }
//                        $withdraw = $relativefirst->withdraw_amount;
//                        $balance = $relativefirst->remain_balance;
                        $financeleft = $relativefirst->financeleft;

//                        $finalWithamount = $withdraw + $amount * $percentage;
//                        $finalamount = $balance + $amount * $percentage;
                        $finalfinanceleft = $financeleft + $amount * $percentage;

//                        $update = User::where('reff_key', $refferby)->update(['remain_balance' => $finalamount, 'withdraw_amount' => $finalWithamount, 'financeleft' => $finalfinanceleft]);
                        $update = User::where('reff_key', $refferby)->update(['financeleft' => $finalfinanceleft]);
                        $refferby = $relativefirst->reff_by;
                        /*echo "<pre>";
                        print_r($relativefirst);
                        print_r($i); */
                    }
                }
                /////   systems  person gave profit
                $system = User::where('id', $u_id->u_id)->first();
                if (!empty($system->system_position)) {
                    $endingLimit = $system->system_position - 3;
                    $positionwise = User::where('system_position', '<', $system->system_position)->where('system_position', '>=', $endingLimit)->orderBy('system_position', 'DESC')->get();

                    if (!empty($check = $positionwise->toArray())) {
                        $cc = 1;
                        foreach ($positionwise as $users) {
                            switch ($cc) {
                                case 1:
                                    $percentage = 0.05;
                                    $cc++;
                                    break;
                                case 2:
                                    $percentage = 0.03;
                                    $cc++;
                                    break;
                                case 3:
                                    $percentage = 0.02;
                                    $cc++;
                                    break;
                            }
//                            $withdraw = $users->withdraw_amount;
//                            $balance = $users->remain_balance;
                            $financeright = $users->financeright;

//                            $finalWithamount = $withdraw + $amount * $percentage;
//                            $finalamount = $balance + $amount * $percentage;
                            $finalfinanceright = $financeright + $amount * $percentage;
//                            $update = User::where('id', $users->id)->update(['remain_balance' => $finalamount, 'withdraw_amount' => $finalWithamount, 'financeright' => $finalfinanceright]);
                            $update = User::where('id', $users->id)->update(['financeright' => $finalfinanceright]);
                        }
                    }
                }


//                $update_balance = User::where('id', $u_id->u_id)->update(['remain_balance' => $total]);
                return ["error" => null, 'success' => '200'];
            } elseif (!empty($request->get('id')) && $request->get('deposite') == 'doPending') {
                $id = $request->get('id');

                $model = FundDetail::where('id', $id)->update(['deposite_status' => 2]);
                return ["error" => null, 'success' => '200'];
            } else {
                return ['error' => 409];
            }
        }
    }

    public function profitReq()
    {
        $model = Profit::all();
        return view('admin.profitpending.index')->withUsers($model);
    }

    public function profitReqAjax(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->get('id'))) {
                $id = $request->get('id');

                $model = Profit::where('id', $id)->update(['withdraw_profit' => 1]);
                $model = WithDraw::where('profit_id', $id)->update(['is_withdraw' => 1]);
                return ["error" => null, 'success' => '200'];
            } else {
                return ['error' => 409];
            }
        }
    }

    public function profitAjax(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->get('id')) && $request->get('profit')) {
                $id = $request->get('id');

                $model = FundDetail::where('id', $id)->update(['profit' => $request->get('profit')]);
                $model1 = FundDetail::select('u_id')->where('id', $id)->first();
                $uid = $model1->u_id;

                $model1 = Profit::create(['u_id' => $uid, 'profit' => $request->get('profit')]);

                return ["error" => null, 'success' => '200'];
            } else {
                return ['error' => 409];
            }
        }
    }

    public function approvedIndex()
    {
        $model = FundDetail::where('deposite_status', 1)->get();
        return view('admin.approved.index')->withUsers($model);
    }

    public function approvedAjax()
    {


    }

    public function withdrawIndex()
    {
        $model = WithDraw::all();
        return view('admin.withdraw.index')->withUsers($model);
    }

    public function withdrawAjax(Request $request)
    {
        if ($request->ajax()) {

            if (!empty($request->get('id')) && $request->get('withdrawStatus') && $request->get('u_id')) {

                $id = $request->get('id');
                $u_id = $request->get('u_id');
                $user = User::where('id', $u_id)->first();

                $tran = WithDraw::where('id', $id)->first();
                $model = WithDraw::where('id', $id)->update(['is_withdraw' => 1]);
                activity('Withdraw Approved')
                    ->causedBy(auth()->user())
//                    ->performedOn($user)
                    ->withProperties(['transaction_id' => $id, 'u_id' => $request->get('u_id'), 'User Name' => $user->name, 'Type' => $tran->coin_type])
                    ->log('User Approved Withdraw Request');
//
//                $withid = WithDraw::where('id', $id)->first();
//                $balance = User::where('id', $u_id)->first();
//
//                $total = $balance->remain_balance - $withid->withdraw_amount;
//                $totalWithdraw = $balance->withdraw_amount - $withid->withdraw_amount;
//                $update_balance = User::where('id', $u_id)->update(['remain_balance' => $total, 'withdraw_amount' => $totalWithdraw]);

                /*$model=FundDetail::where('id',$id)
                 ->update(['withdraw_status'=> $request->get('withdrawStatus')]);*/
                return ["error" => null, 'success' => '200'];
            } else {
                return ['error' => 409];
            }
        }

    }

    public function weeklyIndex()
    {
        $model = Weekly::where('is_active', 1)->first();
        if (empty((array)$model)) {
            $model = ['weekly_age' => ''];

        }

        return view('admin.weekly.index')->withUsers((object)$model);
    }

    public function weeklyStore(Request $request)
    {

        /*echo "<pre>";
        print_r($request->all());exit;*/
        $dateDaily = Weekly::where('is_active', 1)->first();
        $checkDate = date('Y-m-d');
        if ($checkDate != $dateDaily->date) {
            $model = Weekly::all();
            $data = Weekly::where('is_active', 1)->update(['is_active' => 0]);

            if ($checkDate != $dateDaily->date)

                $data = Weekly::create($request->all());
            $startDate = date('Y-m-d');
            $date = Weekly::where('is_active', 1)->update(['date' => $startDate]);
            $active_weekly = Weekly::where('is_active', 1)->first();
            ///$mytime = Carbon::now();
            /*$ldate = date('Y-m-d ');
            $start = '2019-11-28';
            $end = '2019-11-28';

            $date1 = "2019-12-02";
            $date2 = "2019-12-01";*/


            if (!empty((array)$model)) {

                /*$users=User::TodayProfit();*/
                $users = User::where('admin', 2)->get();


                foreach ($users as $user) {
                    /*$startDate= date('Y-m-d',strtotime((string)$active_weekly->created_at));
                    $endDate= date('Y-m-d',strtotime("-7 day",strtotime((string)$active_weekly->created_at)));*/

                    $startDate = date('Y-m-d');
                    $endDate = date('Y-m-d');
                    $percentage = (float)$request->get('weekly_age');
                    $deposite_approved_weekly_profit = FundDetail::weeklyProfit($user->id, $startDate, $endDate, $percentage);
                }
            } else {
                $data = Weekly::create($request->all());

            }

            $get = Weekly::where('is_active', 1)->first();


            $notification = array(
                'message' => 'Profit Send Successfully',
                'alert-type' => 'success',
                'heading' => 'Succeed',
            );
            activity('Set Percentage')
                ->causedBy(auth()->user())
//                    ->performedOn($user)
                ->withProperties((['Percentage' => $percentage]))
                ->log("Set Daily Percentage");
            /*Session::flash('alert-type','success');        view('admin.weekly.index')*/

            return redirect()->route('admin.weekly.index')->withUsers($get)->with($notification);
        }

        $get = Weekly::where('is_active', 1)->first();


        $notification = array(
            'message' => 'You are already Set Today Profit',
            'alert-type' => 'error',
            'heading' => 'Failed',
        );

        return redirect()->route('admin.weekly.index')->withUsers($get)->with($notification);
    }

    public function coinIndex()
    {
        $model = AdminCoin::where('is_active', 1)->first();
        if (empty((array)$model)) {
            $model = (object)$model = ['btc_id' => '', 'qr_code' => '', 'usd_id' => '', 'acc_name' => '', 'branch_code' => '', 'swift_code' => '', 'bank_type' => ''];

        }

        return view('admin.coin.index')->withUsers($model);
    }

    public function coinStore(Request $request)
    {
        $posterName = "";
        if (request()->hasFile('poster')) {

            /* Remove previous poster */
            $file_path = 'images/qrcodes/' . "/" . $request->get('oldPoster');
            if (file_exists($file_path) && !is_dir($file_path)) {
                @unlink($file_path);
            }

            /* Add new poster */
            $posterName = time() . '.' . request()->poster->getClientOriginalExtension();
            request()->poster->move('images/qrcodes/', $posterName);
        } else {
            $posterName = $request->get('oldPoster');
        }


        $model = AdminCoin::all();


        if (!empty((array)$model)) {
            $data = AdminCoin::where('is_active', 1)->update(['is_active' => 0]);


            $admincoin = AdminCoin::create([
                'btc_id' => $request->get('btc_id'),
                'qr_code' => $posterName,
                'usd_id' => $request->get('usd_id'),
                'acc_name' => $request->get('acc_name'),
                'branch_code' => $request->get('branch_code'),
                'bank_type' => $request->get('bank_type'),
                'swift_code' => $request->get('swift_code'),
                'is_active' => 1,
            ]);

        } else {
            $admincoin = AdminCoin::create([
                'btc_id' => $request->get('btc_id'),
                'qr_code' => $posterName,
                'usd_id' => $request->get('usd_id'),
                'acc_name' => $request->get('acc_name'),
                'branch_code' => $request->get('branch_code'),
                'bank_type' => $request->get('bank_type'),
                'swift_code' => $request->get('swift_code'),
                'is_active' => 1,
            ]);
        }
        $get = AdminCoin::where('is_active', 1)->first();
        return view('admin.coin.index')->withUsers($get);
    }

    public function notificationIndex(Request $request)
    {
        return view('admin.notification.index');
    }

    public function notificationStore(Request $request)
    {
        $message = $request->get('message');
        $model = User::where('admin', '=', 2)->get();
        foreach ($model as $user) {
            $user->notify(new MessageFromAdmin($message));
        }
        $notification = array(
            'message' => 'Notification Send Successfully',
            'alert-type' => 'success',
            'heading' => 'Succeeded',
        );
        activity('Notification Send')
            ->causedBy(auth()->user())
//                    ->performedOn($user)
            ->withProperties(['text' => $message])
            ->log('Notification Send to All Users');
//            ->log_name('Newsletter Send');
        return redirect()->route('admin.notification.index')->with($notification);;
    }

    public function contactUsers()
    {
        $model = Contact::all();
        Contact::markAsRead();
        return view('admin.contact.index')->withUsers($model);
    }

    public function contactStatus(Request $request)
    {
        if ($request->ajax()) {

            if (!empty($request->get('status')) && !empty($request->get('id'))) {
                $old_status = '';
                $new_status = '';
                $ticket = Contact::where('id', $request->get('id'))->first();
                if ($ticket->status == 1) {
                    $old_status = 'Resolved';

                } elseif ($ticket->status == 2) {
                    $old_status = 'In process';
                } else {
                    $old_status = 'Unresolved';
                }
                //request status
                if ($request->get('status') == 1) {
                    $new_status = 'Resolved';

                } elseif ($request->get('status') == 2) {
                    $new_status = 'In process';
                } else {
                    $new_status = 'Unresolved';
                }

                Contact::where('id', $request->get('id'))->update(['status' => $request->get('status')]);

                activity('Update Ticket')
                    ->causedBy(auth()->user())
//                    ->performedOn($user)
                    ->withProperties(['status' => $new_status, 'id' => $request->get('id')])
                    ->log("Update Ticket Status form {$old_status} to {$new_status}");


                return ["error" => null, 'success' => '201'];
            }
        }
    }

    public function giftList()
    {
        $model = Gift::all();
//        activity('Watch Gift List')
//            ->causedBy(auth()->user())
////                    ->performedOn($user)
////            ->withProperties()
//            ->log('Visit gift List Page');
        return view('admin.gift.list')->withGifts($model);
    }

//update
    public function giftAdd()
    {
        $hash = bin2hex(random_bytes(16));
        /*$bytes = openssl_random_pseudo_bytes(32);
        $hash = bin2hex($bytes);*/

        /* echo serialize($hash);
         exit;*/
        return view('admin.gift.add')->withHash($hash);
    }

//update
    public function giftStore(Request $request)
    {

        $check = Gift::where('gift_code', $request->get('gift_code'))->first();


        if (!empty($check)) {
            return redirect()->route('admin.gift.add');
        } else {


            $request->merge(['created_by' => auth()->user()->id]);

            $post = $request->all();

            /*echo "<pre>";
            print_r($post);exit;*/
            $create = Gift::create($post);
            $notification = array(
                'message' => 'Gift Created Successfully',
                'alert-type' => 'success',
                'heading' => 'Succeeded',
            );
            activity('Gift Created')
                ->causedBy(auth()->user())
//                    ->performedOn($user)
                ->withProperties($request->except('_token', "created_by"))
                ->log('Gift code added');
            return redirect()->route('admin.gift.list')->with($notification);

        }
    }

//update
    public function adminGiftAjax(Request $request)
    {
        if ($request->ajax()) {
//            dd($request->all(),$request->get('id'),empty($request->get('id')));
            if (!empty($request->get('id'))) {
                $checkData = Gift::where('id', $request->get('id'))->first();

                if ($checkData->created_by == auth()->user()->id) {

                    if ($checkData->hedge_id == null && $checkData->slots == null && $checkData->total_slots_amount == null) {
//                        dd($request->all());


                        $update = Gift::where('id', $request->get('id'))->update(['status' => 1]);
                        $getUser = User::where('id', $checkData->user_id)->first();

                        $time_deposited = date('Y-m-d');
                        $request->merge(['amount' => $checkData->amount, 'coin_type' => 'gift', 'deposit_approved_time' => $time_deposited, 'deposite_status' => 1, 'u_id' => $checkData->user_id]);


                        $FundDetail = FundDetail::create($request->all());
                        activity('Approved Gift')
                            ->causedBy(auth()->user())
//                    ->performedOn($user)
                            ->withProperties($request->all())
                            ->log("User Approve a gift created by Admin ");

                        //using person
                        $id = User::where('id', $checkData->user_id)->first();
                        $frozen = User::select('frozen')->where('id', $id->id)->first();
                        $newfrozen = $frozen->frozen + $checkData->amount;
                        User::where('id', $id->id)->update(['frozen' => $newfrozen]);

                        //$id->id user  of gift code from table of gift
                        User::updateFinanceCommission($checkData->user_id, $checkData->amount);
                        //                        $update_balance = User::where('id', $id->id)->update(['remain_balance' => $total]);


                        /*$remainig=$getUser->remain_balance + $checkData->amount;
                        $withdrawable= $getUser->withdraw_amount + $checkData->amount;
                        $userUpate=User::where('id',$checkData->user_id)->update(['remain_balance'=>$remainig,'withdraw_amount'=>$withdrawable]);*/
                        return ["error" => null, 'success' => '200','message'=>'Gift Approved Successfully!'];
                    }
                 elseif ($checkData->hedge_id != null && $checkData->slots != null && $checkData->total_slots_amount != null) {
//                    dd(1);
                    $hedge = Hedge::where('id', $checkData->hedge_id)->first();
                    $hedge_slots = $hedge->available_slot - $checkData->slots;
//                        dd($hedge_slots,$hedge_slots>=0);
                    $closing_date = date('Y-m-d H:i:s', strtotime($hedge->slot_closing_date));
//                    dd(now()->diff($closing_date), now());

                    if ($hedge_slots >= 0) {
//                                            dd(now()->diff($closing_date)->format("%r%a"));
//dd(now()->diff($closing_date)->format("%r%a"));
                        //Is 1 if the interval represents a negative time period and 0 otherwise
                        if (now()->diff($closing_date)->format("%r%a") >= 0) {
//dd($request->get('id'));
                            activity('Approved Gift')
                                ->causedBy(auth()->user())
//                    ->performedOn($user)
                                ->withProperties($request->all())
                                ->log("User Approve a gift created by Admin ");
                            $checkData->update(['status' => 1]);

                            HedgeDetails::updateInvestment($checkData->hedge_id, $checkData->user_id, $checkData->total_slots_amount);
                            //update hedge slots
                            $hedge_update = Hedge::where('id', $checkData->hedge_id)->update(['available_slot' => $hedge_slots]);

                            $hedge_fund = HedgeFund::create(['user_id' => $checkData->user_id, 'hedge_id' => $checkData->hedge_id, 'slot' => $checkData->slots,
                                'per_slot' => $checkData->total_slots_amount / $checkData->slots, 'amount' => $checkData->total_slots_amount,
                                'btc_rate' => $checkData->btc_rate, 'type' => 'gift', 'deposite_status' => '1',
                                'deposit_approved_time' => now(), 'gift_id' => $request->get('id')]);
//                                 dd($request->all(),$userinfomodel);
                            User::updateFinanceCommission($checkData->user_id, $checkData->total_slots_amount);
                            return ["error" => null, 'success' => '203', 'message' => 'Gift code verified , Congratulation you\'r Completed your  Bookings!'];

                        } else {
                            $hash = bin2hex(random_bytes(16));

                            $checkData->update(['status' => 3]);
                            activity('Approved Gift')
                                ->causedBy(auth()->user())
//                    ->performedOn($user)
                                ->withProperties($request->all())
                                ->log("User Approve a gift created by Admin and Compensation due to 'Hedge is closed'");

                            $gift = Gift::create(['gift_code' => $hash, 'amount' => $checkData->amount, 'coin_type' => 'comp' . $checkData->id, 'btc_rate' => $checkData->btc_rate, 'created_by' => $checkData->user_id, "status" => '0']);

                            return ["error" => 115, 'message' => 'Hedge is closed Now. Gift Code cannot be approved. A new gift code has been assigned to the recipient of same amount.'];


                        }

                    } else {
                        $hash = bin2hex(random_bytes(16));

                        $checkData->update(['status' => 3]);
                        activity('Approved Gift')
                            ->causedBy(auth()->user())
//                    ->performedOn($user)
                            ->withProperties($request->all())
                            ->log("User Approve a gift created by Admin and Compensation due to 'available slots'");
                        $gift = Gift::create(['gift_code' => $hash, 'amount' => $checkData->amount, 'coin_type' => 'comp' . $checkData->id, 'btc_rate' => $checkData->btc_rate, 'created_by' => $checkData->user_id, "status" => '0']);

                        return ["error" => 111, 'message' => 'You are too late to this hedge don\'t have available slots now! A new gift code has been assigned to the recipient of same amount.'];
                    }
                } else {
                    return ["error" => 111, 'message' => 'Undefined Database Gift Status! Please Contact to Admin'];

                }
            }else{
                return ["error" => 111, 'message' => 'You are not the same persion who create this gift code'];

            }
            }else{
                return ["error" => 111, 'message' => 'id not fund'];

            }


        }
    }


    public function allUser(Request $request)
    {
        if ($request->get('status')) {
            $user = User::where('admin', '!=', 1)->where('remember_token', '!=', null)->get();
            return view('admin.users.list')->withUsers($user);
        }
        $user = User::where('admin', '!=', 1)->get();
        return view('admin.users.list')->withUsers($user);
    }

//update
    public function userDetail(Request $request, $id)
    {


        $request->session()->put('user', $id);
        $user = User::where('id', $id)->first();

        activity('View User Detail')
            ->causedBy(auth()->user())
//                    ->performedOn($user)
            ->withProperties(['User Id' => $id, 'User Name' => $user->name, 'Email' => $user->email])
            ->log("View User Detail With");

        $userData = User::where('id', $user->id)->first();
        $id = $user->id;


        $gift = Gift::where('user_id', $id)->where('status', 1)->sum('amount');
        $model_user = User::where('id', $id)->first();
//            $total_deposite=FundDetail::where('u_id',$id)->where('deposite_status',1)->sum('amount');
        $total_withdraw = Withdraw::where('u_id', $id)->where('is_withdraw', 1)->sum('withdraw_amount');
        $exchangeProfit = Profit::where('u_id', $id)->sum('profit');

        if ($total_withdraw <= 0) {
            $total_withdraw = 0.0;
        }
//            use first time to get frozen data
//            $model_user->update(['acc_type'=>$total_deposite]);

        $startDate = date('Y-m-d');
        $endDate = date('Y-m-d', strtotime("-60 day", strtotime($startDate)));
        $validWithdraw = FundDetail::where('u_id', $id)->where('deposite_status', 1)->where('deposit_approved_time', '<=', $endDate)->where('deposite_initial_status', 0)->get();
        $array = FundDetail::where('u_id', $id)->where('deposite_status', 1)->where('deposit_approved_time', '<=', $endDate)->where('deposite_initial_status', 0)->get()->toArray();
//

        if (sizeof($array) == 0) {
//                        User::where('id',$loopUser->id)->update(['unfrozen'=>0]);

        } else {
            foreach ($validWithdraw as $transc) {
                $frozzen = $model_user->frozen - $transc->amount;
                $model_user->update(['frozen' => $frozzen]);

                // unfrozen_amount = unfrozen
                $unfrozen_amount = $model_user->unfrozen + $transc->amount;
//                            $model_user=User::where('id',$loopUser->id)->update(['unfrozen'=>$finalamount]);
                $model_user->update(['unfrozen' => $unfrozen_amount]);

                //frozen =  frozen blance
                FundDetail::where('id', $transc->id)->update(['deposite_initial_status' => 1]);
            }
        }
        $usd_withdrawed = WithDraw::where('u_id', $id)->where('coin_type', 'usd')->where('is_withdraw', 1)->sum('withdraw_amount');
        $profit_withdrawed = WithDraw::where('u_id', $id)->where('coin_type', 'profit')->where('is_withdraw', 1)->sum('withdraw_amount');
        $left_withdrawed = WithDraw::where('u_id', $id)->where('coin_type', 'left')->where('is_withdraw', 1)->sum('withdraw_amount');
        $right_withdrawed = WithDraw::where('u_id', $id)->where('coin_type', 'right')->where('is_withdraw', 1)->sum('withdraw_amount');
//            var_dump($model_user->frozen,$model_user->unfrozen );
//            exit();
        $exchange_assets = ($model_user->frozen + $model_user->unfrozen + $exchangeProfit) - ($usd_withdrawed + $profit_withdrawed);
        $balance = ($model_user->financeleft + $model_user->financeright + $exchangeProfit + $model_user->unfrozen + $model_user->frozen) - ($usd_withdrawed + $left_withdrawed + $right_withdrawed + $profit_withdrawed);

        $usd_invested = ($model_user->unfrozen + $model_user->frozen) - $usd_withdrawed;

        $profit_withdraw_able = $exchangeProfit - $profit_withdrawed;
        $left_withdraw_able = $model_user->financeleft - $left_withdrawed;
        $right_withdraw_able = $model_user->financeright - $right_withdrawed;
        $usd_withdraw_able = $model_user->unfrozen - $usd_withdrawed;
        $allowed_withdraw = 1;
        $is_allowed_withdraw = WithDraw::where('u_id', $id)->where('is_withdraw', '!=', 1)->get()->toArray();
        if (sizeof($is_allowed_withdraw) != 0) {
            $allowed_withdraw = 0;
        }
        $startDate = date('Y-m-d');
        $tprofit = Profit::where('u_id', $id)->sum('profit');
        $todayProfit = Profit::where('u_id', $id)->first();
        $todayProfit = Profit::where('u_id', $id)->where('profit_time', $startDate)->sum('profit');
        $todayProfit = (int)$todayProfit;


        $withdraw = Withdraw::where('u_id', $id)->first();
        $withdraw = Withdraw::where('u_id', $id)->where('is_withdraw', 1)->sum('withdraw_amount');
        $withdraw = (int)$withdraw;

        $fund = FundDetail::where('u_id', $id)->where('deposite_status', 1)->sum('amount');
        if ($balance > 0) {
            $total_percent = $tprofit / $balance * 100;
        } else {
            $total_percent = 0;
        }

//        $model = User::where('id', $id)->first();
//        $exchangeInvested = FundDetail::where('u_id', $id)->where('deposite_status', 1)->sum('amount');
//        $exchangeProfit = Profit::where('u_id', $id)->sum('profit');
//        $exchangeShow = $exchangeInvested;
//        $balance = $model->remain_balance;


//        $gift= Gift::where('user_id',$id)->where('status',1)->sum('amount');
//        $model_user=User::where('id',$id)->first();
////            $total_deposite=FundDetail::where('u_id',$id)->where('deposite_status',1)->sum('amount');
//        $total_withdraw=Withdraw::where('u_id',$id)->where('is_withdraw',1)->sum('withdraw_amount');
//        $exchangeProfit=Profit::where('u_id',$id)->sum('profit');
//        if($total_withdraw<=0)
//        {
//            $total_withdraw = 0.0;
//        }
////            use first time to get frozen data
////            $model_user->update(['acc_type'=>$total_deposite]);
//
//        $startDate= date('Y-m-d');
//        $endDate= date('Y-m-d',strtotime("-60 day",strtotime($startDate))) ;
//        $validWithdraw= FundDetail::where('u_id',$id)->where('deposite_status',1)->where('deposit_approved_time','<=',$endDate)->where('deposite_initial_status',0)->get();
//        $array=FundDetail::where('u_id',$id)->where('deposite_status',1)->where('deposit_approved_time','<=',$endDate)->where('deposite_initial_status',0)->get()->toArray();
////
//
//
//        $withdraw2 = Withdraw::where('u_id', $id)->where('is_withdraw', 1)->sum('withdraw_amount');
//
//        $minus = $withdraw2 - ($model->financeleft + $model->financeright);
//        if ($minus <= 0) {
//            $minus = 0;
//        }
//
//        $exchange2ndtime = $exchangeShow + $exchangeProfit - $minus;
//        /*echo "<pre>";
//        print_r($balance);exit;*/
//
//        $startDate = date('Y-m-d');
//        $endDate = date('Y-m-d', strtotime("-60 day", strtotime($startDate)));
//        $validWithdraw = FundDetail::where('u_id', $id)->where('deposite_status', 1)->where('deposit_approved_time', '<=', $endDate)->where('deposite_initial_status', 0)->get();
//        $array = FundDetail::where('u_id', $id)->where('deposite_status', 1)->where('deposit_approved_time', '<=', $endDate)->where('deposite_initial_status', 0)->get()->toArray();
//
//        $withdrawUpdate = User::where('id', $id)->first();
//        /*echo "<pre>";
//        print_r($withdrawUpdate->withdraw_amount);exit;*/
//        if (sizeof($array) == 0) {
//
//        } else {
//            foreach ($validWithdraw as $user) {
//
//                $finalamount = $withdrawUpdate->withdraw_amount + $user->amount;
//                $withdrawUpdate = User::where('id', $id)->update(['withdraw_amount' => $finalamount]);
//
//                $updateDeposit = FundDetail::where('id', $user->id)->update(['deposite_initial_status' => 1]);
//            }
//
//
//        }
//
//
//        $startDate = date('Y-m-d');
//        $tprofit = Profit::where('u_id', $id)->sum('profit');
//        $todayProfit = Profit::where('u_id', $id)->first();
//        $todayProfit = Profit::where('u_id', $id)->where('profit_time', $startDate)->sum('profit');
//        $todayProfit = (int)$todayProfit;
//
//
//        $withdraw = Withdraw::where('u_id', $id)->first();
//        $withdraw = Withdraw::where('u_id', $id)->where('is_withdraw', 1)->sum('withdraw_amount');
//        $withdraw = (int)$withdraw;
//
//        $fund = FundDetail::where('u_id', $id)->where('deposite_status', 1)->sum('amount');
//        if ($balance > 0) {
//            $total_percent = $tprofit / $balance * 100;
//        } else {
//            $total_percent = 0;
//        }


        //print_r($total_percent);exit;


        $weekly = Weekly::where('is_active', 1)->first();

        if (!empty($weekly)) {
            /* $model=date('Y-m-d');
              $weekly=date('Y-m-d',strtotime("-1 day",strtotime((string)$model)));
             $endDate= date('Y-m-d',strtotime("-7 day",strtotime((string)$model)));
              //print_r($endDate);exit;
             $profitLastWeekAvgPercent=Profit::where('u_id',$id)->whereBetween('profit_time',[$endDate,$weekly])->sum('percentage');
             $profitLastWeekCount=Profit::where('u_id',$id)->whereBetween('profit_time',[$endDate,$weekly])->count();

             if($profitLastWeekCount<=0)
             {
               $profitLastWeekCount=1;
             }
             $totalAvgAmount=$profitLastWeekAvgPercent /$profitLastWeekCount;*/
            $model = date('Y-m-d');
            $daily = date('Y-m-d', strtotime("0 day", strtotime((string)$model)));
            $profitLastDayPercent = Profit::where('u_id', $id)->where('profit_time', '=', $daily)->first();

            if (empty($profitLastDayPercent)) {
                $totalAvgAmount = 0;
            } else {
                $totalAvgAmount = $profitLastDayPercent->percentage;
            }


        } else {
            $totalAvgAmount = 0;
        }


        $coin = AdminCoin::where('is_active', 1)->first();
        /*echo "<pre>";
        print_r($coin);exit;*/
        $usercoin = User::where('id', $id)->first();


        /*auth()->user()->unReadNotifications->markAsRead();*/


        /*for($i=1;$i<=3;$i++)
        {
           if($i==3)
           {
               $percentage=0.05;
           }
           else
           {
                $percentage=0.1;
           }
       }

       echo "<pre>";
       print_r($refferby->level);exit;*/
        $currentId = $id;
        $data = User::where('id', $currentId)->first();
        // --------right Team Finance Code -----------
        $lteambalance = $data->financeleft;
        if (empty($lteambalance)) {
            $lteambalance = 0;
        }


        // --------right Team Finance  Code End -----------


        // --------left Team Finance Code -----------
        $rteambalance = $data->financeright;
        if (empty($rteambalance)) {
            $rteambalance = 0;
        }

        // --------left Team Finance  Code End -----------


        // --------left Team count Code ------------


        $reffkey = $data->reff_key;


        $datainner = User::where('reff_by', $reffkey)->get();
        $leftCountTeam = 0;


        if (!empty($check1 = $datainner->toArray())) {
            foreach ($datainner as $value) {
                $datainner1 = User::where('reff_by', $value->reff_key)
                    ->get();

                $leftCountTeam++;

                if (!empty($check2 = $datainner1->toArray())) {
                    foreach ($datainner1 as $value1) {


                        $datainner2 = User::where('reff_by', $value1->reff_key)
                            ->get();

                        $leftCountTeam++;
                        if (!empty($datainner2->toArray())) {
                            foreach ($datainner2 as $value2) {
                                $leftCountTeam++;

                            }
                        }
                    }
                }
            }
        }


        /*echo "<pre>";
          print_r($leftCountTeam);exit;*/

        // --------left Team count Code End ------------


        // --------right Team count Code ------------

        $rightTeam = 0;
        if (!empty($data->system_position)) {
            $afterSystem = User::where('system_position', '>', $data->system_position)
                ->get();


            if (!empty($check2 = $afterSystem->toArray())) {
                foreach ($afterSystem as $value7) {

                    $rightTeam++;
                }
            }
        }
        // --------right Team count Code End ------------


        $reffral_key = $data->reff_key;


//        $allUnapproveddeposite = FundDetail::where('u_id', $id)->where('deposite_status', 0)->get();
//        foreach ($allUnapproveddeposite as $key => $value) {
//            $doApproved = FundDetail::paymentVerify($value->id);
//        }

//dd($left_withdraw_able,$right_withdraw_able);

        return view('admin.users.detail')->withModel($model_user)->withCoin($coin)
            ->withPercent($total_percent)->withBalance($balance)->withTodayP($todayProfit)->withWithdraw($withdraw)->withFund($fund)->withAvg($totalAvgAmount)->withRightTeam($rightTeam)
            ->withLeftTeam($leftCountTeam)->withLbalance($lteambalance)->withRbalance($rteambalance)->withReffKey($reffral_key)->withExchange($usd_invested)->withUsercoin($usercoin)->withleftwithdrawable($left_withdraw_able)->withrightwithdrawable($right_withdraw_able)
            ->withExchange2ndtime($exchange_assets);;
    }

    public function sosDetail($id)
    {
        $contact = Contact::where('id', $id)->first();
        $comments = Comments::where('contact_id', $id)->get();
        return view('admin.contact.detail')->withContact($contact)->withComments($comments);
    }

    public function sosDetailStore(Request $request)
    {
        $posterName = "";


        if (request()->hasFile('contact_pic1')) {

            /*echo "<pre>";
       print_r(request()->contact_pic->getClientOriginalExtension());exit; getClientOriginalName  getClientOriginalExtension*/

//            $posterName = time() . '.' . request()->contact_pic1->getClientOriginalName();
            $posterName = time() . '.' . 'ad';
            request()->contact_pic1->move(public_path('images/comment_pic'), $posterName);
        }

        $request->merge(['comment_pic' => $posterName]);

        $model = Comments::create($request->all());
        if (request()->hasFile('contact_pic1')) {
            activity('Add Reply')
                ->causedBy(auth()->user())
//                    ->performedOn($user)
                ->withProperties(['File Attachment' => 'True', 'text' => $request->get('text')])
                ->log("User Reply to ticket No {$request->get('contact_id')}");
        } else {
            activity('Add Reply')
                ->causedBy(auth()->user())
//                    ->performedOn($user)
                ->withProperties(['text' => $request->get('text')])
                ->log("User Reply to ticket No {$request->get('contact_id')}");
        }
        $notification = array(
            'message' => 'Message Send to User Successfully',
            'alert-type' => 'success',
            'heading' => 'Succeeded',
        );

        return redirect()->route('admin.sos.detail', $request->get('contact_id'))->with($notification);
    }

    public function addRoles()
    {

        $admins = User::with('roles')->with('permissions')->where('admin', 1)->where('id', '!=', 2)->get();
        $roles = Role::where('id', '!=', 3)->get();
        $permissions = Permission::where('id', '!=', 9)->get();

        return view("admin.roles", compact('admins'))->withroles($roles)->withpermissions($permissions);
    }

    public function add_Users(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'reff_key' => '',
            'admin' => '1',
            'reff_by' => 'super admin',
//            'system_position'=>,
//            'level'=> '',
            'btc_address' => '',
            'acc_name' => '',
            'acc_no' => '',
            'branch_code' => '',
            'acc_type' => '',
            'swift_code' => '',
            'email_status' => 1,
            'email_token' => ''

        ]);
        return response()->json([
            'message' => 'User Added Successfully. Please Add Role to this User.'

        ]);
    }

    public function add_Roles_Permissions_Ajax(Request $request)
    {


        if ($request->ajax()) {

            if (!empty($request->get('user_id')) && $request->get('action') == 'add_roles') {
                $message = '';
                if (!empty($request->get('password'))) {
                    $this->validate($request, [

                        'password' => ['required', 'string', 'min:8']
                    ]);
                    $pasw = Hash::make($request->get('password_field'));
                    User::where('admin', 1)->where('id', $request->get('user_id'))->update(['password' => $pasw]);
                    $message = ' Your Password is Updated Butt ';
                }

                $User_id = User::where('id', $request->get('user_id'))->first();
//                dd($User_id);
                if (!empty($request->get('permission')) && !empty($request->get('selected_role'))) {
                    //add roles and permissions both
//                    dd('roles and permissions both', $request->all());
                    //on user id  assigns roles array remove existing
                    $User_id->syncRoles($request->get('selected_role'));

                    // if user have any permissions remove these and then assign again
                    $User_id = User::where('id', $request->get('user_id'))->first();
                    $permissions = $User_id->getDirectPermissions();
                    $User_id->revokePermissionTo($permissions);

                    //on user id  assigns permission array
                    $User_id->givePermissionTo($request->get('permission'));
                    return ["error" => null, 'success' => '200'];

                } elseif (!empty($request->get('selected_role'))) {
                    //add roles only
//                    dd('selected_role', $request->all(), $request->get('action'));

                    //on user id  assigns roles array
//                 $User_id->assignRole($request->get('selected_role')); // for multiple roles

                    $User_id->syncRoles($request->get('selected_role'));

                    return ["error" => null, 'success' => '200'];

                } elseif (!empty($request->get('permission'))) {

                    // if user have any permissions remove these and then assign again
                    $User_id = User::where('id', $request->get('user_id'))->first();
                    $permissions = $User_id->getDirectPermissions();
                    $User_id->revokePermissionTo($permissions);
                    //add permission onlye
                    //on user id  assigns permission array
                    $User_id->givePermissionTo($request->get('permission'));
//                    dd('permission', $request->all(), $request->get('action'));
                    return ["error" => null, 'success' => '200'];


                } else {
                    //no extra permissions selected
                    //no role is selected
                    return [
                        'error' => 421,
                        'message' => $message . 'No Role OR Any Permission is selected'
                    ];
                }

            } elseif (!empty($request->get('user_id')) && $request->get('action') == 'removeRoles') {
                //remove roles and permissions
//                dd('remove',$request->all());
                if ($request->get('user_id') != 2) {
                    $User_id = User::where('id', $request->get('user_id'))->first();

                    $User_id->syncRoles();

                    return ["error" => null, 'success' => '200'];
                } else {
                    return ["error" => 417, 'message' => 'System is not allowed to do this Action'];

                }

            } elseif ((!empty($request->get('user_id')) && $request->get('action') == 'removePermission')) {
                if ($request->get('user_id') != 2) {
                    $User_id = User::where('id', $request->get('user_id'))->first();
                    $permissions = $User_id->getDirectPermissions();
                    $User_id->revokePermissionTo($permissions);

                    return ["error" => null, 'success' => '200'];
                } else {
                    return ["error" => 417, 'message' => 'System is not allowed to do this Action'];

                }
            } else {
                return [
                    'error' => 409,
                    'message' => 'User ID is Missing'
                ];
            }

        }
    }

    public function view_User_Info(Request $request)
    {
//        $encrypted = Crypt::encryptString('Hello world.');
//
//        $decrypted = Crypt::decryptString($encrypted);
//        dd($decrypted);
        $user_info = User::with('roles')->with('permissions')->where('admin', 1)->where('id', $request->get('user_id'))->first();//            ->makeVisible(['password']);

        $activity_filters = DB::table('activity_log')->select('log_name')->distinct('log_name')->where('causer_id', $request->get('user_id'))->get();
//        $user_info =Crypt::decryptString($user_info->password);
//        $user_info = ;
        return response()->json([
            'user' => $user_info,
            'activity_filters' => $activity_filters,
            'error' => null
        ]);
    }

    public function view_User_Logs(Request $request)
    {
//        $encrypted = Crypt::encryptString('Hello world.');
//
//        $decrypted = Crypt::decryptString($encrypted);
//        dd($decrypted);
//        dd(auth()->user()->actions);
        $user = User::where('id', $request->get('user_id'))->first();
        $user_Log = $user->actions;
//        dd($request->get('user_id'),$user_Log);


        return response()->json([
            'user_Log' => $user_Log
        ]);


    }

    public function user_Log_Filer_ById(Request $request)
    {
        if (!empty($request->get('filer_name'))) {
            $user_Log = Activity::where('causer_id', $request->get('user_id'))->inLog($request->get('filer_name'))->get();

        } else {
            $user = User::where('id', $request->get('user_id'))->first();
            $user_Log = $user->actions;
        }
//        dd($request->all(),$user_Log);
        return response()->json([
            'user_Log' => $user_Log
        ]);

    }


}
