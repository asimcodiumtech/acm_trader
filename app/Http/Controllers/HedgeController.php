<?php

namespace App\Http\Controllers;

use App\Hedge;
use App\HedgeDetails;
use App\HedgeFund;
use App\HedgeProfit;
use App\HedgeProfitHistory;
use App\HedgeWithdraw;
use App\Models\Gift;
use App\Models\WithDraw;
use App\User;
use Carbon\Carbon;
use Cryptobox;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class HedgeController extends Controller
{


//    public function userIndex()
//    {
//        $hedge = Hedge::all();
//        return view('user.hedge.hedge_dashboard', compact('hedge'));
//    }
    public function userIndex(Request $request)
    {
        $todaydate = Carbon::now()->format('y-m-d');

//        $hedge = Hedge::where('available_slot','!=',0)->orwhere('slot_opening_date','<=',$todaydate , 'slot_closing_date','<',$todaydate)->get();
        $hedge = Hedge::where('available_slot', '!=', 0)->Where(function ($query) {
            $query->where('slot_opening_date', '<=', Carbon::now()->format('y-m-d'))
                ->where('slot_closing_date', '>=', Carbon::now()->format('y-m-d'));
        })
            ->get()->toArray();;
        $check = [];
        foreach ($hedge as $key => $value) {
            $fundhedge = HedgeDetails::where('hedge_id', $value['id'])->get()->count() ? HedgeDetails::where('hedge_id', $value['id'])->get()->sum('invested_amount') : 0;
            $check[] = array_merge($value, ['total_invested' => $fundhedge]);

        }
        $hedge = $check;
//        return view('user.hedge.hedge_dashboard', compact('hedge'));
//        if(auth()->user()->admin ==1)
//        {

//            $nousers=User::where('admin',2)->get()->count();
//            $nodeposite=FundDetail::where('deposite_status',1)->sum('amount');
//            $nowithdraw=Withdraw::where('is_withdraw',1)->sum('withdraw_amount');
//            $noActive=User::where('admin',2)->where('remember_token','!=',null)->get()->count();
//            $userss=User::where('admin','!=',1)->get();
//
//
//            /*print_r($nowithdraw);exit;*/
//            $request->session()->forget('user');
//            return view('admin.dashboard')->withNousers($nousers)->withNodeposite($nodeposite)->withNoWithdraw($nowithdraw)->withActive($noActive);
//        }
        if (auth()->user()->admin == 2) {
            $userinfomodel = User::user_Balance();
            $model_user = User::where('id', auth()->user()->id)->first();


            // Change path to your files
            // --------------------------------------
            DEFINE("CRYPTOBOX_PHP_FILES_PATH", app_path() . '/lib');

            // path to directory with files: cryptobox.class.php / cryptobox.callback.php / cryptobox.newpayment.php;
            // cryptobox.newpayment.php will be automatically call through ajax/php two times - payment received/confirmed
            DEFINE("CRYPTOBOX_IMG_FILES_PATH", asset("/images") . '/');      // path to directory with coin image files (directory 'images' by default)
            DEFINE("CRYPTOBOX_JS_FILES_PATH", asset("/cryptojs") . '/');            // path to directory with files: ajax.min.js/support.min.js

            // Change values below
            // --------------------------------------
            DEFINE("CRYPTOBOX_LANGUAGE_HTMLID", "alang");    // any value; customize - language selection list html id; change it to any other - for example 'aa';  default 'alang'
            DEFINE("CRYPTOBOX_COINS_HTMLID", "acoin");        // any value;  customize - coins selection list html id; change it to any other - for example 'bb';   default 'acoin'
            DEFINE("CRYPTOBOX_PREFIX_HTMLID", "acrypto_");    // any value; prefix for all html elements; change it to any other - for example 'cc';    default 'acrypto_'

            // Open Source Bitcoin Payment Library
            // ---------------------------------------------------------------
            //dd(CRYPTOBOX_PHP_FILES_PATH . "/cryptobox.class.php");

            require_once(CRYPTOBOX_PHP_FILES_PATH . "/cryptobox.class.php");
            /*********************************************************/
            /****  PAYMENT BOX CONFIGURATION VARIABLES  ****/
            /*********************************************************/
            // IMPORTANT: Please read description of options here - https://gourl.io/api-php.html#options
            $userID = auth()->user()->id;            // place your registered userID or md5(userID) here (user1, user7, uo43DC, etc).
            // You can use php $_SESSION["userABC"] for store userID, amount, etc
            // You don't need to use userID for unregistered website visitors - $userID = "";
            // if userID is empty, system will autogenerate userID and save it in cookies
            $userFormat = "COOKIE";     // save userID in cookies (or you can use IPADDRESS, SESSION, MANUAL)
            $orderID = "deposit";           // invoice number - 000383
            $amountUSD = 0.01;         // invoice amount - 2.21 USD; or you can use - $amountUSD = convert_currency_live("EUR", "USD", 22.37); // convert 22.37EUR to USD
            $period = "1 MINUTE";   // one time payment, not expiry
            $def_language = "en";         // default Language in payment box
            $def_coin = "bitcoin";      // default Coin in payment box
            // List of coins that you accept for payments
            //$coins = array('bitcoin', 'bitcoincash', 'bitcoinsv', 'litecoin', 'dogecoin', 'dash', 'speedcoin', 'reddcoin', 'potcoin', 'feathercoin', 'vertcoin', 'peercoin', 'monetaryunit', 'universalcurrency');
            $coins = array('bitcoin');  // for example, accept payments in bitcoin, bitcoincash, litecoin, dash, speedcoin
            // Create record for each your coin - https://gourl.io/editrecord/coin_boxes/0 ; and get free gourl keys
            // It is not bitcoin wallet private keys! Place GoUrl Public/Private keys below for all coins which you accept
            $all_keys = array("bitcoin" => array("public_key" => "-your public key for Bitcoin box-", "private_key" => "-your private key for Bitcoin box-"),
                "bitcoincash" => array("public_key" => "-your public key for BitcoinCash box-", "private_key" => "-your private key for BitcoinCash box-"),
                "litecoin" => array("public_key" => "-your public key for Litecoin box-", "private_key" => "-your private key for Litecoin box-")); // etc.
            // Demo Keys; for tests (example - 5 coins)
            $all_keys = array(
                "bitcoin" => array("public_key" => "50135AAiWAInBitcoin77BTCPUBuIS97PyiG1pptLIw5N3ekj6",
                    "private_key" => "50135AAiWAInBitcoin77BTCPRVwNygkqzetvtaDlV1oNU5bYt")); // Demo keys!

            $def_coin = strtolower($def_coin);
            if (!in_array($def_coin, $coins)) $coins[] = $def_coin;
            foreach ($coins as $v) {
                if (!isset($all_keys[$v]["public_key"]) || !isset($all_keys[$v]["private_key"])) die("Please add your public/private keys for '$v' in \$all_keys variable");
                elseif (!strpos($all_keys[$v]["public_key"], "PUB")) die("Invalid public key for '$v' in \$all_keys variable");
                elseif (!strpos($all_keys[$v]["private_key"], "PRV")) die("Invalid private key for '$v' in \$all_keys variable");
                elseif (strpos(CRYPTOBOX_PRIVATE_KEYS, $all_keys[$v]["private_key"]) === false)
                    die("Please add your private key for '$v' in variable \$cryptobox_private_keys, file /lib/cryptobox.config.php.");
            }

            // Current selected coin by user
            $coinName = cryptobox_selcoin($coins, $def_coin);
            // Current Coin public/private keys
            $public_key = $all_keys[$coinName]["public_key"];
            $private_key = $all_keys[$coinName]["private_key"];

            /** PAYMENT BOX **/
            $options = array(
                "public_key" => $public_key, // your public key from gourl.io
                "private_key" => $private_key,    // your private key from gourl.io
                "webdev_key" => "",          // optional, gourl affiliate key
                "orderID" => $orderID,        // order id or product name
                "userID" => $userID,         // unique identifier for every user
                "userFormat" => $userFormat,     // save userID in COOKIE, IPADDRESS, SESSION  or MANUAL
                "amount" => 0,           // product price in btc/bch/bsv/ltc/doge/etc OR setup price in USD below

                "amountUSD" => $amountUSD,  // we use product price in USD
                "period" => $period,         // payment valid period
                "language" => $def_language  // text on EN - english, FR - french, etc
            );

            // Initialise Payment Class
            $box = new Cryptobox ($options);

            // coin name
            $coinName = $box->coin_name();

            $html = $box->display_cryptobox_bootstrap($coins, $def_coin, $def_language, null, 200, 200, false, "default", "default", 250, "", "ajax", false);

            $myVar = htmlspecialchars($html, ENT_QUOTES);
            $check = htmlentities($html);

//            $startDate= date('Y-m-d');
//            $todayProfit=HedgeProfit::where('u_id',auth()->user()->id)->first();
//            $todayProfit=HedgeProfit::where('user_id',auth()->user()->id)->where('profit_time',$startDate)->sum('amount');
//            $todayProfit=(int)$todayProfit;
            $user_hedge_funds = HedgeDetails::userFundDetails();
            $hedgeInvested = HedgeDetails::where('user_id', auth()->user()->id)->get()->count() ? HedgeDetails::where('user_id', auth()->user()->id)->get()->sum('invested_amount') : 0;
            $model = date('Y-m-d');
            $daily = date('Y-m-d', strtotime("0 day", strtotime((string)$model)));
            $allowed_withdraw = 1;
            $coin = 1;
            $fund = 1;
            $reffral_key = 1;
            $rteambalance = 1;
            $leftCountTeam = 1;
            $lteambalance = 1;
            $notification = 1;
            $usercoin = 1;
            $rightTeam = 1;
            $myHedgeList = HedgeDetails::where('user_id', auth()->user()->id)->get()->toArray();
            $check = [];
            foreach ($myHedgeList as $key => $value) {
                $hedgeindex = Hedge::where('id', $value['hedge_id'])->first();
                $todayper = HedgeProfit::where('hedge_id', $value['hedge_id'])->where('profit_time', $daily)->first();
                if (!empty($todayper)) {
                    $check[] = array_merge($value, ['percentage' => $todayper['percentage'], 'title' => $hedgeindex->title]);

                } else {
                    $check[] = array_merge($value, ['percentage' => 0, 'title' => $hedgeindex->title]);

                }
            }
            $myHedgeListPer = $check;
//           dd($check);
            $totalAvgAmount = HedgeProfit::where('user_id', auth()->user()->id)->where('profit_time', '=', $daily)->get()->count() ? HedgeProfit::where('user_id', auth()->user()->id)->where('profit_time', '=', $daily)->get()->sum('amount') : 0;
            $withdraw = 1;
            $total_percent = 1;
            $hedgeCount = HedgeDetails::where('user_id', auth()->user()->id)->get()->count() ? HedgeDetails::where('user_id', auth()->user()->id)->get()->count() : 0;
            $total_profit = HedgeDetails::where('user_id', auth()->user()->id)->get()->count() ? HedgeDetails::where('user_id', auth()->user()->id)->get()->sum('total_profit') : 0;
            $hedgeWithdrawable = HedgeDetails::where('user_id', auth()->user()->id)->get()->count() ? HedgeDetails::where('user_id', auth()->user()->id)->get()->sum('total_withdraw') : 0;
//            $balance = (HedgeDetails::where('user_id', auth()->user()->id)->get()->count() ? HedgeDetails::where('user_id', auth()->user()->id)->get()->sum('balance') : 0)+ $total_profit;
            $balance = $hedgeInvested + $total_profit;
            $todayProfit = 0;
            return view('user.hedge.hedge_dashboard', compact('allowed_withdraw', 'hedge', 'total_profit', 'hedgeWithdrawable', 'hedgeCount', 'myHedgeListPer'))->withModel($model_user)->withCoin($coin)
                ->withPercent($total_percent)->withBalance($balance)->withTodayP($todayProfit)->withWithdraw($withdraw)->withFund($fund)->withAvg($totalAvgAmount)->withNotify($notification)->withRightTeam($rightTeam)
                ->withLeftTeam($leftCountTeam)->withLbalance($lteambalance)->withRbalance($rteambalance)->withReffKey($reffral_key)->withhedgeInvested($hedgeInvested)->withUsercoin($usercoin)
                ->withExchange2ndtime($userinfomodel['exchange_assets'])->withprofitdrawable($userinfomodel['profit_withdraw_able'])->withleftwithdrawable($userinfomodel['left_withdraw_able'])->withrightwithdrawable($userinfomodel['right_withdraw_able'])->withusdwithdrawable($userinfomodel['usd_withdraw_able']);
        } else {
            return view('home');
        }
    }

    public function hedgeBookingBtcAjax(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->get('hedgeid'))) {


                // Change path to your files
                // --------------------------------------
                DEFINE("CRYPTOBOX_PHP_FILES_PATH", app_path() . '/lib');

                // path to directory with files: cryptobox.class.php / cryptobox.callback.php / cryptobox.newpayment.php;
                // cryptobox.newpayment.php will be automatically call through ajax/php two times - payment received/confirmed
                DEFINE("CRYPTOBOX_IMG_FILES_PATH", asset("/images") . '/');      // path to directory with coin image files (directory 'images' by default)
                DEFINE("CRYPTOBOX_JS_FILES_PATH", asset("/cryptojs") . '/');            // path to directory with files: ajax.min.js/support.min.js

                // Change values below
                // --------------------------------------
                DEFINE("CRYPTOBOX_LANGUAGE_HTMLID", "alang");    // any value; customize - language selection list html id; change it to any other - for example 'aa';  default 'alang'
                DEFINE("CRYPTOBOX_COINS_HTMLID", "acoin");        // any value;  customize - coins selection list html id; change it to any other - for example 'bb';   default 'acoin'
                DEFINE("CRYPTOBOX_PREFIX_HTMLID", "acrypto_");    // any value; prefix for all html elements; change it to any other - for example 'cc';    default 'acrypto_'

                // Open Source Bitcoin Payment Library
                // ---------------------------------------------------------------
                //dd(CRYPTOBOX_PHP_FILES_PATH . "/cryptobox.class.php");

                require_once(CRYPTOBOX_PHP_FILES_PATH . "/cryptobox.class.php");


                /*********************************************************/
                /****  PAYMENT BOX CONFIGURATION VARIABLES  ****/
                /*********************************************************/
//                if ($request->get('type') == "btc_hedge_booking") {
//                    $amountusdbtc = $request->get('amount');
//                } else {
//                    $amountusdbtc = $request->get('amount') + $request->get('amount') * 0.1;
//                    /*print_r($amountusdbtc);exit;*/
//
//                }

                // Current Coin public/private keys

                $input_slots = $request->get('input_slots');
                $hedge = Hedge::where('id', $request->get('hedgeid'))->first();
                $total_amount = $hedge->amount * $input_slots;
                $hedgeid = $request->get('hedgeid');

                // IMPORTANT: Please read description of options here - https://gourl.io/api-php.html#options



                $userID = auth()->user()->id."_".$input_slots."_".$hedgeid."";            // place your registered userID or md5(userID) here (user1, user7, uo43DC, etc).


//dd($userID,$arry[1],$arry[0]);


                // You can use php $_SESSION["userABC"] for store userID, amount, etc
                // You don't need to use userID for unregistered website visitors - $userID = "";
                // if userID is empty, system will autogenerate userID and save it in cookies
                $userFormat = "COOKIE";     // save userID in cookies (or you can use IPADDRESS, SESSION, MANUAL)
                $orderID = 'hedge_booking';           // invoice number - 000383
//                $amountUSD = $amountusdbtc;         // invoice amount - 2.21 USD; or you can use - $amountUSD = convert_currency_live("EUR", "USD", 22.37); // convert 22.37EUR to USD

                $period = "1 MINUTE";   // one time payment, not expiry
                $def_language = "en";         // default Language in payment box
                $def_coin = "bitcoin";      // default Coin in payment box


                // List of coins that you accept for payments
                //$coins = array('bitcoin', 'bitcoincash', 'bitcoinsv', 'litecoin', 'dogecoin', 'dash', 'speedcoin', 'reddcoin', 'potcoin', 'feathercoin', 'vertcoin', 'peercoin', 'monetaryunit', 'universalcurrency');
                $coins = array('bitcoin');  // for example, accept payments in bitcoin, bitcoincash, litecoin, dash, speedcoin

                // Create record for each your coin - https://gourl.io/editrecord/coin_boxes/0 ; and get free gourl keys
                // It is not bitcoin wallet private keys! Place GoUrl Public/Private keys below for all coins which you accept

                $all_keys = array("bitcoin" => array("public_key" => "-your public key for Bitcoin box-", "private_key" => "-your private key for Bitcoin box-"),
                    "bitcoincash" => array("public_key" => "-your public key for BitcoinCash box-", "private_key" => "-your private key for BitcoinCash box-"),
                    "litecoin" => array("public_key" => "-your public key for Litecoin box-", "private_key" => "-your private key for Litecoin box-")); // etc.

                // Demo Keys; for tests (example - 5 coins)
                /*$all_keys = array(
                      "speedcoin" => array( "public_key" => "49401AACb3hFSpeedcoin77SPDPUB1hYbx8qxI8ORiyCheDccG",
                                        "private_key" => "49401AACb3hFSpeedcoin77SPDPRVl3NeOVA1ofQlTp5pozYB6"));*/ // Demo keys!


                $all_keys = array("bitcoin" => array("public_key" => "50135AAiWAInBitcoin77BTCPUBuIS97PyiG1pptLIw5N3ekj6",
                    "private_key" => "50135AAiWAInBitcoin77BTCPRVwNygkqzetvtaDlV1oNU5bYt"));

                //  IMPORTANT: Add in file /lib/cryptobox.config.php your database settings and your gourl.io coin private keys (need for Instant Payment Notifications) -
                /* if you use demo keys above, please add to /lib/cryptobox.config.php -
        $cryptobox_private_keys = array("25654AAo79c3Bitcoin77BTCPRV0JG7w3jg0Tc5Pfi34U8o5JE", "25678AACxnGODogecoin77DOGEPRVFvl6IDdisuWHVJLo5m4eq",
                    "25656AAeOGaPBitcoincash77BCHPRV8quZcxPwfEc93ArGB6D", "25657AAOwwzoLitecoin77LTCPRV7hmp8s3ew6pwgOMgxMq81F",
                    "25678AACxnGODogecoin77DOGEPRVFvl6IDdisuWHVJLo5m4eq", "25658AAo79c3Dash77DASHPRVG7w3jg0Tc5Pfi34U8o5JEiTss",
                    "20116AA36hi8Speedcoin77SPDPRVNOwjzYNqVn4Sn5XOwMI2c");
        Also create table "crypto_payments" in your database, sql code - https://github.com/cryptoapi/Payment-Gateway#mysql-table
        Instruction - https://gourl.io/api-php.html
    */

                // Re-test - all gourl public/private keys
                $def_coin = strtolower($def_coin);
                if (!in_array($def_coin, $coins)) $coins[] = $def_coin;
                foreach ($coins as $v) {
                    if (!isset($all_keys[$v]["public_key"]) || !isset($all_keys[$v]["private_key"])) die("Please add your public/private keys for '$v' in \$all_keys variable");
                    elseif (!strpos($all_keys[$v]["public_key"], "PUB")) die("Invalid public key for '$v' in \$all_keys variable");
                    elseif (!strpos($all_keys[$v]["private_key"], "PRV")) die("Invalid private key for '$v' in \$all_keys variable");
                    elseif (strpos(CRYPTOBOX_PRIVATE_KEYS, $all_keys[$v]["private_key"]) === false)
                        die("Please add your private key for '$v' in variable \$cryptobox_private_keys, file /lib/cryptobox.config.php.");
                }


                // Current selected coin by user
                $coinName = cryptobox_selcoin($coins, $def_coin);

                $public_key = $all_keys[$coinName]["public_key"];
                $private_key = $all_keys[$coinName]["private_key"];

//                dd($hedge ,$total_amount);
                /** PAYMENT BOX **/
                $options = array(
                    "dir" => 12,
                    "public_key" => $public_key, // your public key from gourl.io
                    "private_key" => $private_key,    // your private key from gourl.io
                    "webdev_key" => "",          // optional, gourl affiliate key
                    "orderID" => $orderID,        // order id or product name
                    "userID" => $userID,         // unique identifier for every user
                    "userFormat" => $userFormat,     // save userID in COOKIE, IPADDRESS, SESSION  or MANUAL
                    "amount" => 0,           // product price in btc/bch/bsv/ltc/doge/etc OR setup price in USD below
                    "amountUSD" => $total_amount,  // we use product price in USD
                    "period" => $period,         // payment valid period
                    "language" => $def_language,  // text on EN - english, FR - french, etc
                    'input_slots' => $input_slots,
//                    'hedgeid' => $hedgeid,

                );

                // Initialise Payment Class
                $box = new Cryptobox ($options);

                // coin name
                $coinName = $box->coin_name();

                // php code end :)
                // ---------------------

                // NOW PLACE IN FILE "lib/cryptobox.newpayment.php", function cryptobox_new_payment(..) YOUR ACTIONS -
                // WHEN PAYMENT RECEIVED (update database, send confirmation email, update user membership, etc)
                // IPN function cryptobox_new_payment(..) will automatically appear for each new payment two times - payment received and payment confirmed
                // Read more - https://gourl.io/api-php.html#ipn
                /*$data=print_r($box->display_cryptobox_bootstrap($coins, $def_coin, $def_language, null, 200, 200, false, "default", "default", 250, "", "ajax", false));*/
                $html = $box->display_cryptobox_bootstrap($coins, $def_coin, $def_language, null, 200, 200, false, "default", "default", 250, "", "ajax", false);

                $myVar = htmlspecialchars($html, ENT_QUOTES);
                $check = htmlentities($html);
                return ["error" => null, 'success' => '200', 'box' => $box,
                    'coins' => $coins,
                    'def_coin' => $def_coin,
                    'def_language' => $def_language, 'data' => $box->display_cryptobox_bootstrap($coins, $def_coin, $def_language, null, 200, 200, false, "default", "default", 250, "", "ajax", false)];
                /*return response()->json(['success' => 'ok','box' => $box,
            'coins' => $coins,
            'def_coin' => $def_coin,
            'def_language' => $def_language]);*/

            }
        }
    }

    public function profitIndex()
    {
        $prof = HedgeProfit::where('user_id', auth()->user()->id)->get()->toArray();
        $check = [];
//        dd($prof);
        $sum = 0;
        foreach ($prof as $key => $value) {
            $hedge = Hedge::where('id', $value['hedge_id'])->select('title', 'code')->first();

            $check[] = array_merge($value, ['total_amount' => $sum + $value['amount']], ['hedge_title' => $hedge['title']], ['hedge_code' => $hedge['code']]);
            $sum = $sum + $value['amount'];
        }
//        dd($check);

        return view('user.hedge.profit.index', compact('check'));
    }

    public function depositList()
    {
        $prof = HedgeFund::where('user_id', auth()->user()->id)->get()->toArray();
        $check = [];
        $sum = 0;
        foreach ($prof as $key => $value) {
            $hedge = Hedge::where('id', $value['hedge_id'])->select('title', 'code')->first();
//           dd($hedge);
            $check[] = array_merge($value, ['total_amount' => $sum + $value['amount']], ['hedge_title' => $hedge['title']], ['hedge_code' => $hedge['code']]);
            $sum = $sum + $value['amount'];
        }
//        dd($check);

        return view('user.hedge.deposit.list', compact('check'));
    }

    public function withdrawIndex()
    {
        $prof = HedgeWithdraw::where('user_id', auth()->user()->id)->get()->toArray();
        $check = [];
        $sum = 0;
        foreach ($prof as $key => $value) {
            $hedge = Hedge::where('id', $value['hedge_id'])->select('title', 'code')->first();
//           dd($hedge);
            $check[] = array_merge($value, ['total_amount' => $sum + $value['amount']], ['hedge_title' => $hedge['title']], ['hedge_code' => $hedge['code']]);
            $sum = $sum + $value['amount'];
        }
//        dd($check);

        return view('user.hedge.withdraw.index', compact('check'));
    }

    public function myHedgeIndex()
    {
        $prof = HedgeDetails::where('user_id', auth()->user()->id)->get()->toArray();
        $check = [];
//        $sum = 0;
        foreach ($prof as $key => $value) {
            $hedfund = HedgeFund::where('user_id', auth()->user()->id)->where('hedge_id', $value['hedge_id'])->get();

            $hedge = Hedge::where('id', $value['hedge_id'])->select('title', 'code')->first();
//           dd($hedge);
            $check[] = array_merge($value, ['hedge_title' => $hedge['title']], ['hedge_code' => $hedge['code']], ['slot' => $hedfund->sum('slot')]);
//            $sum = $sum + $value['amount'];
        }
//        dd($check);
//        $prof = HedgeFund::where('user_id', auth()->user()->id)->get()->toArray();
//        $check = [];
//        $sum = 0;
//        foreach ($prof as $key => $value) {
//            $hedge = Hedge::where('id', $value['hedge_id'])->select('title', 'code')->first();
////           dd($hedge);
//            $check[] = array_merge($value, ['total_amount' => $sum + $value['amount']], ['hedge_title' => $hedge['title']], ['hedge_code' => $hedge['code']]);
//            $sum = $sum + $value['amount'];
//        }
//        dd($check);

        return view('user.hedge.myhedge.index', compact('check'));
    }

//bookingAjax
    public function checkHedgeInfo(Request $request)
    {
        dd($request->all());
    }

    public function bookingAjax(Request $request)
    {
        if ($request->ajax()) {
//            $is_allowed_withdraw = WithDraw::where('u_id', auth()->user()->id)->where('is_withdraw', '!=', 1)->get()->toArray();
//            if (sizeof($is_allowed_withdraw) != 0) {
//                return ['error' => 1010];
//            }
//            if ($request->get('withdraw_amount') < 20) {
//                return ['error' => 1212];
//            }
            if (!empty($request->get('hedgeid')) && $request->get('input_slots') && $request->get('coin_type')) {
                //inputs
                $input_slot = $request->get('input_slots');
                $coin_type = $request->get('coin_type');
                $hedge_id = $request->get('hedgeid');
                $btc_rate = $request->get('btc_rate');
                $user_id = auth()->user()->id;
                $time_deposited = date('Y-m-d');
                $user = User::where('id', $user_id)->first();
                $hedge = Hedge::where('id', $hedge_id)->first();

                if (!$hedge->available_slot < $input_slot) {

                    $total_amount = $hedge->amount * $input_slot;
                    $userinfomodel = User::user_Balance($user_id);


                    if ($coin_type == 'right') {

                        if ($userinfomodel['right_withdraw_able'] >= $total_amount) {
                            HedgeDetails::updateInvestment($hedge_id, $user_id, $total_amount);

                            $hedge_slots = $hedge->available_slot - $input_slot;
                            $hedge_update = Hedge::where('id', $hedge_id)->update(['available_slot' => $hedge_slots]);

                            $model = WithDraw::create(['u_id' => $user_id, 'btc_id' => $user->btc_address, 'btc_rate' => $btc_rate, 'bank_type' => 'hedge_booking', 'swift_code' => $hedge_id, 'withdraw_amount' => $total_amount, 'coin_type' => $coin_type, 'is_withdraw' => 1]);

                            $hedge_fund = HedgeFund::create(['user_id' => $user_id, 'hedge_id' => $hedge_id, 'slot' => $input_slot, 'per_slot' => $hedge->amount, 'amount' => $total_amount, 'btc_rate' => $btc_rate, 'type' => $coin_type, 'deposite_status' => '1', 'deposit_approved_time' => $time_deposited]);
//                                 dd($request->all(),$userinfomodel);
                            User::updateFinanceCommission($user_id, $total_amount);

                            return ["error" => null, 'success' => '200'];
                        } else {
                            return ['error' => 405, 'message' => "You don't have sufficient fund !!"];
                        }

                    } elseIf ($coin_type == 'left') {
                        if ($userinfomodel['left_withdraw_able'] >= $total_amount) {
                            HedgeDetails::updateInvestment($hedge_id, $user_id, $total_amount);

                            $hedge_slots = $hedge->available_slot - $input_slot;
                            $hedge_update = Hedge::where('id', $hedge_id)->update(['available_slot' => $hedge_slots]);


                            $model = WithDraw::create(['u_id' => $user_id, 'btc_id' => $user->btc_address, 'btc_rate' => $btc_rate, 'bank_type' => 'hedge_booking', 'swift_code' => $hedge_id, 'withdraw_amount' => $total_amount, 'coin_type' => $coin_type, 'is_withdraw' => 1]);
                            $hedge_fund = HedgeFund::create(['user_id' => $user_id, 'hedge_id' => $hedge_id, 'slot' => $input_slot, 'per_slot' => $hedge->amount, 'amount' => $total_amount, 'btc_rate' => $btc_rate, 'type' => $coin_type, 'deposite_status' => '1', 'deposit_approved_time' => $time_deposited]);
                            User::updateFinanceCommission($user_id, $total_amount);

                            return ["error" => null, 'success' => '200'];
                        } else {
                            return ['error' => 405, 'message' => "You don't have sufficient fund !!"];

                        }
                    } elseif ($coin_type == 'profit') {
                        if ($userinfomodel['profit_withdraw_able'] >= $total_amount) {
                            HedgeDetails::updateInvestment($hedge_id, $user_id, $total_amount);

                            $hedge_slots = $hedge->available_slot - $input_slot;
                            $hedge_update = Hedge::where('id', $hedge_id)->update(['available_slot' => $hedge_slots]);


                            $model = WithDraw::create(['u_id' => $user_id, 'btc_id' => $user->btc_address, 'btc_rate' => $btc_rate, 'bank_type' => 'hedge_booking', 'swift_code' => $hedge_id, 'withdraw_amount' => $total_amount, 'coin_type' => $coin_type, 'is_withdraw' => 1]);
                            $hedge_fund = HedgeFund::create(['user_id' => $user_id, 'hedge_id' => $hedge_id, 'slot' => $input_slot, 'per_slot' => $hedge->amount, 'amount' => $total_amount, 'btc_rate' => $btc_rate, 'type' => $coin_type, 'deposite_status' => '1', 'deposit_approved_time' => $time_deposited]);
                            User::updateFinanceCommission($user_id, $total_amount);

                            return ["error" => null, 'success' => '200'];
                        } else {
                            return ['error' => 405, 'message' => "You don't have sufficient fund !!"];

                        }
                    } elseif ($coin_type == 'usd') {
                        if ($userinfomodel['usd_withdraw_able'] >= $total_amount) {
                            HedgeDetails::updateInvestment($hedge_id, $user_id, $total_amount);

                            $hedge_slots = $hedge->available_slot - $input_slot;
                            $hedge_update = Hedge::where('id', $hedge_id)->update(['available_slot' => $hedge_slots]);

                            $model = WithDraw::create(['u_id' => $user_id, 'btc_id' => $user->btc_address, 'btc_rate' => $btc_rate, 'bank_type' => 'hedge_booking', 'swift_code' => $hedge_id, 'withdraw_amount' => $total_amount, 'coin_type' => $coin_type, 'is_withdraw' => 1]);
                            $hedge_fund = HedgeFund::create(['user_id' => $user_id, 'hedge_id' => $hedge_id, 'slot' => $input_slot, 'per_slot' => $hedge->amount, 'amount' => $total_amount, 'btc_rate' => $btc_rate, 'type' => $coin_type, 'deposite_status' => '1', 'deposit_approved_time' => $time_deposited]);
                            User::updateFinanceCommission($user_id, $total_amount);

                            return ["error" => null, 'success' => '200'];
                        } else {
                            return ['error' => 405, 'message' => "You don't have sufficient fund !!"];

                        }
                    } elseif ($coin_type == 'gift') {
                        $gift_code = $request->get('giftcode');

                        $checkData = Gift::where('gift_code', $gift_code)->where('status', 0)->first();

                        if (!empty($checkData)) {

                            if ($checkData->amount == $total_amount) {
                                if ($checkData->created_by != auth()->user()->id) {
                                    //assinged to user but need to approved by crater approval 2 padding or need approval
                                    $update = Gift::where('gift_code', $gift_code)->where('status', 0)->update(['user_id' => auth()->user()->id, 'hedge_id' => $hedge_id, 'slots' => $input_slot, 'total_slots_amount' => $total_amount, 'status' => 2]);

                                    /*$getUser=User::where('id',auth()->user()->id)->first();

                                    $remainig=$getUser->remain_balance + $check->amount;
                                    $withdrawable= $getUser->withdraw_amount + $check->amount;
                                    $userUpate=User::where('id',auth()->user()->id)->update(['remain_balance'=>$remainig,'withdraw_amount'=>$withdrawable]);*/
                                    return ["error" => null, 'success' => '203', 'message' => 'Gift code verified , kindly pay the affiliate(seller) to Complete your Bookings'];
                                } elseif ($checkData->created_by == auth()->user()->id) {
                                    //update status of gift code
                                    $checkData->update(['user_id' => auth()->user()->id, 'status' => 1, 'hedge_id' => $hedge_id, 'slots' => $input_slot, 'total_slots_amount' => $total_amount,]);

                                    HedgeDetails::updateInvestment($hedge_id, $user_id, $total_amount);
                                    //update hedge slots
                                    $hedge_slots = $hedge->available_slot - $input_slot;
                                    $hedge_update = Hedge::where('id', $hedge_id)->update(['available_slot' => $hedge_slots]);

                                    $hedge_fund = HedgeFund::create(['user_id' => $user_id, 'hedge_id' => $hedge_id, 'slot' => $input_slot, 'per_slot' => $hedge->amount, 'amount' => $total_amount, 'btc_rate' => $btc_rate, 'type' => $coin_type, 'deposite_status' => '1', 'deposit_approved_time' => $time_deposited, 'gift_id' => $checkData->id]);
//                                 dd($request->all(),$userinfomodel);
                                    User::updateFinanceCommission($user_id, $checkData->amount);


                                    return ["error" => null, 'success' => '203', 'message' => 'Gift code verified , Congratulation you\'r Completed your  Bookings!'];


                                }
                                return ['error' => '303', 'message' => 'You are not the right Person!'];
                            } else {
                                if ($checkData->amount < $total_amount) {
                                    return ['error' => 405, 'message' => "You Gift amount is less then total amount Please try another gift code!!"];

                                } else {
                                    return ['error' => 405, 'message' => "You Gift amount is Greater then total amount Please try another gift code with same amount !!"];

                                }
                            }

                        }
                        return ['error' => '301', 'message' => "Token not valid or Processed!"];
                    }
                    return ['error' => '444', 'message' => "Invalid Coin type!"];


                } else {
                    return ['error' => 121, 'message' => "your selected hedge don't have sufficient number of slots your trying to booked"];

                }
//                $commingAmount = $request->get('withdraw_amount');
////                $checkamount = User::where('id', $request->get('u_id'))->first();
//                $userinfomodel = User::user_Balance($request->get('u_id'));


//                if (!empty($request->get('btc_id'))) {
//                    $update = [
//
//                        'btc_address' => $request->get('btc_id')
//                    ];
//                    $user = User::where('id', auth()->user()->id)->update($update);
//
//                    return ["error" => null, 'success' => '200'];
//                }

//                $checkamount1 = $checkamount->remain_balance;
//                $withdraw_amount = $checkamount->withdraw_amount;
                /*echo "<pre>";
                print_r($request->all());exit;*/
//                $commingAmount = $request->get('withdraw_amount');


//                if ($convert_amount <= $checkamount1) {
//                    if ($convert_amount <= $withdraw_amount) {
//
////                        if (!empty($request->get('acc_name')) && $request->get('usd_id') && $request->get('branch_code')) {
////
////                            $updateModel = $model = WithDraw::create($request->all());
////                            $update = [
////                                'acc_name' => $request->get('acc_name'),
////                                'acc_no' => $request->get('usd_id'),
////                                'branch_code' => $request->get('branch_code'),
////                                'acc_type' => $request->get('bank_type'),
////                                'swift_code' => $request->get('swift_code')
////                            ];
////                            $user = User::where('id', auth()->user()->id)->update($update);
////                            return ["error" => null, 'success' => '200'];
////                        }
//
//
//                    } else {
//                        return ['error' => 405];
//
//                    }
            } else {
                return ['error' => 408, 'message' => 'You have not Filled All fields '];

            }

            /*$model=FundDetail::where('id',$id)
            ->update(['withdraw_status'=> $request->get('withdrawStatus'),
                    'withdraw_id'=> $request->get('withdrawId')]);*/

//            } elseif (!empty($request->get('id')) && $request->get('withdrawStatus')) {
//                $id = $request->get('id');
//
//                $model = FundDetail::where('id', $id)->update(['withdraw_status' => $request->get('withdrawStatus')]);
//                return ["error" => null, 'success' => '200'];
//            } else {
//                return ['error' => 409];
        }
    }


//admin
    public function index()
    {
        $hedge = Hedge::all()->toArray();
        $check = [];
        //$check = [];
        //        foreach ($bookings as $key => $value) {
        //            $name = User::where('id', $value['user_id'])->first();
        //            $check []= array_merge($value, ['user_name' => $name->name], ['email' => $name->email]);
        //
        //        }
        foreach ($hedge as $key => $value) {

            $fundhedge = HedgeDetails::where('hedge_id', $value['id'])->get()->count() ? HedgeDetails::where('hedge_id', $value['id'])->get()->sum('invested_amount') : 0;
            $check[] = array_merge($value, ['total_invested' => $fundhedge]);

        }

        $hedge = $check;

        return view('admin.hedge', compact('hedge'));
    }

    public function add_Hedge(Request $request)
    {
        $this->validate($request, [
            'title' => ['required', 'string', 'max:255'],
            'slot' => ['required', 'int'],
            'amount' => ['required', 'int'],
            'slot_closing_date' => ['required'],
            'slot_opening_date' => ['required'],
            'due_date' => ['required'],

        ]);

        Hedge::create([
            'title' => $request->title,
            'slot' => $request->slot,
            'available_slot' => $request->slot,
            'amount' => $request->amount,
            'slot_closing_date' => date('Y-m-d H:i:s', strtotime($request->slot_closing_date)),
            'slot_opening_date' => date('Y-m-d H:i:s', strtotime($request->slot_opening_date)),
            'due_date' => date('Y-m-d H:i:s', strtotime($request->due_date)),
            'code' => Str::random(6),

        ]);
        return response()->json([
            'message' => 'Hedge Added Successfully.'

        ]);
    }

    public function view_Hedge_Info(Request $request)
    {
//        dd($request->all());
//        $encrypted = Crypt::encryptString('Hello world.');
//
//        $decrypted = Crypt::decryptString($encrypted);
//        dd($decrypted);
//        $user_info = User::with('roles')->with('permissions')->where('admin', 1)->where('id', $request->get('user_id'))->first();//            ->makeVisible(['password']);
        $hedge = Hedge::where('id', $request->get('hedge_id'))->first();
//        $activity_filters =  DB::table('activity_log')->select('log_name')->distinct('log_name')->where('causer_id',$request->get('user_id'))->get();
//        $user_info =Crypt::decryptString($user_info->password);
//        $user_info = ;
//        dd($hedge);
        return response()->json([
            'hedge' => $hedge,
//            'activity_filters'=>$activity_filters,
            'error' => null
        ]);
    }

    public function update_Hedge_Info(Request $request)
    {


        if ($request->ajax()) {

            if (!empty($request->get('hedge_id')) && $request->get('action') == 'update_hedge') {
                $message = '';
//                dd($request->all());


                $this->validate($request, [
                    'slot_opening_date' => 'required|date|after_or_equal:dat:today|before:due_date|before:slot_closing_date',
                    'title' => ['required', 'string', 'max:255'],
                    'slot' => ['required', 'int'],
                    'amount' => ['required', 'int'],
                    'slot_closing_date' => 'required|date|after:slot_opening_date|before:due_date',
//                    'slot_opening_date' =>'required|date|after:slot_opening_date',
//                    'slot_opening_date' => ['required'],
                    'due_date' => 'required|date|after:slot_closing_date',

                ]);
                $hedge1 = Hedge::where('id', $request->get('hedge_id'))->first();
                if ($hedge1->slot > $request->get('slot')) {
                    $diff = $hedge1->slot - $request->get('slot');
                    if ($hedge1->available_slot >= $diff) {
                        $sub_dif = $hedge1->available_slot - $diff;
                        Hedge::where('id', $request->get('hedge_id'))->update(['available_slot' => $sub_dif, 'slot' => $request->get('slot'),
                            'title' => $request->get('title'), 'amount' => $request->get('amount'), 'slot_closing_date' => date('Y-m-d H:i:s', strtotime($request->slot_closing_date)), 'slot_opening_date' => date('Y-m-d H:i:s', strtotime($request->slot_opening_date)),
                            'due_date' => date('Y-m-d H:i:s', strtotime($request->get('due_date')))]);
                    } else {
                        return [
                            'error' => 409,
                            'message' => 'seats are sold'
                        ];
                    }
                } else {
                    $diff = $request->get('slot') - $hedge1->slot;

                    Hedge::where('id', $request->get('hedge_id'))->update(['slot' => $request->get('slot'), 'available_slot' => $hedge1->available_slot + $diff, 'title' => $request->get('title'), 'amount' => $request->get('amount'), 'slot_closing_date' => date('Y-m-d H:i:s', strtotime($request->slot_closing_date)), 'slot_opening_date' => date('Y-m-d H:i:s', strtotime($request->slot_opening_date)),
                        'due_date' => date('Y-m-d H:i:s', strtotime($request->get('due_date')))]);


                    return ["error" => null, 'success' => '200'];

                }

            } else {
                return [
                    'error' => 409,
                    'message' => 'User ID is Missing'
                ];
            }

        }
    }

    public function profitStore(Request $request)
    {
//        $startDate = date('Y-m-d');

//        set_profit
        /*echo "<pre>";
        print_r($request->all());exit;*/
//        $dateDaily = Weekly::where('is_active', 1)->first();
//        $checkDate = date('Y-m-d');
//        if ($checkDate != $dateDaily->date) {
//            $model = Weekly::all();
//            $data = Weekly::where('is_active', 1)->update(['is_active' => 0]);

        $this->validate($request, [
            'hedge_profit_perc' => ['required', 'numeric'],
            'hedge_id' => ['required', 'numeric']
        ]);
//            if ($checkDate != $dateDaily->date)
        if (!empty($request->get('hedge_id'))) {
            HedgeProfitHistory::create(['hedge_id' => $request->get('hedge_id'), 'percentage' => $request->get('hedge_profit_perc'), 'profit_time' => date('Y-m-d')]);
//        dd($request->all());
            $hedge_users = HedgeDetails::where('hedge_id', $request->get('hedge_id'))->get();

            /*$users=User::TodayProfit();*/
            $users = User::where('admin', 1)->get();


            foreach ($hedge_users as $user) {

                $percentage = (float)$request->get('hedge_profit_perc');
                $exchange_assets = $user->invested_amount + $user->total_profit;
                (float)$percentage_amount = (float)$exchange_assets * (float)$percentage / 100;
                $new_total_profit = $user->total_profit + $percentage_amount;

                $user->update(['total_profit' => $new_total_profit]);

                HedgeProfit::create(['user_id' => $user->user_id, 'amount' => (float)$percentage_amount, 'percentage' => (float)$percentage, 'profit_time' => date('Y-m-d'), 'hedge_id' => $request->get('hedge_id')]);


            }


//        $get = Weekly::where('is_active', 1)->first();


//        $notification = array(
//            'message' => 'Profit Send Successfully',
//            'alert-type' => 'success',
//            'heading' => 'Succeed',
//        );
//            activity('Set Percentage')
//                ->causedBy(auth()->user())
////                    ->performedOn($user)
//                ->withProperties((['Percentage' =>$percentage]))
//                ->log("Set Daily Percentage");
//            /*Session::flash('alert-type','success');        view('admin.weekly.index')*/
            return ["error" => null, 'success' => '200', 'message' => 'Profit Send Successfully'];

//        return redirect()->route('admin.weekly.index')->withUsers($get)->with($notification);
        }

    }

//        $get = Weekly::where('is_active', 1)->first();
//
//
//        $notification = array(
//            'message' => 'You are already Set Today Profit',
//            'alert-type' => 'error',
//            'heading' => 'Failed',
//        );
//
//        return redirect()->route('admin.weekly.index')->withUsers($get)->with($notification);
//    }
    public function hedgeProfitList(Request $request)
    {
        $profit = HedgeProfitHistory::where('hedge_id', $request->get('hedge_id'))->get();

        return response()->json([
            'hedge_profit_list' => $profit
        ]);

    }

    public
    function hedgeBookingList(Request $request)
    {
        $bookings = HedgeFund::where('hedge_id', $request->get('hedge_id'))->get()->toArray();
        $check = [];
        foreach ($bookings as $key => $value) {
            $name = User::where('id', $value['user_id'])->first();
            $check [] = array_merge($value, ['user_name' => $name->name], ['email' => $name->email]);
        }

//        dd($check);
        return response()->json([
            'hedgeBookingList' => $check
        ]);


    }

}
