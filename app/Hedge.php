<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hedge extends Model
{

    protected $fillable = [
        'title',
        'slot',
        'amount',
        'total_invested',
        'slot_closing_date',
        'slot_opening_date',
        'status',
        'due_date',
        'code',
        'available_slot',
    ];
}
