<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_account_info extends Model
{
    public $timestamps = false;
    protected $table = 'user_account_info';
    protected $fillable = [
        'user_id','usd_invested','exchange_assets','unfrozen_amount','frozen_amount',
        'allowed_withdraw_check' , 'name','total_deposits','total_profit','total_withdraws',
        'difference' ,'remaning_blance','financeleft','financeright'
    ];
}
