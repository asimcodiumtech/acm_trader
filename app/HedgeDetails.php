<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HedgeDetails extends Model
{
    protected $fillable = [
        'user_id',
        'hedge_id',
        'invested_amount',
        'total_invested',
        'total_profit',
        'balance',
        'total_withdraw'
    ];

    public static function updateInvestment($hedgeId, $userId, $amount)
    {
        $hedge_details = self::where('hedge_id', $hedgeId)->where('user_id', $userId)->first();

        if ($hedge_details !== null) {
            $invested_new = $hedge_details->invested_amount + $amount;
            $hedge_details->update(['invested_amount' => $invested_new]);
        } else {
            HedgeDetails::create(['hedge_id' => $hedgeId, 'user_id' => $userId, 'invested_amount' => $amount]);
        }

    }

    public static function fundDetails($hedgeId, $userId=null)
    {
        if (!empty($userId)) {
            $fundDetails = array(
                'invested_amount' => [self::where('hedge_id', $hedgeId)->where('user_id', $userId)->get()->count() ? self::where('hedge_id', $hedgeId)->where('user_id', $userId)->get()->sum('invested_amount') : 0],

                'total_profit' => [self::where('hedge_id', $hedgeId)->where('user_id', $userId)->get()->count() ? self::where('hedge_id', $hedgeId)->where('user_id', $userId)->get()->sum('total_profit') : 0],
                'balance' => [self::where('hedge_id', $hedgeId)->where('user_id', $userId)->get()->count() ? self::where('hedge_id', $hedgeId)->where('user_id', $userId)->get()->sum('balance') : 0],
                'total_withdraw' => [self::where('hedge_id', $hedgeId)->where('user_id', $userId)->get()->count() ? self::where('hedge_id', $hedgeId)->where('user_id', $userId)->get()->sum('total_withdraw') : 0]

            );

            return $fundDetails;

        } else {
            $userId= auth()->user()->id;
            $fundDetails = array(
                'invested_amount' => [self::where('hedge_id', $hedgeId)->where('user_id', $userId)->get()->count() ? self::where('hedge_id', $hedgeId)->where('user_id', $userId)->get()->sum('invested_amount') : 0],

                'total_profit' => [self::where('hedge_id', $hedgeId)->where('user_id', $userId)->get()->count() ? self::where('hedge_id', $hedgeId)->where('user_id', $userId)->get()->sum('total_profit') : 0],
                'balance' => [self::where('hedge_id', $hedgeId)->where('user_id', $userId)->get()->count() ? self::where('hedge_id', $hedgeId)->where('user_id', $userId)->get()->sum('balance') : 0],
                'total_withdraw' => [self::where('hedge_id', $hedgeId)->where('user_id', $userId)->get()->count() ? self::where('hedge_id', $hedgeId)->where('user_id', $userId)->get()->sum('total_withdraw') : 0]

            );
            return $fundDetails;
        }
    }
    public static  function userFundDetails( $userId = null)
    {
        if (!empty($userId)) {
            $fundDetails = array(
                'invested_amount' => [self::where('user_id', $userId)->get()->count() ? self::where('user_id', $userId)->get()->sum('invested_amount') : 0],

                'total_profit' => [self::where('user_id', $userId)->get()->count() ? self::where('user_id', $userId)->get()->sum('total_profit') : 0],
                'balance' => [self::where('user_id', $userId)->get()->count() ? self::where('user_id', $userId)->get()->sum('balance') : 0],
                'total_withdraw' => [self::where('user_id', $userId)->get()->count() ? self::where('user_id', $userId)->get()->sum('total_withdraw') : 0]

            );
            return $fundDetails;

        } else {
            $userId= auth()->user()->id;
            $fundDetails = array(
                'invested_amount' => [self::where('user_id', $userId)->get()->count() ? self::where('user_id', $userId)->get()->sum('invested_amount') : 0],

                'total_profit' => [self::where('user_id', $userId)->get()->count() ? self::where('user_id', $userId)->get()->sum('total_profit') : 0],
                'balance' => [self::where('user_id', $userId)->get()->count() ? self::where('user_id', $userId)->get()->sum('balance') : 0],
                'total_withdraw' => [self::where('user_id', $userId)->get()->count() ? self::where('user_id', $userId)->get()->sum('total_withdraw') : 0]

            );
//            dd($fundDetails['invested_amount']);

            return $fundDetails;
        }
    }
}
