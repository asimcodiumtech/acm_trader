<?php

namespace App;

use App\Models\FundDetail;
use App\Models\Profit;
use App\Models\WithDraw;
use Cache;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\CausesActivity;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles, LogsActivity, CausesActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $fillable = [
        'name',
        'email',
        'password',
        'pwd_code',
        'pin_code',
        'remain_balance',
        'withdraw_amount',
        'reff_key',
        'reff_by',
        'level',
        'system_position',
        'financeleft',
        'financeright',
        'btc_address',
        'acc_name',
        'acc_no',
        'branch_code',
        'unfrozen',
        'frozen',
        'remember_token',
        'app_token',
        'email_status',
        'email_token',
        'admin'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'pin_code'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function updateFinanceCommission($user_id, $amount=0){

        $id = User::where('id', $user_id)->first();
        $refferby = User::where('id', $user_id)->first();
        $refferby = $refferby->reff_by;

        for ($i = 1; $i <= 3; $i++) {
            $relativefirst = User::where('reff_key', $refferby)->first();
            if (!empty($refferby)) {
                if ($i == 3) {
                    $percentage = 0.05;
                } else {
                    $percentage = 0.1;
                }
//                                        $withdraw = $relativefirst->withdraw_amount;
//                                        $balance = $relativefirst->remain_balance;
                $financeleft = $relativefirst->financeleft;

//                                        $finalWithamount = $withdraw + $amount * $percentage;
//                                        $finalamount = $balance + $amount * $percentage;
                $finalfinanceleft = $financeleft + $amount * $percentage;

                $update = User::where('reff_key', $refferby)->update(['financeleft' => $finalfinanceleft]);
                $refferby = $relativefirst->reff_by;

            }
        }
        $system = User::where('id', $user_id)->first();
        if (!empty($system->system_position)) {
            $endingLimit = $system->system_position - 3;
            $positionwise = User::where('system_position', '<', $system->system_position)->where('system_position', '>=', $endingLimit)->orderBy('system_position', 'DESC')->get();

            if (!empty($check = $positionwise->toArray())) {
                $cc = 1;
                foreach ($positionwise as $users) {
                    switch ($cc) {
                        case 1:
                            $percentage = 0.05;
                            $cc++;
                            break;
                        case 2:
                            $percentage = 0.03;
                            $cc++;
                            break;
                        case 3:
                            $percentage = 0.02;
                            $cc++;
                            break;
                    }
//                                            $withdraw = $users->withdraw_amount;
//                                            $balance = $users->remain_balance;
                    $financeright = $users->financeright;

//                                            $finalWithamount = $withdraw + $amount * $percentage;
//                                            $finalamount = $balance + $amount * $percentage;
                    $finalfinanceright = $financeright + $amount * $percentage;
                    $update = User::where('id', $users->id)->update(['financeright' => $finalfinanceright]);
                }
            }
        }
        return true;

    }
    public static function user_Balance($id = null)
{

    if (!empty($id)) {


        $model_user = User::where('id', $id)->first();
//            $total_deposite=FundDetail::where('u_id',$id)->where('deposite_status',1)->sum('amount');
        $total_withdraw = Withdraw::where('u_id', $id)->where('is_withdraw', 1)->sum('withdraw_amount');
        $exchangeProfit = Profit::where('u_id', $id)->sum('profit');

        if ($total_withdraw <= 0) {
            $total_withdraw = 0.0;
        }
//            use first time to get frozen data
//            $model_user->update(['acc_type'=>$total_deposite]);

        $startDate = date('Y-m-d');
        $endDate = date('Y-m-d', strtotime("-60 day", strtotime($startDate)));
        $validWithdraw = FundDetail::where('u_id', $id)->where('deposite_status', 1)->where('deposit_approved_time', '<=', $endDate)->where('deposite_initial_status', 0)->get();
        $array = FundDetail::where('u_id', $id)->where('deposite_status', 1)->where('deposit_approved_time', '<=', $endDate)->where('deposite_initial_status', 0)->get()->toArray();
//

        if (sizeof($array) == 0) {
//                        User::where('id',$loopUser->id)->update(['unfrozen'=>0]);

        } else {
            foreach ($validWithdraw as $transc) {
                $frozzen = $model_user->frozen - $transc->amount;
                $model_user->update(['frozen' => $frozzen]);

                // unfrozen_amount = unfrozen
                $unfrozen_amount = $model_user->unfrozen + $transc->amount;
//                            $model_user=User::where('id',$loopUser->id)->update(['unfrozen'=>$finalamount]);
                $model_user->update(['unfrozen' => $unfrozen_amount]);

                //frozen =  frozen blance
                FundDetail::where('id', $transc->id)->update(['deposite_initial_status' => 1]);
            }
        }
        $usd_withdrawed = WithDraw::where('u_id', $id)->where('coin_type', 'usd')->where('is_withdraw', 1)->sum('withdraw_amount');
        $profit_withdrawed = WithDraw::where('u_id', $id)->where('coin_type', 'profit')->where('is_withdraw', 1)->sum('withdraw_amount');
        $left_withdrawed = WithDraw::where('u_id', $id)->where('coin_type', 'left')->where('is_withdraw', 1)->sum('withdraw_amount');
        $right_withdrawed = WithDraw::where('u_id', $id)->where('coin_type', 'right')->where('is_withdraw', 1)->sum('withdraw_amount');
//            var_dump($model_user->frozen,$model_user->unfrozen );
//            exit();
        $exchange_assets = ($model_user->frozen + $model_user->unfrozen + $exchangeProfit) - ($usd_withdrawed + $profit_withdrawed);
        $balance = ($model_user->financeleft + $model_user->financeright + $exchangeProfit + $model_user->unfrozen + $model_user->frozen) - ($usd_withdrawed + $left_withdrawed + $right_withdrawed + $profit_withdrawed);

        $usd_invested = ($model_user->unfrozen + $model_user->frozen) - $usd_withdrawed;

        $profit_withdraw_able = $exchangeProfit - $profit_withdrawed;
        $left_withdraw_able = $model_user->financeleft - $left_withdrawed;
        $right_withdraw_able = $model_user->financeright - $right_withdrawed;
        $usd_withdraw_able = $model_user->unfrozen - $usd_withdrawed;
        $allowed_withdraw = 1;
        $is_allowed_withdraw = WithDraw::where('u_id', $id)->where('is_withdraw', '!=', 1)->get()->toArray();
        if (sizeof($is_allowed_withdraw) != 0) {
            $allowed_withdraw = 0;
        }

        $userdata = array(
            'balance' => round($balance, 0, PHP_ROUND_HALF_UP),
            'usd_invested' => round($usd_invested, 0, PHP_ROUND_HALF_UP),
            'profit_withdraw_able' => round($profit_withdraw_able, 0, PHP_ROUND_HALF_UP),
            'left_withdraw_able' => round($left_withdraw_able, 0, PHP_ROUND_HALF_UP),
            'right_withdraw_able' => round($right_withdraw_able, 0, PHP_ROUND_HALF_UP),
            'usd_withdraw_able' => round($usd_withdraw_able, 0, PHP_ROUND_HALF_UP),
            'allowed_withdraw' => round($allowed_withdraw, 0, PHP_ROUND_HALF_UP),
            'exchange_assets' => $exchange_assets,


        );
        return $userdata;

    }
    $model_user = User::where('id', auth()->user()->id)->first();
//            $total_deposite=FundDetail::where('u_id',auth()->user()->id)->where('deposite_status',1)->sum('amount');
    $total_withdraw = Withdraw::where('u_id', auth()->user()->id)->where('is_withdraw', 1)->sum('withdraw_amount');
    $exchangeProfit = Profit::where('u_id', auth()->user()->id)->sum('profit');

    if ($total_withdraw <= 0) {
        $total_withdraw = 0.0;
    }
//            use first time to get frozen data
//            $model_user->update(['acc_type'=>$total_deposite]);

    $startDate = date('Y-m-d');
    $endDate = date('Y-m-d', strtotime("-60 day", strtotime($startDate)));
    $validWithdraw = FundDetail::where('u_id', auth()->user()->id)->where('deposite_status', 1)->where('deposit_approved_time', '<=', $endDate)->where('deposite_initial_status', 0)->get();
    $array = FundDetail::where('u_id', auth()->user()->id)->where('deposite_status', 1)->where('deposit_approved_time', '<=', $endDate)->where('deposite_initial_status', 0)->get()->toArray();
//

    if (sizeof($array) == 0) {
//                        User::where('id',$loopUser->id)->update(['unfrozen'=>0]);

    } else {
        foreach ($validWithdraw as $transc) {
            $frozzen = $model_user->frozen - $transc->amount;
            $model_user->update(['frozen' => $frozzen]);

            // unfrozen_amount = unfrozen
            $unfrozen_amount = $model_user->unfrozen + $transc->amount;
//                            $model_user=User::where('id',$loopUser->id)->update(['unfrozen'=>$finalamount]);
            $model_user->update(['unfrozen' => $unfrozen_amount]);

            //frozen =  frozen blance
            FundDetail::where('id', $transc->id)->update(['deposite_initial_status' => 1]);
        }
    }
    $usd_withdrawed = WithDraw::where('u_id', auth()->user()->id)->where('coin_type', 'usd')->where('is_withdraw', 1)->sum('withdraw_amount');
    $profit_withdrawed = WithDraw::where('u_id', auth()->user()->id)->where('coin_type', 'profit')->where('is_withdraw', 1)->sum('withdraw_amount');
    $left_withdrawed = WithDraw::where('u_id', auth()->user()->id)->where('coin_type', 'left')->where('is_withdraw', 1)->sum('withdraw_amount');
    $right_withdrawed = WithDraw::where('u_id', auth()->user()->id)->where('coin_type', 'right')->where('is_withdraw', 1)->sum('withdraw_amount');
//            var_dump($model_user->frozen,$model_user->unfrozen );
//            exit();
    $exchange_assets = ($model_user->frozen + $model_user->unfrozen + $exchangeProfit) - ($usd_withdrawed + $profit_withdrawed);
    $balance = ($model_user->financeleft + $model_user->financeright + $exchangeProfit + $model_user->unfrozen + $model_user->frozen) - ($usd_withdrawed + $left_withdrawed + $right_withdrawed + $profit_withdrawed);

    $usd_invested = ($model_user->unfrozen + $model_user->frozen) - $usd_withdrawed;

    $profit_withdraw_able = $exchangeProfit - $profit_withdrawed;
    $left_withdraw_able = $model_user->financeleft - $left_withdrawed;
    $right_withdraw_able = $model_user->financeright - $right_withdrawed;
    $usd_withdraw_able = $model_user->unfrozen - $usd_withdrawed;
    $allowed_withdraw = 1;
    $is_allowed_withdraw = WithDraw::where('u_id', auth()->user()->id)->where('is_withdraw', '!=', 1)->get()->toArray();
    if (sizeof($is_allowed_withdraw) != 0) {
        $allowed_withdraw = 0;
    }
    $userdata = array(
        'balance' => $balance,
        'usd_invested' => $usd_invested,
        'profit_withdraw_able' => $profit_withdraw_able,
        'left_withdraw_able' => $left_withdraw_able,
        'right_withdraw_able' => $right_withdraw_able,
        'usd_withdraw_able' => $usd_withdraw_able,
        'allowed_withdraw' => $allowed_withdraw,
        'exchange_assets' => $exchange_assets,

    );
    return $userdata;
}


    public static function balance($id = null)
    {
        if (!empty($id)) {


            $userdata = array(

                'deposit' => FundDetail::where('u_id', $id)
                    ->where('deposite_status', 1)->get()->count() ? FundDetail::where('deposite_status', 1)->sum('amount') : 0,
//            'profit' => Profit::where('u_id',$id)->get()->count() ? Profit::where('u_id',$id)->where('withdraw_profit','!=',1)->sum('profit'): 0,
                'profit' => Profit::where('u_id', $id)->get()->count() ? Profit::where('u_id', $id)->sum('profit') : 0,
                'withdraw' => WithDraw::where('u_id', $id)->get()->count() ? WithDraw::where('u_id', $id)->where('is_withdraw', 1)->sum('withdraw_amount') : 0,


            );

            return $userdata;

        }


        $userdata = array(

            'deposit' => FundDetail::where('u_id', auth()->user()->id)
                ->where('deposite_status', 1)->get()->count() ? FundDetail::where('deposite_status', 1)->sum('amount') : 0,
            'profit' => Profit::where('u_id', auth()->user()->id)->get()->count() ? Profit::where('u_id', auth()->user()->id)->sum('profit') : 0,
            'withdraw' => WithDraw::where('u_id', auth()->user()->id)->get()->count() ? WithDraw::where('u_id', auth()->user()->id)->where('is_withdraw', 1)->sum('withdraw_amount') : 0,


        );

        return $userdata;
    }

    public static function gainKey($refferby)
    {
        $relativefirst = self::where('reff_key', $refferby)->first();
        $refferby1 = $relativefirst->reff_by;
        return $refferby1;
    }

    public static function TodayProfit()
    {
        $startDate = date('Y-m-d');
        $modelsub = self::join('fund_details', 'fund_details.u_id', '=', 'users.id')
            ->select('fund_details.u_id')
            ->where('fund_details.deposit_approved_time', '=', $startDate)->distinct()->get();

        return $modelsub;
    }

    public function isOnline($id = null)
    {
        /*if(!empty($id))
        {

            $token=bin2hex(random_bytes(16));
            $user=self::where('id',$id)->update(['remember_token'=>$token]);
        }
        else
        {
            $user=self::where('id',$id)->update(['remember_token'=>null]);

        }*/
        return Cache::has('active' . $this->id);
    }

}
