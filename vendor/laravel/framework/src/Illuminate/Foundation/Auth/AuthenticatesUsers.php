<?php

namespace Illuminate\Foundation\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Cache;
trait AuthenticatesUsers
{
    use RedirectsUsers, ThrottlesLogins;

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm(Request $request)
    {
        if (!empty($request->get('token'))) 
        {
            $token = $request->get('token');
            $model1 = User::where('email_token', $token)->first();
            if (!empty($model1)) 
            {
                if ($model1->email_status == 2) 
                {   $model = User::where('email_token', $token)->update(['email_status' => 1]);
                    
                   
                     $notification = array(
        'message' => 'Email Verified Successfully',
        'alert-type' => 'success',
        'heading' => 'Successed',
            );
                    
                    return redirect()->route('login')->with($notification);
                }
                if ($model1->email_status == 1) 
                {   
                    
                   
                     $notification = array(
        'message' => 'Your Email is Already Verified',
        'alert-type' => 'success',
        'heading' => 'Successed',
            );
                    
                    return redirect()->route('login')->with($notification);
                }
            }
            $notification = array(
        'message' => 'Please Do email Verify',
        'alert-type' => 'error',
        'heading' => 'Successed',
            );
            
            return redirect()->route('login')->with($notification);
        } 
        else 
        {
            return view('auth.login');
        }
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {   


        
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
        
        $model1 = User::where('email', $request->get('email'))->first();
        
        if (!empty($model1)) {
            
            if($model1->email_status == 2)
                {
                      $notification = array(
        'message' => 'Check your Email for verify or Resend Again',
        'alert-type' => 'warning',
        'heading' => 'Warning',
            );
                        
                        return redirect()->route('login')->with($notification);
                    
                    
                }
                elseif($model1->email_status == 0)
                {
                      $notification = array(
        'message' => 'Do Email Verify First',
        'alert-type' => 'error',
        'heading' => 'Failed',
            );
                        
                        return redirect()->route('login')->with($notification);
                    
                    
                }
                else
                {
                    
                }
        }
          
        if ($this->attemptLogin($request)) {

            
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {   
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {     
        
        return $request->only($this->username(), 'password','remember_token');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);
        $data=$this->authenticated($request, $this->guard()->user());

             $update=User::where('id',$this->guard()->user()->id)->update(['remember_token'=>$request->get('_token')]);
            /*echo "<pre>";
              print_r($request->all());exit;*/
        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        
              
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {   
        
            /*echo "<pre>";
              print_r($request->all());exit;*/
              $id=$this->guard()->user()->id;
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();
          $update=User::where('id',$id)->update(['remember_token'=>null]);
          Cache::forget('active'.$id);
        return $this->loggedOut($request) ?: redirect('/');
    }

    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        //
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
