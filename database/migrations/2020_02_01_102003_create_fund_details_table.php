<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFundDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fund_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('u_id')->nullable();
            $table->string('trans_id')->nullable();
            $table->string('coin_type');
            $table->integer('amount');
            $table->boolean('deposite_status')->default(0);
            $table->boolean('withdraw_status')->default(0);
            $table->integer('profit')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fund_details');
    }
}
