<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHedgeFundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hedge_funds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('hedge_id');
            $table->integer('slot');
            $table->integer('per_slot');
            //slot * per_slot =  amount
            $table->float('amount');
            $table->float('btc_rate');
            $table->string('type');
            $table->tinyInteger('deposite_status');
            $table->date('deposit_approved_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hedge_funds');
    }
}
