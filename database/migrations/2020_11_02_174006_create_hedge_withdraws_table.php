<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHedgeWithdrawsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hedge_withdraws', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('hedge_id');

            //slot * per_slot =  amount
            $table->float('amount');
            $table->float('btc_rate');
            //hedge profit or invested amount
            $table->string('type');
            $table->tinyInteger('withdraw_status');
            $table->date('withdraw_approved_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hedge_withdraws');
    }
}
