<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHedgeProfitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hedge_profits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('percentage');
            $table->float('amount');
            $table->integer('user_id');
            $table->integer('hedge_id');
            $table->dateTime('profit_time');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hedge_profits');
    }
}
