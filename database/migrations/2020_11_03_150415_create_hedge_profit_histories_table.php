<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHedgeProfitHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hedge_profit_histories', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('hedge_id');
            $table->float('percentage');
            $table->dateTime('profit_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hedge_profit_histories');
    }
}
