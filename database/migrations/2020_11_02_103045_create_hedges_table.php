<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHedgesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hedges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('slot');
            $table->float('amount');
            $table->float('total_invested')->default(0);
            $table->dateTime('slot_closing_date');
            $table->dateTime('slot_opening_date');
            $table->integer('status')->default(1);
            $table->integer('available_slot')->nullable();
            $table->dateTime('due_date');
            $table->string('code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hedges');
    }
}
